<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hasil Ujian</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Hasil Ujian</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('hasil-ujian.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$hasilUjians->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pengetahuan Umun</th>
							<th>Minat Bakat</th>
							<th>Akademik</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $hasilUjians; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hasilUjian): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($hasilUjian->siswa->nama_lengkap); ?></td>
								<td><?php echo e($hasilUjian->pengetahuan_umum); ?></td>
								<td><?php echo e($hasilUjian->minat_bakat); ?></td>
								<td><?php echo e($hasilUjian->akademik); ?></td>
								<td><?php echo e($hasilUjian->status); ?></td>
								<td>
									<a href="<?php echo e(route('hasil-ujian.edit', ['hasil-ujian' => $hasilUjian->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['hasil-ujian.destroy', $hasilUjian->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($hasilUjians->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>