<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Per Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Per Mata Pelajaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('nilai-per-mata-pelajaran.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.nilai-permapel')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'akademik.filter.nilai-permatkul', 'method' => 'get'))); ?>

					<div class="input-group">
						<select name="mata_pelajaran_id" class="form-control">
                            <?php if(!$mataPelajarans->isEmpty()): ?>
                                <?php $__currentLoopData = $mataPelajarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajaran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($mataPelajaran->id); ?>">
                                    <?php echo e($mataPelajaran->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                        </select>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					<?php echo $__env->make('partials._flash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<table class="table table-bordered">
					<?php if(!$nilaiPermapels->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Mata Pelajaran</th>
							<th>Grid Nilai</th>
							<th>Siswa</th>
							<th>Nilai</th>
							<th>Predikat</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $nilaiPermapels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nilaiPermapel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($nilaiPermapel->guru->nama_guru); ?></td>
								<td><?php echo e($nilaiPermapel->mata_pelajaran->nama); ?></td>
								<td><?php echo e($nilaiPermapel->gridNilai->nilai); ?></td>
								<td><?php echo e($nilaiPermapel->siswa->nama_lengkap); ?></td>
								<td><?php echo e($nilaiPermapel->nilai); ?></td>
								<td><?php echo e($nilaiPermapel->predikat); ?></td>
								<td>
									<a href="<?php echo e(route('nilai-per-mata-pelajaran.edit', ['nilai-per-mata-pelajaran' => $nilaiPermapel->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['nilai-per-mata-pelajaran.destroy', $nilaiPermapel->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($nilaiPermapels->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>