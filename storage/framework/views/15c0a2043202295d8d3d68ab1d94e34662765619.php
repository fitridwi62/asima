<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Staff</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Staff</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('staff.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.staff-pegawai')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'staff.filter.staff', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="nama_lengkap" placeholder="Masukkan Data Staff" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$staffs->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Lengkap</th>
							<th>Jabatan</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Jenis Kelamin</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($staff->nama_lengkap); ?></td>
								<td><?php echo e($staff->jabatan->nama); ?></td>
								<td><?php echo e($staff->email); ?></td>
								<td><?php echo e($staff->phone); ?></td>
								<td><?php echo e($staff->jenis_kelamin); ?></td>
								<td><?php echo e($staff->status); ?></td>
								<td>
									<a href="<?php echo e(route('staff.edit', ['id' => $staff->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['staff.destroy', $staff->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($staffs->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>