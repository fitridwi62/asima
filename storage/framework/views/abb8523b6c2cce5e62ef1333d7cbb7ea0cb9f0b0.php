<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Kehadiran Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Kehadiran Guru</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('kehadiran-guru.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.data-kehadiran-guru')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'keuangan-siswa.filter.kehadiran-guru', 'method' => 'get'))); ?>

					<div class="input-group">
                        <select name="data_guru_id" class="form-control">
                            <?php if(!$dataGurus->isEmpty()): ?>
                                <?php $__currentLoopData = $dataGurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($guru->id); ?>">
                                    <?php echo e($guru->nama_guru); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                        </select>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$kehadiranGurus->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Tanggal</th>
							<th>Jam Masuk</th>
							<th>Jam Keluar</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $kehadiranGurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kehadiranGuru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($kehadiranGuru->guru->nama_guru); ?></td>
								<td><?php echo e($kehadiranGuru->date); ?></td>
								<td><?php echo e($kehadiranGuru->jam_masuk); ?></td>
								<td><?php echo e($kehadiranGuru->jam_keluar); ?></td>
								<td><?php echo e($kehadiranGuru->status); ?></td>
								<td>
                                <a href="<?php echo e(route('kehadiran-guru.edit', ['kehadiran-guru' => $kehadiranGuru->id])); ?>" class="btn btn-sm btn-success">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</a>
                                <?php echo Form::open(['route' => ['kehadiran-guru.destroy', $kehadiranGuru->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

								<button class="btn btn-sm btn-danger btn-delete" type="submit">
									<i class="fa fa-trash" aria-hidden="true"></i>
								</button>
                            	<?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($kehadiranGurus->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>