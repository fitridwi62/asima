<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Role</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Role</li>
				  </ol>
				</nav>

               
				 <!-- form start -->
                 <form action="<?php echo e(route('roles.store')); ?>" method="POST">
                        <?php echo csrf_field(); ?>

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>
                            <div class="col-md-10">
                                <input required type="text" name="name" class="form-control <?php echo e($errors->has('name') ? 'is-invalid' : ''); ?>">
                                <?php if($errors->has('name')): ?>
                                    <div class="invalid-feedback">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="guard_name" class="col-md-2 col-form-label text-md-right">Guard Name</label>
                            <div class="col-md-10">
                                <input required type="text" name="guard_name" class="form-control <?php echo e($errors->has('guard_name') ? ' is-invalid' : ''); ?>" >
                                <?php if($errors->has('guard_name')): ?>
                                    <div class="invalid-feedback">
                                        <strong><?php echo e($errors->first('guard_name')); ?></strong>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="permissions" class="col-md-2 col-form-label text-md-right">Permissions</label>
                            <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-2">
                                    <input type="checkbox" name="permissions[]" value="<?php echo e($permission->id); ?>"><?php echo e($permission->name); ?>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="form-group row">
                            <label for="guard_name" class="col-md-2 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>