<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rombongan Belajar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="<?php echo e(route ('rombongan_belajar.index')); ?>" class="btn btn-sm btn-primary">
						Kembali 
					</a>
				</div>

				 <!-- form start -->
	            <?php echo e(Form::model($rombel, array(
                   'method' => 'PATCH',
                   'route' => array('rombongan_belajar.update', $rombel->id)))); ?>

	              <div class="box-body">
	                <div class="col-md-4">
	                	<div class="form-group">
		                  <label for="nama">Rombongan Belajar</label>
		                  <?php echo Form::text('nama', old('nama'), ['class' => 'form-control']); ?>

		                </div>
	                </div>
	                <div class="col-md-4">
	                	
		                	<div class="form-group">
			                  <label for="nama">Jurusan</label>
			                  <select name="jurusan_id" class="form-control">
			                  <?php if(!$jurusans->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $jurusans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurusan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($jurusan->id); ?>" <?php if((int)$rombel->jurusan_id === (int)$jurusan->id): ?> selected="true" <?php endif; ?>>
			                  	 		<?php echo e($jurusan->nama_jurusan); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
	                	<div class="form-group">
		                  <label for="nama">Kelas</label>
		                  <select name="kelas" class="form-control">
			                  <?php if(!$kelas->isEmpty()): ?>
			                  	<?php $__currentLoopData = $kelas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  		<?php if((int)$rombel->kelas === (int)$value->id): ?>
				                  	 	<option value="<?php echo e($value->id); ?>" selected="true">
				                  	 		<?php echo e($value->nama_kelas); ?>

				                  	 	</option>
				                  	<?php else: ?>
				                  		<option value="<?php echo e($value->id); ?>">
				                  	 		<?php echo e($value->nama_kelas); ?>

				                  	 	</option>

			                  	 	<?php endif; ?>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
		                  </select>
		                </div>
	                </div>
	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>