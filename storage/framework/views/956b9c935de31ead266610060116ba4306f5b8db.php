<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Buku</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('data-buku.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.data-buku')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'staff.filter.data-buku', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="kode" placeholder="Masukkan Data Buku" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$dataBukus->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Nama Buku</th>
							<th>Pengarang</th>
							<th>Tahun Terbit</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $dataBukus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataBuku): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($dataBuku->kode); ?></td>
								<td><?php echo e($dataBuku->nama_buku); ?></td>
								<td><?php echo e($dataBuku->pengarang_buku); ?></td>
								<td><?php echo e($dataBuku->tahun_terbit); ?></td>
								<td><?php echo e($dataBuku->status); ?></td>
								<td>
									<a href="<?php echo e(route('data-buku.edit', ['data-buku' => $dataBuku->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['data-buku.destroy', $dataBuku->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($dataBukus->links()); ?>

			</div>
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>