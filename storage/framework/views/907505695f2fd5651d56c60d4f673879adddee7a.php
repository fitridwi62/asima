<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa Diterima</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa diterima</li>
				  </ol>
				</nav>
				<table class="table table-bordered">
					<?php if(!$siswaDiterimas->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pengetahuan Umun</th>
							<th>Minat Bakat</th>
							<th>Akademik</th>
							<th>Status</th>
						</tr>
						<?php $__currentLoopData = $siswaDiterimas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswaDiterima): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($siswaDiterima->siswa->nama_lengkap); ?></td>
								<td><?php echo e($siswaDiterima->pengetahuan_umum); ?></td>
								<td><?php echo e($siswaDiterima->minat_bakat); ?></td>
								<td><?php echo e($siswaDiterima->akademik); ?></td>
								<td><?php echo e($siswaDiterima->status); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($siswaDiterimas->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>