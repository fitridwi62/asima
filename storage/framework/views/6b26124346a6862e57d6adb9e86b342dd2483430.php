<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran SPP</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran SPP</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('pembayaran-spp.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.pembayaran-spp')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'keuangan-siswa.filter.pembayaran-spp', 'method' => 'get'))); ?>

					<div class="input-group">
						<?php if(!$jenisPembayaran->isEmpty()): ?>
							<select name="jenis_pembayaran_id" class="form-control">
								<?php $__currentLoopData = $jenisPembayaran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenisPembayaranx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($jenisPembayaranx->id); ?>">
										<?php echo e($jenisPembayaranx->nama); ?>

									</option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
							<?php else: ?>
						<?php endif; ?>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$pembayaranSpps->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Jenis Pembayaran</th>
							<th>Biaya SPP</th>
							<th>Biaya Ekstra</th>
							<th>Biaya Bimbingan Belajar</th>
							<th>Tanggal</th>
							<th>Total</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $pembayaranSpps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembayaranSpp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($pembayaranSpp->jenis_pembayaran->nama); ?></td>
								<td><?php echo e($pembayaranSpp->biaya_spp); ?></td>
								<td><?php echo e($pembayaranSpp->pembayaran_extra); ?></td>
								<td><?php echo e($pembayaranSpp->pembayaran_bimbingan_belajar); ?></td>
								<td><?php echo e($pembayaranSpp->tanggal_bayar); ?></td>
								<td>
									<?php echo e($pembayaranSpp->biaya_spp + $pembayaranSpp->pembayaran_extra + $pembayaranSpp->pembayaran_bimbingan_belajar); ?>

								</td>
								<td><?php echo e($pembayaranSpp->status); ?></td>
								<td>
									<a href="<?php echo e(route('pembayaran-spp.edit', ['pembayaran-spp' => $pembayaranSpp->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['pembayaran-spp.destroy', $pembayaranSpp->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($pembayaranSpps->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>