<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pendapatan</h3>
			</div>
			<?php if(Session::has('success')): ?>
				<div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><?php echo e(Session::get('success')); ?></strong>
                </div>
			<?php endif; ?>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Status Karyawan</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('status_karyawan.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$statusKaryawans->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Jabatan</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $statusKaryawans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $statusKaryawan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($statusKaryawan->nama); ?></td>
								<td>
									<a href="<?php echo e(route('status_karyawan.edit', ['status_karyawan' => $statusKaryawan->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['status_karyawan.destroy', $statusKaryawan->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div> -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>