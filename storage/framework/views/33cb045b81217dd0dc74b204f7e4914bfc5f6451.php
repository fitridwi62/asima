<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 <?php echo e(Form::open(array('route' => 'raport.store'))); ?>

	              <div class="box-body">
                  <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            <?php if(!$siswas->isEmpty()): ?>
                            <select name="siswa_id" class="form-control">
                                <?php $__currentLoopData = $siswas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($siswa->id); ?>">
                                        <?php echo e($siswa->nama_lengkap); ?>

                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php else: ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Mata Pelajaran</label>
                            <?php if(!$mapels->isEmpty()): ?>
                            <select name="matkul_id" class="form-control">
                                <?php $__currentLoopData = $mapels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mapel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($mapel->id); ?>">
                                        <?php echo e($mapel->nama); ?>

                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php else: ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Pengetahuan</label>
                            <input type="text" name="nilai_pengetahuan" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Keterampilan</label>
                            <input type="text" name="nilai_keterampilan" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Akhir</label>
                            <input type="text" name="nilai_akhir" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Predikat Nilai</label>
                            <input type="text" name="predikat" placeholder=""  class="form-control">
                        </div>
                    </div>
                    
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-plus" aria-hidden="true"></i> </button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>


                <!-- start data -->
                <table class="table table-bordered">
					<?php if(!$raports->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Mata Pelajaran</th>
							<th>Nilai Pengetahuan</th>
							<th>Nilai Keterampilan</th>
							<th>Nilai Akhir</th>
							<th>Predikat</th>
						</tr>
						<?php $__currentLoopData = $raports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $raport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($raport->siswaItem->nama_lengkap); ?></td>
								<td><?php echo e($raport->mapel->nama); ?></td>
								<td><?php echo e($raport->nilai_pengetahuan); ?></td>
								<td><?php echo e($raport->nilai_keterampilan); ?></td>
								<td><?php echo e($raport->nilai_akhir); ?></td>
								<td><?php echo e($raport->predikat); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                <a href="<?php echo e(route('akademik.nilai.ekstra-create')); ?>" class="btn btn-warning">
                                    Lanjutkan Setup Nilai Ekstrakurikuler
                                </a>
                            </td>
                        <td>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
                <!-- end data -->
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<style>
.Ekstratitle {
    margin-bottom: 12px;
    font-weight: bold;
    border-bottom: 1px solid #ececec;
    text-transform: uppercase;
    padding: 5px 0px;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>