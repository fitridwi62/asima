<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'mata-pelajar-perkelas.store'))); ?>

	              <div class="box-body">
	                <div class="col-md-6">
		                	<div class="form-group">
			                  <label for="nama">Kelas</label>
			                  <select name="kelas_id" class="form-control">
			                  <?php if(!$kelass->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $kelass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kelas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($kelas->id); ?>">
			                  	 		<?php echo e($kelas->nama_kelas); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <select name="mata_pelajar_id" class="form-control">
                            <?php if(!$mataPelajars->isEmpty()): ?>
                                <?php $__currentLoopData = $mataPelajars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($mataPelajar->id); ?>">
                                    <?php echo e($mataPelajar->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
                  <div class="col-md-12">
                        <div class="form-group">
                        <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>
                    </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Simpan</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>