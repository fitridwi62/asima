<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jadwal Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'jadwal-mata-pelajaran.store'))); ?>

	              <div class="box-body">
	                <div class="col-md-4">
		                	<div class="form-group">
			                  <label for="nama">Kelas</label>
			                  <select name="kelas_id" class="form-control">
			                  <?php if(!$kelass->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $kelass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kelas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($kelas->id); ?>">
			                  	 		<?php echo e($kelas->nama_kelas); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <select name="mata_pelajaran_id" class="form-control">
                            <?php if(!$mataPelajarans->isEmpty()): ?>
                                <?php $__currentLoopData = $mataPelajarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajaran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($mataPelajaran->id); ?>">
                                    <?php echo e($mataPelajaran->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Siswa</label>
                            <select name="siswa_id" class="form-control">
                            <?php if(!$siswa->isEmpty()): ?>
                                <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($siswax->id); ?>">
                                    <?php echo e($siswax->nama_lengkap); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            <?php if(!$dataGuru->isEmpty()): ?>
                                <?php $__currentLoopData = $dataGuru; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($guru->id); ?>">
                                    <?php echo e($guru->nama_guru); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
                  <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <?php echo Form::date('tanggal', old('tanggal'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Mulai</label>
                            <?php echo Form::text('jam_mulai', old('jam_mulai'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            <?php echo Form::text('jam_selesai', old('jam_selesai'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Durasi</label>
                            <?php echo Form::text('durasi', old('durasi'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
	              <!-- /.box-body -->

	              <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>