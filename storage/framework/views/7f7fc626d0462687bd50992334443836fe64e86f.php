<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Uang Masuk</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Uang Masuk</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'uang-masuk.store'))); ?>

	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		<?php echo Form::date('tanggal', old('tanggal'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             <?php if(!$siswas->isEmpty()): ?>
                                <select name="siswa_id" class="form-control">
                                    <?php $__currentLoopData = $siswas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($siswa->id); ?>">
                                            <?php echo e($siswa->nama_lengkap); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Jenis Pembayaran</label>
                             <?php if(!$jenisPembayarans->isEmpty()): ?>
                                <select name="jenis_pembayaran_id" class="form-control">
                                    <?php $__currentLoopData = $jenisPembayarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenisPembayaran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($jenisPembayaran->id); ?>">
                                            <?php echo e($jenisPembayaran->nama); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Siswa</label>
	                  		<?php echo Form::text('nama_siswa', old('nama_siswa'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		<?php echo Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Debit</label>
	                  		<?php echo Form::text('no_akun_debit', old('no_akun_debit'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Kredit</label>
	                  		<?php echo Form::text('no_akun_kredit', old('no_akun_kredit'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Harga</label>
	                  		<?php echo Form::text('harga', old('harga'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>