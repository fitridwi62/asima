<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pendaftaran Siswa Baru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 <?php echo e(Form::open(array('route' => 'daftar-siswa.store', 'files' => true, 'enctype' => 'multipart/form-data'))); ?>

	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">No Pendaftaran</label>
                            <?php echo Form::text('no_pendaftaran', old('no_pendaftaran'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            <?php echo Form::text('nama_lengkap', old('nama_lengkap'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Jenis Kelamin</label>
                            <?php echo Form::text('jenis_kelamin', old('jenis_kelamin'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Tempat Lahir</label>
                            <?php echo Form::text('tempat_lahir', old('tempat_lahir'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Tanggal Lahir</label>
                            <?php echo Form::date('tanggal_lahir', old('tanggal_lahir'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="nama">Alamat</label>
                            <?php echo Form::text('alamat', old('alamat'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor Telp</label>
                            <?php echo Form::text('telp', old('telp'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jenjang</label>
                             <?php if(!$jenjangs->isEmpty()): ?>
                                <select name="jenjang_id" class="form-control">
                                    <?php $__currentLoopData = $jenjangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenjang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($jenjang->id); ?>">
                                            <?php echo e($jenjang->nama_jenjang); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                                <div class="dataempty">
                                    data empty
                                </div>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Email</label>
                            <?php echo Form::text('email', old('email'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor WA</label>
                            <?php echo Form::text('no_wa', old('no_wa'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jurusan</label>
                             <?php if(!$jurusans->isEmpty()): ?>
                                <select name="jurusan_id" class="form-control">
                                    <?php $__currentLoopData = $jurusans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurusan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($jurusan->id); ?>">
                                            <?php echo e($jurusan->nama_jurusan); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                                <div class="dataempty">
                                    data empty
                                </div>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama Orang Tua</label>
                            <?php echo Form::text('nama_orang_tua', old('nama_orang_tua'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Pekerjaan Orang Tua</label>
                            <?php echo Form::text('pekerjaan_orang_tua', old('pekerjaan_orang_tua'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor Telp Orang Tua</label>
                            <?php echo Form::text('no_telp_ortu', old('no_telp_ortu'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Penghasilan Orang Tua</label>
                            <?php echo Form::text('penghasilan_ortu', old('penghasilan_ortu'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Photo</label>
                            <?php echo Form::file('photo', old('photo'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>