<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					<?php echo $__env->make('partials._flash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<table class="table table-bordered">
					<?php if(!$siswa_lists->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Nilai Pengetahuan</th>
							<th>Nilai Keterampilan</th>
							<th>Nilai Akhir</th>
							<th>Predikat</th>
						</tr>
						<?php $__currentLoopData = $siswa_lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswa_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td>
									<a href="<?php echo e(route('akademik.nilai-raport.persiswa', $siswa_list->siswa_id)); ?>">
										<?php echo e($siswa_list->siswaItem->nama_lengkap); ?>

									</a>
							    </td>
								<td><?php echo e($siswa_list->nilai_pengetahuan); ?></td>
								<td><?php echo e($siswa_list->nilai_keterampilan); ?></td>
								<td><?php echo e($siswa_list->nilai_akhir); ?></td>
								<td><?php echo e($siswa_list->predikat); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>