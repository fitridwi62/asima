<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Guru</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'data_guru.store', 'files' => true, 'enctype' => 'multipart/form-data'))); ?>

	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Induk Guru</label>
	                  		<?php echo Form::text('no_induk_guru', old('no_induk_guru'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Guru</label>
	                  		<?php echo Form::text('nama_guru', old('nama_guru'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor KTP</label>
	                  		<?php echo Form::text('nomer_ktp', old('nomer_ktp'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jenis Kelamin</label>
	                  		<?php echo Form::text('jenis_kelamin', old('jenis_kelamin'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Telp</label>
	                  		<?php echo Form::text('telp', old('telp'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Alamat</label>
	                  		<?php echo Form::text('alamat', old('alamat'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Email</label>
	                  		<?php echo Form::text('email', old('email'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pendidikan terakhir</label>
	                  		<?php echo Form::text('pendidikan_terakhir', old('pendidikan_terakhir'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Sertifikat</label>
	                  		<?php echo Form::text('sertifikat', old('sertifikat'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Photo</label>
	                  		<?php echo Form::file('photo', old('photo'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-12" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>