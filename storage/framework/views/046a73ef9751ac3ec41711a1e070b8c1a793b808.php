<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Sekolah</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="<?php echo e(route ('data_sekolah.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$data_sekolahs->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Telephone</th>
							<th>Alamat</th>
							<th>Jenjang</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $data_sekolahs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_sekolah): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($data_sekolah->nama); ?></td>
								<td><?php echo e($data_sekolah->email); ?></td>
								<td><?php echo e($data_sekolah->telephone); ?></td>
								<td><?php echo e($data_sekolah->alamat); ?></td>
								<td><?php echo e($data_sekolah->jenjang); ?></td>
								<td>
									<a href="<?php echo e(route('data_sekolah.edit', ['data-sekolah' => $data_sekolah->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['data_sekolah.destroy', $data_sekolah->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>