<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'pembayaran.store'))); ?>

	              <div class="box-body">
                    <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             <?php if(!$siswas->isEmpty()): ?>
                                <select name="siswa_id" class="form-control">
                                    <?php $__currentLoopData = $siswas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siswa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($siswa->id); ?>">
                                            <?php echo e($siswa->nama_lengkap); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">SPP</label>
                             <?php if(!$pembayaranSpps->isEmpty()): ?>
                                <select name="pembayaranspp_id" class="form-control">
                                    <?php $__currentLoopData = $pembayaranSpps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembayaranSpp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($pembayaranSpp->id); ?>">
                                            <?php echo e($pembayaranSpp->biaya_spp); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Semester</label>
                             <?php if(!$pembayaranSemesters->isEmpty()): ?>
                                <select name="pembayaransemester_id" class="form-control">
                                    <?php $__currentLoopData = $pembayaranSemesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembayaranSemester): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($pembayaranSemester->id); ?>">
                                            <?php echo e($pembayaranSemester->biaya); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Tanggal</label>
                                <?php echo Form::date('tanggal', old('tanggal'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Status</label>
                                <select name="status" class="form-control">
                                    <option>Pilih Status Pembayaran</option>
                                    <option value="lunas">Lunas</option>
                                    <option value="belum-lunas">Belum lunas</option>
                                </select>
                            </div>
                        </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>