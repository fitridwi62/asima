<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'kehadiran-guru.store'))); ?>

	              <div class="box-body">
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            <?php if(!$dataGurus->isEmpty()): ?>
                                <?php $__currentLoopData = $dataGurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($guru->id); ?>">
                                    <?php echo e($guru->nama_guru); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	                </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <?php echo Form::date('date', old('date'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Masuk</label>
                            <?php echo Form::text('jam_masuk', old('jam_masuk'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            <?php echo Form::text('jam_keluar', old('jam_keluar'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="masuk">Masuk</option>
                                <option value="izin">Izin</option>
                                <option value="sakit">Sakit</option>
                                <option value="alpha">Alpha</option>
                            </select>
                            </div>
                        </div>
                 </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>