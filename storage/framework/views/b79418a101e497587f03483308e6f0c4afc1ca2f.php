<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran SPP</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'pembayaran-spp.store'))); ?>

	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jenis Pembayaran</label>
                             <?php if(!$jenisPembayaran->isEmpty()): ?>
                                <select name="jenis_pembayaran_id" class="form-control">
                                    <?php $__currentLoopData = $jenisPembayaran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenisPembayaranx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($jenisPembayaranx->id); ?>">
                                            <?php echo e($jenisPembayaranx->nama); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya SPP</label>
                            <?php echo Form::text('biaya_spp', old('biaya_spp'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya Ekstrakuriler</label>
                            <?php echo Form::text('pembayaran_extra', old('pembayaran_extra'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya Bimbingan Belajar</label>
                            <?php echo Form::text('pembayaran_bimbingan_belajar', old('pembayaran_bimbingan_belajar'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <?php echo Form::date('tanggal_bayar', old('tanggal_bayar'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option>Pilih Status Pembayaran</option>
                                <option value="lunas">Lunas</option>
                                <option value="belum-lunas">Belum Lunas</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>