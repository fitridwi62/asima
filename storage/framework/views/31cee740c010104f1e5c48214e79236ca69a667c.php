<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Wali Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="<?php echo e(route ('wali_kelas.index')); ?>" class="btn btn-sm btn-primary">
						Kembali 
					</a>
				</div>

				 <!-- form start -->
	            <?php echo e(Form::model($wali_kelas, array(
                   'method' => 'PATCH',
                   'route' => array('wali_kelas.update', $wali_kelas->id)))); ?>

	              <div class="box-body">
	                <div class="col-md-6">
	                	
		                	<div class="form-group">
			                  <label for="nama">Guru</label>
			                  <select name="data_guru_id" class="form-control">
			                  <?php if(!$data_gurus->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $data_gurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($data_guru->id); ?>">
			                  	 		<?php echo e($data_guru->nama_guru); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-6">
	                	
		                <div class="form-group">
			                  <label for="nama">Kurikulum</label>
			                  <select name="kurikulum_id" class="form-control">
			                  <?php if(!$kurikulums->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $kurikulums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurikulum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($kurikulum->id); ?>" <?php if((string)$wali_kelas->kurikulum_id === (string)$kurikulum->id): ?> selected <?php endif; ?>>
			                  	 		<?php echo e($kurikulum->nama); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			            </div>
	                </div>
	                
	                <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Mata Pelajaran</label>
		                   <select name="kode_mata_pelajaran" class="form-control" <?php if((string)$wali_kelas->kode_mata_pelajaran === (string)$kurikulum->id): ?> selected <?php endif; ?>>
			                  <?php if(!$mapel->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $mapel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($item->id); ?>">
			                  	 		<?php echo e($item->nama); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			              </select>
		                </div>
	                </div>
	                <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Kelas</label>
		                  
		                  <select name="kelas" class="form-control">
			                  <?php if(!$kelas->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $kelas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($item->id); ?>" <?php if((string)$wali_kelas->kelas === (string)$item->id): ?> selected <?php endif; ?>>
			                  	 		<?php echo e($item->nama_kelas); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			               </select>
		                </div>
	                </div>
	                 
	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>