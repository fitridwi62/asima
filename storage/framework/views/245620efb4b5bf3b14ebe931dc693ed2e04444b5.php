<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Kembali
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$pembayarans->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pembayaran</th>
							<th>SPP</th>
							<th>Semester</th>
						</tr>
						<?php $__currentLoopData = $pembayarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembayaran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($pembayaran->siswa->nama_lengkap); ?></td>
								<td><?php echo e($pembayaran->pembayaranSPP->biaya_spp); ?></td>
								<td><?php echo e($pembayaran->pembayaranSemester->biaya); ?></td>
								<td><?php echo e($pembayaran->pembayaranSemester->biaya); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>