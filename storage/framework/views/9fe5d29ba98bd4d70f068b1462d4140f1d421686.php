<?php if(session()->has('flash_notification.message')): ?>
  <div class="col-md-offset-2 col-md-8 col-md-offset-2">
    <div class="alert alert-<?php echo e(session()->get('flash_notification.level')); ?>">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo session()->get('flash_notification.message'); ?>

    </div>
  </div>
<?php endif; ?>