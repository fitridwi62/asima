<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Tipe Inventori</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Tipe Inventori</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('tipe-inventori.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'inventori.filter.tipe-inventori-asset', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="nama" placeholder="Masukkan Tipe Inventori Asset" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$tipeInventories->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $tipeInventories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipeInventory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($tipeInventory->nama); ?></td>
								<td>
									<a href="<?php echo e(route('tipe-inventori.edit', ['tipe-inventori' => $tipeInventory->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['tipe-inventori.destroy', $tipeInventory->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($tipeInventories->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>