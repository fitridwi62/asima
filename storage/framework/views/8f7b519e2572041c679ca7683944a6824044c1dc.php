<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Test Interview</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Test Interview</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'test-interview.store'))); ?>

	              <div class="box-body">
                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Daftar Siswa</label>
                             <?php if(!$daftar_siswas->isEmpty()): ?>
                                <select name="daftar_siswa_id" class="form-control">
                                    <?php $__currentLoopData = $daftar_siswas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $daftar_siswa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($daftar_siswa->id); ?>">
                                            <?php echo e($daftar_siswa->nama_lengkap); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                                <div class="dataempty">
                                    data empty
                                </div>
                             <?php endif; ?>
                            </div>
                        </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai Terbaik</label>
	                  		<?php echo Form::text('nilai_terbaik', old('nilai_terbaik'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Prestasi</label>
	                  		<?php echo Form::text('prestasi', old('prestasi'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Hoby</label>
	                  		<?php echo Form::text('hoby', old('hoby'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Perbuatan Apa Yang Paling Terburuk Ketika Sekolah</label>
	                  		<?php echo Form::text('aktivitas', old('aktivitas'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jarak Rumah Ke Sekolah</label>
	                  		<?php echo Form::text('jarak_rumah', old('jarak_rumah'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pendapatan Orang Tua</label>
	                  		<?php echo Form::text('pendapatan_orang_tua', old('pendapatan_orang_tua'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pekerjaan Orang Tua</label>
	                  		<?php echo Form::text('pekerjaan_orang_tua', old('pekerjaan_orang_tua'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Apabila Diterima Disini Apa Yang Anda Lakukan ? </label>
	                  		<?php echo Form::text('note', old('note'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>