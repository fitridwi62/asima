<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Permintaan Dana</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Permintaan Dana</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('permintaan-dana.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.pengeluaran.permintaan-dana')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'accounting.filter.pengeluaran.permintaan-dana', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="kode_permintaan_dana" placeholder="Masukkan Kode Permintaan Dana" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					<?php echo $__env->make('partials._flash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<table class="table table-bordered">
					<?php if(!$permintaanDanas->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode Permintaan Dana</th>
							<th>Tanggal</th>
							<th>Kode Kwintansi</th>
							<th>Debit</th>
							<th>Kredit</th>
                            <th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $permintaanDanas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permintaanDana): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($permintaanDana->kode_permintaan_dana); ?></td>
								<td><?php echo e($permintaanDana->tanggal); ?></td>
								<td><?php echo e($permintaanDana->kode_kwintansi); ?></td>
								<td><?php echo e($permintaanDana->harga); ?></td>
								<td><?php echo e($permintaanDana->harga); ?></td>
								<td><?php echo e($permintaanDana->status); ?></td>
								<td>
									<a href="<?php echo e(route('permintaan-dana.edit', ['permintaanDana' => $permintaanDana->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['permintaan-dana.destroy', $permintaanDana->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($permintaanDanas->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>