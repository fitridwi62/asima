<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Barang Masuk</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Barang Masuk</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('barang-masuk.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.barang-masuk')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'inventori.filter.barang-masuk', 'method' => 'get'))); ?>

					<div class="input-group">
						<?php if(!$barangs->isEmpty()): ?>
                                <select name="barang_id" class="form-control">
                                    <?php $__currentLoopData = $barangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $barang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($barang->id); ?>">
                                            <?php echo e($barang->nama); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                        <?php endif; ?>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					<?php echo $__env->make('partials._flash', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<table class="table table-bordered">
					<?php if(!$barangMasuks->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Barang</th>
							<th>Harga</th>
							<th>Tanggal</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $barangMasuks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $barangMasuk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($barangMasuk->nama); ?></td>
								<td><?php echo e($barangMasuk->barang->nama); ?></td>
								<td><?php echo e($barangMasuk->harga); ?></td>
								<td><?php echo e($barangMasuk->tanggal); ?></td>
								<td><?php echo e($barangMasuk->status); ?></td>
								<td>
									<a href="<?php echo e(route('barang-masuk.edit', ['barang-masuk' => $barangMasuk->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['barang-masuk.destroy', $barangMasuk->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($barangMasuks->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>