<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('siswa.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('data-siswa.export')); ?>" class="btn btn-sm btn-success">
						<i class="fa fa-upload" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('siswa.import-create')); ?>" class="btn btn-sm btn-warning">
						<i class="fa fa-download" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf-siswa')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'siswa.search', 'method' => 'get'))); ?>

					<div class="col-md-12">
					    <div class="col-md-4">
							<input type="text" name="search" placeholder="Masukkan Nama Siswa" class="form-control">
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<select name="kelas_id" class="form-control">
								<?php if(!$kelass->isEmpty()): ?>
									<?php $__currentLoopData = $kelass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $x): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($x->id); ?>">
										<?php echo e($x->nama_kelas); ?>

									</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php else: ?>
								<div class="alert alert-danger">
									Data empty
								</div>
								<?php endif; ?>
								</select>
							</div>
	                 	</div>
						 <div class="col-md-3">
							<div class="form-group">
								<select name="jenjang_id" class="form-control">
								<?php if(!$jenjangs->isEmpty()): ?>
									<?php $__currentLoopData = $jenjangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenjang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($jenjang->id); ?>">
										<?php echo e($jenjang->nama_jenjang); ?>

									</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php else: ?>
								<div class="alert alert-danger">
									Data empty
								</div>
								<?php endif; ?>
								</select>
							</div>
	                 	</div>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$siswas->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nisn</th>
							<th>Nis</th>
							<th>Nama Lengkap</th>
							<th>Jenis Kelamin</th>
							<th>Jenjang</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $siswas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $siswa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($siswas->firstItem() + $key); ?></td>
								<td><?php echo e($siswa->nisn); ?></td>
								<td><?php echo e($siswa->nis); ?></td>
								<td><?php echo e($siswa->nama_lengkap); ?></td>
								<td><?php echo e($siswa->jenis_kelamin); ?></td>
								<td><?php echo e($siswa->jenjang->nama_jenjang); ?></td>
								<td><?php echo e($siswa->kelas->nama_kelas); ?></td>
								<td>
									<a href="<?php echo e(route('siswa.show', ['siswa' => $siswa->id])); ?>" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="<?php echo e(route('siswa.edit', ['siswa' => $siswa->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['siswa.destroy', $siswa->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($siswas->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>