<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata Pelajaran Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Mata Pelajaran Perkelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('mata-pelajar-perkelas.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.matkul-perkelas')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'akademik.filter.matkul-perkelas', 'method' => 'get'))); ?>

					<div class="input-group">
                        <select name="mata_pelajar_id" class="form-control">
                            <?php if(!$mataPelajarans->isEmpty()): ?>
                                <?php $__currentLoopData = $mataPelajarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($mataPelajar->id); ?>">
                                    <?php echo e($mataPelajar->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                        </select>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$mataPelajarPerkelass->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Kelas</th>
							<th>Mata Pelajaran</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $mataPelajarPerkelass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajarPerkelas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($mataPelajarPerkelas->kelas->nama_kelas); ?></td>
								<td><?php echo e($mataPelajarPerkelas->mata_pelajar->nama); ?></td>
								<td><?php echo e($mataPelajarPerkelas->status); ?></td>
								<td>
									<a href="<?php echo e(route('mata-pelajar-perkelas.edit', ['mata-pelajar-perkelas' => $mataPelajarPerkelas->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['mata-pelajar-perkelas.destroy', $mataPelajarPerkelas->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($mataPelajarPerkelass->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>