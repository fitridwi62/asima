<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jenis Pembayaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('pembayaran.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.pembayaran')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$pembayarans->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>SPP</th>
							<th>Semester</th>
							<th>Total</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $pembayarans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembayaran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($pembayaran->siswa->nama_lengkap); ?></td>
								<td><?php echo e($pembayaran->pembayaranSPP->biaya_spp); ?></td>
								<td><?php echo e($pembayaran->pembayaranSemester->biaya); ?></td>
								<td><?php echo e($pembayaran->pembayaranSemester->biaya + $pembayaran->pembayaranSPP->biaya_spp); ?></td>
								<td><?php echo e($pembayaran->status); ?></td>
								<td>
									<a href="<?php echo e(route('pembayaran.edit', ['pembayaran' => $pembayaran->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['pembayaran.destroy', $pembayaran->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($pembayarans->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>