<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Orang Tua</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Orang Tua</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('pdf.data-ortu')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>

				<table class="table table-bordered">
					<?php if(!$dataOrangTuas->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Siswa</th>
							<th>Nama Orang Tua</th>
							<th>Lulusan</th>
							<th>Pekerjaan</th>
							<th>Jenjang</th>
							<th>No.Telp</th>
							<th>Penghasilan</th>
						</tr>
						<?php $__currentLoopData = $dataOrangTuas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataOrangTua): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($dataOrangTua->nama_lengkap); ?></td>
								<td><?php echo e($dataOrangTua->nama_ayah_kandung); ?></td>
								<td><?php echo e($dataOrangTua->lulusan_sekolah_ortu); ?></td>
								<td><?php echo e($dataOrangTua->pekerjaan_ortu); ?></td>
								<td><?php echo e($dataOrangTua->jenjang->nama_jenjang); ?></td>
								<td><?php echo e($dataOrangTua->no_telp_ortu); ?></td>
								<td><?php echo e($dataOrangTua->penghasilan_ortu); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
			<?php echo e($dataOrangTuas->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>