<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Profile</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Profile</li>
				  </ol>
				</nav>
                <div class="row">
                    <div class="col-md-12">
						<div class="col-md-4">
							<a href="<?php echo e(route('akademik.jadwal-mata-pelajaran.persiswa')); ?>">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-address-card" aria-hidden="true"></i>
									</span>
									Jadwal Mata Pelajaran
								</div>
							</a>
						</div>

						<div class="col-md-4">
							<a href="<?php echo e(route('akademik.getEkstra.persiswa')); ?>">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-paint-brush" aria-hidden="true"></i>
									</span>
									Ekstrakurikuler
								</div>
							</a>
						</div> 

						<div class="col-md-4">
							<a href="<?php echo e(route('akademik.pembayaran.persiswa')); ?>">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-credit-card" aria-hidden="true"></i>
									</span>
									Pembayaran
								</div>
							</a>
						</div>

						<div class="col-md-4">
							<a href="<?php echo e(route('akademik.nilai.persiswa')); ?>">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-bar-chart" aria-hidden="true"></i>
									</span>
									Nilai
								</div>
							</a>
						</div>

						<div class="col-md-4">
							<a href="<?php echo e(route('akademik.raport.persiswa')); ?>">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-cubes" aria-hidden="true"></i>
									</span>
									Raport
								</div>
							</a>
						</div>

						<div class="col-md-4">
							<a href="#">
								<div class="prductItem">
									<span class="profileIcon">
										<i class="fa fa-bullhorn" aria-hidden="true"></i>
									</span>
									Pengumuman
								</div>
							</a>
						</div>
                         
                    </div>
                </div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>