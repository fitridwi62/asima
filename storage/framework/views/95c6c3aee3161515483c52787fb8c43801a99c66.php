<html>
<head>
	<title>Data Mata Pelajaran Perkelas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Mata Pelajaran Perkelas</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">#</th>
				<th>Kelas</th>
				<th>Mata Pelajaran</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
            <?php $__currentLoopData = $mataPelajarPerkelass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mataPelajarPerkelas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
                    <td><?php echo e($loop->index +1); ?></td>
                    <td><?php echo e($mataPelajarPerkelas->kelas->nama_kelas); ?></td>
					<td><?php echo e($mataPelajarPerkelas->mata_pelajar->nama); ?></td>
					<td><?php echo e($mataPelajarPerkelas->status); ?></td>
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table>
</body>
</html>