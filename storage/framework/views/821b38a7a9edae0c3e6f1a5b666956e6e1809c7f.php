<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('ekstrakurikuler.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.data-ekskul')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'ekstra.search', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Ekstrakurikuler" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$extraKurikulers->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Penanggung Jawab</th>
							<th>Tanggal</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
							<th>Jumlah Pertemuan</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $extraKurikulers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ekstrakurikuler): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($ekstrakurikuler->nama_ekstra); ?></td>
								<td><?php echo e($ekstrakurikuler->penangung_jawab); ?></td>
								<td><?php echo e($ekstrakurikuler->tanggal); ?></td>
								<td><?php echo e($ekstrakurikuler->jam_mulai); ?></td>
								<td><?php echo e($ekstrakurikuler->jam_selesai); ?></td>
								<td><?php echo e($ekstrakurikuler->pertemuan); ?></td>
								<td>
									<a href="<?php echo e(route('ekstrakurikuler.edit', ['ekstrakurikuler' => $ekstrakurikuler->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['ekstrakurikuler.destroy', $ekstrakurikuler->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($extraKurikulers->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>