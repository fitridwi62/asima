<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'siswa.store', 'files' => true, 'enctype' => 'multipart/form-data'))); ?>

	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nis') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nis</label>
							<?php echo $errors->first('nis', '<p class="help-block">NIS Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nis', old('nis'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nisn') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nisn</label>
							  <?php echo $errors->first('nisn', '<p class="help-block">NISN Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nisn', old('nisn'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nama_lengkap') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nama Lengkap</label>
							<?php echo $errors->first('nama_lengkap', '<p class="help-block">Nama Lengkap Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nama_lengkap', old('nama_lengkap'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nama_panggilan') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nama Panggilan</label>
							<?php echo $errors->first('nama_panggilan', '<p class="help-block">Nama Panggilan Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nama_panggilan', old('nama_panggilan'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>

					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nomor_formulir') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nomor Formulir</label>
							<?php echo $errors->first('nomor_formulir', '<p class="help-block">Nomor Formolir Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nomor_formulir', old('nomor_formulir'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 
	                 
	                  <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('tempat_lahir') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Tempat Lahir</label>
							<?php echo $errors->first('tempat_lahir', '<p class="help-block">Tempat Lahir Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('tempat_lahir', old('tempat_lahir'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('tanggal_lahir') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Tanggal Lahir</label>
							  <?php echo $errors->first('tempat_lahir', '<p class="help-block">Tanggal Lahir Wajib di isi</p>'); ?>

	                  		<?php echo Form::date('tanggal_lahir', old('tanggal_lahir'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('jenis_kelamin') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Jenis Kelamin</label>
	                  		<select name="jenis_kelamin" class="form-control">
							  	<option>Pilih Jenis Kelamin</option>
							  	<option value="laki-laki">Laki - Laki</option>
							  	<option value="Wanita">Wanita</option>
							</select>
							<?php echo $errors->first('jenis_kelamin', '<p class="help-block">Jenis Kelamin Wajib di isi</p>'); ?>

	                	</div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Agama</label>
                            <select name="agama_id" class="form-control">
                            <?php if(!$agamas->isEmpty()): ?>
                                <?php $__currentLoopData = $agamas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agama): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($agama->id); ?>">
                                    <?php echo e($agama->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Jenjang</label>
                            <select name="jenjang_id" class="form-control">
                            <?php if(!$jenjangs->isEmpty()): ?>
                                <?php $__currentLoopData = $jenjangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenjang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($jenjang->id); ?>">
                                    <?php echo e($jenjang->nama_jenjang); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Kelas</label>
                            <select name="kelas_id" class="form-control">
                            <?php if(!$kelas->isEmpty()): ?>
                                <?php $__currentLoopData = $kelas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $x): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($x->id); ?>">
                                    <?php echo e($x->nama_kelas); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	                 </div>
					 <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Tahun Akademik</label>
                            <select name="tahun_akademik_id" class="form-control">
                            <?php if(!$tahunAkademiks->isEmpty()): ?>
                                <?php $__currentLoopData = $tahunAkademiks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tahunAkademik): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($tahunAkademik->id); ?>">
                                    <?php echo e($tahunAkademik->nama); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Wali Kelas</label>
	                  		<?php echo Form::text('wali_kelas', old('wali_kelas'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('phone') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Phone</label>
							  <?php echo $errors->first('phone', '<p class="help-block">Nomor Telpn Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('phone', old('phone'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('umur') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Umur</label>
							<?php echo $errors->first('umur', '<p class="help-block">Umur Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('umur', old('umur'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Anak Ke</label>
	                  		<?php echo Form::text('anak_ke', old('anak_ke'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Suku</label>
	                  		<select name="suku" class="form-control">
							  	<option> Pilih Suku</option>
								  <option value="papua">Papua</option>
								  <option value="non-papua">Non Papua</option>
								  <option value="port-numbay">Pot Numbay</option>
							</select>
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nama_ayah_kandung') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nama Orang Tua Laki - Laki</label>
							<?php echo $errors->first('nama_ayah_kandung', '<p class="help-block">Nama Orang Tua Laki - Laki Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nama_ayah_kandung', old('nama_ayah_kandung'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('lulusan_sekolah_ortu') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Lulusan Sekolah Orang Tua</label>
							  <?php echo $errors->first('lulusan_sekolah_ortu', '<p class="help-block">Lulusan Sekolah Orang Tua Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('lulusan_sekolah_ortu', old('lulusan_sekolah_ortu'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('pekerjaan_ortu') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Pekerjaan Ortu</label>
							<?php echo $errors->first('pekerjaan_ortu', '<p class="help-block">Pekerjaan Ortu Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('pekerjaan_ortu', old('pekerjaan_ortu'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('jarak_rumah') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Jarak Rumah</label>
							<?php echo $errors->first('jarak_rumah', '<p class="help-block">Jarak Rumah Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('jarak_rumah', old('jarak_rumah'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('no_telp_ortu') ? 'has-error' : ''; ?>">
	                  		<label for="nama">No.Telp Orang Tua</label>
							<?php echo $errors->first('no_telp_ortu', '<p class="help-block">Jarak Rumah Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('no_telp_ortu', old('no_telp_ortu'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('penghasilan_ortu') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Penghasilan Orang Tua</label>
							<?php echo $errors->first('penghasilan_ortu', '<p class="help-block">Jarak Rumah Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('penghasilan_ortu', old('penghasilan_ortu'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('nama_ibu_kandung') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Nama Ibu Kandung</label>
							  <?php echo $errors->first('nama_ibu_kandung', '<p class="help-block">Nama Ibu Kandung Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('nama_ibu_kandung', old('nama_ibu_kandung'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('pekerjaan_ibu') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Pekerjaan Ibu</label>
							<?php echo $errors->first('nama_ibu_kandung', '<p class="help-block">Pekerjaan Ibu Kandung Wajib di isi</p>'); ?>

	                  		<?php echo Form::text('pekerjaan_ibu', old('pekerjaan_ibu'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('akte_photo') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Akte Photo</label>
	                  		<?php echo Form::file('akte_photo', old('akte_photo'), ['class' => 'form-control']); ?>

							  <?php echo $errors->first('akte_photo', '<p class="help-block">Berkas Akte Wajib di isi</p>'); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('kk_photo') ? 'has-error' : ''; ?>">
	                  		<label for="nama">KK Photo</label>
	                  		<?php echo Form::file('kk_photo', old('kk_photo'), ['class' => 'form-control']); ?>

							  <?php echo $errors->first('kk_photo', '<p class="help-block">Berkas KK Wajib di isi</p>'); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('surat_pernyataan_photo') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Surat Pernyataan Photo</label>
	                  		<?php echo Form::file('surat_pernyataan_photo', old('surat_pernyataan_photo'), ['class' => 'form-control']); ?>

							<?php echo $errors->first('kk_photo', '<p class="help-block">Surat Pernyataan Wajib di isi</p>'); ?>

	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group <?php echo $errors->has('photo') ? 'has-error' : ''; ?>">
	                  		<label for="nama">Photo</label>
	                  		<?php echo Form::file('photo', old('photo'), ['class' => 'form-control']); ?>

							<?php echo $errors->first('photo', '<p class="help-block">Photo Wajib di isi</p>'); ?>

	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status Siswa</label>
	                  		<select name="status_siswa" class="form-control">
							  	<option> Pilih Status Siswa</option>
								  <option value="masuk">Masuk</option>
								  <option value="grading">Grading</option>
							</select>
	                	</div>
	                 </div>
					 <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Keterangan</label>
	                  		<?php echo Form::textarea('keterangan', null, array('class'=>'form-control')); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-12" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>