<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Peminjaman Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Peminjaman Buku</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'peminjaman-buku.store'))); ?>

	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode</label>
	                  		<?php echo Form::text('kode', old('kode'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal Pinjam</label>
	                  		<?php echo Form::date('tanggal_peminjam', old('tanggal_peminjam'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal Pengembalian</label>
	                  		<?php echo Form::date('tanggal_kembalian', old('tanggal_kembalian'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jumlah Buku</label>
	                  		<?php echo Form::text('jumlah_buku', old('jumlah_buku'), ['class' => 'form-control']); ?>

	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Buku</label>
                             <?php if(!$bukus->isEmpty()): ?>
                                <select name="buku_id" class="form-control">
                                    <?php $__currentLoopData = $bukus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $buku): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($buku->id); ?>">
                                            <?php echo e($buku->kode); ?> - <?php echo e($buku->nama_buku); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                             <?php endif; ?>
                            </div>
                        </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>