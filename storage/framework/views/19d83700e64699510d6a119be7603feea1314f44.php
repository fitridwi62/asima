<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ruang kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ruang kelas</li>
				  </ol>
				</nav>

				 <!-- form start -->
	              <?php echo e(Form::model($ruang_kelas, array(
                   'method' => 'PATCH',
                   'route' => array('ruang_kelas.update', $ruang_kelas->id)))); ?>

	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode ruang kelas</label>
	                  		<?php echo Form::text('kode_ruangan', old('kode_ruangan'), ['class' => 'form-control', 'required' => 'true']); ?>

	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama ruang kelas</label>
	                  		<?php echo Form::text('nama_ruangan', old('nama_ruangan'), ['class' => 'form-control', 'required' => 'true']); ?>

	                	</div>
	              	</div>
	              	<div class="col-md-12" >
	                	<button type="submit" class="btn btn-primary">Simpan Data</button>
	              	</div>
	          	</div>
	              <!-- /.box-body -->
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>