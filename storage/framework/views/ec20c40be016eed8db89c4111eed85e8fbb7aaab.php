<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Peminjam Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Peminjam Buku</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('peminjaman-buku.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.data-peminjaman-buku')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'perpustakaan.filter.peminjaman-buku', 'method' => 'get'))); ?>

					<div class="input-group">
						<?php if(!$bukus->isEmpty()): ?>
                                <select name="buku_id" class="form-control">
                                    <?php $__currentLoopData = $bukus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $buku): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($buku->id); ?>">
                                            <?php echo e($buku->kode); ?> - <?php echo e($buku->nama_buku); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                             <?php else: ?>
                        <?php endif; ?>
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$peminjamanBukus->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Tanggal Pinjam</th>
							<th>Tanggal Pengembalian</th>
							<th>Jumlah</th>
							<th>Buku</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $peminjamanBukus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $peminjamanBuku): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($peminjamanBuku->kode); ?></td>
								<td><?php echo e($peminjamanBuku->tanggal_peminjam); ?></td>
								<td><?php echo e($peminjamanBuku->tanggal_kembalian); ?></td>
								<td><?php echo e($peminjamanBuku->jumlah_buku); ?></td>
								<td><?php echo e($peminjamanBuku->buku->nama_buku); ?></td>
								<td>
									<a href="<?php echo e(route('peminjaman-buku.edit', ['pinjaman-buku' => $peminjamanBuku->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['peminjaman-buku.destroy', $peminjamanBuku->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<?php echo e($peminjamanBukus->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>