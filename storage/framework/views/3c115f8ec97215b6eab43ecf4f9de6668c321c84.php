<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Create jurusan</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="<?php echo e(route ('data_sekolah.index')); ?>" class="btn btn-sm btn-primary">
						Back 
					</a>
				</div>

				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'jurusan.store'))); ?>

	              <div class="box-body">
	                <div class="form-group">
	                  <label for="nama">Kode jurusan</label>
	                  <?php echo Form::text('kode_jurusan', old('kode_jurusan'), ['class' => 'form-control', 'required' => 'true']); ?>

	                </div>
	                <div class="form-group">
	                  <label for="Email">Nama jurusan</label>
	                  <?php echo Form::text('nama_jurusan', old('nama_jurusan'), ['class' => 'form-control', 'required' => 'true']); ?>

	                </div>
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>