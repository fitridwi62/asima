<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            <?php echo e(Form::open(array('route' => 'mata_pelajaran.store'))); ?>

	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kode Pelajaran</label>
                            <?php echo Form::text('kode', old('kode'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <?php echo Form::text('nama', old('nama'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                   
	                <div class="col-md-4">
	                	
		                	<div class="form-group">
			                  <label for="nama">Kurikulum</label>
			                  <select name="kurikulum_id" class="form-control">
			                  <?php if(!$kurikulums->isEmpty()): ?>
			                  	 <?php $__currentLoopData = $kurikulums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurikulum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                  	 	<option value="<?php echo e($kurikulum->id); ?>">
			                  	 		<?php echo e($kurikulum->nama); ?>

			                  	 	</option>
			                  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                 <?php else: ?>
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  <?php endif; ?>
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            <?php if(!$dataGurus->isEmpty()): ?>
                                <?php $__currentLoopData = $dataGurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($guru->id); ?>">
                                    <?php echo e($guru->nama_guru); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Jenjang</label>
                            <select name="jenjang_id" class="form-control">
                            <?php if(!$jenjangs->isEmpty()): ?>
                                <?php $__currentLoopData = $jenjangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenjang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($jenjang->id); ?>">
                                    <?php echo e($jenjang->nama_jenjang); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            <?php endif; ?>
                            </select>
                        </div>
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            <?php echo Form::close(); ?>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>