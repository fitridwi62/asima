<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?php echo e(route('home.index')); ?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Guru</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="<?php echo e(route('data_guru.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="<?php echo e(route('pdf.data-guru')); ?>" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				<?php echo e(Form::open(array('route' => 'keuangan-siswa.filter.data-guru', 'method' => 'get'))); ?>

					<div class="input-group">
						<input type="text" name="nama_guru" placeholder="Masukkan Data Guru" class="form-control">
						<span class="input-group-btn">
							<?php echo Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']); ?>

						</span>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
				<table class="table table-bordered">
					<?php if(!$data_gurus->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>No. Induk Guru</th>
							<th>Nama Guru</th>
							<th>No. KTP</th>
							<th>Jenis Kelamin</th>
							<th>Telp</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $data_gurus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_guru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($data_guru->no_induk_guru); ?></td>
								<td><?php echo e($data_guru->nama_guru); ?></td>
								<td><?php echo e($data_guru->nomer_ktp); ?></td>
								<td><?php echo e($data_guru->jenis_kelamin); ?></td>
								<td><?php echo e($data_guru->telp); ?></td>
								<td>
									<a href="<?php echo e(route('data_guru.edit', ['data-guru' => $data_guru->id])); ?>" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="<?php echo e(route('data_guru.edit', ['data-guru' => $data_guru->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['data_guru.destroy', $data_guru->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
                <?php echo e($data_gurus->links()); ?>

			</div> 
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>