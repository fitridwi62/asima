<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rombongan Belajar</h3>
			</div>
			<?php if(Session::has('success')): ?>
				<div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><?php echo e(Session::get('success')); ?></strong>
                </div>
			<?php endif; ?>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="<?php echo e(route('rombongan_belajar.create')); ?>" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					<?php if(!$rombongan_belajars->isEmpty()): ?>
						<tr>
							<th style="width: 10px">#</th>
							<th>Rombongan Belajar</th>
							<th>Jurusan</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
						<?php $__currentLoopData = $rombongan_belajars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rombongan_belajar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->index +1); ?></td>
								<td><?php echo e($rombongan_belajar->nama); ?></td>
								<td><?php echo e($rombongan_belajar->jurusan->nama_jurusan); ?></td>
								<td><?php echo e($rombongan_belajar->nama_kelas); ?></td>
								<td>
									<a href="<?php echo e(route('rombongan_belajar.edit', ['rombongan-belajar' => $rombongan_belajar->id])); ?>" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									<?php echo Form::open(['route' => ['rombongan_belajar.destroy', $rombongan_belajar->id], 'method' => 'DELETE', 'style' => 'display:inline;']); ?>

										<button class="btn btn-sm btn-danger btn-delete" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    <?php echo Form::close(); ?>

								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    <?php endif; ?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>