<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekening_siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_keuangan');
            $table->string('nama_siswa');
            $table->decimal('pbn_tahunan', 10, 2)->nullable();
            $table->decimal('spp', 10, 2)->nullable();
            $table->decimal('biaya_program', 10, 2)->nullable();
            $table->enum('status', ['sudah-bayar', 'belum-bayar'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekening_siswas');
    }
}
