<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpkAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spk_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe_asset');
            $table->string('nama');
            $table->integer('gedung_id')->unsigned();
            $table->foreign('gedung_id')->references('id')->on('gedungs')->onDelete('cascade');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spk_assets');
    }
}
