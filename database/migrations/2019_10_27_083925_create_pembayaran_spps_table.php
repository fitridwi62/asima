<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranSppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran_spps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_pembayaran_id')->unsigned();
            $table->foreign('jenis_pembayaran_id')->references('id')->on('jenis_pembayarans')->onDelete('cascade');
            $table->decimal('biaya_spp', 10, 2);
            $table->decimal('pembayaran_extra', 10, 2)->nullable();
            $table->decimal('pembayaran_bimbingan_belajar', 10, 2)->nullable();
            $table->date('tanggal_bayar');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_spps');
    }
}
