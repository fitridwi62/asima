<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalUangKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_uang_keluars', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('nomor_kwitansi');
            $table->integer('reimbursment_id')->unsigned();
            $table->foreign('reimbursment_id')->references('id')->on('pengelauaran_jurnal_kas_kecil_reimbursments')->onDelete('cascade');
            $table->text('description');
            $table->string('nomor_akun_debit')->nullable();
            $table->string('nomor_akun_kredit')->nullable();
            $table->decimal('debit', 10, 2);
            $table->enum('status', ['valid']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnal_uang_keluars');
    }
}
