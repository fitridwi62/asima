<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_gurus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_induk_guru');
            $table->string('nama_guru');
            $table->string('nomer_ktp');
            $table->string('jenis_kelamin');
            $table->string('telp');
            $table->string('alamat');
            $table->string('email');
            $table->string('pendidikan_terakhir');
            $table->string('sertifikat');
            $table->string('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_gurus');
    }
}
