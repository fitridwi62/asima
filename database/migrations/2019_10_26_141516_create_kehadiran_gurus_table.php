<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKehadiranGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kehadiran_gurus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_guru_id')->unsigned();
            $table->foreign('data_guru_id')->references('id')->on('data_gurus')->onDelete('cascade');
            $table->date('date');
            $table->string('jam_masuk');
            $table->string('jam_keluar');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kehadiran_gurus');
    }
}
