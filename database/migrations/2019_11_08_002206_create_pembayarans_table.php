<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siswa_id')->unsigned();
            $table->foreign('siswa_id')->references('id')->on('siswas')->onDelete('cascade');
            $table->integer('pembayaranspp_id')->unsigned();
            $table->foreign('pembayaranspp_id')->references('id')->on('pembayaran_spps')->onDelete('cascade');
            $table->integer('pembayaransemester_id')->unsigned();
            $table->foreign('pembayaransemester_id')->references('id')->on('pembayaran_semesters')->onDelete('cascade');
            $table->date('tanggal');
            $table->enum('status', ['lunas', 'belum-lunas']);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
}
