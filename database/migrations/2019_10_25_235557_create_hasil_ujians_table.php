<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilUjiansTable extends Migration
{
    public function up()
    {
        Schema::create('hasil_ujians', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daftar_siswa_id')->unsigned();
            $table->foreign('daftar_siswa_id')->references('id')->on('daftar_siwas')->onDelete('cascade');
            $table->integer('pengetahuan_umum')->nullabel();
            $table->integer('minat_bakat')->nullable();
            $table->integer('akademik')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('hasil_ujians');
    }
}
