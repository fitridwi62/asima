<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaftarSiwasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_siwas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pendaftaran');
            $table->string('nama_lengkap');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('telp');
            $table->integer('jenjang_id')->unsigned();
            $table->foreign('jenjang_id')->references('id')->on('jenjangs')->onDelete('cascade');
            $table->string('email');
            $table->string('no_wa');
            //$table->integer('jurusan_id')->unsigned();
            //$table->foreign('jurusan_id')->references('id')->on('jurusans')->onDelete('cascade');
            $table->string('nama_orang_tua');
            $table->string('pekerjaan_orang_tua');
            $table->string('no_telp_ortu');
            $table->string('penghasilan_ortu');
            $table->string('photo');
            $table->enum('status', ['terima', 'gagal', 'proses']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_siwas');
    }
}
