<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHutangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hutangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_rekening');
            $table->string('nama_siswa');
            $table->date('tanggal');
            $table->decimal('pbn', 10, 2)->nullable();
            $table->decimal('spp', 10, 2)->nullable();
            $table->decimal('pro', 10, 2)->nullable();
            $table->enum('status', ['tahun-sebelumnya']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hutangs');
    }
}
