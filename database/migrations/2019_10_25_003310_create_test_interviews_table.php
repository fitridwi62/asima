<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_interviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daftar_siswa_id')->unsigned();
            $table->foreign('daftar_siswa_id')->references('id')->on('daftar_siwas')->onDelete('cascade');
            $table->string('nilai_terbaik');
            $table->string('prestasi')->nullable();
            $table->string('hoby');
            $table->string('aktivitas');
            $table->string('jarak_rumah');
            $table->decimal('pendapatan_orang_tua', 10,2);
            $table->string('pekerjaan_orang_tua');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_interviews');
    }
}
