<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengelauaranJurnalKasKecilReimbursmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengelauaran_jurnal_kas_kecil_reimbursments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('kode_reibusment');
            //$table->integer('permintaanDana_id')->unsigned();
            //$table->foreign('permintaanDana_id')->references('id')->on('pengeluaran_permintaan_danas')->onDelete('cascade');
            $table->text('deskripsi');
            $table->string('nomor_akun_debit');
            $table->string('nomor_akun_kredit');
            $table->decimal('debit', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengelauaran_jurnal_kas_kecil_reimbursments');
    }
}
