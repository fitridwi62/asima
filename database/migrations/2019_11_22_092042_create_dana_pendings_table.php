<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDanaPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dana_pendings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('kode_formolir');
            $table->text('deskripsi');
            $table->decimal('jumlah_nominal', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dana_pendings');
    }
}
