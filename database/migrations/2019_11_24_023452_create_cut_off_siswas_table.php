<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutOffSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cut_off_siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('nomor_rekening');
            $table->string('nama_siswa');
            $table->text('deskripsi');
            $table->decimal('kas_kredit', 10, 2);
            $table->string('dibuat');
            $table->string('diperiksa');
            $table->string('disetujui');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cut_off_siswas');
    }
}
