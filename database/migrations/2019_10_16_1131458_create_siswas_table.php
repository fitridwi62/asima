<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nis');
            $table->string('nisn');
            $table->string('nama_lengkap');
            $table->string('nama_panggilan')->nullable();
            $table->string('nomor_formulir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('tempat_lahir');
            $table->string('jenis_kelamin');
            $table->integer('agama_id')->unsigned();
            $table->foreign('agama_id')->references('id')->on('agamas')->onDelete('cascade');
            $table->integer('jenjang_id')->unsigned();
            $table->foreign('jenjang_id')->references('id')->on('jenjangs')->onDelete('cascade');
            $table->integer('kelas_id')->unsigned();
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
            $table->integer('tahun_akademik_id')->unsigned();
            $table->foreign('tahun_akademik_id')->references('id')->on('tahun_akademiks')->onDelete('cascade');
            $table->string('wali_kelas')->nullable();
            $table->string('phone');
            $table->string('umur');
            $table->string('anak_ke');
            $table->enum('suku', ['papua', 'non-papua', 'port-numbay']);
            $table->string('nama_ayah_kandung');
            $table->string('lulusan_sekolah_ortu');
            $table->string('pekerjaan_ortu');
            $table->string('jarak_rumah');
            $table->string('no_telp_ortu');
            $table->string('penghasilan_ortu');
            $table->string('nama_ibu_kandung');
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('akte_photo')->nullable();
            $table->string('kk_photo')->nullable();
            $table->string('surat_pernyataan_photo')->nullable();
            $table->string('photo')->nullable();
            $table->enum('status_siswa', ['masuk', 'grading'])->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
