<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengembaliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengembalians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            $table->integer('pinjamanBuku_id')->unsigned();
            $table->foreign('pinjamanBuku_id')->references('id')->on('peminjaman_bukus')->onDelete('cascade');
            $table->date('tanggal');
            $table->enum('status', ['sudah-dikembalikan', 'belum-dikembalikan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengembalians');
    }
}
