<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalKasMutasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_kas_mutasis', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('jam_mutasi');
            $table->string('nomor_akun_debit')->nullable();
            $table->string('nomor_akun_kredit')->nullable();
            $table->enum('status', ['valid']);
            $table->decimal('debit', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnal_kas_mutasis');
    }
}
