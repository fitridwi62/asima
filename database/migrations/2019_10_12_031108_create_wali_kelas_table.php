<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaliKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wali_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kurikulum_id')->unsigned();
            $table->foreign('kurikulum_id')->references('id')->on('kurikulums')->onDelete('cascade');
            $table->integer('data_guru_id')->unsigned();
            $table->foreign('data_guru_id')->references('id')->on('data_gurus')->onDelete('cascade');
            $table->string('kode_mata_pelajaran');
            $table->string('mata_pelajaran');
            $table->string('kelas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wali_kelas');
    }
}
