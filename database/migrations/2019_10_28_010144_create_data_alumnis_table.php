<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataAlumnisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_alumnis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('tahun_angkatan');
            $table->string('email');
            $table->string('phone');
            $table->string('alamat');
            $table->string('sekolah_ke')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_alumnis');
    }
}
