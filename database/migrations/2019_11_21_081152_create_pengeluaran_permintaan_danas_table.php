<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengeluaranPermintaanDanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengeluaran_permintaan_danas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_permintaan_dana');
            $table->date('tanggal');
            $table->string('kode_kwintansi');
            $table->string('nomor_akun_debit')->nullable();
            $table->string('nomor_akun_kredit')->nullable();
            $table->decimal('harga', 10, 2);
            $table->enum('status', ['ada', 'habis'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluaran_permintaan_danas');
    }
}
