<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalSPPsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_s_p_ps', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('nomor_rekening');
            $table->string('nama_siswa');
            $table->text('deskripsi');
            $table->decimal('kas_debit', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnal_s_p_ps');
    }
}
