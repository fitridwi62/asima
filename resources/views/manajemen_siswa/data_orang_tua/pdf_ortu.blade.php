<html>
<head>
	<title>Data Orang Tua Siswa</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Orang Tua Siswa</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Siswa</th>
                <th>Nama Orang Tua</th>
                <th>Lulusan</th>
                <th>Pekerjaan</th>
                <th>Jurusan</th>
                <th>No.Telp</th>
                <th>Penghasilan</th>
			</tr>
		</thead>
		<tbody>
            @foreach($dataOrangTuas as $dataOrangTua)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $dataOrangTua->nama_lengkap }}</td>
                <td>{{ $dataOrangTua->nama_ayah_kandung }}</td>
                <td>{{ $dataOrangTua->lulusan_sekolah_ortu }}</td>
                <td>{{ $dataOrangTua->pekerjaan_ortu }}</td>
                <td>{{ $dataOrangTua->jurusan->nama_jurusan }}</td>
                <td>{{ $dataOrangTua->no_telp_ortu }}</td>
                <td>{{ $dataOrangTua->penghasilan_ortu }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>