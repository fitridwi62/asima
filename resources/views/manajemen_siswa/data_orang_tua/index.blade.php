@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Orang Tua</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Orang Tua</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pdf.data-ortu') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>

				<table class="table table-bordered">
					@if(!$dataOrangTuas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Siswa</th>
							<th>Nama Orang Tua</th>
							<th>Lulusan</th>
							<th>Pekerjaan</th>
							<th>Jenjang</th>
							<th>No.Telp</th>
							<th>Penghasilan</th>
						</tr>
						@foreach($dataOrangTuas as $dataOrangTua)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $dataOrangTua->nama_lengkap }}</td>
								<td>{{ $dataOrangTua->nama_ayah_kandung }}</td>
								<td>{{ $dataOrangTua->lulusan_sekolah_ortu }}</td>
								<td>{{ $dataOrangTua->pekerjaan_ortu }}</td>
								<td>{{ $dataOrangTua->jenjang->nama_jenjang }}</td>
								<td>{{ $dataOrangTua->no_telp_ortu }}</td>
								<td>{{ $dataOrangTua->penghasilan_ortu }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
			{{ $dataOrangTuas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection