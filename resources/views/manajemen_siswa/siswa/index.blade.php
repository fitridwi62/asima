@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('siswa.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('data-siswa.export') }}" class="btn btn-sm btn-success">
						<i class="fa fa-upload" aria-hidden="true"></i>
					</a>

					<a href="{{ route('siswa.import-create') }}" class="btn btn-sm btn-warning">
						<i class="fa fa-download" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf-siswa') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'siswa.search', 'method' => 'get')) }}
					<div class="col-md-12">
					    <div class="col-md-4">
							<input type="text" name="search" placeholder="Masukkan Nama Siswa" class="form-control">
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<select name="kelas_id" class="form-control">
								@if(!$kelass->isEmpty())
									@foreach($kelass as $x)
									<option value="{{$x->id}}">
										{{ $x->nama_kelas }}
									</option>
									@endforeach
								@else
								<div class="alert alert-danger">
									Data empty
								</div>
								@endif
								</select>
							</div>
	                 	</div>
						 <div class="col-md-3">
							<div class="form-group">
								<select name="jenjang_id" class="form-control">
								@if(!$jenjangs->isEmpty())
									@foreach($jenjangs as $jenjang)
									<option value="{{$jenjang->id}}">
										{{ $jenjang->nama_jenjang }}
									</option>
									@endforeach
								@else
								<div class="alert alert-danger">
									Data empty
								</div>
								@endif
								</select>
							</div>
	                 	</div>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$siswas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nisn</th>
							<th>Nis</th>
							<th>Nama Lengkap</th>
							<th>Jenis Kelamin</th>
							<th>Jenjang</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
						@foreach($siswas as $key => $siswa)
							<tr>
								<td>{{ $siswas->firstItem() + $key }}</td>
								<td>{{ $siswa->nisn }}</td>
								<td>{{ $siswa->nis }}</td>
								<td>{{ $siswa->nama_lengkap }}</td>
								<td>{{ $siswa->jenis_kelamin }}</td>
								<td>{{ $siswa->jenjang->nama_jenjang }}</td>
								<td>{{ $siswa->kelas->nama_kelas }}</td>
								<td>
									<a href="{{ route('siswa.show', ['siswa' => $siswa->id]) }}" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="{{ route('siswa.edit', ['siswa' => $siswa->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['siswa.destroy', $siswa->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $siswas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection