@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('siswa.index') }}" class="btn btn-sm btn-primary">
						 Kembali
					</a>
				</div>
				<table class="table table-bordered">
						<tr>
                            <td style="font-weight:bold;">NISN</td>
                            <td>{{ $siswa->nisn }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">NIS</td>
                            <td>{{ $siswa->nis }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Lengkap</td>
                            <td>{{ $siswa->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Panggilan</td>
                            <td>{{ $siswa->nama_panggilan }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nomor Formulir</td>
                            <td>{{ $siswa->nomor_formulir }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Tempat Lahir</td>
                            <td>{{ $siswa->tempat_lahir }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Tanggal Lahir</td>
                            <td>{{ $siswa->tanggal_lahir }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jenis Kelamin</td>
                            <td>{{ $siswa->jenis_kelamin }}</td>
                        </tr>
                        
                        <tr>
                            <td style="font-weight:bold;">Agama</td>
                            <td>{{ $siswa->agama->nama }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jenjang</td>
                            <td>{{ $siswa->jenjang->nama_jenjang }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Kelas</td>
                            <td>{{ $siswa->kelas->nama_kelas }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Wali Kelas</td>
                            <td>{{ $siswa->wali_kelas }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Phone</td>
                            <td>{{ $siswa->phone }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Umur</td>
                            <td>{{ $siswa->umur }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Anak Ke</td>
                            <td>{{ $siswa->anak_ke }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Suku</td>
                            <td>{{ $siswa->suku }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Orang Tua (Laki - Laki)</td>
                            <td>{{ $siswa->nama_ayah_kandung }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Lulusan Telp Orang Tua</td>
                            <td>{{ $siswa->lulusan_sekolah_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Pekerjaan Orang Tua</td>
                            <td>{{ $siswa->pekerjaan_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jarak Rumah Ke Sekolah</td>
                            <td>{{ $siswa->jarak_rumah }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nomor Telpn Orang Tua</td>
                            <td>{{ $siswa->no_telp_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Penghasilan Orang Tua</td>
                            <td>{{ $siswa->penghasilan_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Ibu Kandung</td>
                            <td>{{ $siswa->nama_ibu_kandung }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Pekerjaan Ibu </td>
                            <td>{{ $siswa->pekerjaan_ibu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Status Siswa</td>
                            <td>{{ $siswa->status_siswa }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Photo</td>
                            <td>
                            <img src="{{ asset('siswa/'. $siswa->photo) }}" alt="news image" class="img-fluid" style="width:100px; heigt: 100px; border-radius: 5px;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">
                                <a href="{{ route('siswa.index') }}" class="btn btn-danger" style="font-size:12px;">
                                    TUTUP
                                </a>
                            </td>
                            <td>
                            </td>
                        </tr>
				</table>
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div> -->
		</div>
		<!-- /.box -->
	</div>
@endsection