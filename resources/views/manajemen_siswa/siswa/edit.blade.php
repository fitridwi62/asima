@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ Form::model($siswa, array('method' => 'PATCH',
                    'route' => array('siswa.update', $siswa->id),
                    'enctype' => 'multipart/form-data', 'files'=> 'true')) }}
					<div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nis</label>
	                  		{!! Form::text('nis', old('nis'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nisn</label>
	                  		{!! Form::text('nisn', old('nisn'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Lengkap</label>
	                  		{!! Form::text('nama_lengkap', old('nama_lengkap'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Panggilan</label>
	                  		{!! Form::text('nama_panggilan', old('nama_panggilan'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Formulir</label>
	                  		{!! Form::text('nomor_formulir', old('nomor_formulir'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 
	                 
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tempat Lahir</label>
	                  		{!! Form::text('tempat_lahir', old('tempat_lahir'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal Lahir</label>
	                  		{!! Form::date('tanggal_lahir', old('tanggal_lahir'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jenis Kelamin</label>
	                  		<select name="jenis_kelamin" class="form-control">
							  	<option>Pilih Jenis Kelamin</option>
							  	<option value="laki-laki">Laki - Laki</option>
							  	<option value="Wanita">Wanita</option>
							</select>
	                	</div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Agama</label>
                            <select name="agama_id" class="form-control">
                            @if(!$agamas->isEmpty())
                                @foreach($agamas as $agama)
                                <option value="{{$agama->id}}">
                                    {{ $agama->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Jenjang</label>
                            <select name="jenjang_id" class="form-control">
                            @if(!$jenjangs->isEmpty())
                                @foreach($jenjangs as $jenjang)
                                <option value="{{$jenjang->id}}">
                                    {{ $jenjang->nama_jenjang }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	                 </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Kelas</label>
                            <select name="kelas_id" class="form-control">
                            @if(!$kelas->isEmpty())
                                @foreach($kelas as $x)
                                <option value="{{$x->id}}">
                                    {{ $x->nama_kelas }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	                 </div>
					 <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Tahun Akademik</label>
                            <select name="tahun_akademik_id" class="form-control">
                            @if(!$tahunAkademiks->isEmpty())
                                @foreach($tahunAkademiks as $tahunAkademik)
                                <option value="{{$tahunAkademik->id}}">
                                    {{ $tahunAkademik->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Wali Kelas</label>
	                  		{!! Form::text('wali_kelas', old('wali_kelas'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Phone</label>
	                  		{!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Umur</label>
	                  		{!! Form::text('umur', old('umur'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Anak Ke</label>
	                  		{!! Form::text('anak_ke', old('anak_ke'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Suku</label>
	                  		<select name="suku" class="form-control">
							  	<option> Pilih Suku</option>
								  <option value="papua">Papua</option>
								  <option value="non-papua">Non Papua</option>
								  <option value="port-numbay">Pot Numbay</option>
							</select>
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Orang Tua Laki - Laki</label>
	                  		{!! Form::text('nama_ayah_kandung', old('nama_ayah_kandung'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Lulusan Sekolah Orang Tua</label>
	                  		{!! Form::text('lulusan_sekolah_ortu', old('lulusan_sekolah_ortu'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pekerjaan Ortu</label>
	                  		{!! Form::text('pekerjaan_ortu', old('pekerjaan_ortu'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jarak Rumah</label>
	                  		{!! Form::text('jarak_rumah', old('jarak_rumah'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No.Telp Orang Tua</label>
	                  		{!! Form::text('no_telp_ortu', old('no_telp_ortu'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Penghasilan Orang Tua</label>
	                  		{!! Form::text('penghasilan_ortu', old('penghasilan_ortu'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Ibu Kandung</label>
	                  		{!! Form::text('nama_ibu_kandung', old('nama_ibu_kandung'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pekerjaan Ibu</label>
	                  		{!! Form::text('pekerjaan_ibu', old('pekerjaan_ibu'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Akte Photo</label>
	                  		{!! Form::file('akte_photo', old('akte_photo'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">KK Photo</label>
	                  		{!! Form::file('kk_photo', old('kk_photo'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Surat Pernyataan Photo</label>
	                  		{!! Form::file('surat_pernyataan_photo', old('surat_pernyataan_photo'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Photo</label>
	                  		{!! Form::file('photo', old('photo'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status Siswa</label>
	                  		<select name="status_siswa" class="form-control">
							  	<option> Pilih Status Siswa</option>
								  <option value="masuk">Masuk</option>
								  <option value="grading">Grading</option>
							</select>
	                	</div>
	                 </div>
					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Keterangan</label>
	                  		<input type="textarea" name="keterangan" class="form-control">
	                	</div>
	                 </div>
	                 <div class="col-md-12" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection