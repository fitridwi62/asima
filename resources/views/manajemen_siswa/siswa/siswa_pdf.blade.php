<html>
<head>
	<title>Siswa PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Siswa</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nisn</th>
				<th>Nis</th>
				<th>Nama Lengkap</th>
				<th>Jenis Kelamin</th>
				<th>Jurusan</th>
			</tr>
		</thead>
		<tbody>
            @foreach($siswas as $siswa)
			<tr>
                <td>{{ $loop->index +1 }}</td>
				<td>{{ $siswa->nisn }}</td>
				<td>{{ $siswa->nis }}</td>
				<td>{{ $siswa->nama_lengkap }}</td>
				<td>{{ $siswa->jenis_kelamin }}</td>
                <td>{{ $siswa->jurusan->nama_jurusan }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>