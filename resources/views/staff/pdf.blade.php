<html>
<head>
	<title>Laporan Data Pegawai</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Pegawai</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Nama Lengkap</th>
                <th>Jabatan</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Jenis Kelamin</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach($staffs as $staff)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $staff->nama_lengkap }}</td>
                <td>{{ $staff->jabatan->nama }}</td>
                <td>{{ $staff->email }}</td>
                <td>{{ $staff->phone }}</td>
                <td>{{ $staff->jenis_kelamin }}</td>
                <td>{{ $staff->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>