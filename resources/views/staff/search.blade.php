@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Staff</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Staff</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('staff.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.staff-pegawai') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'staff.filter.staff', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama_lengkap" placeholder="Masukkan Data Staff" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$staffs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Lengkap</th>
							<th>Jabatan</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Jenis Kelamin</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($staffs as $staff)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $staff->nama_lengkap }}</td>
								<td>{{ $staff->jabatan->nama }}</td>
								<td>{{ $staff->email }}</td>
								<td>{{ $staff->phone }}</td>
								<td>{{ $staff->jenis_kelamin }}</td>
								<td>{{ $staff->status }}</td>
								<td>
									<a href="{{ route('staff.edit', ['id' => $staff->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['staff.destroy', $staff->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $staffs->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection