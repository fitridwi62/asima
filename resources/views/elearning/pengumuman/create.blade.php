@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pengumuman</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pengumuman</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'pengumuman.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama</label>
	                  		{!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Guru</label>
                             @if(!$dataGurus->isEmpty())
                                <select name="data_guru_id" class="form-control">
                                    @foreach($dataGurus as $dataGuru)
                                        <option value="{{ $dataGuru->id }}">
                                            {{ $dataGuru->nama_guru }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="penting">Penting</option>
                                <option value="active">Active</option>
                            </select>
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection