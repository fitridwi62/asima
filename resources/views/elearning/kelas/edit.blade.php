@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Kelas</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($ekelas, array(
                   'method' => 'PATCH',
                   'route' => array('e-kelas.update', $ekelas->id))) 
                }}
	              <div class="box-body">
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Guru</label>
                             @if(!$dataGurus->isEmpty())
                                <select name="data_guru_id" class="form-control">
                                    @foreach($dataGurus as $dataGuru)
                                        <option value="{{ $dataGuru->id }}">
                                            {{ $dataGuru->nama_guru }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>

                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kelas</label>
                             @if(!$kelass->isEmpty())
                                <select name="kelas_id" class="form-control">
                                    @foreach($kelass as $kelas)
                                        <option value="{{ $kelas->id }}">
										{{ $kelas->nama_kelas }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Grade</label>
	                  		{!! Form::text('grade', old('grade'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection