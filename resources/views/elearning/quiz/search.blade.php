@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Tugas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Tugas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('tugas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$etugass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Mata Pelajaran</th>
							<th>Kelas</th>
							<th>Nama</th>
							<th>Batas Waktu</th>
							<th>Tugas File</th>
							<th>Action</th>
						</tr>
						@foreach($etugass as $etugas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $etugas->mata_pelajaran->nama }}</td>
								<td>{{ $etugas->kelas->nama_kelas }}</td>
								<td>{{ $etugas->nama  }}</td>
								<td>{{ $etugas->batas_waktu  }}</td>
								<td>
								<a href="{{ URL::to('/') }}/tugas/{{ $etugas->file }}" target="_blank">Download Tugas</a>
								</td>
								<td>
									<a href="{{ route('tugas.edit', ['tugas' => $etugas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['tugas.destroy', $etugas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $etugass->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection