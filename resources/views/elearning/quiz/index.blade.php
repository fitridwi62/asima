@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Quiz</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Quiz</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('quiz.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'elearning.filter.e-quiz', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$kelass->isEmpty())
                                <select name="kelas_id" class="form-control">
                                    @foreach($kelass as $kelas)
                                        <option value="{{ $kelas->id }}">
                                            {{ $kelas->nama_kelas }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$equizs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Mata Pelajaran</th>
							<th>Kelas</th>
							<th>Nama</th>
							<th>Batas Waktu</th>
							<th>Quiz File</th>
							<th>Action</th>
						</tr>
						@foreach($equizs as $equiz)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $equiz->guru->nama_guru }}</td>
								<td>{{ $equiz->mata_pelajaran->nama }}</td>
								<td>{{ $equiz->kelas->nama_kelas }}</td>
								<td>{{ $equiz->nama  }}</td>
								<td>{{ $equiz->batas_waktu  }}</td>
								<td>
									<a href="{{ URL::to('/') }}/quiz/{{ $equiz->file }}" target="_blank">Download Quiz</a>
								</td>
								<td>
									<a href="{{ route('quiz.edit', ['quiz' => $equiz->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['quiz.destroy', $equiz->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $equizs->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection