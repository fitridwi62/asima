@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Role</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Role</li>
				  </ol>
				</nav>

               
				 <!-- form start -->
                 <form action="{{ route('roles.store') }}" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>
                            <div class="col-md-10">
                                <input required type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="guard_name" class="col-md-2 col-form-label text-md-right">Guard Name</label>
                            <div class="col-md-10">
                                <input required type="text" name="guard_name" class="form-control {{ $errors->has('guard_name') ? ' is-invalid' : '' }}" >
                                @if ($errors->has('guard_name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('guard_name') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="permissions" class="col-md-2 col-form-label text-md-right">Permissions</label>
                            @foreach ($permissions as $key => $permission)
                                <div class="col-md-2">
                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}">{{ $permission->name }}
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            <label for="guard_name" class="col-md-2 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>

					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection