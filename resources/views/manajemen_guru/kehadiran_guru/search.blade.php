@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Kehadiran Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Kehadiran Guru</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('kehadiran-guru.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-kehadiran-guru') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'keuangan-siswa.filter.kehadiran-guru', 'method' => 'get')) }}
					<div class="input-group">
                        <select name="data_guru_id" class="form-control">
                            @if(!$dataGurus->isEmpty())
                                @foreach($dataGurus as $guru)
                                <option value="{{$guru->id}}">
                                    {{ $guru->nama_guru }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                        </select>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$kehadiranGurus->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Tanggal</th>
							<th>Jam Masuk</th>
							<th>Jam Keluar</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($kehadiranGurus as $kehadiranGuru)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $kehadiranGuru->guru->nama_guru }}</td>
								<td>{{ $kehadiranGuru->date }}</td>
								<td>{{ $kehadiranGuru->jam_masuk }}</td>
								<td>{{ $kehadiranGuru->jam_keluar }}</td>
								<td>{{ $kehadiranGuru->status }}</td>
								<td>
                                <a href="{{ route('kehadiran-guru.edit', ['kehadiran-guru' => $kehadiranGuru->id]) }}" class="btn btn-sm btn-success">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</a>
                                {!! Form::open(['route' => ['kehadiran-guru.destroy', $kehadiranGuru->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
								<button class="btn btn-sm btn-danger btn-delete" type="submit">
									<i class="fa fa-trash" aria-hidden="true"></i>
								</button>
                            	{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $kehadiranGurus->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection