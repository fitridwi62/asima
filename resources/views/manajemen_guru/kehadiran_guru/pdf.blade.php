<html>
<head>
	<title>Laporan Data Kehadiran Guru</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Kehadiran Guru</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Guru</th>
                <th>Tanggal</th>
                <th>Jam Masuk</th>
                <th>Jam Keluar</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach($kehadiranGurus as $kehadiranGuru)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $kehadiranGuru->guru->nama_guru }}</td>
                <td>{{ $kehadiranGuru->date }}</td>
                <td>{{ $kehadiranGuru->jam_masuk }}</td>
                <td>{{ $kehadiranGuru->jam_keluar }}</td>
                <td>{{ $kehadiranGuru->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>