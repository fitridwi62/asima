@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($kehadiranGuru, array(
                   'method' => 'PATCH',
                   'route' => array('kehadiran-guru.update', $kehadiranGuru->id))) 
                 }}
	             <div class="box-body">
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            @if(!$dataGurus->isEmpty())
                                @foreach($dataGurus as $guru)
                                <option value="{{$guru->id}}">
                                    {{ $guru->nama_guru }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	                </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            {!! Form::date('date', old('date'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Masuk</label>
                            {!! Form::text('jam_masuk', old('jam_masuk'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            {!! Form::text('jam_keluar', old('jam_keluar'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="masuk">Masuk</option>
                                <option value="izin">Izin</option>
                                <option value="sakit">Sakit</option>
                                <option value="alpha">Alpha</option>
                            </select>
                            </div>
                        </div>
                 </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection