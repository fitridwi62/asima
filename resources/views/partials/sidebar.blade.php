
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('dashbord/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('data_sekolah.index') }}"><i class="fa fa-circle-o"></i> Data Sekolah</a></li>
            <li><a href="{{ route('kurikulum.index') }}"><i class="fa fa-circle-o"></i> Kurikulum</a></li>
            <li><a href="{{ route('tahun_akademik.index') }}"><i class="fa fa-circle-o"></i> Tahun Akademik</a></li>
            <li><a href="{{ route('ruang_kelas.index') }}"><i class="fa fa-circle-o"></i> Ruang Kelas</a></li>
            <li><a href="{{ route('jurusan.index') }}"><i class="fa fa-circle-o"></i> Jurusan</a></li>
            <li><a href="{{ route('agama.index') }}"><i class="fa fa-circle-o"></i> Agama</a></li>
            <li><a href="{{ route('jabatan.index') }}"><i class="fa fa-circle-o"></i> Jabatan</a></li>
            <li><a href="{{ route('bagian.index') }}"><i class="fa fa-circle-o"></i> Bagian</a></li>
             <li><a href="{{ route('pendapatan.index') }}"><i class="fa fa-circle-o"></i> Pendapatan</a></li>
             <li><a href="{{ route('status_karyawan.index') }}"><i class="fa fa-circle-o"></i> Status Karyawan</a></li>
             <li><a href="{{ route('kendaraan.index') }}"><i class="fa fa-circle-o"></i> Kendaraan</a></li>
            <li><a href="{{ route('rombongan_belajar.index') }}"><i class="fa fa-circle-o"></i> Rombongan Belajar</a></li>
            <li><a href="{{ route('wali_kelas.index') }}"><i class="fa fa-circle-o"></i> Wali Kelas</a></li>
            <li><a href="{{ route('jenjang.index') }}"><i class="fa fa-circle-o"></i> Jenjang</a></li>
            <li><a href="{{ route('type-personal-package.index') }}"><i class="fa fa-circle-o"></i> Type Personal Nilai</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>PSB</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('daftar-siswa.index') }}"><i class="fa fa-circle-o"></i> Pendaftaran PSB / PPDB</a></li>
            <li><a href="{{ route('test-interview.index') }}"><i class="fa fa-circle-o"></i> Test Interview</a></li>
            <li><a href="{{ route('hasil-ujian.index') }}"><i class="fa fa-circle-o"></i> Hasil Seleksi</a></li>
            <li><a href="{{ route('siswa-diterima.index') }}"><i class="fa fa-circle-o"></i> Data Siswa diterima</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Data akademik</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('kelas.index') }}"><i class="fa fa-circle-o"></i> Kelas</a></li>
            <li><a href="{{ route('mata_pelajaran.index') }}"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
            <li><a href="{{ route('jadwal-mata-pelajaran.index') }}"><i class="fa fa-circle-o"></i> Jadwal Pelajaran</a></li>
            <li><a href="{{ route('siswa.index') }}"><i class="fa fa-circle-o"></i>Data Siswa</a></li>
            <li><a href="{{ route('data-orang-tua.index') }}"><i class="fa fa-circle-o"></i>Data Orang Tua</a></li>
            <li><a href="{{ route('ekstrakurikuler.index') }}"><i class="fa fa-circle-o"></i> Ekstrakurikuler</a></li>
            <li><a href="{{ route('bimbingan-belajar.index') }}"><i class="fa fa-circle-o"></i> Bimbingan Belajar</a></li>
            <li><a href="{{ route('ruang-kelas-perkelas.index') }}"><i class="fa fa-circle-o"></i> Ruang Kelas Perkelas</a></li>
            <li><a href="{{ route('mata-pelajar-perkelas.index') }}"><i class="fa fa-circle-o"></i> Mata Pelajaran Perkelas</a></li>
            <li><a href="{{ route('mata-pelajaran-guru.index') }}"><i class="fa fa-circle-o"></i> Mata Pelajaran Guru</a></li>
            <li><a href="{{ route('siswa-perkelas.index') }}"><i class="fa fa-circle-o"></i>Siswa Perkelas</a></li>
            <li><a href="{{ route('data-alumni.index') }}"><i class="fa fa-circle-o"></i> Data Alumni</a></li>
            <li><a href="{{ route('nilai-per-kelas.index') }}"><i class="fa fa-circle-o"></i>Data Nilai Perkelas</a></li>
            <li><a href="{{ route('nilai-per-mata-pelajaran.index') }}"><i class="fa fa-circle-o"></i> Nilai Permata Pelajaran</a></li>
            <li><a href="{{ route('grid-nilai.index') }}"><i class="fa fa-circle-o"></i> Grid Nilai</a></li>
            <li><a href="{{ route('akademik.nilai-raport.persiswa-list') }}"><i class="fa fa-circle-o"></i> Raport</a></li>
            <li><a href="{{ route('ekstra-siswa-list.index') }}"><i class="fa fa-circle-o"></i> Ekstra Siswa</a></li>
            <li><a href="{{ route('akademik.jadwal-mata-pelajaran') }}"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="{{ route('pengumuman-kelas-siswa.index') }}"><i class="fa fa-circle-o"></i> Pengumuman Kelas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Keuangan siswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('jenis-pembayaran.index') }}"><i class="fa fa-circle-o"></i> Jenis Pembayaran</a></li>
            <li><a href="{{ route('pembayaran-spp.index') }}"><i class="fa fa-circle-o"></i> Pembayaran SPP</a></li>
            <li><a href="{{ route('pembayaran-semester.index') }}"><i class="fa fa-circle-o"></i> Pembayaran Semester</a></li>
            <li><a href="{{ route('pembayaran-buku.index') }}"><i class="fa fa-circle-o"></i> Pembayaran Buku</a></li>
            <li><a href="{{ route('pembayaran.index') }}"><i class="fa fa-circle-o"></i> Pembayaran</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> Manajemen Guru<span></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{ route('data_guru.index') }}"><i class="fa fa-circle-o"></i> Data Guru</a></li>
          <li><a href="{{ route('kehadiran-guru.index') }}"><i class="fa fa-circle-o"></i> Kehadiran Guru</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> Kepegawaian<span></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('staff.index') }}"><i class="fa fa-circle-o"></i>Data Pegawai</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Perpustakaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('data-buku.index') }}"><i class="fa fa-circle-o"></i> Data Buku</a></li>
            <li><a href="{{ route('peminjaman-buku.index') }}"><i class="fa fa-circle-o"></i> Peminjam</a></li>
            <li><a href="{{ route('pengembalian-buku.index') }}"><i class="fa fa-circle-o"></i> Pengembalian</a></li>
            <li><a href="{{ route('petugas.index') }}"><i class="fa fa-circle-o"></i> Petugas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>E-learning</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('pengumuman.index') }}"><i class="fa fa-circle-o"></i> Pengumuman</a></li>
            <li><a href="{{ route('e-kelas.index') }}"><i class="fa fa-circle-o"></i> Kelas</a></li>
            <li><a href="{{ route('quiz.index') }}"><i class="fa fa-circle-o"></i> Quiz</a></li>
            <li><a href="{{ route('materi.index') }}"><i class="fa fa-circle-o"></i> Materi</a></li>
            <li><a href="{{ route('tugas.index') }}"><i class="fa fa-circle-o"></i> Tugas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Inventori / asset</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li><a href="{{ route('tipe-inventori.index') }}"><i class="fa fa-circle-o"></i> Tipe Inventori Asset</a></li>
            <li><a href="{{ route('kategori-inventori.index') }}"><i class="fa fa-circle-o"></i>Kategori Inventori</a></li>
            <li><a href="{{ route('gedung.index') }}"><i class="fa fa-circle-o"></i> Gedung</a></li>
            <li><a href="{{ route('tipe-donasi.index') }}"><i class="fa fa-circle-o"></i>Tipe Donasi</a></li>
            <li><a href="{{ route('asset.index') }}"><i class="fa fa-circle-o"></i> Asset</a></li>
            <li><a href="{{ route('donasi.index') }}"><i class="fa fa-circle-o"></i> Donasi</a></li>
            <li><a href="{{ route('barang.index') }}"><i class="fa fa-circle-o"></i> Barang</a></li>
            <li><a href="{{ route('barang-masuk.index') }}"><i class="fa fa-circle-o"></i> Barang Masuk</a></li>
            <li><a href="{{ route('barang-keluar.index') }}"><i class="fa fa-circle-o"></i>Barang Keluar</a></li>
            <li><a href="{{ route('spk-asset.index') }}"><i class="fa fa-circle-o"></i>SPK Asset</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Accounting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li><a href="{{ route('tipe-pajak.index') }}"><i class="fa fa-circle-o"></i> Tipe Pajak</a></li>
            <li><a href="{{ route('coa.index') }}"><i class="fa fa-circle-o"></i> COA</a></li>
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-share"></i> <span>Pengeluaran</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('uang-masuk.index') }}"><i class="fa fa-circle-o"></i> Uang Masuk</a></li>
                    <li><a href="{{ route('permintaan-dana.index') }}"><i class="fa fa-circle-o"></i> Permintaan Dana</a></li>
                    <li><a href="{{ route('cek-list.index') }}"><i class="fa fa-circle-o"></i> Cek List</a></li>
                    <li><a href="{{ route('reibusment.index') }}"><i class="fa fa-circle-o"></i> Reibusment</a></li>
                    <li><a href="{{ route('jurnal-edc.index') }}"><i class="fa fa-circle-o"></i> Jurnal Kas EDC</a></li>
                    <li><a href="{{ route('jurnal-uang-keluar.index') }}"><i class="fa fa-circle-o"></i> Jurnal Uang Keluar</a></li>
                    <li><a href="{{ route('kas-mutasi.index') }}"><i class="fa fa-circle-o"></i> Kas Mutasi</a></li>
                    <li><a href="{{ route('dana-pending.index') }}"><i class="fa fa-circle-o"></i> Dalam Belum Tertagih</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-share"></i> <span>Pemasukan</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('rekening-siswa.index') }}"><i class="fa fa-circle-o"></i> Rekening Siswa</a></li>
                    <li><a href="{{ route('jurnal-pbn.index') }}"><i class="fa fa-circle-o"></i> Jurnal PBN</a></li>
                    <li><a href="{{ route('jurnal-spp.index') }}"><i class="fa fa-circle-o"></i> Jurnal SPP</a></li>
                    <li><a href="{{ route('jurnal-program-bulanan.index') }}"><i class="fa fa-circle-o"></i> Jurnal Program Bulanan</a></li>
                    <li><a href="{{ route('cut-off-siswa.index') }}"><i class="fa fa-circle-o"></i> Cut Off Siswa</a></li>
                    <li><a href="{{ route('hutang.index') }}"><i class="fa fa-circle-o"></i> Hutang</a></li>
                </ul>
            </li>
          </ul>
        </li>
        <li><a href="{{ route('pengumuman.event.create') }}"><i class="fa fa-circle-o"></i> Pengumuman Event</a></li>
        <li><a href="{{ route('roles.index') }}"><i class="fa fa-circle-o"></i> Role</a></li>
        <li>
            <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                   <i class="fa fa-circle-o"></i>  Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
        </li>
       
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>