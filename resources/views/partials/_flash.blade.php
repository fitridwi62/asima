@if (session()->has('flash_notification.message'))
  <div class="col-md-offset-2 col-md-8 col-md-offset-2">
    <div class="alert alert-{{ session()->get('flash_notification.level') }}">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      {!! session()->get('flash_notification.message') !!}
    </div>
  </div>
@endif