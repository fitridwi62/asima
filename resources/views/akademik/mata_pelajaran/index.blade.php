@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Mata Pelajaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('mata_pelajaran.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.mapel') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'mata-pelajaran.search', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Mata Pelajaran" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$mata_pelajarans->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Kurikulum</th>
							<th>Jenjang</th>
							<th>Guru</th>
							<th>Action</th>
						</tr>
						@foreach($mata_pelajarans as $mata_pelajaran)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $mata_pelajaran->kode }}</td>
								<td>{{ $mata_pelajaran->nama }}</td>
								<td>{{ $mata_pelajaran->kurikulum->nama }}</td>
								<td>{{ $mata_pelajaran->jenjang->nama_jenjang }}</td>
								<td>{{ $mata_pelajaran->guru->nama_guru }}</td>
								<td>
									<a href="{{ route('mata_pelajaran.edit', ['mata-pelajaran' => $mata_pelajaran->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['mata_pelajaran.destroy', $mata_pelajaran->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $mata_pelajarans->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection