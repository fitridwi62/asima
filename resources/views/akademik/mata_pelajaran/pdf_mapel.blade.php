<html>
<head>
	<title>Data Mata Pelajaran PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Mata Pelajaran</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Kode</th>
                <th>Nama</th>
                <th>Kurikulum</th>
                <th>Jenjang</th>
                <th>Guru</th>
			</tr>
		</thead>
		<tbody>
            @foreach($mata_pelajarans as $mata_pelajaran)
			<tr>
                <td>{{ $loop->index +1 }}</td>
				<td>{{ $mata_pelajaran->kode }}</td>
				<td>{{ $mata_pelajaran->nama }}</td>
				<td>{{ $mata_pelajaran->kurikulum->nama }}</td>
				<td>{{ $mata_pelajaran->jenjang->nama_jenjang }}</td>
				<td>{{ $mata_pelajaran->guru->nama_guru }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>