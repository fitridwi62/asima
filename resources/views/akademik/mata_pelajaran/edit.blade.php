@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($mata_pelajaran, array(
                   'method' => 'PATCH',
                   'route' => array('mata_pelajaran.update', $mata_pelajaran->id))) 
                 }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kode Pelajaran</label>
                            {!! Form::text('kode', old('kode'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            {!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                   
	                <div class="col-md-4">
	                	
		                	<div class="form-group">
			                  <label for="nama">Kurikulum</label>
			                  <select name="kurikulum_id" class="form-control">
			                  @if(!$kurikulums->isEmpty())
			                  	 @foreach($kurikulums as $kurikulum)
			                  	 	<option value="{{$kurikulum->id}}">
			                  	 		{{ $kurikulum->nama }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            @if(!$gurus->isEmpty())
                                @foreach($gurus as $guru)
                                <option value="{{$guru->id}}">
                                    {{ $guru->nama_guru }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Jenjang</label>
                            <select name="jenjang_id" class="form-control">
                            @if(!$jenjangs->isEmpty())
                                @foreach($jenjangs as $jenjang)
                                <option value="{{$guru->id}}">
                                    {{ $jenjang->nama_jenjang }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection