@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('ekstrakurikuler.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-ekskul') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'ekstra.search', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Ekstrakurikuler" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$extraKurikulers->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Penanggung Jawab</th>
							<th>Tanggal</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
							<th>Jumlah Pertemuan</th>
							<th>Action</th>
						</tr>
						@foreach($extraKurikulers as $ekstrakurikuler)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $ekstrakurikuler->nama_ekstra }}</td>
								<td>{{ $ekstrakurikuler->penangung_jawab }}</td>
								<td>{{ $ekstrakurikuler->tanggal }}</td>
								<td>{{ $ekstrakurikuler->jam_mulai }}</td>
								<td>{{ $ekstrakurikuler->jam_selesai }}</td>
								<td>{{ $ekstrakurikuler->pertemuan }}</td>
								<td>
									<a href="{{ route('ekstrakurikuler.edit', ['ekstrakurikuler' => $ekstrakurikuler->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['ekstrakurikuler.destroy', $ekstrakurikuler->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $extraKurikulers->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection