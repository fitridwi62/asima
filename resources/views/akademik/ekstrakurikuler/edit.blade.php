@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($ekstra, array(
                   'method' => 'PATCH',
                   'route' => array('ekstrakurikuler.update', $ekstra->id))) 
                 }}
	             <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama</label>
                            {!! Form::text('nama_ekstra', old('nama_ekstra'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Penangung Jawab</label>
                            {!! Form::text('penangung_jawab', old('penangung_jawab'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            {!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Mulai</label>
                            {!! Form::text('jam_mulai', old('jam_mulai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            {!! Form::text('jam_selesai', old('jam_selesai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jumlah Pertemuan</label>
                            {!! Form::text('pertemuan', old('pertemuan'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection