<html>
<head>
	<title>Data Nilai Perkelas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Nilai Perkelas</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
				<th>Mata Pelajaran</th>
                <th>Kelas</th>
                <th>Siswa</th>
                <th>Nilai</th>
                <th>Predikat</th>
			</tr>
		</thead>
		<tbody>
            @foreach($nilaiPerkelass as $nilaiPerkelas)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $nilaiPerkelas->mata_pelajaran->nama }}</td>
                <td>{{ $nilaiPerkelas->kelas->nama_kelas }}</td>
                <td>{{ $nilaiPerkelas->siswa->nama_lengkap }}</td>
                <td>{{ $nilaiPerkelas->nilai }}</td>
                <td>{{ $nilaiPerkelas->predikat }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>