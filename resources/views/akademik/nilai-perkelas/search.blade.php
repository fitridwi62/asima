@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Perkelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('nilai-per-kelas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.nilai-perkelas') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.nilai-perkelas', 'method' => 'get')) }}
					<div class="input-group">
                       @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$nilaiPerkelass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Mata Pelajaran</th>
							<th>Kelas</th>
							<th>Siswa</th>
							<th>Nilai</th>
							<th>Predikat</th>
							<th>Action</th>
						</tr>
						@foreach($nilaiPerkelass as $nilaiPerkelas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $nilaiPerkelas->mata_pelajaran->nama }}</td>
								<td>{{ $nilaiPerkelas->kelas->nama_kelas }}</td>
								<td>{{ $nilaiPerkelas->siswa->nama_lengkap }}</td>
								<td>{{ $nilaiPerkelas->nilai }}</td>
								<td>{{ $nilaiPerkelas->predikat }}</td>
								<td>
									<a href="{{ route('nilai-per-kelas.edit', ['nilai-perkelas' => $nilaiPerkelas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['nilai-per-kelas.destroy', $nilaiPerkelas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $nilaiPerkelass->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection