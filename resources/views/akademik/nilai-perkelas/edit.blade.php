@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Perkelas</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($nilaiPerkelas, array(
                   'method' => 'PATCH',
                   'route' => array('nilai-per-kelas.update', $nilaiPerkelas->id))) 
                 }}
	              <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Mata Pelajaran</label>
                            @if(!$mataPelajarans->isEmpty())
                            <select name="mata_pelajaran_id" class="form-control">
                                @foreach($mataPelajarans as $mataPelajaran)
                                    <option value="{{ $mataPelajaran->id }}">
                                        {{ $mataPelajaran->nama }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kelas</label>
                             @if(!$kelas->isEmpty())
                                <select name="kelas_id" class="form-control">
                                    @foreach($kelas as $kelasx)
                                        <option value="{{ $kelasx->id }}">
                                            {{ $kelasx->nama_kelas }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai</label>
	                  		{!! Form::text('nilai', old('nilai'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>


                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Predikat</label>
	                  		{!! Form::text('predikat', old('predikat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection