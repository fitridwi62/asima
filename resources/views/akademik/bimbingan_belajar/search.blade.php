@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Bimbingan Belajar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Bimbingan Belajar</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('bimbingan-belajar.create') }}" class="btn btn-sm btn-primary">
						Buat Bimbingan Belajar
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'bimbingan-belajar.search-filter', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Mata Pelajaran" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$bimbinganBelajars->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Guru</th>
							<th>Tanggal</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
							<th>Jumlah Pertemuan</th>
							<th>Action</th>
						</tr>
						@foreach($bimbinganBelajars as $bimbinganBelajar)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $bimbinganBelajar->nama }}</td>
								<td>{{ $bimbinganBelajar->guru->nama_guru }}</td>
								<td>{{ $bimbinganBelajar->tanggal }}</td>
								<td>{{ $bimbinganBelajar->jam_mulai }}</td>
								<td>{{ $bimbinganBelajar->jam_selesai }}</td>
								<td>{{ $bimbinganBelajar->pertemuan }}</td>
								<td>
									<a href="{{ route('bimbingan-belajar.edit', ['bimbingan-belajar' => $bimbinganBelajar->id]) }}" class="btn btn-sm btn-success">Edit</a>
									{!! Form::open(['route' => ['bimbingan-belajar.destroy', $bimbinganBelajar->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $bimbinganBelajars->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection