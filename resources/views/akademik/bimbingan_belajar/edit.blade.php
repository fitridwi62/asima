@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Bimbingan Belajar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($bimbinganBelajar, array(
                   'method' => 'PATCH',
                   'route' => array('bimbingan-belajar.update', $bimbinganBelajar->id))) 
                 }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama</label>
                            {!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Guru</label>
                             @if(!$gurus->isEmpty())
                                <select name="data_guru_id" class="form-control">
                                    @foreach($gurus as $guru)
                                        <option value="{{ $guru->id }}">
                                            {{ $guru->nama_guru }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            {!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Mulai</label>
                            {!! Form::text('jam_mulai', old('jam_mulai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            {!! Form::text('jam_selesai', old('jam_selesai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jumlah Pertemuan</label>
                            {!! Form::text('pertemuan', old('pertemuan'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection