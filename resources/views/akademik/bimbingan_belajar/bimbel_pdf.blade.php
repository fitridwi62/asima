<html>
<head>
	<title>Bimbel PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Bimbingan Belajar</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Nama</th>
				<th>Guru</th>
				<th>Tanggal</th>
				<th>Jam Mulai</th>
				<th>Jam Selesai</th>
				<th>Jumlah Pertemuan</th>
			</tr>
		</thead>
		<tbody>
            @foreach($bimbinganBelajars as $bimbinganBelajar)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $bimbinganBelajar->nama }}</td>
                <td>{{ $bimbinganBelajar->guru->nama_guru }}</td>
                <td>{{ $bimbinganBelajar->tanggal }}</td>
                <td>{{ $bimbinganBelajar->jam_mulai }}</td>
                <td>{{ $bimbinganBelajar->jam_selesai }}</td>
                <td>{{ $bimbinganBelajar->pertemuan }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>