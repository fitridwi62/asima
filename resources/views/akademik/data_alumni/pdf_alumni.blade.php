<html>
<head>
	<title>Data Alumni PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Alumni PDF</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Nama</th>
                <th>Tahun Angkatan</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Alamat</th>
                <th>Sekolah ke</th>
			</tr>
		</thead>
		<tbody>
            @foreach($dataAlumnis as $dataAlumni)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $dataAlumni->nama }}</td>
                <td>{{ $dataAlumni->tahun_angkatan }}</td>
                <td>{{ $dataAlumni->email }}</td>
                <td>{{ $dataAlumni->phone }}</td>
                <td>{{ $dataAlumni->alamat }}</td>
                <td>{{ $dataAlumni->sekolah_ke }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>