@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Alumni</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($dataAlumni, array(
                   'method' => 'PATCH',
                   'route' => array('data-alumni.update', $dataAlumni->id))) 
                 }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama</label>
                            {!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tahun Angkatan</label>
                            {!! Form::text('tahun_angkatan', old('tahun_angkatan'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Email</label>
                            {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Phone</label>
                            {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Alamat</label>
                            {!! Form::text('alamat', old('alamat'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Sekelah ke</label>
                            {!! Form::text('sekolah_ke', old('sekolah_ke'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection