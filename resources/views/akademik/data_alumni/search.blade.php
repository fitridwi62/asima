@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Alumni</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Alumni</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('data-alumni.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('data-alumni_pdf') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.data-alumni', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Data Alumni" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$dataAlumnis->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Tahun Angkatan</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Alamat</th>
							<th>Sekolah ke</th>
							<th>Action</th>
						</tr>
						@foreach($dataAlumnis as $dataAlumni)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $dataAlumni->nama }}</td>
								<td>{{ $dataAlumni->tahun_angkatan }}</td>
								<td>{{ $dataAlumni->email }}</td>
								<td>{{ $dataAlumni->phone }}</td>
								<td>{{ $dataAlumni->alamat }}</td>
								<td>{{ $dataAlumni->sekolah_ke }}</td>
								<td>
									<a href="{{ route('data-alumni.edit', ['data-alumni' => $dataAlumni->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['data-alumni.destroy', $dataAlumni->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $dataAlumnis->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection