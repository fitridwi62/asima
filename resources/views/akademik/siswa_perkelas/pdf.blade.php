<html>
<head>
	<title>Data Siswa Perkelas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Siswa Perkelas</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Kelas</th>
                <th>Siswa</th>
			</tr>
		</thead>
		<tbody>
            @foreach($siswaPerkelasLists as $siswaPerkelasList)
			<tr>
                <td>{{ $loop->index +1 }}</td>
				<td>{{ $siswaPerkelasList->kelas->nama_kelas }}</td>
				<td>{{ $siswaPerkelasList->siswa->nama_lengkap }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>