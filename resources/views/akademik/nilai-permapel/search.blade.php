@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Per Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Per Mata Pelajaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('nilai-per-mata-pelajaran.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.nilai-permapel') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.nilai-permatkul', 'method' => 'get')) }}
					<div class="input-group">
						<select name="mata_pelajaran_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajaran)
                                <option value="{{$mataPelajaran->id}}">
                                    {{ $mataPelajaran->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                        </select>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$nilaiPermapels->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Mata Pelajaran</th>
							<th>Grid Nilai</th>
							<th>Siswa</th>
							<th>Nilai</th>
							<th>Predikat</th>
							<th>Action</th>
						</tr>
						@foreach($nilaiPermapels as $nilaiPermapel)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $nilaiPermapel->guru->nama_guru }}</td>
								<td>{{ $nilaiPermapel->mata_pelajaran->nama }}</td>
								<td>{{ $nilaiPermapel->gridNilai->nilai }}</td>
								<td>{{ $nilaiPermapel->siswa->nama_lengkap }}</td>
								<td>{{ $nilaiPermapel->nilai }}</td>
								<td>{{ $nilaiPermapel->predikat }}</td>
								<td>
									<a href="{{ route('nilai-per-mata-pelajaran.edit', ['nilai-per-mata-pelajaran' => $nilaiPermapel->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['nilai-per-mata-pelajaran.destroy', $nilaiPermapel->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $nilaiPermapels->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection