<html>
<head>
	<title>Data Nilai Perkelas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Nilai Perkelas</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
				<th>Guru</th>
                <th>Mata Pelajaran</th>
                <th>Grid Nilai</th>
                <th>Siswa</th>
                <th>Nilai</th>
                <th>Predikat</th>
			</tr>
		</thead>
		<tbody>
            @foreach($nilaiPermapels as $nilaiPermapel)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $nilaiPermapel->guru->nama_guru }}</td>
                <td>{{ $nilaiPermapel->mata_pelajaran->nama }}</td>
                <td>{{ $nilaiPermapel->gridNilai->nilai }}</td>
                <td>{{ $nilaiPermapel->siswa->nama_lengkap }}</td>
                <td>{{ $nilaiPermapel->nilai }}</td>
                <td>{{ $nilaiPermapel->predikat }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>