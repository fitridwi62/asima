@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Per Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Per Mata Pelajaran</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ Form::open(array('route' => 'nilai-per-mata-pelajaran.store')) }}
	              <div class="box-body">
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Guru</label>
                             @if(!$gurus->isEmpty())
                                <select name="data_guru_id" class="form-control">
                                    @foreach($gurus as $guru)
                                        <option value="{{ $guru->id }}">
                                            {{ $guru->nama_guru }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Mata Pelajaran</label>
                            @if(!$mataPelajarans->isEmpty())
                            <select name="mata_pelajaran_id" class="form-control">
                                @foreach($mataPelajarans as $mataPelajaran)
                                    <option value="{{ $mataPelajaran->id }}">
                                        {{ $mataPelajaran->nama }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Grid Nilai</label>
                             @if(!$gridNilais->isEmpty())
                                <select name="nilai_id" class="form-control">
                                    @foreach($gridNilais as $gridNilai)
                                        <option value="{{ $gridNilai->id }}">
                                            {{ $gridNilai->categori_nilai }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>


                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai</label>
	                  		{!! Form::text('nilai', old('nilai'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>


                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Predikat</label>
	                  		{!! Form::text('predikat', old('predikat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection