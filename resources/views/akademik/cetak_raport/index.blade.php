<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('dashbord/raport/asset/css/bootstrap.css') }}">
    <title>Papua Kaih</title>
    <style>
    .raportHeader ul{
        list-style: none;
        margin:0;
        padding:0;
        border-left: 1px solid #ccc;
        margin-left: 38%;
    }
    .raportHeader {
        margin-top:10px;
        margin-bottom: 10px;
    }
    .raportHeader ul li{
            margin-left: 4%;
            font-size: 10px;
    }
  </style>
</head>
<body>
    <div class="col-12 fullContent">
        <div class="container-md">
            <div class="col-md-12">
                <div class="row" style="background: #f5f5f5;">
                    <div class="col-md-4">
                        <div class="raportLogo">
                             <img src="{{ asset('images/papua-logo.png') }}" class="img-logo" alt="asima" style="width:179px; margin-top: 10px; margin-bottom: 10px; position:relative; right: -17px;">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="raportHeader">
                             <ul>
                                 <li>Perum Taman Buper Bhayangkara Permai Blok A-5 No.1 Waena - Heram Kota Jayapura</li>
                                 <li>Phone: (0967) 5189064</li>
                                 <li>Email: info@papuakasih.com</li>
                                 <li>www.papuakasih.com</li>
                             </ul>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <tr>
                        <td>
                            <table class="table-in-column">
                                <tr>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Nama Sekolah</td>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Papua Kasih</td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Alamat</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Jalan Sentani Papua</td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Nama Siswa</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                    <td style="border: 0; font-weight: bold; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">
                                        {{ $nama_lengkap->siswaItem->nama_lengkap }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Nomor Induk</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">
                                    {{ $nama_lengkap->siswaItem->nis }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                        <td>
                            <table class="table-in-column">
                                <tr>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Kelas</td>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">
                                            {{ $nama_lengkap->siswaItem->kelas->nama_kelas }}
                                        </td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Semester</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">II</td>
                                </tr>

                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">Tahun Pelajaran</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px;">:</td>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-size: 14px;">
                                    {{ $nama_lengkap->siswaItem->tahun_akademik->nama }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                  </table>
                <table class="table">
                    <thead>
                        <tr style="background:#fbfbfb; text-align: center;">
                          <th scope="col" style="font-size: 14px;">No</th>
                          <th scope="col" style="font-size: 14px;">Mata Pelajaran</th>
                          <th scope="col" style="font-size: 14px;">Pengetahuan</th>
                          <th scope="col" style="font-size: 14px;">Keterampilan</th>
                          <th scope="col" style="font-size: 14px;">Nilai Akhir</th>
                          <th scope="col" style="font-size: 14px;">Predikat</th>
                        </tr>
                      </thead>
                      <tbody>
                            @if(!$raportMapelNilai->isEmpty())
                                <?php $total_nilai = 0;?>
                                <?php $total_siswa = 0;?>
                                @foreach($raportMapelNilai as $key => $raportMapelNilaix)
                                <tr>
                                    <th scope="row" style="text-align: center;">
                                       
                                    </th>
                                    <td style="text-align: center; font-size: 14px;">
                                        {{ $raportMapelNilaix->mapel->nama }}
                                    </td>
                                    <td style="text-align: center; font-size: 14px;">
                                        {{ $raportMapelNilaix->nilai_pengetahuan }}
                                    </td>
                                    <td style="text-align: center; font-size: 14px;">
                                        {{ $raportMapelNilaix->nilai_keterampilan }}
                                    </td>
                                    <td style="text-align: center; font-size: 14px;">
                                        {{ $raportMapelNilaix->nilai_akhir }}
                                    </td>
                                    <td style="text-align: center; font-size: 14px;">
                                        {{ $raportMapelNilaix->predikat }}
                                    </td>
                                   
                                </tr>
                                <?php $total_nilai = $total_nilai + ($raportMapelNilaix->nilai_akhir);?>
                                <?php $total_siswa = $total_siswa + ($raportMapelNilaix->id);?>
                                @endforeach
                            @else
                                <div class="alert alert-danger">
                                    Data empty
                                </div>
                            @endif
                          <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px; text-align: center;">Jumlah</td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px; text-align: center;">
                                {{ $total_nilai }}
                            </td>
                            <td></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px; text-align: center;">Peringkat ke :</td>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px; text-align: center;">1</td>
                            <td style="font-weight: bold; font-size: 14px; text-align: center;"></td>
                            <td style="font-size: 14px; text-align: center;"> Dari Siswa : <b>{{ $totalSiswa }}</b></td>
                          </tr>
                      </tbody>
                </table>

                <table class="table">
                    <thead>
                        <tr style="background:#fbfbfb; text-align: center;">
                          <th scope="col" style="font-size: 14p; text-align: center;">No</th>
                          <th scope="col" style="font-size: 14p; text-align: center;">Ektrakurikuler</th>
                          <th scope="col" style="font-size: 14p; text-align: center;">Nilai</th>
                          <th scope="col" style="font-size: 14p; text-align: center;">Predikat</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!$ekstraNilais->isEmpty())
                            @foreach($ekstraNilais as $ekstraNilai)
                                <tr>
                                    <th scope="row" style="text-align: center;">{{ $loop->index +1 }}</th>
                                    <td style="font-size: 14px; text-align: center;">{{ $ekstraNilai->nama_ekstrakurikuler }}</td>
                                    <td style="font-size: 14px; text-align: center;">{{ $ekstraNilai->nilai }}</td>
                                    <td style="font-size: 14px; text-align: center;">{{ $ekstraNilai->predikat }}</td>
                                </tr>
                            @endforeach
                        @else

                        @endif

                      </tbody>
                </table>

                <table class="table">
                    <tr>
                        <td>
                            <table class="table-in-column">
                                <tr>
                                        <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-weight: bold; text-align: center;">Nama Orang Tua</td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3em; padding-bottom: 3px;">(......................)</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table class="table-in-column">
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-weight: bold; text-align: center;">Wali Kelas</td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3em; padding-bottom: 3px;">(......................)</td>
                                </tr>
                            </table>
                        </td>

                        <td>
                            <table class="table-in-column">
                                <tr>
                                    <td style="border: 0; padding-top: 3px; padding-bottom: 3px; font-weight: bold; text-align: center;">Kepala Sekolah</td>
                                </tr>
                                <tr>
                                    <td style="border: 0; padding-top: 3em; padding-bottom: 3px;">(......................)</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                  </table>
            </div>
        </div>
    </div>
</body>
</html>