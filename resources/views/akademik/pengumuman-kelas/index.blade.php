@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pengumuman Siswa Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pengumuman Siswa Perkelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pengumuman-kelas-siswa.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$pengumumanKelas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Judul</th>
							<th>Kelas</th>
							<th>Siswa</th>
							<th>Action</th>
						</tr>
						@foreach($pengumumanKelas as $pengumumanKelasx)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pengumumanKelasx->judul }}</td>
								<td>{{ $pengumumanKelasx->siswa->kelas->nama_kelas }}</td>
								<td>{{ $pengumumanKelasx->siswa->nama_lengkap }}</td>
								<td>
									<a href="{{ route('pengumuman-kelas-siswa.edit', ['pengumumanKelas' => $pengumumanKelasx->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['pengumuman-kelas-siswa.destroy', $pengumumanKelasx->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $pengumumanKelas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection