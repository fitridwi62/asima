@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata Pelajaran Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Mata Pelajaran Guru</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('mata-pelajaran-guru.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.matkul-guru') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.matkul-guru', 'method' => 'get')) }}
					<div class="input-group">
                        <select name="mata_pelajar_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajar)
                                <option value="{{$mataPelajar->id}}">
                                    {{ $mataPelajar->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                        </select>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$mataPelajaranGurus->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kelas</th>
							<th>Mata Pelajaran</th>
							<th>Guru</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						    @foreach($mataPelajaranGurus as $mataPelajaranGuru)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $mataPelajaranGuru->kelas->nama_kelas }}</td>
								<td>{{ $mataPelajaranGuru->mata_pelajaran->nama }}</td>
								<td>{{ $mataPelajaranGuru->guru->nama_guru }}</td>
								<td>{{ $mataPelajaranGuru->status }}</td>
								<td>
									<a href="{{ route('mata-pelajaran-guru.edit', ['mata-pelajar-guru' => $mataPelajaranGuru->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['mata-pelajaran-guru.destroy', $mataPelajaranGuru->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $mataPelajaranGurus->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection