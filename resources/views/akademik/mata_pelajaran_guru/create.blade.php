@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            {{ Form::open(array('route' => 'mata-pelajaran-guru.store')) }}
	              <div class="box-body">
	                <div class="col-md-6">
		                	<div class="form-group">
			                  <label for="nama">Kelas</label>
			                  <select name="kelas_id" class="form-control">
			                  @if(!$kelas->isEmpty())
			                  	 @foreach($kelas as $kelasx)
			                  	 	<option value="{{$kelasx->id}}">
			                  	 		{{ $kelasx->nama_kelas }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <select name="mata_pelajar_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajaran)
                                <option value="{{$mataPelajaran->id}}">
                                    {{ $mataPelajaran->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            @if(!$dataGurus->isEmpty())
                                @foreach($dataGurus as $dataGuru)
                                <option value="{{$dataGuru->id}}">
                                    {{ $dataGuru->nama_guru }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>
                    </div>
                </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Simpan</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection