<html>
<head>
	<title>Data Mata Pelajaran Untuk Guru</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Mata Pelajaran Untuk Guru</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
				<th>Kelas</th>
				<th>Mata Pelajaran</th>
				<th>Guru</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach($mataPelajaranGurus as $mataPelajaranGuru)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $mataPelajaranGuru->kelas->nama_kelas }}</td>
                <td>{{ $mataPelajaranGuru->mata_pelajaran->nama }}</td>
                <td>{{ $mataPelajaranGuru->guru->nama_guru }}</td>
                <td>{{ $mataPelajaranGuru->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>