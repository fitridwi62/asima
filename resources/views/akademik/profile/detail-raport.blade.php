@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Kembali
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$nilaiRaport->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Kelas</th>
						</tr>
						@foreach($nilaiRaport as $nilaiRaportx)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $nilaiRaportx->siswaItem->nama_lengkap }}</td>
								<td>{{ $nilaiRaportx->siswaItem->kelas->nama_kelas }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection