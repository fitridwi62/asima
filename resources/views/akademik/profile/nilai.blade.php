@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Nilai Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Nilai Mata Pelajaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Kembali
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$nilais->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Mata Pelajaran</th>
							<th>Guru</th>
							<th>Nilai</th>
						</tr>
						@foreach($nilais as $nilai)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $nilai->siswa->nama_lengkap }}</td>
								<td>{{ $nilai->mata_pelajaran->nama }}</td>
								<td>{{ $nilai->guru->nama_guru }}</td>
								<td>{{ $nilai->nilai }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection