@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Kembali
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$pembayarans->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pembayaran</th>
							<th>SPP</th>
							<th>Semester</th>
						</tr>
						@foreach($pembayarans as $pembayaran)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pembayaran->siswa->nama_lengkap }}</td>
								<td>{{ $pembayaran->pembayaranSPP->biaya_spp }}</td>
								<td>{{ $pembayaran->pembayaranSemester->biaya }}</td>
								<td>{{ $pembayaran->pembayaranSemester->biaya }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection