@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Kembali
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$ekstraPersiswas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Ekstrakurikuler</th>
						</tr>
						@foreach($ekstraPersiswas as $ekstraListItem)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $ekstraListItem->siswa->nama_lengkap }}</td>
								<td>{{ $ekstraListItem->ekstra->nama_ekstra }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection