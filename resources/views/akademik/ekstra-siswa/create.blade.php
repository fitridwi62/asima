@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler Siswa</li>
				  </ol><div class="box-body">
				 <!-- form start -->
	            {{ Form::open(array('route' => 'ekstra-siswa.store')) }}
	              <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             @if(!$siswas->isEmpty())
                                <select name="siswa_id" class="form-control">
                                    @foreach($siswas as $siswa)
                                        <option value="{{ $siswa->id }}">
                                            {{ $siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="nama">Ekstrakuriuler</label>
                            @if(!$ekstraList->isEmpty())
                                <select name="ekstra_id" class="form-control">
                                    @foreach($ekstraList as $ekstraLis)
                                        <option value="{{ $ekstraLis->id }}">
                                            {{ $ekstraLis->nama_ekstra }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                             
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            {!! Form::close() !!}

                
					
				</div>
				<!-- /.box-body -->

                <table class="table table-bordered">
					@if(!$ekstras->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Ekstrakurikuler</th>
						</tr>
						@foreach($ekstras as $ekstra)
							<tr>
								<td>{{ $loop->index +1 }}</td>
                                <td>{{ $ekstra->siswaItem->nama_lengkap }}</td>
								<td>{{ $ekstra->ekstra->nama_ekstra }}</td>
							</tr>
						@endforeach
                        <tr>
                            <td>
                                <a href="{{ route('ekstra-item.siswa') }}" class="btn btn-warning">
                                    Lanjutkan Ekstrakurikuler List
                                </a>
                            </td>
                            <td></td>
                            <td></td>
                        <td>
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
				


			</div>

		</div>
		<!-- /.box -->
	</div>
@endsection