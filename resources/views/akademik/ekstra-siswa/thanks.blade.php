@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler</li>
				  </ol>
				</nav>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
                </div>
                <section class="portfolio-flyer py-5" id="gallery">
                    <div class="container pt-lg-3 pb-md-5">
                        <div class="row">
                            <div class="col-lg-12 thankTitle">Ekstrakurikuler Anda Sudah Di Setup</div>
                            <div class="col-lg-12 thankOrder">
                                <a href="{{ route('ekstra-siswa.index') }}" class="btn btn-warning">Lihat Ekstrakurikuler</a>
                            </div>
                        </div>
                    </div>
                </section>
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection
<?php 
    Session::forget('session_raport_id');
    Session::forget('session_id');
?>