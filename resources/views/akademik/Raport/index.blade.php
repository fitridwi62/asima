@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$siswa_lists->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Nilai Pengetahuan</th>
							<th>Nilai Keterampilan</th>
							<th>Nilai Akhir</th>
							<th>Predikat</th>
						</tr>
						@foreach($siswa_lists as $siswa_list)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>
									<a href="{{ route('akademik.nilai-raport.persiswa', $siswa_list->siswa_id) }}">
										{{ $siswa_list->siswaItem->nama_lengkap }}
									</a>
							    </td>
								<td>{{ $siswa_list->nilai_pengetahuan }}</td>
								<td>{{ $siswa_list->nilai_keterampilan }}</td>
								<td>{{ $siswa_list->nilai_akhir }}</td>
								<td>{{ $siswa_list->predikat }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			
		</div>
		<!-- /.box -->
	</div>
@endsection