@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
                </div>
                <section class="portfolio-flyer py-5" id="gallery">
                    <div class="container pt-lg-3 pb-md-5">
                        <div class="row">
                            <div class="col-lg-12 thankTitle">Nilai Anda Sudah Di Setup</div>
                            <div class="col-lg-12 thankOrder">
                                <!-- start data -->
                                <table class="table table-bordered">
                                    @if(!$raports->isEmpty())
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Siswa</th>
                                            <th>Nilai Pengetahuan</th>
                                            <th>Nilai Keterampilan</th>
                                            <th>Nilai Akhir</th>
                                            <th>Predikat</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($raports as $raport)
                                            <tr>
                                                <td>{{ $loop->index +1 }}</td>
                                                <td>{{ $raport->siswaItem->nama_lengkap }}</td>
                                                <td>{{ $raport->nilai_pengetahuan }}</td>
                                                <td>{{ $raport->nilai_keterampilan }}</td>
                                                <td>{{ $raport->nilai_akhir  }}</td>
                                                <td>{{ $raport->predikat }}</td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <div class="alert alert-danger">
                                            Data empty
                                        </div>
                                    @endif
                                </table>
                                <!-- end data -->

                                			<!-- start data -->
                <table class="table table-bordered">
					@if(!$raportEkstras->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama Ekstra</th>
							<th>Nilai</th>
							<th>Predikat</th>
						</tr>
						@foreach($raportEkstras as $raportEkstra)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $raportEkstra->nama_ekstrakurikuler }}</td>
								<td>{{ $raportEkstra->nilai }}</td>
								<td>{{ $raportEkstra->predikat  }}</td>
							</tr>
						@endforeach
                        <tr>
                            <td>
                                <a href="{{ route('raport.add-nilai') }}" class="btn btn-warning">
                                    Review Nilai Raport Anda
                                </a>
                            </td>
                            <td></td>
                            <td></td>
                        <td>
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
                <!-- end data -->
                            </div>
                        </div>
                    </div>
                </section>
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection
<?php 
    Session::forget('raport_ekstra_id');
?>