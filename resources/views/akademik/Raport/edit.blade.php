@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($raport, array(
                   'method' => 'PATCH',
                   'route' => array('raport.update', $raport->id))) 
                 }}
	              <div class="box-body">

				  <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Kelas</label>
                            @if(!$kelas->isEmpty())
                            <select name="kelas_id" class="form-control">
                                @foreach($kelas as $kelasx)
                                    <option value="{{ $kelasx->id }}">
                                        {{ $kelasx->nama_kelas }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
					</div>
					
                  <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Mata Pelajaran</label>
                            @if(!$mapels->isEmpty())
                            <select name="mata_pelajaran_id" class="form-control">
                                @foreach($mapels as $mapel)
                                    <option value="{{ $mapel->id }}">
                                        {{ $mapel->nama }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai Pengetahuan</label>
	                  		{!! Form::text('nilai_pengetahuan', old('nilai_pengetahuan'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai Akhir</label>
	                  		{!! Form::text('nilai_akhir', old('nilai_akhir'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Predikat Nilai</label>
	                  		{!! Form::text('predikat_nilai', old('predikat_nilai'), ['class' => 'form-control']) !!}
	                	</div>
                     </div>
                     
                     <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Tahun Akademik</label>
                            @if(!$tahun_akademiks->isEmpty())
                            <select name="tahun_akademik_id" class="form-control">
                                @foreach($tahun_akademiks as $tahun_akademik)
                                    <option value="{{ $tahun_akademik->id }}">
                                        {{ $tahun_akademik->nama }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection