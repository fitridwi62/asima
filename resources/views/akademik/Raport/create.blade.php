@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Raport</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Raport</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ Form::open(array('route' => 'raport.store')) }}
	              <div class="box-body">
                  <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="nama">Mata Pelajaran</label>
                            @if(!$mapels->isEmpty())
                            <select name="matkul_id" class="form-control">
                                @foreach($mapels as $mapel)
                                    <option value="{{ $mapel->id }}">
                                        {{ $mapel->nama }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Pengetahuan</label>
                            <input type="text" name="nilai_pengetahuan" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Keterampilan</label>
                            <input type="text" name="nilai_keterampilan" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai Akhir</label>
                            <input type="text" name="nilai_akhir" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Predikat Nilai</label>
                            <input type="text" name="predikat" placeholder=""  class="form-control">
                        </div>
                    </div>
                    
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-plus" aria-hidden="true"></i> </button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}

                <!-- start data -->
                <table class="table table-bordered">
					@if(!$raports->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Mata Pelajaran</th>
							<th>Nilai Pengetahuan</th>
							<th>Nilai Keterampilan</th>
							<th>Nilai Akhir</th>
							<th>Predikat</th>
						</tr>
						@foreach($raports as $raport)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $raport->siswaItem->nama_lengkap }}</td>
								<td>{{ $raport->mapel->nama }}</td>
								<td>{{ $raport->nilai_pengetahuan }}</td>
								<td>{{ $raport->nilai_keterampilan }}</td>
								<td>{{ $raport->nilai_akhir  }}</td>
								<td>{{ $raport->predikat }}</td>
							</tr>
						@endforeach
                        <tr>
                            <td>
                                <a href="{{ route('akademik.nilai.ekstra-create') }}" class="btn btn-warning">
                                    Lanjutkan Setup Nilai Ekstrakurikuler
                                </a>
                            </td>
                        <td>
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
                <!-- end data -->
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
<style>
.Ekstratitle {
    margin-bottom: 12px;
    font-weight: bold;
    border-bottom: 1px solid #ececec;
    text-transform: uppercase;
    padding: 5px 0px;
}
</style>
@endsection