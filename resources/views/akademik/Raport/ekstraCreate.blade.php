@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ekstrakurikuler</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ekstrakurikuler</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ Form::open(array('route' => 'raport.mapel')) }}
	              <div class="box-body">
				  <div class="col-md-3">
                        <div class="form-group">
                        <label for="nama">Siswa</label>
                            @if(!$siswas->isEmpty())
                            <select name="siswa_id" class="form-control">
                                @foreach($siswas as $siswa)
                                    <option value="{{ $siswa->id }}">
                                        {{ $siswa->nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            @else
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nama ekstrakurikuler</label>
                            <input type="text" name="nama_ekstrakurikuler" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Nilai</label>
                            <input type="text" name="nilai" placeholder=""  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama">Predikat</label>
                            <input type="text" name="predikat" placeholder=""  class="form-control">
                        </div>
                    </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-plus" aria-hidden="true"></i> </button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}					
				</div>
				<!-- /.box-body -->

				<!-- start data -->
                <table class="table table-bordered">
					@if(!$raportEkstras->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Nama Ekstra</th>
							<th>Nilai</th>
							<th>Predikat</th>
						</tr>
						@foreach($raportEkstras as $raportEkstra)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $raportEkstra->siswa->nama_lengkap }}</td>
								<td>{{ $raportEkstra->nama_ekstrakurikuler }}</td>
								<td>{{ $raportEkstra->nilai }}</td>
								<td>{{ $raportEkstra->predikat  }}</td>
							</tr>
						@endforeach
                        <tr>
                            <td>
                                <a href="{{ route('akademik.nilai.review-nilai') }}" class="btn btn-warning">
                                    Review Nilai Raport Anda
                                </a>
                            </td>
                        <td>
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
                <!-- end data -->
		</div>
		<!-- /.box -->
	</div>
<style>
.Ekstratitle {
    margin-bottom: 12px;
    font-weight: bold;
    border-bottom: 1px solid #ececec;
    text-transform: uppercase;
    padding: 5px 0px;
}
</style>
@endsection