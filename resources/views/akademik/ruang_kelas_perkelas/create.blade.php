@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ruang Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            {{ Form::open(array('route' => 'ruang-kelas-perkelas.store')) }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kelas</label>
                             @if(!$kelass->isEmpty())
                                <select name="kelas_id" class="form-control">
                                    @foreach($kelass as $kelas)
                                        <option value="{{ $kelas->id }}">
                                            {{ $kelas->nama_kelas }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama</label>
                            {!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection