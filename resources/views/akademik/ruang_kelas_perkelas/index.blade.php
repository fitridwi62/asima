@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ruang Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ruang Kelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('ruang-kelas-perkelas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.ruang-kelas-perkelas') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.ruang-kelas-perkelas', 'method' => 'get')) }}
					<div class="input-group">
                            @if(!$kelass->isEmpty())
                                <select name="kelas_id" class="form-control">
                                    @foreach($kelass as $kelas)
                                        <option value="{{ $kelas->id }}">
                                            {{ $kelas->nama_kelas }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$ruangKelass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kelas</th>
							<th>Nama</th>
							<th>Action</th>
						</tr>
						@foreach($ruangKelass as $ruangKelas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $ruangKelas->kelas->nama_kelas }}</td>
								<td>{{ $ruangKelas->nama }}</td>
								<td>
									<a href="{{ route('ruang-kelas-perkelas.edit', ['ruang-kelas-perkelas' => $ruangKelas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['ruang-kelas-perkelas.destroy', $ruangKelas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $ruangKelass->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection