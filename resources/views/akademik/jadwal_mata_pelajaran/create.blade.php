@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jadwal Mata pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            {{ Form::open(array('route' => 'jadwal-mata-pelajaran.store')) }}
	              <div class="box-body">
	                <div class="col-md-4">
		                	<div class="form-group">
			                  <label for="nama">Kelas</label>
			                  <select name="kelas_id" class="form-control">
			                  @if(!$kelass->isEmpty())
			                  	 @foreach($kelass as $kelas)
			                  	 	<option value="{{$kelas->id}}">
			                  	 		{{ $kelas->nama_kelas }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <select name="mata_pelajaran_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajaran)
                                <option value="{{$mataPelajaran->id}}">
                                    {{ $mataPelajaran->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Siswa</label>
                            <select name="siswa_id" class="form-control">
                            @if(!$siswa->isEmpty())
                                @foreach($siswa as $siswax)
                                <option value="{{ $siswax->id }}">
                                    {{ $siswax->nama_lengkap }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama">Guru</label>
                            <select name="data_guru_id" class="form-control">
                            @if(!$dataGuru->isEmpty())
                                @foreach($dataGuru as $guru)
                                <option value="{{$guru->id}}">
                                    {{ $guru->nama_guru }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            {!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Mulai</label>
                            {!! Form::text('jam_mulai', old('jam_mulai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jam Selesai</label>
                            {!! Form::text('jam_selesai', old('jam_selesai'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Durasi</label>
                            {!! Form::text('durasi', old('durasi'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
	              <!-- /.box-body -->

	              <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection