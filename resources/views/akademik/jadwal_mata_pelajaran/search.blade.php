@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jadwal Mata Pelajaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jadwal Mata Pelajaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('jadwal-mata-pelajaran.create') }}" class="btn btn-sm btn-primary">
						Buat Jadwal Mata Pelajaran
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'jadwal-matkul.search-jadwal-matkul', 'method' => 'get')) }}
					<div class="input-group">
						<select name="mata_pelajaran_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajaran)
                                <option value="{{$mataPelajaran->id}}">
                                    {{ $mataPelajaran->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                        </select>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$jadwalMataPelajarans->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kelas</th>
							<th>Mata Pelajaran</th>
							<th>Guru</th>
							<th>Tanggal</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
							<th>Durasi</th>
							<th>Action</th>
						</tr>
						@foreach($jadwalMataPelajarans as $jadwalMataPelajaran)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $jadwalMataPelajaran->kelas->nama_kelas }}</td>
								<td>{{ $jadwalMataPelajaran->mata_pelajaran->nama }}</td>
								<td>{{ $jadwalMataPelajaran->guru->nama_guru }}</td>
								<td>{{ $jadwalMataPelajaran->tanggal }}</td>
								<td>{{ $jadwalMataPelajaran->jam_mulai }}</td>
								<td>{{ $jadwalMataPelajaran->jam_selesai }}</td>
								<td>{{ $jadwalMataPelajaran->durasi }} jam</td>
								<td>
									<a href="{{ route('jadwal-mata-pelajaran.edit', ['jadwal-mata-pelajaran' => $jadwalMataPelajaran->id]) }}" class="btn btn-sm btn-success">Edit</a>
									{!! Form::open(['route' => ['jadwal-mata-pelajaran.destroy', $jadwalMataPelajaran->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $jadwalMataPelajarans->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection