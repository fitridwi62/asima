<html>
<head>
	<title>Data Jadwal Mata Pelajaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Jadwal Mata Pelajaran</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th>Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Guru</th>
                <th>Tanggal</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
                <th>Durasi</th>
			</tr>
		</thead>
		<tbody>
            @foreach($jadwalMataPelajarans as $jadwalMataPelajaran)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $jadwalMataPelajaran->kelas->nama_kelas }}</td>
                <td>{{ $jadwalMataPelajaran->mata_pelajaran->nama }}</td>
                <td>{{ $jadwalMataPelajaran->guru->nama_guru }}</td>
                <td>{{ $jadwalMataPelajaran->tanggal }}</td>
                <td>{{ $jadwalMataPelajaran->jam_mulai }}</td>
                <td>{{ $jadwalMataPelajaran->jam_selesai }}</td>
                <td>{{ $jadwalMataPelajaran->durasi }} jam</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>