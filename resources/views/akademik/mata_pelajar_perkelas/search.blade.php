@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata Pelajaran Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Mata Pelajaran Perkelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('mata-pelajar-perkelas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.matkul-perkelas') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'akademik.filter.matkul-perkelas', 'method' => 'get')) }}
					<div class="input-group">
                        <select name="mata_pelajar_id" class="form-control">
                            @if(!$mataPelajarans->isEmpty())
                                @foreach($mataPelajarans as $mataPelajar)
                                <option value="{{$mataPelajar->id}}">
                                    {{ $mataPelajar->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                        </select>
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$mataPelajarPerkelass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kelas</th>
							<th>Mata Pelajaran</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($mataPelajarPerkelass as $mataPelajarPerkelas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $mataPelajarPerkelas->kelas->nama_kelas }}</td>
								<td>{{ $mataPelajarPerkelas->mata_pelajar->nama }}</td>
								<td>{{ $mataPelajarPerkelas->status }}</td>
								<td>
									<a href="{{ route('mata-pelajar-perkelas.edit', ['mata-pelajar-perkelas' => $mataPelajarPerkelas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['mata-pelajar-perkelas.destroy', $mataPelajarPerkelas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $mataPelajarPerkelass->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection