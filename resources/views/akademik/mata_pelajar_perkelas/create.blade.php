@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mata pelajaran Perkelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            {{ Form::open(array('route' => 'mata-pelajar-perkelas.store')) }}
	              <div class="box-body">
	                <div class="col-md-6">
		                	<div class="form-group">
			                  <label for="nama">Kelas</label>
			                  <select name="kelas_id" class="form-control">
			                  @if(!$kelass->isEmpty())
			                  	 @foreach($kelass as $kelas)
			                  	 	<option value="{{$kelas->id}}">
			                  	 		{{ $kelas->nama_kelas }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Mata Pelajaran</label>
                            <select name="mata_pelajar_id" class="form-control">
                            @if(!$mataPelajars->isEmpty())
                                @foreach($mataPelajars as $mataPelajar)
                                <option value="{{$mataPelajar->id}}">
                                    {{ $mataPelajar->nama }}
                                </option>
                                @endforeach
                            @else
                            <div class="alert alert-danger">
                                Data empty
                            </div>
                            @endif
                            </select>
                        </div>
	              </div>
                  <div class="col-md-12">
                        <div class="form-group">
                        <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>
                    </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Simpan</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection