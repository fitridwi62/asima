@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Grid Nilai</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Grid Nilai</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('grid-nilai.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.grid-nilai') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$gridNilais->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Kategori Nilai</th>
							<th>Action</th>
						</tr>
						@foreach($gridNilais as $gridNilai)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $gridNilai->nilai }}</td>
								<td>{{ $gridNilai->categori_nilai }}</td>
								<td>
									<a href="{{ route('grid-nilai.edit', ['grid-nilai' => $gridNilai->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['grid-nilai.destroy', $gridNilai->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $gridNilais->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection