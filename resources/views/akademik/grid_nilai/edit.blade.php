@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Grid Nilai</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Grid Nilai</li>
				  </ol>
				</nav>

				 <!-- form start -->
	             {{ 
                   Form::model($gridNilai, array(
                   'method' => 'PATCH',
                   'route' => array('grid-nilai.update', $gridNilai->id))) 
                 }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai</label>
	                  		{!! Form::text('nilai', old('nilai'), ['class' => 'form-control']) !!}
	                	</div>
                     </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kategori Nilai</label>
	                  		<select name="categori_nilai" class="form-control">
                                <option value="A">A</option>     
                                <option value="B">B</option>     
                                <option value="C">C</option>     
                                <option value="D">D</option>     
                            </select>
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection