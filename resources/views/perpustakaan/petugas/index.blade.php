@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Petugas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Petugas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('petugas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-petugas') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'perpustakaan.filter.petugas', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama_lengkap" placeholder="Masukkan Data Petugas" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$petugass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Jenis Kelamin</th>
							<th>WA</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($petugass as $petugas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $petugas->nama_lengkap }}</td>
								<td>{{ $petugas->email }}</td>
								<td>{{ $petugas->phone }}</td>
								<td>{{ $petugas->jenis_kelamin }}</td>
								<td>{{ $petugas->wa }}</td>
								<td>{{ $petugas->status }}</td>
								<td>
									<a href="{{ route('petugas.edit', ['petugas' => $petugas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['petugas.destroy', $petugas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $petugass->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection