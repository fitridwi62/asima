@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pengembalian Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pengembalian Buku</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pengembalian-buku.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-pengembalian-buku') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'perpustakaan.filter.pengembalian-buku', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$peminjamanBukus->isEmpty())
                                <select name="pinjamanBuku_id" class="form-control">
                                    @foreach($peminjamanBukus as $peminjamanBuku)
                                        <option value="{{ $peminjamanBuku->id }}">
                                            {{ $peminjamanBuku->kode }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                    	@endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$pengembalians->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Peminjaman Buku</th>
							<th>Tanggal</th>
							<th>Jumlah</th>
							<th>Action</th>
						</tr>
						@foreach($pengembalians as $pengembalian)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pengembalian->kode }}</td>
								<td>{{ $pengembalian->peminjan_buku->kode }}</td>
								<td>{{ $pengembalian->tanggal }}</td>
								<td>{{ $pengembalian->status }}</td>
								<td>
									<a href="{{ route('pengembalian-buku.edit', ['pengembalian-buku' => $pengembalian->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['pengembalian-buku.destroy', $pengembalian->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $pengembalians->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection