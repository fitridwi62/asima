<html>
<head>
	<title>Laporan Data Pengembalian Buku</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Pengembalian Buku</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Kode</th>
                <th>Peminjaman Buku</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
			</tr>
		</thead>
		<tbody>
        @foreach($pengembalians as $pengembalian)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $pengembalian->kode }}</td>
                <td>{{ $pengembalian->peminjan_buku->kode }}</td>
                <td>{{ $pengembalian->tanggal }}</td>
                <td>{{ $pengembalian->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>