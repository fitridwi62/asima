@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Peminjaman Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Peminjaman Buku</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'pengembalian-buku.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode</label>
	                  		{!! Form::text('kode', old('kode'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Peminjam buku</label>
                             @if(!$peminjamanBukus->isEmpty())
                                <select name="pinjamanBuku_id" class="form-control">
                                    @foreach($peminjamanBukus as $peminjamanBuku)
                                        <option value="{{ $peminjamanBuku->id }}">
                                            {{ $peminjamanBuku->kode }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="sudah-dikembalikan">Sudah Dikembalikan</option>
                                <option value="belum-dikembalikan">Belum Dikembalikan</option>
                            </select>
	                	</div>
	                 </div>
                     
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection