<html>
<head>
	<title>Laporan Data Peminjaman Buku</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Peminjaman Buku</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Kode</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Pengembalian</th>
                <th>Jumlah</th>
                <th>Buku</th>
			</tr>
		</thead>
		<tbody>
        @foreach($peminjamanBukus as $peminjamanBuku)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $peminjamanBuku->kode }}</td>
                <td>{{ $peminjamanBuku->tanggal_peminjam }}</td>
                <td>{{ $peminjamanBuku->tanggal_kembalian }}</td>
                <td>{{ $peminjamanBuku->jumlah_buku }}</td>
                <td>{{ $peminjamanBuku->buku->nama_buku }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>