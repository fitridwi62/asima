@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Peminjaman Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Peminjaman Buku</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'peminjaman-buku.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode</label>
	                  		{!! Form::text('kode', old('kode'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal Pinjam</label>
	                  		{!! Form::date('tanggal_peminjam', old('tanggal_peminjam'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal Pengembalian</label>
	                  		{!! Form::date('tanggal_kembalian', old('tanggal_kembalian'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jumlah Buku</label>
	                  		{!! Form::text('jumlah_buku', old('jumlah_buku'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Buku</label>
                             @if(!$bukus->isEmpty())
                                <select name="buku_id" class="form-control">
                                    @foreach($bukus as $buku)
                                        <option value="{{ $buku->id }}">
                                            {{ $buku->kode }} - {{ $buku->nama_buku }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection