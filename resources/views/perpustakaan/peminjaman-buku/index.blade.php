@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Peminjam Buku</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Peminjam Buku</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('peminjaman-buku.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-peminjaman-buku') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'perpustakaan.filter.peminjaman-buku', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$bukus->isEmpty())
                                <select name="buku_id" class="form-control">
                                    @foreach($bukus as $buku)
                                        <option value="{{ $buku->id }}">
                                            {{ $buku->kode }} - {{ $buku->nama_buku }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$peminjamanBukus->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Tanggal Pinjam</th>
							<th>Tanggal Pengembalian</th>
							<th>Jumlah</th>
							<th>Buku</th>
							<th>Action</th>
						</tr>
						@foreach($peminjamanBukus as $peminjamanBuku)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $peminjamanBuku->kode }}</td>
								<td>{{ $peminjamanBuku->tanggal_peminjam }}</td>
								<td>{{ $peminjamanBuku->tanggal_kembalian }}</td>
								<td>{{ $peminjamanBuku->jumlah_buku }}</td>
								<td>{{ $peminjamanBuku->buku->nama_buku }}</td>
								<td>
									<a href="{{ route('peminjaman-buku.edit', ['pinjaman-buku' => $peminjamanBuku->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['peminjaman-buku.destroy', $peminjamanBuku->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $peminjamanBukus->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection