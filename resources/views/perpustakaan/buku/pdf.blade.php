<html>
<head>
	<title>Laporan Data Buku Perpustakaan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Kehadiran Buku Perpustakaan</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Kode</th>
                <th>Nama Buku</th>
                <th>Pengarang</th>
                <th>Tahun Terbit</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach($dataBukus as $dataBuku)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $dataBuku->kode }}</td>
                <td>{{ $dataBuku->nama_buku }}</td>
                <td>{{ $dataBuku->pengarang_buku }}</td>
                <td>{{ $dataBuku->tahun_terbit }}</td>
                <td>{{ $dataBuku->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>