@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Role Permission</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Role Permission</li>
				  </ol>
				</nav>
				<table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <td>:</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>:</td>
                            <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <td>:</td>
                            <td>
                                @foreach ($roles as $row)
                                <input type="radio" name="role" 
                                    {{ $user->hasRole($row) ? 'checked':'' }}
                                    value="{{ $row }}"> {{  $row }} <br>
                                @endforeach
                            </td>
                        </tr>
                    </thead>
                </table>
				
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div> -->
		</div>
		<!-- /.box -->
	</div>
@endsection