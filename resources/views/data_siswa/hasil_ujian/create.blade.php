@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hasil Ujian</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Hasil Ujian</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'hasil-ujian.store')) }}
	              <div class="box-body">
                    <div class="col-md-12">
                            <div class="form-group">
                            <label for="nama">Daftar Siswa</label>
                             @if(!$siswas->isEmpty())
                                <select name="daftar_siswa_id" class="form-control">
                                    @foreach($siswas as $daftar_siswa)
                                        <option value="{{ $daftar_siswa->id }}">
                                            {{ $daftar_siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                                <div class="dataempty">
                                    data empty
                                </div>
                             @endif
                            </div>
                        </div>
	                 <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Pengetahuan Umum</label>
	                  		{!! Form::text('pengetahuan_umum', old('pengetahuan_umum'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Minat Bakat</label>
	                  		{!! Form::text('minat_bakat', old('minat_bakat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Akademik</label>
	                  		{!! Form::text('akademik', old('akademik'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

					 <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
							  	<option value="diterima">Diterima</option>
							  	<option value="gagal">Gagal</option>
							</select>
	                	</div>
	                 </div>
                     
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection