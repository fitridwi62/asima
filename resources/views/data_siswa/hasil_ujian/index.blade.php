@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hasil Ujian</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Hasil Ujian</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('hasil-ujian.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$hasilUjians->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pengetahuan Umun</th>
							<th>Minat Bakat</th>
							<th>Akademik</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($hasilUjians as $hasilUjian)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $hasilUjian->siswa->nama_lengkap }}</td>
								<td>{{ $hasilUjian->pengetahuan_umum }}</td>
								<td>{{ $hasilUjian->minat_bakat }}</td>
								<td>{{ $hasilUjian->akademik }}</td>
								<td>{{ $hasilUjian->status }}</td>
								<td>
									<a href="{{ route('hasil-ujian.edit', ['hasil-ujian' => $hasilUjian->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['hasil-ujian.destroy', $hasilUjian->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $hasilUjians->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection