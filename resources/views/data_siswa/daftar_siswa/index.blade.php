@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Daftar Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Daftar Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('daftar-siswa.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$daftarSiswas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nomor Daftar</th>
							<th>Nama Lengkap</th>
							<th>Jenis Kelamin</th>
							<th>Jenjang</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($daftarSiswas as $daftarSiswa)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $daftarSiswa->no_pendaftaran }}</td>
								<td>{{ $daftarSiswa->nama_lengkap }}</td>
								<td>{{ $daftarSiswa->jenis_kelamin }}</td>
								<td>{{ $daftarSiswa->jenjang->nama_jenjang }}</td>
								<td>{{ $daftarSiswa->status }}</td>
								<td>
									<a href="{{ route('daftar-siswa.show', ['daftar-siswa' => $daftarSiswa->id]) }}" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="{{ route('daftar-siswa.edit', ['daftar-siswa' => $daftarSiswa->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['daftar-siswa.destroy', $daftarSiswa->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $daftarSiswas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection