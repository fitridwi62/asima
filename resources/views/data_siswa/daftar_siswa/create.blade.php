@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pendaftaran Siswa Baru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ Form::open(array('route' => 'daftar-siswa.store', 'files' => true, 'enctype' => 'multipart/form-data')) }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">No Pendaftaran</label>
                            {!! Form::text('no_pendaftaran', old('no_pendaftaran'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            {!! Form::text('nama_lengkap', old('nama_lengkap'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Jenis Kelamin</label>
                            {!! Form::text('jenis_kelamin', old('jenis_kelamin'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Tempat Lahir</label>
                            {!! Form::text('tempat_lahir', old('tempat_lahir'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Tanggal Lahir</label>
                            {!! Form::date('tanggal_lahir', old('tanggal_lahir'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="nama">Alamat</label>
                            {!! Form::text('alamat', old('alamat'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor Telp</label>
                            {!! Form::text('telp', old('telp'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jenjang</label>
                             @if(!$jenjangs->isEmpty())
                                <select name="jenjang_id" class="form-control">
                                    @foreach($jenjangs as $jenjang)
                                        <option value="{{ $jenjang->id }}">
                                            {{ $jenjang->nama_jenjang }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                                <div class="dataempty">
                                    data empty
                                </div>
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Email</label>
                            {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor WA</label>
                            {!! Form::text('no_wa', old('no_wa'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jurusan</label>
                             @if(!$jurusans->isEmpty())
                                <select name="jurusan_id" class="form-control">
                                    @foreach($jurusans as $jurusan)
                                        <option value="{{ $jurusan->id }}">
                                            {{ $jurusan->nama_jurusan }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                                <div class="dataempty">
                                    data empty
                                </div>
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nama Orang Tua</label>
                            {!! Form::text('nama_orang_tua', old('nama_orang_tua'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Pekerjaan Orang Tua</label>
                            {!! Form::text('pekerjaan_orang_tua', old('pekerjaan_orang_tua'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Nomor Telp Orang Tua</label>
                            {!! Form::text('no_telp_ortu', old('no_telp_ortu'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Penghasilan Orang Tua</label>
                            {!! Form::text('penghasilan_ortu', old('penghasilan_ortu'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Photo</label>
                            {!! Form::file('photo', old('photo'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection