@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Detail Daftar Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Detail Daftar Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('daftar-siswa.index') }}" class="btn btn-sm btn-primary">
						Siswa Baru
					</a>
				</div>
				<table class="table table-bordered">
						<tr>
                            <td style="font-weight:bold;">Nomor Pendaftaran</td>
                            <td>{{ $daftarSiswa->no_pendaftaran }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Lengkap</td>
                            <td>{{ $daftarSiswa->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jenis Kelamin</td>
                            <td>{{ $daftarSiswa->jenis_kelamin }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Tempat Lahir</td>
                            <td>{{ $daftarSiswa->tempat_lahir }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Tanggal Lahir</td>
                            <td>{{ $daftarSiswa->tanggal_lahir }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Alamat</td>
                            <td>{{ $daftarSiswa->alamat }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nomor Telp</td>
                            <td>{{ $daftarSiswa->telp }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jenjang</td>
                            <td>{{ $daftarSiswa->jenjang->nama_jenjang }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Email</td>
                            <td>{{ $daftarSiswa->email }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nomor WA</td>
                            <td>{{ $daftarSiswa->no_wa }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jurusan</td>
                            <td>{{ $daftarSiswa->jurusan->nama_jurusan }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nama Orang Tua</td>
                            <td>{{ $daftarSiswa->nama_orang_tua }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Pekerjaan Orang Tua</td>
                            <td>{{ $daftarSiswa->pekerjaan_orang_tua }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nomor Telp Orang Tua</td>
                            <td>{{ $daftarSiswa->no_telp_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Penghasilan Orang Tua</td>
                            <td>{{ $daftarSiswa->penghasilan_ortu }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Photo</td>
                            <td>
                            <img src="{{ asset('psb/'. $daftarSiswa->photo) }}" alt="news image" class="img-fluid" style="width:100px; heigt: 100px; border-radius: 5px;">
                            </td>
                        </tr>
				</table>
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div> -->
		</div>
		<!-- /.box -->
	</div>
@endsection