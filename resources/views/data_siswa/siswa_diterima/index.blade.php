@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Siswa Diterima</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Siswa diterima</li>
				  </ol>
				</nav>
				<table class="table table-bordered">
					@if(!$siswaDiterimas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Pengetahuan Umun</th>
							<th>Minat Bakat</th>
							<th>Akademik</th>
							<th>Status</th>
						</tr>
						@foreach($siswaDiterimas as $siswaDiterima)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $siswaDiterima->siswa->nama_lengkap }}</td>
								<td>{{ $siswaDiterima->pengetahuan_umum }}</td>
								<td>{{ $siswaDiterima->minat_bakat }}</td>
								<td>{{ $siswaDiterima->akademik }}</td>
								<td>{{ $siswaDiterima->status }}</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $siswaDiterimas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection