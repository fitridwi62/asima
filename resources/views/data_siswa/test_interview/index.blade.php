@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Test Interview</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Test Interview</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('test-interview.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$testInterviews->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>Nilai Terbaik</th>
							<th>Prestasi</th>
							<th>Hoby</th>
							<th>Jarak Rumah</th>
							<th>Action</th>
						</tr>
						@foreach($testInterviews as $test_interview)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $test_interview->daftarSiswa->nama_lengkap }}</td>
								<td>{{ $test_interview->nilai_terbaik }}</td>
								<td>{{ $test_interview->prestasi }}</td>
								<td>{{ $test_interview->hoby }}</td>
								<td>{{ $test_interview->jarak_rumah }}</td>
								<td>
									<a href="{{ route('test-interview.show', ['test-interview' => $test_interview->id]) }}" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="{{ route('test-interview.edit', ['test-interview' => $test_interview->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['test-interview.destroy', $test_interview->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $testInterviews->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection