@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Test Interview</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Test Interview</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($testInterview, array(
                   'method' => 'PATCH',
                   'route' => array('test-interview.update', $testInterview->id))) 
                }}
	              <div class="box-body">
                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Daftar Siswa</label>
                             @if(!$daftar_siswas->isEmpty())
                                <select name="daftar_siswa_id" class="form-control">
                                    @foreach($daftar_siswas as $daftar_siswa)
                                        <option value="{{ $daftar_siswa->id }}">
                                            {{ $daftar_siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                                <div class="dataempty">
                                    data empty
                                </div>
                             @endif
                            </div>
                        </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nilai Terbaik</label>
	                  		{!! Form::text('nilai_terbaik', old('nilai_terbaik'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Prestasi</label>
	                  		{!! Form::text('prestasi', old('prestasi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Hoby</label>
	                  		{!! Form::text('hoby', old('hoby'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Perbuatan Apa Yang Paling Terburuk Ketika Sekolah</label>
	                  		{!! Form::text('aktivitas', old('aktivitas'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jarak Rumah Ke Sekolah</label>
	                  		{!! Form::text('jarak_rumah', old('jarak_rumah'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pendapatan Orang Tua</label>
	                  		{!! Form::text('pendapatan_orang_tua', old('pendapatan_orang_tua'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pekerjaan Orang Tua</label>
	                  		{!! Form::text('pekerjaan_orang_tua', old('pekerjaan_orang_tua'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Apabila Diterima Disini Apa Yang Anda Lakukan ? </label>
	                  		{!! Form::text('note', old('note'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection