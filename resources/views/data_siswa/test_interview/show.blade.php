@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Detail Siswa Interview</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Detail Siswa Interview</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('test-interview.index') }}" class="btn btn-sm btn-primary">
						Siswa Baru
					</a>
				</div>
				<table class="table table-bordered">
						<tr>
                            <td style="font-weight:bold;">Nama Siswa</td>
                            <td>
                                {{ $testInterview->daftarSiswa->nama_lengkap }}
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Nilai Terabaik</td>
                            <td>{{ $testInterview->nilai_terbaik }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Prestasi</td>
                            <td>{{ $testInterview->prestasi }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Hoby</td>
                            <td>{{ $testInterview->hoby }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Perbuatan Paling Buruk</td>
                            <td>{{ $testInterview->aktivitas }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Jarak Rumah</td>
                            <td>{{ $testInterview->jarak_rumah }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Pendapatan Orang Tua</td>
                            <td>{{ $testInterview->pendapatan_orang_tua }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Pekerjaan Orang Tua</td>
                            <td>{{ $testInterview->pekerjaan_orang_tua }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold;">Note</td>
                            <td>{{ $testInterview->note }}</td>
                        </tr>
				</table>
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div> -->
		</div>
		<!-- /.box -->
	</div>
@endsection