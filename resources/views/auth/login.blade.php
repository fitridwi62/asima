@extends('layouts.app')

@section('content')
<div class="container register">
    <div class="row">
        <div class="col-md-3 register-left">
            <img src="{{ asset('images/login-logo.png') }}" class="img-logo" alt="asima">
            <p>Aplikasi Sistem Informasi Manajemen dan Keuangan Sekolah</p>
            <div class="registerContent">
                <ul>
                    <li class="colList">
                        <span class="registerIcon">
                            <img src="{{ asset('images/icon-studen.png') }}" alt="">
                        </span>
                        <a href="{{ route('daftar.siswa-baru') }}">
                            <span class="registerTitle">Pendaftaran </span> 
                            <span class="registerSubtitle">Siswa Baru</span>
                        </a>
                    </li>
                    <li class="colList">
                        <span class="registerIcon">
                                <img src="{{ asset('images/icon-2.png') }}" alt="">
                            </span>
                        <a href="{{ route('pendaftaran.guru.baru') }}">
                        <span class="registerTitle">Pendaftaran </span>
                         <span class="registerSubtitle">Guru Baru</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
            <div class="col-md-9 register-right">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h3 class="register-heading">Login</h3>
                        <div class="row register-form">
                            <div class="col-md-12">
                                <form method="POST" action="{{ route('login') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                    <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Login') }}
                                                </button>

                                                @if (Route::has('password.request'))
                                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
