@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
				  </ol>
				</nav>

				 <!-- form start -->
	             {{ 
                   Form::model($pembayaran , array(
                   'method' => 'PATCH',
                   'route' => array('pembayaran.update', $pembayaran->id))) 
                }}
	              <div class="box-body">
                    <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             @if(!$siswas->isEmpty())
                                <select name="siswa_id" class="form-control">
                                    @foreach($siswas as $siswa)
                                        <option value="{{ $siswa->id }}">
                                            {{ $siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">SPP</label>
                             @if(!$pembayaranSpps->isEmpty())
                                <select name="pembayaranspp_id" class="form-control">
                                    @foreach($pembayaranSpps as $pembayaranSpp)
                                        <option value="{{ $pembayaranSpp->id }}">
                                            {{ $pembayaranSpp->biaya_spp }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Semester</label>
                             @if(!$pembayaranSemesters->isEmpty())
                                <select name="pembayaransemester_id" class="form-control">
                                    @foreach($pembayaranSemesters as $pembayaranSemester)
                                        <option value="{{ $pembayaranSemester->id }}">
                                            {{ $pembayaranSemester->biaya }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Tanggal</label>
                                {!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Status</label>
                                <select name="status" class="form-control">
                                    <option>Pilih Status Pembayaran</option>
                                    <option value="lunas">Lunas</option>
                                    <option value="belum-lunas">Belum lunas</option>
                                </select>
                            </div>
                        </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection