@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jenis Pembayaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pembayaran.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pembayaran') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$pembayarans->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Siswa</th>
							<th>SPP</th>
							<th>Semester</th>
							<th>Total</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($pembayarans as $pembayaran)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pembayaran->siswa->nama_lengkap }}</td>
								<td>{{ $pembayaran->pembayaranSPP->biaya_spp }}</td>
								<td>{{ $pembayaran->pembayaranSemester->biaya }}</td>
								<td>{{ $pembayaran->pembayaranSemester->biaya + $pembayaran->pembayaranSPP->biaya_spp  }}</td>
								<td>{{ $pembayaran->status }}</td>
								<td>
									<a href="{{ route('pembayaran.edit', ['pembayaran' => $pembayaran->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['pembayaran.destroy', $pembayaran->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $pembayarans->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection