@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran SPP</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
                 {{ 
                   Form::model($pembayaranSpp, array(
                   'method' => 'PATCH',
                   'route' => array('pembayaran-spp.update', $pembayaranSpp->id))) 
                 }}
	              <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jenis Pembayaran</label>
                             @if(!$jenisPembayaran->isEmpty())
                                <select name="jenis_pembayaran_id" class="form-control">
                                    @foreach($jenisPembayaran as $jenisPembayaranx)
                                        <option value="{{ $jenisPembayaranx->id }}">
                                            {{ $jenisPembayaranx->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya SPP</label>
                            {!! Form::text('biaya_spp', old('biaya_spp'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya Ekstrakuriler</label>
                            {!! Form::text('pembayaran_extra', old('pembayaran_extra'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Biaya Bimbingan Belajar</label>
                            {!! Form::text('pembayaran_bimbingan_belajar', old('pembayaran_bimbingan_belajar'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tanggal</label>
                            {!! Form::date('tanggal_bayar', old('tanggal_bayar'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Status</label>
                            <select name="status" class="form-control">
                                <option>Pilih Status Pembayaran</option>
                                <option value="lunas">Lunas</option>
                                <option value="belum-lunas">Belum Lunas</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save Data</button>
                        </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection