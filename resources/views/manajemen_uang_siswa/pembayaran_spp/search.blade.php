@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran SPP</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran SPP</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pembayaran-spp.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pembayaran-spp') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'keuangan-siswa.filter.pembayaran-spp', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$jenisPembayaran->isEmpty())
							<select name="jenis_pembayaran_id" class="form-control">
								@foreach($jenisPembayaran as $jenisPembayaranx)
									<option value="{{ $jenisPembayaranx->id }}">
										{{ $jenisPembayaranx->nama }}
									</option>
								@endforeach
							</select>
							@else
						@endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$pembayaranSpps->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Jenis Pembayaran</th>
							<th>Biaya SPP</th>
							<th>Biaya Ekstra</th>
							<th>Biaya Bimbingan Belajar</th>
							<th>Tanggal</th>
							<th>Total</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($pembayaranSpps as $pembayaranSpp)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pembayaranSpp->jenis_pembayaran->nama }}</td>
								<td>{{ $pembayaranSpp->biaya_spp }}</td>
								<td>{{ $pembayaranSpp->pembayaran_extra }}</td>
								<td>{{ $pembayaranSpp->pembayaran_bimbingan_belajar }}</td>
								<td>{{ $pembayaranSpp->tanggal_bayar }}</td>
								<td>
									{{ $pembayaranSpp->biaya_spp + $pembayaranSpp->pembayaran_extra + $pembayaranSpp->pembayaran_bimbingan_belajar }}
								</td>
								<td>{{ $pembayaranSpp->status }}</td>
								<td>
									<a href="{{ route('pembayaran-spp.edit', ['pembayaran-spp' => $pembayaranSpp->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['pembayaran-spp.destroy', $pembayaranSpp->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $pembayaranSpps->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection