<html>
<head>
	<title>Laporan Pembayaran SPP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Pembayaran SPP</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Jenis Pembayaran</th>
                <th>Biaya SPP</th>
                <th>Biaya Ekstra</th>
                <th>Biaya Bimbingan Belajar</th>
                <th>Tanggal</th>
                <th>Total</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
          @foreach($pembayaranSpps as $pembayaranSpp)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $pembayaranSpp->jenis_pembayaran->nama }}</td>
                <td>{{ $pembayaranSpp->biaya_spp }}</td>
                <td>{{ $pembayaranSpp->pembayaran_extra }}</td>
                <td>{{ $pembayaranSpp->pembayaran_bimbingan_belajar }}</td>
                <td>{{ $pembayaranSpp->tanggal_bayar }}</td>
                <td>
                    {{ $pembayaranSpp->biaya_spp + $pembayaranSpp->pembayaran_extra + $pembayaranSpp->pembayaran_bimbingan_belajar }}
                </td>
                <td>{{ $pembayaranSpp->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>