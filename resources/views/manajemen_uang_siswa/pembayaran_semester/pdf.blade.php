<html>
<head>
	<title>Laporan Pembayaran Semester</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Pembayaran Semester</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Nama</th>
				<th>Biaya</th>
			</tr>
		</thead>
		<tbody>
            @foreach($pembayaranSemesters as $pembayaranSemester)
			<tr>
                <td>{{ $loop->index +1 }}</td>
				<td>{{ $pembayaranSemester->nama }}</td>
				<td>{{ $pembayaranSemester->biaya }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>