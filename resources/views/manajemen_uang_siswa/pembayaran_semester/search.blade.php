@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pembayaran Semester</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Pembayaran Semester</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('pembayaran-semester.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pembayaran-semester') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'keuangan-siswa.filter.pembayaran-semester', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama" placeholder="Masukkan Pembayaran Semester" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$pembayaranSemesters->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Biaya</th>
							<th>Action</th>
						</tr>
						@foreach($pembayaranSemesters as $pembayaranSemester)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $pembayaranSemester->nama }}</td>
								<td>{{ $pembayaranSemester->biaya }}</td>
								<td>
									<a href="{{ route('pembayaran-semester.edit', ['jenis-pembayaran' => $pembayaranSemester->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['pembayaran-semester.destroy', $pembayaranSemester->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $pembayaranSemesters->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection