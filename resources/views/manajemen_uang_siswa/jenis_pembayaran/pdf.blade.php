<html>
<head>
	<title>Laporan Jenis Pembayaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Jenis Pembayaran</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
			    <th>Nama</th>
			</tr>
		</thead>
		<tbody>
            @foreach($jenisPembayarans as $jenisPembayaran)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $jenisPembayaran->nama }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>