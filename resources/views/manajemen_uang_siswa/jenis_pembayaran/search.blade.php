@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jenis Pembayaran</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jenis Pembayaran</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('jenis-pembayaran.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.jenis-pembayaran') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'keuangan-siswa.filter.jenis-pembayaran', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama" placeholder="Masukkan Data Alumni" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$jenisPembayarans->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Action</th>
						</tr>
						@foreach($jenisPembayarans as $jenisPembayaran)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $jenisPembayaran->nama }}</td>
								<td>
									<a href="{{ route('jenis-pembayaran.edit', ['jenis-pembayaran' => $jenisPembayaran->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['jenis-pembayaran.destroy', $jenisPembayaran->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $jenisPembayarans->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection