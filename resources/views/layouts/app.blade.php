<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ASIMA</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('form-login/asset/css/custome.css') }}" rel="stylesheet">
    <style>
    .form-group.has-error .help-block {
        color: #dd4b39;
        font-size: 10px;
        font-family: inherit;
    }
    li.colList {
        background: #fff;
        margin-top: 10px;
        padding: 5px 0px;
        border-radius: 6px;
    }
    .registerContent ul{
        list-style:none;
        margin:0;
        padding: 0;
    }
    .registerContent ul li a{
        text-decoration: none;
    }

    span.registerTitle {
        display: block;
        font-size: 16px;
        color: #6b6565;
        font-family: inherit;
    }
    span.registerSubtitle {
        font-size: 14px;
        color: #989494;
    }
  </style>
</head>
<body>
    @yield('content')
</body>
</html>
