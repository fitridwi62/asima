@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Asset</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Assset</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($asset, array(
                   'method' => 'PATCH',
                   'route' => array('asset.update', $asset->id))) 
                }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Asset</label>
	                  		{!! Form::text('nama_asset', old('nama_asset'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No.Asset</label>
	                  		{!! Form::text('no_asset', old('no_asset'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Asset Tetap</label>
	                  		{!! Form::text('asset_tetap', old('asset_tetap'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Biaya Akuisisi</label>
	                  		{!! Form::text('biaya_akuisisi', old('biaya_akuisisi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::text('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kredit Dibebankan Ke</label>
	                  		{!! Form::text('kredit_to', old('kredit_to'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection