@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Barang Keluar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Barang Keluar</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('barang-keluar.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.barang-keluar') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'inventori.filter.barang-keluar', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$barangs->isEmpty())
                                <select name="barang_id" class="form-control">
                                    @foreach($barangs as $barang)
                                        <option value="{{ $barang->id }}">
                                            {{ $barang->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$barangKeluars->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Barang</th>
							<th>Harga</th>
							<th>Tanggal</th>
							<th>Author</th>
							<th>Action</th>
						</tr>
						@foreach($barangKeluars as $barangKeluar)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $barangKeluar->nama }}</td>
								<td>{{ $barangKeluar->barang->nama }}</td>
								<td>{{ $barangKeluar->harga }}</td>
								<td>{{ $barangKeluar->tanggal }}</td>
								<td>{{ $barangKeluar->author }}</td>
								<td>
									<a href="{{ route('barang-keluar.edit', ['barang-keluar' => $barangKeluar->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['barang-keluar.destroy', $barangKeluar->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $barangKeluars->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection