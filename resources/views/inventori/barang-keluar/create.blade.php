@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Barang Keluar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Barang Keluar</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'barang-keluar.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama</label>
	                  		{!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Barang</label>
                             @if(!$barangs->isEmpty())
                                <select name="barang_id" class="form-control">
                                    @foreach($barangs as $barang)
                                        <option value="{{ $barang->id }}">
                                            {{ $barang->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                        </div>
                    </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Harga</label>
	                  		{!! Form::text('harga', old('harga'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Dikeluarkan Oleh</label>
	                  		{!! Form::text('author', old('author'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                              <option>Pilih Status</option>
                              <option value="proses">Proses</option>
                              <option value="sukses">Sukses</option>
                            </select>
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection