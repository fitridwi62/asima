<html>
<head>
	<title>Laporan Data Barang Keluar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Barang Keluar</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Nama</th>
                <th>Barang</th>
                <th>Harga</th>
                <th>Tanggal</th>
                <th>Author</th>
			</tr>
		</thead>
		<tbody>
            @foreach($barangKeluars as $barangKeluar)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $barangKeluar->nama }}</td>
                <td>{{ $barangKeluar->barang->nama }}</td>
                <td>{{ $barangKeluar->harga }}</td>
                <td>{{ $barangKeluar->tanggal }}</td>
                <td>{{ $barangKeluar->author }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>