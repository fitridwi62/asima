@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Gedung</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Gedung</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('gedung.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.gedung') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'inventori.filter.gedung', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama" placeholder="Masukkan Data Gedung" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$gedungs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Kode</th>
							<th>Action</th>
						</tr>
						@foreach($gedungs as $gedung)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $gedung->nama }}</td>
								<td>{{ $gedung->kode }}</td>
								<td>
									<a href="{{ route('gedung.edit', ['gedung' => $gedung->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['gedung.destroy', $gedung->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $gedungs->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection