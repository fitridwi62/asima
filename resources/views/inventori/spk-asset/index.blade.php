@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">SPK Asset</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">SPK Asset</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('spk-asset.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.spk-asset') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'inventori.filter.spk-asset', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$gedungs->isEmpty())
                                <select name="gedung_id" class="form-control">
                                    @foreach($gedungs as $gedung)
                                        <option value="{{ $gedung->id }}">
                                            {{ $gedung->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$spkAssets->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Tipe Asset</th>
							<th>Nama</th>
							<th>Gedung</th>
							<th>Total</th>
							<th>Action</th>
						</tr>
						@foreach($spkAssets as $spkAsset)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $spkAsset->tipe_asset }}</td>
								<td>{{ $spkAsset->nama }}</td>
								<td>{{ $spkAsset->gedung->nama }}</td>
								<td>{{ $spkAsset->total }}</td>
								<td>
									<a href="{{ route('spk-asset.edit', ['spk-asset' => $spkAsset->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['spk-asset.destroy', $spkAsset->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $spkAssets->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection