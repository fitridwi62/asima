@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Barang</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Barang</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($barang, array(
                   'method' => 'PATCH',
                   'route' => array('barang.update', $barang->id))) 
                }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode</label>
	                  		{!! Form::text('kode', old('kode'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama</label>
	                  		{!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Unit</label>
	                  		{!! Form::text('unit', old('unit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                              <option>Pilih Status</option>
                              <option value="habis">Habis</option>
                              <option value="ada">ada</option>
                            </select>
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection