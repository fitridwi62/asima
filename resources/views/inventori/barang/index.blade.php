@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Barang</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Barang</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('barang.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.barang') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'inventori.filter.barang', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="kode" placeholder="Masukkan Kode Barang" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$barangs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Unit</th>
							<th>Tanggal</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($barangs as $barang)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $barang->kode }}</td>
								<td>{{ $barang->nama }}</td>
								<td>{{ $barang->unit }}</td>
								<td>{{ $barang->tanggal }}</td>
								<td>{{ $barang->status }}</td>
								<td>
									<a href="{{ route('barang.edit', ['barang' => $barang->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['barang.destroy', $barang->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $barangs->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection