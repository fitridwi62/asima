@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Donasi</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Donasi</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('donasi.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.donasi') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'inventori.filter.donasi', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$tipeDonasis->isEmpty())
                                <select name="tipe_donasi_id" class="form-control">
                                    @foreach($tipeDonasis as $tipeDonasi)
                                        <option value="{{ $tipeDonasi->id }}">
                                            {{ $tipeDonasi->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$donasis->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Tipe Donasi</th>
							<th>Tanggal</th>
							<th>Harga</th>
							<th>Total</th>
							<th>Action</th>
						</tr>
						@foreach($donasis as $donasi)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $donasi->tipeDonasi->nama }}</td>
								<td>{{ $donasi->tanggal }}</td>
								<td>{{ $donasi->harga }}</td>
								<td>{{ $donasi->total }}</td>
								<td>
									<a href="{{ route('donasi.edit', ['donasi' => $donasi->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['donasi.destroy', $donasi->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $donasis->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection