@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Donasi</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Donasi</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($donasi, array(
                   'method' => 'PATCH',
                   'route' => array('donasi.update', $donasi->id))) 
                 }}
	              <div class="box-body">
                    <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Tipe Donasi</label>
                             @if(!$tipeDonasis->isEmpty())
                                <select name="tipe_donasi_id" class="form-control">
                                    @foreach($tipeDonasis as $tipeDonasi)
                                        <option value="{{ $tipeDonasi->id }}">
                                            {{ $tipeDonasi->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Harga</label>
	                  		{!! Form::text('harga', old('harga'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Total</label>
	                  		{!! Form::text('total', old('total'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection