<html>
<head>
	<title>Laporan Data Donasi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Donasi</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Tipe Donasi</th>
                <th>Tanggal</th>
                <th>Harga</th>
                <th>Total</th>
			</tr>
		</thead>
		<tbody>
            @foreach($donasis as $donasi)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $donasi->tipeDonasi->nama }}</td>
                <td>{{ $donasi->tanggal }}</td>
                <td>{{ $donasi->harga }}</td>
                <td>{{ $donasi->total }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>