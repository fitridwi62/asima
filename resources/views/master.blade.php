@include('partials.meta')
@include('partials.header')
@include('partials.sidebar')
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        @yield('content')
    </section>
    <!-- /.content -->
  </div>
@include('partials.footer')
