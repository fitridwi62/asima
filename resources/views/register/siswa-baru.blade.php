@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top:10%; margin-bottom: 10%;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-weight:bold; text-transform: uppercase;">Pendaftaran Siswa Baru</div>
                <div class="card-body">
                {{ Form::open(array('route' => 'pendaftaran.siswa-baru.store', 'files' => true, 'enctype' => 'multipart/form-data')) }}
	              <div class="row box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">No Pendaftaran</label>
                            {!! Form::text('no_pendaftaran', old('no_pendaftaran'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('nama_lengkap') ? 'has-error' : '' !!}">
                            <label for="nama">Nama Lengkap</label>
                            {!! $errors->first('nama_lengkap', '<p class="help-block">Nama Lengkap Wajib di isi</p>') !!}
                            {!! Form::text('nama_lengkap', old('nama_lengkap'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="nama">Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control">
                                <option>Pilih Jenis Kelamin</option>
                                <option value="pria">Pria</option>
                                <option value="prempuan">Prempuan</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {!! $errors->has('tempat_lahir') ? 'has-error' : '' !!}">
                            <label for="nama">Tempat Lahir</label>
                            {!! $errors->first('tempat_lahir', '<p class="help-block">Tempat Lahir Wajib di isi</p>') !!}
                            {!! Form::text('tempat_lahir', old('tempat_lahir'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {!! $errors->has('tanggal_lahir') ? 'has-error' : '' !!}">
                            <label for="nama">Tanggal Lahir</label>
                            {!! $errors->first('tanggal_lahir', '<p class="help-block">Tanggal Lahir Wajib di isi</p>') !!}
                            {!! Form::date('tanggal_lahir', old('tanggal_lahir'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                            <label for="nama">Alamat</label>
                            {!! $errors->first('alamat', '<p class="help-block">Alamat Wajib di isi</p>') !!}
                            {!! Form::text('alamat', old('alamat'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('telp') ? 'has-error' : '' !!}">
                            <label for="nama">Nomor Telp</label>
                            {!! $errors->first('telp', '<p class="help-block">Nomor Telp Wajib di isi</p>') !!}
                            {!! Form::text('telp', old('telp'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Jenjang</label>
                             @if(!$jenjangs->isEmpty())
                                <select name="jenjang_id" class="form-control">
                                    @foreach($jenjangs as $jenjang)
                                        <option value="{{ $jenjang->id }}">
                                            {{ $jenjang->nama_jenjang }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                                <div class="dataempty">
                                    data empty
                                </div>
                             @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                            <label for="nama">Email</label>
                            {!! $errors->first('email', '<p class="help-block">Email Wajib di isi</p>') !!}
                            {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                            <label for="nama">Nomor WA</label>
                            {!! $errors->first('no_wa', '<p class="help-block">Nomor WA Wajib di isi</p>') !!}
                            {!! Form::text('no_wa', old('no_wa'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('nama_orang_tua') ? 'has-error' : '' !!}">
                            <label for="nama">Nama Orang Tua</label>
                            {!! $errors->first('nama_orang_tua', '<p class="help-block">Nomor WA Wajib di isi</p>') !!}
                            {!! Form::text('nama_orang_tua', old('nama_orang_tua'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('pekerjaan_orang_tua') ? 'has-error' : '' !!}">
                            <label for="nama">Pekerjaan Orang Tua</label>
                            {!! $errors->first('pekerjaan_orang_tua', '<p class="help-block">Pekerjaan Orang Tua Wajib di isi</p>') !!}
                            {!! Form::text('pekerjaan_orang_tua', old('pekerjaan_orang_tua'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('no_telp_ortu') ? 'has-error' : '' !!}">
                            <label for="nama">Nomor Telp Orang Tua</label>
                            {!! $errors->first('no_telp_ortu', '<p class="help-block">Nomor Telp Orang Tua Wajib di isi</p>') !!}
                            {!! Form::text('no_telp_ortu', old('no_telp_ortu'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('penghasilan_ortu') ? 'has-error' : '' !!}">
                            <label for="nama">Penghasilan Orang Tua</label>
                            {!! $errors->first('penghasilan_ortu', '<p class="help-block">Penghasilan Orang Tua Wajib di isi</p>') !!}
                            {!! Form::text('penghasilan_ortu', old('penghasilan_ortu'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('photo') ? 'has-error' : '' !!}">
                            <label for="nama">Photo</label>
                            {!! $errors->first('photo', '<p class="help-block">Photo Wajib di isi</p>') !!}
                            {!! Form::file('photo', old('photo'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	            {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
