@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top:10%; margin-bottom: 10%;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-weight:bold; text-transform: uppercase;">Pendaftaran Guru Baru</div>
                <div class="card-body">
                    {{ Form::open(array('route' => 'pendaftaran.guru.baru.store', 'files' => true, 'enctype' => 'multipart/form-data')) }}
                    <div class="row box-body">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('no_induk_guru') ? 'has-error' : '' !!}">
                                <label for="nama">No Induk Guru</label>
                                {!! $errors->first('no_induk_guru', '<p class="help-block">No Induk Guru Wajib di isi</p>') !!}
                                {!! Form::text('no_induk_guru', old('no_induk_guru'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('nama_guru') ? 'has-error' : '' !!}">
                                <label for="nama">Nama Guru</label>
                                {!! $errors->first('nama_guru', '<p class="help-block">Nama Guru Wajib di isi</p>') !!}
                                {!! Form::text('nama_guru', old('nama_guru'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('nomer_ktp') ? 'has-error' : '' !!}">
                                <label for="nama">Nomor KTP</label>
                                {!! $errors->first('nomer_ktp', '<p class="help-block">Nomor KTP Wajib di isi</p>') !!}
                                {!! Form::text('nomer_ktp', old('nomer_ktp'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama">Jenis Kelamin</label>
                               <select name="jenis_kelamin" class="form-control">
                                    <option>Pilih Jenis Kelamin</option>
                                    <option value="pria">Pria</option>
                                    <option value="prempuan">Prempuan</option>
                               </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('telp') ? 'has-error' : '' !!}">
                                <label for="nama">Telp</label>
                                {!! $errors->first('tlp', '<p class="help-block">Telp Wajib di isi</p>') !!}
                                {!! Form::text('telp', old('telp'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                                <label for="nama">Alamat</label>
                                {!! $errors->first('alamat', '<p class="help-block">Alamat Wajib di isi</p>') !!}
                                {!! Form::text('alamat', old('alamat'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                <label for="nama">Email</label>
                                {!! $errors->first('email', '<p class="help-block">Alamat Wajib di isi</p>') !!}
                                {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('pendidikan_terakhir') ? 'has-error' : '' !!}">
                                <label for="nama">Pendidikan terakhir</label>
                                {!! $errors->first('pendidikan_terakhir', '<p class="help-block">Pendidikan Terakhir Wajib di isi</p>') !!}
                                {!! Form::text('pendidikan_terakhir', old('pendidikan_terakhir'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('sertifikat') ? 'has-error' : '' !!}">
                                <label for="nama">Sertifikat</label>
                                {!! $errors->first('sertifikat', '<p class="help-block">Sertifikat Terakhir Wajib di isi</p>') !!}
                                {!! Form::text('sertifikat', old('sertifikat'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('sertifikat') ? 'has-error' : '' !!}">
                                <label for="nama">Photo</label>
                                {!! $errors->first('photo', '<p class="help-block">Photo Wajib di isi</p>') !!}
                                {!! Form::file('photo', old('photo'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px;">
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
