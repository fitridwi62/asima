@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top:10%; margin-bottom: 10%;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="alert alert-success">
                        <h3>Selamat Anda berhasil Mendaftaran Sebagai Guru Di Sekolah Kami</h3>
                        <p>Data mu sudah tersimpan ke sistem kami, mohon menunggu konfirmasi dari sistem.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
