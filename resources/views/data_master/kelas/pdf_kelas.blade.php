<html>
<head>
	<title>Data Kelas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Kelas</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Kelas</th>
			    <th>Nama Kelas</th>
			</tr>
		</thead>
		<tbody>
            @foreach($kelass as $kelas)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $kelas->kode_kelas }}</td>
				<td>{{ $kelas->nama_kelas }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>