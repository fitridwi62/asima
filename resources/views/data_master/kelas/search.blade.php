@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Kelas</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('kelas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-kelas') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'kelas.search', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="search" placeholder="Masukkan Ekstrakurikuler" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$kelass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Kode Kelas</th>
							<th>Nama Kelas</th>
							<th>Action</th>
						</tr>
						@foreach($kelass as $kelas)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $kelas->kode_kelas }}</td>
								<td>{{ $kelas->nama_kelas }}</td>
								<td>
									<a href="{{ route('kelas.edit', ['kelas' => $kelas->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['kelas.destroy', $kelas->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $kelass->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection