@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit jurusan</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="#" class="btn btn-sm btn-primary">
						Back 
					</a>
				</div>

				 <!-- form start -->
	            {{ 
                   Form::model($jurusan, array(
                   'method' => 'PATCH',
                   'route' => array('jurusan.update', $jurusan->id))) 
                }}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="nama">Kode jurusan</label>
	                  {!! Form::text('kode_jurusan', old('kode_jurusan'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	                <div class="form-group">
	                  <label for="Email">Nama jurusan</label>
	                  {!! Form::text('nama_jurusan', old('nama_jurusan'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Update Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection