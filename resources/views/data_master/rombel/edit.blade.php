@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rombongan Belajar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="{{ route ('rombongan_belajar.index') }}" class="btn btn-sm btn-primary">
						Kembali 
					</a>
				</div>

				 <!-- form start -->
	            {{ 
                   Form::model($rombel, array(
                   'method' => 'PATCH',
                   'route' => array('rombongan_belajar.update', $rombel->id))) 
                }}
	              <div class="box-body">
	                <div class="col-md-4">
	                	<div class="form-group">
		                  <label for="nama">Rombongan Belajar</label>
		                  {!! Form::text('nama', old('nama'), ['class' => 'form-control']) !!}
		                </div>
	                </div>
	                <div class="col-md-4">
	                	
		                	<div class="form-group">
			                  <label for="nama">Jurusan</label>
			                  <select name="jurusan_id" class="form-control">
			                  @if(!$jurusans->isEmpty())
			                  	 @foreach($jurusans as $jurusan)
			                  	 	<option value="{{$jurusan->id}}" @if((int)$rombel->jurusan_id === (int)$jurusan->id) selected="true" @endif>
			                  	 		{{ $jurusan->nama_jurusan }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-4">
	                	<div class="form-group">
		                  <label for="nama">Kelas</label>
		                  <select name="kelas" class="form-control">
			                  @if(!$kelas->isEmpty())
			                  	@foreach($kelas as $value)
			                  		@if((int)$rombel->kelas === (int)$value->id)
				                  	 	<option value="{{$value->id}}" selected="true">
				                  	 		{{ $value->nama_kelas }}
				                  	 	</option>
				                  	@else
				                  		<option value="{{$value->id}}">
				                  	 		{{ $value->nama_kelas }}
				                  	 	</option>

			                  	 	@endif
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
		                  </select>
		                </div>
	                </div>
	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection