@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rombongan Belajar</h3>
			</div>
			@if (Session::has('success'))
				<div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong>{{ Session::get('success') }}</strong>
                </div>
			@endif
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="{{ route('rombongan_belajar.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$rombongan_belajars->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Rombongan Belajar</th>
							<th>Jurusan</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
						@foreach($rombongan_belajars as $rombongan_belajar)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $rombongan_belajar->nama }}</td>
								<td>{{ $rombongan_belajar->jurusan->nama_jurusan }}</td>
								<td>{{ $rombongan_belajar->nama_kelas }}</td>
								<td>
									<a href="{{ route('rombongan_belajar.edit', ['rombongan-belajar' => $rombongan_belajar->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['rombongan_belajar.destroy', $rombongan_belajar->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection