@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Wali Kelas</h3>
			</div>
			@if (Session::has('success'))
				<div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong>{{ Session::get('success') }}</strong>
                </div>
			@endif
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="{{ route('wali_kelas.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$walikelass->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Guru</th>
							<th>Kurikulum</th>
							<th>Kode Mata Pelajaran</th>
							<th>Mata Pelajaran</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
						@foreach($walikelass as $walikelasx)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $walikelasx->data_guru->nama_guru }}</td>
								<td>{{ $walikelasx->kurikulum_nama }}</td>
								<td>{{ $walikelasx->kode }}</td>
								<td>{{ $walikelasx->nama }}</td>
								<td>{{ $walikelasx->nama_kelas }}</td>
								<td>
									<a href="{{ route('wali_kelas.edit', ['wali-kelas' => $walikelasx->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['wali_kelas.destroy', $walikelasx->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection