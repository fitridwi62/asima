@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Wali Kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="{{ route ('wali_kelas.index') }}" class="btn btn-sm btn-primary">
						Kembali 
					</a>
				</div>

				 <!-- form start -->
	            {{ 
                   Form::model($wali_kelas, array(
                   'method' => 'PATCH',
                   'route' => array('wali_kelas.update', $wali_kelas->id))) 
                }}
	              <div class="box-body">
	                <div class="col-md-6">
	                	
		                	<div class="form-group">
			                  <label for="nama">Guru</label>
			                  <select name="data_guru_id" class="form-control">
			                  @if(!$data_gurus->isEmpty())
			                  	 @foreach($data_gurus as $data_guru)
			                  	 	<option value="{{$data_guru->id}}">
			                  	 		{{ $data_guru->nama_guru }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			                </div>
	                </div>
	                <div class="col-md-6">
	                	
		                <div class="form-group">
			                  <label for="nama">Kurikulum</label>
			                  <select name="kurikulum_id" class="form-control">
			                  @if(!$kurikulums->isEmpty())
			                  	 @foreach($kurikulums as $kurikulum)
			                  	 	<option value="{{$kurikulum->id}}" @if((string)$wali_kelas->kurikulum_id === (string)$kurikulum->id) selected @endif>
			                  	 		{{ $kurikulum->nama }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			                  </select>
			            </div>
	                </div>
	                {{-- <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Kode Mata Pelajaran</label>
		                  {!! Form::text('kode_mata_pelajaran', old('kode_mata_pelajaran'), ['class' => 'form-control']) !!}
		                </div>
	                </div> --}}
	                <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Mata Pelajaran</label>
		                   <select name="kode_mata_pelajaran" class="form-control" @if((string)$wali_kelas->kode_mata_pelajaran === (string)$kurikulum->id) selected @endif>
			                  @if(!$mapel->isEmpty())
			                  	 @foreach($mapel as $item)
			                  	 	<option value="{{$item->id}}">
			                  	 		{{ $item->nama }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			              </select>
		                </div>
	                </div>
	                <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Kelas</label>
		                  {{-- {!! Form::text('kelas', old('kelas'), ['class' => 'form-control']) !!} --}}
		                  <select name="kelas" class="form-control">
			                  @if(!$kelas->isEmpty())
			                  	 @foreach($kelas as $item)
			                  	 	<option value="{{$item->id}}" @if((string)$wali_kelas->kelas === (string)$item->id) selected @endif>
			                  	 		{{ $item->nama_kelas }}
			                  	 	</option>
			                  	 @endforeach
			                 @else
			                  	<div class="alert alert-danger">
		                            Data empty
		                        </div>
		                	  @endif
			               </select>
		                </div>
	                </div>
	                 {{--  <div class="col-md-6">
	                	<div class="form-group">
		                  <label for="nama">Kelas</label>
		                  {!! Form::text('kelas', old('kelas'), ['class' => 'form-control']) !!}
		                </div>
	                </div> --}}
	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Save Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection