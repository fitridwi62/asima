@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"Ruang kelas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Ruang kelas</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'ruang_kelas.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode ruang kelas</label>
	                  		{!! Form::text('kode_ruangan', old('kode_ruangan'), ['class' => 'form-control', 'required' => 'true']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama ruang kelas</label>
	                  		{!! Form::text('nama_ruangan', old('nama_ruangan'), ['class' => 'form-control', 'required' => 'true']) !!}
	                	</div>
	              	</div>
	              	<div class="col-md-12" >
	                	<button type="submit" class="btn btn-primary">Simpan Data</button>
	              	</div>
	          	</div>
	          	
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection