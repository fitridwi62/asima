@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Guru</li>
				  </ol>
				</nav>

				 <!-- form start -->
	             {{ Form::model($data_guru, array('method' => 'PATCH',
                    'route' => array('data_guru.update', $data_guru->id),
                    'enctype' => 'multipart/form-data', 'files'=> 'true')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Induk Guru</label>
	                  		{!! Form::text('no_induk_guru', old('no_induk_guru'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Guru</label>
	                  		{!! Form::text('nama_guru', old('nama_guru'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor KTP</label>
	                  		{!! Form::text('nomer_ktp', old('nomer_ktp'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Jenis Kelamin</label>
	                  		{!! Form::text('jenis_kelamin', old('jenis_kelamin'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Telp</label>
	                  		{!! Form::text('telp', old('telp'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Alamat</label>
	                  		{!! Form::text('alamat', old('alamat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Email</label>
	                  		{!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Pendidikan terakhir</label>
	                  		{!! Form::text('pendidikan_terakhir', old('pendidikan_terakhir'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Sertifikat</label>
	                  		{!! Form::text('sertifikat', old('sertifikat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                  <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Photo</label>
	                  		{!! Form::file('photo', old('photo'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-12" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection