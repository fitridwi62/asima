<html>
<head>
	<title>Data Guru</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Guru</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>No. Induk Guru</th>
                <th>Nama Guru</th>
                <th>No. KTP</th>
                <th>Jenis Kelamin</th>
                <th>Telp</th>
			</tr>
		</thead>
		<tbody>
        @foreach($data_gurus as $data_guru)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $data_guru->no_induk_guru }}</td>
                <td>{{ $data_guru->nama_guru }}</td>
                <td>{{ $data_guru->nomer_ktp }}</td>
                <td>{{ $data_guru->jenis_kelamin }}</td>
                <td>{{ $data_guru->telp }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>