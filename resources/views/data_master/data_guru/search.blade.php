@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Guru</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Data Guru</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('data_guru.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.data-guru') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
                <div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'keuangan-siswa.filter.data-guru', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nama_guru" placeholder="Masukkan Data Guru" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table table-bordered">
					@if(!$data_gurus->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>No. Induk Guru</th>
							<th>Nama Guru</th>
							<th>No. KTP</th>
							<th>Jenis Kelamin</th>
							<th>Telp</th>
							<th>Action</th>
						</tr>
						@foreach($data_gurus as $data_guru)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $data_guru->no_induk_guru }}</td>
								<td>{{ $data_guru->nama_guru }}</td>
								<td>{{ $data_guru->nomer_ktp }}</td>
								<td>{{ $data_guru->jenis_kelamin }}</td>
								<td>{{ $data_guru->telp }}</td>
								<td>
									<a href="{{ route('data_guru.edit', ['data-guru' => $data_guru->id]) }}" class="btn btn-sm btn-warning">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a href="{{ route('data_guru.edit', ['data-guru' => $data_guru->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['data_guru.destroy', $data_guru->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
                {{ $data_gurus->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection