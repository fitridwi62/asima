@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Sekolah</h3>
			</div>
			@if (Session::has('success'))
				<div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong>{{ Session::get('success') }}</strong>
                </div>
			@endif
			<!-- /.box-header -->
			<div class="box-body">
				<div class="box-header with-border">
					<a href="{{ route ('data_sekolah.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
				<table class="table table-bordered">
					@if(!$data_sekolahs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Telephone</th>
							<th>Alamat</th>
							<th>Jenjang</th>
							<th>Action</th>
						</tr>
						@foreach($data_sekolahs as $data_sekolah)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $data_sekolah->nama }}</td>
								<td>{{ $data_sekolah->email }}</td>
								<td>{{ $data_sekolah->telephone }}</td>
								<td>{{ $data_sekolah->alamat }}</td>
								<td>{{ $data_sekolah->nama_jenjang }}</td>
								<td>
									<a href="{{ route('data_sekolah.edit', ['data-sekolah' => $data_sekolah->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['data_sekolah.destroy', $data_sekolah->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection