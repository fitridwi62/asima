@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Data Sekolah</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				 <!-- form start -->
	            {{ 
                   Form::model($data_sekolah, array(
                   'method' => 'PATCH',
                   'route' => array('data_sekolah.update', $data_sekolah->id))) 
                }}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="nama">Nama</label>
	                  {!! Form::text('nama', old('nama'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	                <div class="form-group">
	                  <label for="Email">Email</label>
	                  {!! Form::text('email', old('email'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	                <div class="form-group">
	                  <label for="Telephone">Telephone</label>
	                  {!! Form::text('telephone', old('telephone'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	                 <div class="form-group">
	                  <label for="alamat">Alamat</label>
	                  {!! Form::text('alamat', old('alamat'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	                <div class="form-group">
	                  <label for="jenjang">Jenjang</label>
	                  {!! Form::select('jenjang', $jenjang, old('jenjang'), ['class' => 'form-control', 'required' => 'true']) !!}
	                </div>
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Update Data</button>
	              </div>
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection