@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Uang Masuk</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Uang Masuk</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'uang-masuk.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             @if(!$siswas->isEmpty())
                                <select name="siswa_id" class="form-control">
                                    @foreach($siswas as $siswa)
                                        <option value="{{ $siswa->id }}">
                                            {{ $siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Jenis Pembayaran</label>
                             @if(!$jenisPembayarans->isEmpty())
                                <select name="jenis_pembayaran_id" class="form-control">
                                    @foreach($jenisPembayarans as $jenisPembayaran)
                                        <option value="{{ $jenisPembayaran->id }}">
                                            {{ $jenisPembayaran->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                        </div>
                    </div>

                    <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Siswa</label>
	                  		{!! Form::text('nama_siswa', old('nama_siswa'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Debit</label>
	                  		{!! Form::text('no_akun_debit', old('no_akun_debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Kredit</label>
	                  		{!! Form::text('no_akun_kredit', old('no_akun_kredit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Harga</label>
	                  		{!! Form::text('harga', old('harga'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection