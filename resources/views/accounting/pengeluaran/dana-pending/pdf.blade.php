<html>
<head>
	<title>Laporan Data Dana Pending</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Dana Pending</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Tanggal</th>
                <th>Kode Formulir</th>
                <th>Deskripsi</th>
                <th>Jumlah Nominal</th>
			</tr>
		</thead>
		<tbody>
            @foreach($danaPendings as $danaPending)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $danaPending->tanggal }}</td>
                <td>{{ $danaPending->kode_formolir }}</td>
                <td>{{ $danaPending->deskripsi }}</td>
                <td>{{ $danaPending->jumlah_nominal }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>