@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Cek List</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Cek List</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($cekList, array(
                   'method' => 'PATCH',
                   'route' => array('cek-list.update', $cekList->id))) 
                 }}
                 <div class="box-body">
	                 <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">List</label>
	                  		{!! Form::text('list', old('list'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">persen (%)</label>
	                  		{!! Form::text('percent', old('percent'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-controll">
                                <option>Pilih Status</option>
                                <option value="ada">Ada</option>
                                <option value="habis">Habis</option>
                            </select>
	                	</div>
	                 </div>
                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection