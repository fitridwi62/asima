<html>
<head>
	<title>Laporan Data Cek List</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Cek List</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>List</th>
                <th>Description</th>
                <th>Status</th>
                <th>Persen</th>
			</tr>
		</thead>
		<tbody>
            @foreach($cekLists as $cekList)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $cekList->list }}</td>
                <td>{{ $cekList->deskripsi }}</td>
                <td>{{ $cekList->status }}</td>
                <td>{{ $cekList->percent }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>