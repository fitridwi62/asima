@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jurnal Kas EDC</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jurnal Kas EDC</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('jurnal-edc.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pengeluaran.cetak-jurnal-edc') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'accounting.filter.pengeluaran.jurnal-kas-edc', 'method' => 'get')) }}
					<div class="input-group">
						@if(!$jenisPembayarans->isEmpty())
                                <select name="jenis_pembayaran_id" class="form-control">
                                    @foreach($jenisPembayarans as $jenisPembayaran)
                                        <option value="{{ $jenisPembayaran->id }}">
                                            {{ $jenisPembayaran->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                        @endif
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$jurnalEdcs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Tanggal</th>
							<th>Siswa</th>
							<th>Jenis Pembayaraan</th>
							<th>Debit</th>
							<th>Kredit</th>
							<th>Action</th>
						</tr>
						@foreach($jurnalEdcs as $jurnalEdc)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $jurnalEdc->tanggal }}</td>
								<td>{{ $jurnalEdc->siswa->nama_lengkap }}</td>
								<td>{{ $jurnalEdc->pembayaran->nama }}</td>
								<td>{{ $jurnalEdc->debit }}</td>
								<td>{{ $jurnalEdc->debit }}</td>
								<td>
									<a href="{{ route('jurnal-edc.edit', ['jurnal-edc' => $jurnalEdc->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['jurnal-edc.destroy', $jurnalEdc->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
										<button class="btn btn-sm btn-danger btn-delete" type="submit">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $jurnalEdcs->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection