<html>
<head>
	<title>Laporan Data Jurnal Edc</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Jurnal Edc</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Tanggal</th>
                <th>Siswa</th>
                <th>Jenis Pembayaraan</th>
                <th>Debit</th>
                <th>Kredit</th>
			</tr>
		</thead>
		<tbody>
            @foreach($jurnalEdcs as $jurnalEdc)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $jurnalEdc->tanggal }}</td>
                <td>{{ $jurnalEdc->siswa->nama_lengkap }}</td>
                <td>{{ $jurnalEdc->pembayaran->nama }}</td>
                <td>{{ $jurnalEdc->debit }}</td>
                <td>{{ $jurnalEdc->debit }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>