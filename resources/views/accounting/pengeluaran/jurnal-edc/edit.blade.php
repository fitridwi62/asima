@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jurnal Kas EDC</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jurnal Kas EDC</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($jurnalEdc, array(
                   'method' => 'PATCH',
                   'route' => array('jurnal-edc.update', $jurnalEdc->id))) 
                }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Siswa</label>
                             @if(!$siswas->isEmpty())
                                <select name="siswa_id" class="form-control">
                                    @foreach($siswas as $siswa)
                                        <option value="{{ $siswa->id }}">
                                            {{ $siswa->nama_lengkap }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Jenis Pembayaran</label>
                             @if(!$jenisPembayarans->isEmpty())
                                <select name="jenis_pembayaran_id" class="form-control">
                                    @foreach($jenisPembayarans as $jenisPembayaran)
                                        <option value="{{ $siswa->id }}">
                                            {{ $jenisPembayaran->nama }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                        </div>
                    </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Debit</label>
	                  		{!! Form::text('nomor_akun_debit', old('nomor_akun_debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Kredit</label>
	                  		{!! Form::text('nomor_akun_kredit', old('nomor_akun_kredit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nominal</label>
	                  		{!! Form::text('debit', old('debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection