<html>
<head>
	<title>Laporan Data Kas Mutasi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Kas Mutasi</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Tanggal</th>
                <th>Jam Mutasi</th>
                <th>Status</th>
                <th>Debit</th>
                <th>Kredit</th>
			</tr>
		</thead>
		<tbody>
            @foreach($jurnalKasMutasis as $jurnalKasMutasi)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $jurnalKasMutasi->tanggal }}</td>
                <td>{{ $jurnalKasMutasi->jam_mutasi }}</td>
                <td>{{ $jurnalKasMutasi->status }}</td>
                <td>{{ $jurnalKasMutasi->debit }}</td>
                <td>{{ $jurnalKasMutasi->debit }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>