@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jurnal Uang Keluar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jurnal Uang Keluar</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('jurnal-uang-keluar.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pengeluaran.uang-keluar') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'accounting.filter.pengeluaran.uang-keluar', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nomor_kwitansi" placeholder="Masukkan Nomor Kwintasi" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$jurnalUangKeluars->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Tanggal</th>
							<th>Kode Kwitansi</th>
							<th>Permintaan Dana</th>
							<th>Debit</th>
							<th>Kredit</th>
							<th>Action</th>
						</tr>
						@foreach($jurnalUangKeluars as $jurnalUangKeluar)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $jurnalUangKeluar->tanggal }}</td>
								<td>{{ $jurnalUangKeluar->nomor_kwitansi }}</td>
								<td>{{ $jurnalUangKeluar->status }}</td>
								<td>{{ $jurnalUangKeluar->debit }}</td>
								<td>{{ $jurnalUangKeluar->debit }}</td>
								<td>
									<a href="{{ route('jurnal-uang-keluar.edit', ['jurnal-uang-keluar' => $jurnalUangKeluar->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['jurnal-uang-keluar.destroy', $jurnalUangKeluar->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $jurnalUangKeluars->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection