@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jurnal Uang Keluar</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jurnal Uang Keluar</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($jurnalUangKeluar, array(
                   'method' => 'PATCH',
                   'route' => array('jurnal-uang-keluar.update', $jurnalUangKeluar->id))) 
                }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Kwitansi</label>
	                  		{!! Form::text('nomor_kwitansi', old('nomor_kwitansi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
                            <div class="form-group">
                            <label for="nama">Kode Reibusment</label>
                             @if(!$reibusments->isEmpty())
                                <select name="reimbursment_id" class="form-control">
                                    @foreach($reibusments as $reibusment)
                                        <option value="{{ $reibusment->id }}">
                                            {{ $reibusment->kode_reibusment }}
                                        </option>
                                    @endforeach
                                </select>
                             @else
                             @endif
                        </div>
                    </div>
                    
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Debit</label>
	                  		{!! Form::text('nomor_akun_debit', old('nomor_akun_debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">No Akun Kredit</label>
	                  		{!! Form::text('nomor_akun_kredit', old('nomor_akun_kredit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Debit</label>
	                  		{!! Form::text('debit', old('debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="valid">Valid</option>
                            </select>
	                	</div>
	                 </div>

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection