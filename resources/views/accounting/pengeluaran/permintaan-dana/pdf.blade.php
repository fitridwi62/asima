<html>
<head>
	<title>Laporan Data Permintaan Dana</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Permintaan Dana</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Kode Permintaan Dana</th>
                <th>Tanggal</th>
                <th>Kode Kwintansi</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
         @foreach($permintaanDanas as $permintaanDana)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $permintaanDana->kode_permintaan_dana }}</td>
                <td>{{ $permintaanDana->tanggal }}</td>
                <td>{{ $permintaanDana->kode_kwintansi }}</td>
                <td>{{ $permintaanDana->harga }}</td>
                <td>{{ $permintaanDana->harga }}</td>
                <td>{{ $permintaanDana->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>