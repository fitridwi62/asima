@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Permintaan Dana</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Permintaan Dana</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'permintaan-dana.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode Permintaan Dana</label>
	                  		{!! Form::text('kode_permintaan_dana', old('kode_permintaan_dana'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kode Kwintansi</label>
	                  		{!! Form::text('kode_kwintansi', old('kode_kwintansi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Akun Debit</label>
	                  		{!! Form::text('nomor_akun_debit', old('nomor_akun_debit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Akun Kredit</label>
	                  		{!! Form::text('nomor_akun_kredit', old('nomor_akun_kredit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Harga</label>
	                  		{!! Form::text('harga', old('harga'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>


					 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                              <option>Pilih Status</option>
                              <option value="ada">Ada</option>
                              <option value="habis">Habis</option>
                            </select>
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection