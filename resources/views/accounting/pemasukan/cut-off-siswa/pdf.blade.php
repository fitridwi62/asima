<html>
<head>
	<title>Laporan Data Cut Off Siswa</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Cut Off Siswa</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Tanggal</th>
                <th>Nomor Rekening</th>
                <th>Nama Siswa</th>
                <th>Kas Kredit</th>
                <th>Saldo</th>
			</tr>
		</thead>
		<tbody>
            @foreach($cutOffSiswas as $cutOffSiswa)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $cutOffSiswa->tanggal }}</td>
                <td>{{ $cutOffSiswa->nomor_rekening }}</td>
                <td>{{ $cutOffSiswa->nama_siswa }}</td>
                <td>{{ $cutOffSiswa->kas_kredit }}</td>
                <td>{{ $cutOffSiswa->kas_kredit }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>