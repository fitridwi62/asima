@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Cut Off Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Cut Off Siswa</li>
				  </ol>
				</nav>

				 <!-- form start -->
	            {{ Form::open(array('route' => 'cut-off-siswa.store')) }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Rekening</label>
	                  		{!! Form::text('nomor_rekening', old('nomor_rekening'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Siswa</label>
	                  		{!! Form::text('nama_siswa', old('nama_siswa'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Kas Kredit</label>
	                  		{!! Form::text('kas_kredit', old('kas_kredit'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     

                     <div class="col-md-12">
	                 	<div class="form-group">
	                  		<label for="nama">Deskripsi</label>
	                  		{!! Form::textarea('deskripsi', old('deskripsi'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Dibuat Oleh</label>
	                  		{!! Form::text('dibuat', old('dibuat'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Diperiksa Oleh</label>
	                  		{!! Form::text('diperiksa', old('diperiksa'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Disetujui Oleh</label>
	                  		{!! Form::text('disetujui', old('disetujui'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection