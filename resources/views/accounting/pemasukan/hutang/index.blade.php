@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hutang</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Hutang</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('hutang.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pemasukan.hutang') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'accounting.filter.pemasukan.hutang', 'method' => 'get')) }}
					<div class="input-group">
						<input type="date" name="tanggal" placeholder="Masukkan tanggal" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$hutangs->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nomor Rekening</th>
							<th>Nama Siswa</th>
							<th>PBN</th>
							<th>SPP</th>
							<th>PRO</th>
							<th>Total</th>
							<th>Action</th>
						</tr>
						@foreach($hutangs as $hutang)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $hutang->nomor_rekening }}</td>
								<td>{{ $hutang->nama_siswa }}</td>
								<td>{{ $hutang->pbn }}</td>
								<td>{{ $hutang->spp }}</td>
								<td>{{ $hutang->pro }}</td>
								<td>{{ $hutang->pbn + $hutang->spp + $hutang->pro }}</td>
								<td>
									<a href="{{ route('hutang.edit', ['hutang' => $hutang->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['hutang.destroy', $hutang->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $hutangs->links() }}
			</div>
		</div>
		<!-- /.box -->
	</div>
@endsection