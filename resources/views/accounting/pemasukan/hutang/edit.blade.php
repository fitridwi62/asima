@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hutang</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Hutang</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($hutang, array(
                   'method' => 'PATCH',
                   'route' => array('hutang.update', $hutang->id))) 
                 }}
	              <div class="box-body">
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Rekening</label>
	                  		{!! Form::text('nomor_rekening', old('nomor_rekening'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Siswa</label>
	                  		{!! Form::text('nama_siswa', old('nama_siswa'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Tanggal</label>
	                  		{!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">PBN</label>
	                  		{!! Form::text('pbn', old('pbn'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">SPP</label>
	                  		{!! Form::text('spp', old('spp'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">PRO</label>
	                  		{!! Form::text('pro', old('pro'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                    
                     <div class="col-md-4">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                                <option>Pilih Status Hutang</option>
                                <option value="tahun-sebelumnya">
                                    Tahun Sebelumnya
                                </option>
                            </select>
	                	</div>
	                 </div>
	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection