<html>
<head>
	<title>Laporan Data Hutang</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Hutang</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Nomor Rekening</th>
                <th>Nama Siswa</th>
                <th>PBN</th>
                <th>SPP</th>
                <th>PRO</th>
                <th>Total</th>
			</tr>
		</thead>
		<tbody>
            @foreach($hutangs as $hutang)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $hutang->nomor_rekening }}</td>
                <td>{{ $hutang->nama_siswa }}</td>
                <td>{{ $hutang->pbn }}</td>
                <td>{{ $hutang->spp }}</td>
                <td>{{ $hutang->pro }}</td>
                <td>{{ $hutang->pbn + $hutang->spp + $hutang->pro }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>