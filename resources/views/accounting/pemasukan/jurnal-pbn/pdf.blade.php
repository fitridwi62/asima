<html>
<head>
	<title>Laporan Data Jurnal PBN</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Jurnal PBN</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th style="width: 10px">#</th>
                <th>Tanggal</th>
                <th>Nomor Rekening</th>
                <th>Nama Siswa</th>
                <th>Debit</th>
                <th>Kredit</th>
			</tr>
		</thead>
		<tbody>
            @foreach($jurnalPbns as $jurnalPbn)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $jurnalPbn->tanggal }}</td>
                <td>{{ $jurnalPbn->nomor_rekening }}</td>
                <td>{{ $jurnalPbn->nama_siswa }}</td>
                <td>{{ $jurnalPbn->kas_debit }}</td>
                <td>{{ $jurnalPbn->kas_debit }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>