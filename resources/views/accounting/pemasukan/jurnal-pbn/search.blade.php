@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jurnal Program Bangunan Bulanan</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Jurnal Program Bangunan Bulanan</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('jurnal-pbn.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pemasukan.jurnal-pbn') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'accounting.filter.pemasukan.jurnal-pbn', 'method' => 'get')) }}
					<div class="input-group">
						<input type="date" name="tanggal" placeholder="Masukkan Nomor Keuangan" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$jurnalPbns->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Tanggal</th>
							<th>Nomor Rekening</th>
							<th>Nama Siswa</th>
							<th>Debit</th>
							<th>Kredit</th>
							<th>Action</th>
						</tr>
						@foreach($jurnalPbns as $jurnalPbn)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $jurnalPbn->tanggal }}</td>
								<td>{{ $jurnalPbn->nomor_rekening }}</td>
								<td>{{ $jurnalPbn->nama_siswa }}</td>
								<td>{{ $jurnalPbn->kas_debit }}</td>
								<td>{{ $jurnalPbn->kas_debit }}</td>
								<td>
									<a href="{{ route('jurnal-pbn.edit', ['jurnal-pbn' => $jurnalPbn->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['jurnal-pbn.destroy', $jurnalPbn->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $jurnalPbns->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection