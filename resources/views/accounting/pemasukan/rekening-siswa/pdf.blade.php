<html>
<head>
	<title>Laporan Data Rekening Siswa</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Rekening Siswa</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">No</th>
                <th>Nomor Keuangan</th>
                <th>Nama Siswa</th>
                <th>PBN(tahunan)</th>
                <th>SPP(bulanan)</th>
                <th>PRO(tahunan)</th>
                <th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach($rekeningSiswas as $rekeningSiswa)
			<tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $rekeningSiswa->nomor_keuangan }}</td>
                <td>{{ $rekeningSiswa->nama_siswa }}</td>
                <td>{{ $rekeningSiswa->pbn_tahunan }}</td>
                <td>{{ $rekeningSiswa->spp }}</td>
                <td>{{ $rekeningSiswa->biaya_program }}</td>
                <td>{{ $rekeningSiswa->status }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>