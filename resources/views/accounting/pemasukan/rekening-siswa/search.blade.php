@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rekening Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Rekening Siswa</li>
				  </ol>
				</nav>
				<div class="box-header with-border">
					<a href="{{ route('rekening-siswa.create') }}" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</a>

					<a href="{{ route('pdf.pemasukan.rekening-siswa') }}" class="btn btn-sm btn-danger">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-md-12 filter" style="margin-top:10px; margin-bottom:10px;">
				{{ Form::open(array('route' => 'accounting.filter.pemasukan.rekening-siswa', 'method' => 'get')) }}
					<div class="input-group">
						<input type="text" name="nomor_keuangan" placeholder="Masukkan Keuangan" class="form-control">
						<span class="input-group-btn">
							{!! Form::button('Cari', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
						</span>
					</div>
				{{ Form::close() }}
				</div>
				<div class="col-md-offset-2 col-md-8 col-md-offset-2" style="margin-top:10px; margin-bottom: 10px;">
					@include('partials._flash')
				</div>
				<table class="table table-bordered">
					@if(!$rekeningSiswas->isEmpty())
						<tr>
							<th style="width: 10px">#</th>
							<th>Nomor Keuangan</th>
							<th>Nama Siswa</th>
							<th>PBN(tahunan)</th>
							<th>SPP(bulanan)</th>
							<th>PRO(tahunan)</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($rekeningSiswas as $rekeningSiswa)
							<tr>
								<td>{{ $loop->index +1 }}</td>
								<td>{{ $rekeningSiswa->nomor_keuangan }}</td>
								<td>{{ $rekeningSiswa->nama_siswa }}</td>
								<td>{{ $rekeningSiswa->pbn_tahunan }}</td>
								<td>{{ $rekeningSiswa->spp }}</td>
								<td>{{ $rekeningSiswa->biaya_program }}</td>
								<td>{{ $rekeningSiswa->status }}</td>
								<td>
									<a href="{{ route('rekening-siswa.edit', ['rekening-siswa' => $rekeningSiswa->id]) }}" class="btn btn-sm btn-success">
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</a>
									{!! Form::open(['route' => ['rekening-siswa.destroy', $rekeningSiswa->id], 'method' => 'DELETE', 'style' => 'display:inline;']) !!}
									<button class="btn btn-sm btn-danger btn-delete" type="submit">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                                    {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@else
                        <div class="alert alert-danger">
                            Data empty
                        </div>
                    @endif
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				{{ $rekeningSiswas->links() }}
			</div> 
		</div>
		<!-- /.box -->
	</div>
@endsection