@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rekening Siswa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Rekening Siswa</li>
				  </ol>
				</nav>

				 <!-- form start -->
                 {{ 
                   Form::model($rekening_siswa, array(
                   'method' => 'PATCH',
                   'route' => array('rekening-siswa.update', $rekening_siswa->id))) 
                 }}
	              <div class="box-body">
	                 <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nomor Keuangan</label>
	                  		{!! Form::text('nomor_keuangan', old('nomor_keuangan'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Nama Siswa</label>
	                  		{!! Form::text('nama_siswa', old('nama_siswa'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">PBN (Tahunan)</label>
	                  		{!! Form::text('pbn_tahunan', old('pbn_tahunan'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">SPP (bulanan)</label>
	                  		{!! Form::text('spp', old('spp'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>
                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Biaya Program</label>
	                  		{!! Form::text('biaya_program', old('biaya_program'), ['class' => 'form-control']) !!}
	                	</div>
	                 </div>

                     <div class="col-md-6">
	                 	<div class="form-group">
	                  		<label for="nama">Status</label>
	                  		<select name="status" class="form-control">
                                <option>Pilih Status</option>
                                <option value="sudah-bayar">Sudah Bayar</option>
                                <option value="belum-bayar">Belum Bayar</option>
                            </select>
	                	</div>
	                 </div>

	                 <div class="col-md-6" style="margin-top: 15px;">
	                 	<div class="box-footer">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
	                 </div>
	              </div>
	              <!-- /.box-body -->
	            {!! Form::close() !!}
					
				</div>
				<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
@endsection