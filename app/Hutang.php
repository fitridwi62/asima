<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hutang extends Model
{
    protected $table = 'hutangs';

    protected $fillable = [
        'nomor_rekening',
        'nama_siswa',
        'tanggal',
        'pbn',
        'spp',
        'pro',
        'status'
    ];
}
