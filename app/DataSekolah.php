<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSekolah extends Model
{
    protected $table = "data_sekolahs";

    protected $fillable = [
    	'nama',
    	'email',
    	'telephone',
    	'alamat',
    	'jenjang'
    ];
}
