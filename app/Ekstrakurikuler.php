<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ekstrakurikuler extends Model
{
    protected $table = 'ekstrakurikulers';

    protected $fillable = [
        'nama_ekstra',
        'penangung_jawab',
        'tanggal',
        'jam_mulai',
        'jam_selesai',
        'pertemuan'
    ];
}
