<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RaportEkstrakurikuler extends Model
{
    protected $table = 'raport_ekstrakurikulers';
    //protected $primaryKey = 'raport_id';

    protected $fillable = [
        'nama_ekstrakurikuler',
        'nilai',
        'predikat',
        'session_raport_id',
        'siswa_id'
    ];

    public function raport(){
        return $this->belongsTo('App\Raport', 'raport_id');
    }

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }
}
