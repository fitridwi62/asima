<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengumumanKelas extends Model
{
    protected $table = 'pengumuman_kelas';

    protected $fillable = [
        'judul',
        'keterangan',
        'siswa_id',
        'user_id'
    ];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
