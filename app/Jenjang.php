<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenjang extends Model
{
    protected $table = "jenjangs";
    protected $fillable = [
        'kode_jenjang',
        'nama_jenjang'
    ];

    public function mata_pelajaran(){
        return $this->hasMany('App\MataPelajaran');
    }
}
