<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengelauaranJurnalKasKecilReimbursment extends Model
{
    protected $table = 'pengelauaran_jurnal_kas_kecil_reimbursments';

    protected $fillable = [
        'tanggal',
        'kode_reibusment',
        //'permintaanDana_id',
        'deskripsi',
        'nomor_akun_debit',
        'nomor_akun_kredit',
        'debit'
    ];

    public function permintaan_dana(){
        return $this->belongsTo('App\PengeluaranPermintaanDana', 'permintaanDana_id');
    }
}
