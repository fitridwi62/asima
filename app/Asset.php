<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'assets';

    protected $fillable = [
        'nama_asset',
        'no_asset',
        'asset_tetap',
        'tanggal',
        'biaya_akuisisi',
        'deskripsi',
        'kredit_to'
    ];
}
