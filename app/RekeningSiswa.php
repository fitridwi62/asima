<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekeningSiswa extends Model
{
    protected $table = 'rekening_siswas';

    protected $fillable = [
        'nomor_keuangan',
        'nama_siswa',
        'pbn_tahunan',
        'spp',
        'biaya_program',
        'status'
    ];
}
