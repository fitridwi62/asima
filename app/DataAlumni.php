<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataAlumni extends Model
{
    protected $table = 'data_alumnis';
    protected $fillable = [
        'nama',
        'tahun_angkatan',
        'email',
        'phone',
        'alamat',
        'sekolah_ke'
    ];
}
