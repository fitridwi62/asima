<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetugasPerpustakaan extends Model
{
    protected $table = 'petugas_perpustakaans';

    protected $fillable = [
        'nama_lengkap',
        'email',
        'phone',
        'jenis_kelamin',
        'wa',
        'status'
    ];
}
