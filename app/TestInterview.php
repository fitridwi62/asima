<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestInterview extends Model
{
    protected $table = 'test_interviews';

    protected $fillable = [
        'daftar_siswa_id',
        'nilai_terbaik',
        'prestasi',
        'hoby',
        'aktivitas',
        'jarak_rumah',
        'pendapatan_orang_tua',
        'pekerjaan_orang_tua',
        'note'
    ];

    public function daftarSiswa(){
        return $this->belongsTo('App\DaftarSiwa');
    }
}
