<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RaportEkstraItem extends Model
{
    protected $table = 'raport_ekstra_items';
    protected $fillable = [
        'nama_ekstrakurikuler',
        'nilai',
        'predikat',
        'siswa_id'
    ];

    public function siswaItem(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

}
