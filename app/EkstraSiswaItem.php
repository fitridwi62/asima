<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkstraSiswaItem extends Model
{
    protected $table = "ekstra_siswa_items";
    protected $fillable = [
        'siswa_id',
        'ekstra_id',
        'user_id'
    ];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function ekstra(){
        return $this->belongsTo('App\Ekstrakurikuler', 'ekstra_id');
    }
}
