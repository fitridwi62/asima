<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epengumuman extends Model
{
    protected $table = 'epengumumen';

    protected $fillable = [
        'data_guru_id',
        'nama',
        'tanggal',
        'deskripsi',
        'status'
    ];

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }
}
