<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = "siswas";
    protected $fillable = [
    	'nis',
    	'nisn',
		'nama_lengkap',
		'nama_panggilan',
		'nomor_formulir',
    	'tanggal_lahir',
    	'tempat_lahir',
    	'jenis_kelamin',
    	'agama_id',
		'jenjang_id',
		'kelas_id',
		'tahun_akademik_id',
		'wali_kelas',
    	'phone',
    	'umur',
    	'anak_ke',
    	'suku',
    	'nama_ayah_kandung',
    	'lulusan_sekolah_ortu',
    	'pekerjaan_ortu',
    	'jarak_rumah',
		'no_telp_ortu',
		'penghasilan_ortu',
		'nama_ibu_kandung',
		'pekerjaan_ibu',
		'akte_photo',
		'kk_photo',
		'surat_pernyataan_photo',
		'photo',
		'status_siswa',
		'keterangan',
		'user_id'
    ];

    public function agama(){
    	return $this->belongsTo('App\Agama');
    }

    public function jenjang(){
    	return $this->belongsTo('App\Jenjang');
	}
	public function kelas(){
		return $this->belongsTo('App\Kelas', 'kelas_id');
	}

	public function report(){
		return $this->hasMany('App\Raport');
	}

	public function tahun_akademik(){
		return $this->belongsTo('App\TahunAkademik', 'tahun_akademik_id');
	}

	public function user()
    {
        return $this->hasOne('App\User')->withDefault();
    }
}
