<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $table = 'donasis';

    protected $fillable = [
        'tipe_donasi_id',
        'tanggal',
        'harga',
        'total'
    ];

    public function tipeDonasi(){
        return $this->belongsTo('App\TipeDonasi', 'tipe_donasi_id');
    }
}
