<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGuru extends Model
{
    protected $table = "data_gurus";
    protected $fillable = [
    	'no_induk_guru',
    	'nama_guru',
    	'nomer_ktp',
    	'jenis_kelamin',
    	'telp',
    	'alamat',
    	'email',
    	'pendidikan_terakhir',
    	'sertifikat',
    	'photo'
    ];

    public function walikelas(){
        return $this->hasMany('App\WaliKelas');
	}
	
	public function mata_pelajaran(){
		return $this->hasMany('App\MataPelajaran');
	}
}
