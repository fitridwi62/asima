<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkAsset extends Model
{
    protected $table = 'spk_assets';

    protected $fillable = [
        'tipe_asset',
        'nama',
        'gedung_id',
        'total'
    ];

    public function gedung(){
        return $this->belongsTo('App\Gedung', 'gedung_id');
    }
}
