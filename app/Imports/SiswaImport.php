<?php

namespace App\Imports;

use App\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;

class SiswaImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Siswa([
            'nis'               => $row[1],
            'nisn'              => $row[2],
            'nama_lengkap'      => $row[3],
            'tanggal_lahir'     => $row[4],
            'tempat_lahir'      => $row[5],
            'jenis_kelamin'     => $row[6],
            'agama_id'          => $row[7],
            'jenjang_id'        => $row[8],
            'jurusan_id'        => $row[9],
            'phone'             => $row[10],
            'umur'              => $row[11],
            'anak_ke'           => $row[12],
            'suku'              => $row[13],
            'nama_ayah_kandung' => $row[14],
            'lulusan_sekolah_ortu' => $row[15],
            'pekerjaan_ortu'    => $row[16],
            'jarak_rumah'       => $row[17],
            'no_telp_ortu'      => $row[18],
            'penghasilan_ortu'  => $row[19]
        ]);
    }
}
