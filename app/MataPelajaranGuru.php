<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaranGuru extends Model
{
    protected $table = 'mata_pelajaran_gurus';

    protected $fillable = [
        'kelas_id',
        'mata_pelajar_id',
        'data_guru_id',
        'status'
    ];

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function mata_pelajaran(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajar_id');
    }

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }
}
