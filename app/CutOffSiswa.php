<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CutOffSiswa extends Model
{
    protected $table = 'cut_off_siswas';

    protected $fillable = [
        'tanggal',
        'nomor_rekening',
        'nama_siswa',
        'deskripsi',
        'kas_kredit',
        'dibuat',
        'diperiksa',
        'disetujui'
    ];
}
