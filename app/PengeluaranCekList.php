<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranCekList extends Model
{
    protected $table = 'pengeluaran_cek_lists';

    protected $fillable = [
        'list',
        'deskripsi',
        'status',
        'percent'
    ];
}
