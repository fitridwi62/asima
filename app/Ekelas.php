<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ekelas extends Model
{
    protected $table = "ekelas";

    protected $fillable = [
        'data_guru_id',
        'kelas_id',
        'grade',
        'deskripsi'
    ];

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }
}
