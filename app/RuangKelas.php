<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RuangKelas extends Model
{
    protected $table = "ruang_kelas";

    protected $fillable = [
    	'kode_ruangan',
    	'nama_ruangan'
    ];
}
