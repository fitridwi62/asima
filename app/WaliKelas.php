<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaliKelas extends Model
{
    protected $table = "wali_kelas";
    protected $fillable = [
    	'kurikulum_id',
    	'data_guru_id',
    	'kode_mata_pelajaran',
    	'mata_pelajaran',
    	'kelas'
    ];

    public function data_guru(){
    	return $this->belongsTo('App\DataGuru');
    }

    public function kurikulum(){
    	return $this->belongsTo('App\Kurikulum');
    }
}
