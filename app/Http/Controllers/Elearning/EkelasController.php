<?php

namespace App\Http\Controllers\Elearning;

use Redirect;
use Validator;
use App\Ekelas;
use App\DataGuru;
use App\Kelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class EkelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $ekelass                = Ekelas::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        $kelass                 = Kelas::all();
        return view('elearning.kelas.index', [
            'ekelass'           => $ekelass,
            'kelass'            => $kelass
        ]);
    }

    public function create()
    {
        $kelass         = Kelas::all();
        $dataGurus      = DataGuru::all();
        return view('elearning.kelas.create', [
            'kelass'    => $kelass,
            'dataGurus' => $dataGurus
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'grade'     => 'required|numeric',
            'deskripsi' => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $ekelas                     = new Ekelas;
            $ekelas->data_guru_id       = $this->request->data_guru_id;
            $ekelas->kelas_id           = $this->request->kelas_id;
            $ekelas->grade              = $this->request->grade;
            $ekelas->deskripsi          = $this->request->deskripsi;
            $ekelas->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Elearning Kelas Sukses disimpan"
            ]);
            return redirect()->route('e-kelas.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('e-kelas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelass         = Kelas::all();
        $dataGurus      = DataGuru::all();
        $ekelas         = Ekelas::findOrFail($id);
        return view('elearning.kelas.edit', [
            'kelass'    => $kelass,
            'dataGurus' => $dataGurus,
            'ekelas'    => $ekelas
        ]);
    }

    public function update($id)
    {
        $validate      = Validator::make($this->request->all(), [
            'grade'     => 'required|numeric',
            'deskripsi' => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $ekelas                     = Ekelas::findOrFail($id);
            $ekelas->data_guru_id       = $this->request->data_guru_id;
            $ekelas->kelas_id           = $this->request->kelas_id;
            $ekelas->grade              = $this->request->grade;
            $ekelas->deskripsi          = $this->request->deskripsi;
            $ekelas->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Elearning Kelas Sukses disimpan"
            ]);
            return redirect()->route('e-kelas.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('e-kelas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $ekelas         = Ekelas::findOrFail($id);
        $ekelas->delete();
        return redirect()->back();
    }
}
