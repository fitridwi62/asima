<?php

namespace App\Http\Controllers\Elearning;

use Redirect;
use Validator;
use App\DataGuru;
use App\Epengumuman;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class EpengumumanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $epengumumans           = Epengumuman::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        return view('elearning.pengumuman.index', [
            'epengumumans'      => $epengumumans
        ]);
    }

    public function create()
    {
        $dataGurus          = DataGuru::all();
        return view('elearning.pengumuman.create',[
            'dataGurus'     => $dataGurus
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'nama'          => 'required|min:4',
            'tanggal'       => 'required|date',
            'deskripsi'     => 'required|min:4',
            'status'        => 'required'
        ]);
        
        if(!$validate->fails()){
            $pengumuman                     = new Epengumuman;
            $pengumuman->data_guru_id       = $this->request->data_guru_id;
            $pengumuman->nama               = $this->request->nama;
            $pengumuman->tanggal            = $this->request->tanggal;
            $pengumuman->deskripsi          = $this->request->deskripsi;
            $pengumuman->status             = $this->request->status;
            $pengumuman->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Data pengumuman berhasil disimapan"
            ]);
            return redirect()->route('pengumuman.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('pengumuman.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataGurus          = DataGuru::all();
        $pengumuman         = Epengumuman::findOrFail($id);
        return view('elearning.pengumuman.edit',[
            'dataGurus'     => $dataGurus,
            'pengumuman'    => $pengumuman
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:4',
            'tanggal'       => 'required|date',
            'deskripsi'     => 'required|min:4',
            'status'        => 'required'
        ]);
        
        if(!$validate->fails()){
            $pengumuman                     = Epengumuman::findOrFail($id);
            $pengumuman->data_guru_id       = $this->request->data_guru_id;
            $pengumuman->nama               = $this->request->nama;
            $pengumuman->tanggal            = $this->request->tanggal;
            $pengumuman->deskripsi          = $this->request->deskripsi;
            $pengumuman->status             = $this->request->status;
            $pengumuman->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Data pengumuman berhasil disimapan"
            ]);
            return redirect()->route('pengumuman.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('pengumuman.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $pengumuman         = Epengumuman::findOrFail($id);
        $pengumuman->delete();
        return redirect()->back();
    }
}
