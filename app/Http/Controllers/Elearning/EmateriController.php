<?php

namespace App\Http\Controllers\Elearning;

use Redirect;
use Validator;
use App\Kelas;
use App\Emateri;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class EmateriController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $emateris       = Emateri::orderBy('created_at', 'DESC')
                                ->paginate($this->limit);
        $kelass          = Kelas::all();
        return view('elearning.materi.index',[
            'emateris'  => $emateris,
            'kelass'    => $kelass
        ]);
    }

    public function create()
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        return view('elearning.materi.create', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans
        ]); 
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            //'file'          => 'required|mimes:pdf|max:10000',
            'deskripsi'     => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $emateri                        = new Emateri;
            $emateri->data_guru_id          = $this->request->data_guru_id;
            $emateri->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $emateri->jurusan_id            = $this->request->jurusan_id;
            $emateri->kelas_id              = $this->request->kelas_id;
            $emateri->nama                  = $this->request->nama;
            //process upload file
            $file                           = $this->request->file('file');
            $fileName                       = $file->getClientOriginalName();
            $this->request->file('file')->move("materi/", $fileName);
            $emateri->file                  = $fileName;

            $emateri->deskripsi             = $this->request->deskripsi;
            $emateri->status                = $this->request->status;
            $emateri->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Materi <strong>$emateri->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("materi.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('materi.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        $emateri            = Emateri::findOrFail($id);
        return view('elearning.materi.edit', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans,
            'emateri'           => $emateri
        ]); 
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            //'file'          => 'required|mimes:pdf|max:10000',
            'deskripsi'     => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $emateri                        = Emateri::findOrFail($id);
            $emateri->data_guru_id          = $this->request->data_guru_id;
            $emateri->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $emateri->jurusan_id            = $this->request->jurusan_id;
            $emateri->kelas_id              = $this->request->kelas_id;
            $emateri->nama                  = $this->request->nama;
            //process upload file
            if($this->request->hasFile('file') == ""){
                $emateri->file = $emateri->file;
            } else{
                $file       = $this->request->file('file');
                $fileName   = $file->getClientOriginalName();
                $request->file('file')->move("materi/", $fileName);
                $emateri->file = $fileName;
            }

            $emateri->deskripsi             = $this->request->deskripsi;
            $emateri->status                = $this->request->status;
            $emateri->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Materi <strong>$emateri->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("materi.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('materi.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $emateri            = Emateri::findOrFail($id);
        $emateri->delete();
        return redirect()->back();
    }
}
