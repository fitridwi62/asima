<?php

namespace App\Http\Controllers\Elearning;

use Redirect;
use Validator;
use App\Kelas;
use App\Equiz;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class EquizController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $equizs             = Equiz::orderBy('created_at', 'DESC')
                                    ->paginate($this->limit);
        $kelass             = Kelas::all();
        return view('elearning.quiz.index', [
            'equizs'        => $equizs,
            'kelass'        => $kelass
        ]);
    }

    public function create()
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        return view('elearning.quiz.create', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            'nilai'          => 'required|numeric',
            'deskripsi'     => 'required|min:4',
            'batas_waktu'   => 'required'
        ]);

        if(!$validate->fails()){
            $quiz                        = new Equiz;
            $quiz->data_guru_id          = $this->request->data_guru_id;
            $quiz->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $quiz->jurusan_id            = $this->request->jurusan_id;
            $quiz->kelas_id              = $this->request->kelas_id;
            $quiz->nama                  = $this->request->nama;
            $quiz->nilai                  = $this->request->nilai;
            $quiz->deskripsi             = $this->request->deskripsi;
            $quiz->batas_waktu           = $this->request->batas_waktu;
            //process upload file
            if($this->request->hasFile('file') == ""){
                $quiz->file = $quiz->file;
            } else{
                $file       = $this->request->file('file');
                $fileName   = $file->getClientOriginalName();
                $this->request->file('file')->move("quiz/", $fileName);
                $quiz->file = $fileName;
            }
            $quiz->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Quiz <strong>$quiz->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("quiz.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('quiz.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        $quiz               = Equiz::findOrFail($id);
        return view('elearning.quiz.edit', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans,
            'quiz'              => $quiz
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            'nilai'          => 'required|numeric',
            'deskripsi'     => 'required|min:4',
            'batas_waktu'   => 'required'
        ]);

        if(!$validate->fails()){
            $quiz                        = Equiz::findOrFail($id);
            $quiz->data_guru_id          = $this->request->data_guru_id;
            $quiz->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $quiz->jurusan_id            = $this->request->jurusan_id;
            $quiz->kelas_id              = $this->request->kelas_id;
            $quiz->nama                  = $this->request->nama;
            $quiz->nilai                 = $this->request->nilai;
            $quiz->deskripsi             = $this->request->deskripsi;
            $quiz->batas_waktu           = $this->request->batas_waktu;
            //process upload file
            if($this->request->hasFile('file') == ""){
                $quiz->file = $quiz->file;
            } else{
                $file       = $this->request->file('file');
                $fileName   = $file->getClientOriginalName();
                $this->request->file('file')->move("quiz/", $fileName);
                $quiz->file = $fileName;
            }
            $quiz->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Quiz <strong>$quiz->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("quiz.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('quiz.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $quiz               = Equiz::findOrFail($id);
        $quiz->delete();
        return redirect()->back();
    }
}
