<?php

namespace App\Http\Controllers\Elearning;

use Redirect;
use Validator;
use App\Kelas;
use App\ElearningTugas;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class EtugasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $etugass            = ElearningTugas::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        $kelass             = Kelas::all();
        return view('elearning.tugas.index', [
            'etugass'       => $etugass,
            'kelass'        => $kelass
        ]);
    }

    public function create()
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        return view('elearning.tugas.create', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            'file'          => 'required',
            'deskripsi'     => 'required|min:4',
            'batas_waktu'   => 'required'
        ]);

        if(!$validate->fails()){
            $etugas                        = new ElearningTugas;
            $etugas->data_guru_id          = $this->request->data_guru_id;
            $etugas->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $etugas->jurusan_id            = $this->request->jurusan_id;
            $etugas->kelas_id              = $this->request->kelas_id;
            $etugas->nama                  = $this->request->nama;
            //process upload file
            if($this->request->hasFile('file') == ""){
                $etugas->file = $etugas->file;
            } else{
                $file       = $this->request->file('file');
                $fileName   = $file->getClientOriginalName();
                $this->request->file('file')->move("tugas/", $fileName);
                $etugas->file = $fileName;
            }
            $etugas->deskripsi             = $this->request->deskripsi;
            $etugas->batas_waktu           = $this->request->batas_waktu;
            $etugas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Tugas <strong>$etugas->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("tugas.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('tugas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataGurus          = DataGuru::all();
        $kelass             = Kelas::all();
        $jurusans           = Jurusan::all();
        $mataPelajarans     = MataPelajaran::all();
        $etugas             = ElearningTugas::findOrFail($id);
        return view('elearning.tugas.edit', [
            'dataGurus'         => $dataGurus,
            'kelass'            => $kelass,
            'jurusans'          => $jurusans,
            'mataPelajarans'    => $mataPelajarans,
            'etugas'            => $etugas
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4',
            'file'          => 'required',
            'deskripsi'     => 'required|min:4',
            'batas_waktu'   => 'required'
        ]);

        if(!$validate->fails()){
            $etugas                        = ElearningTugas::findOrFail($id);
            $etugas->data_guru_id          = $this->request->data_guru_id;
            $etugas->mata_pelajaran_id     = $this->request->mata_pelajaran_id;
            $etugas->jurusan_id            = $this->request->jurusan_id;
            $etugas->kelas_id              = $this->request->kelas_id;
            $etugas->nama                  = $this->request->nama;
            //process upload file
            if($this->request->hasFile('file') == ""){
                $etugas->file = $etugas->file;
            } else{
                $file       = $this->request->file('file');
                $fileName   = $file->getClientOriginalName();
                $this->request->file('file')->move("tugas/", $fileName);
                $etugas->file = $fileName;
            }
            $etugas->deskripsi             = $this->request->deskripsi;
            $etugas->batas_waktu           = $this->request->batas_waktu;
            $etugas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Tugas <strong>$etugas->nama</strong> berhasil disimpan"
            ]);
            return redirect()->route("tugas.index");
        }else{
            $errors         = $validate->messages();
            return redirect()->route('tugas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $etugas             = ElearningTugas::findOrFail($id);
        $etugas->delete();
        return redirect()->back();
    }
}
