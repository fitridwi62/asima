<?php

namespace App\Http\Controllers\Staff;

use Redirect;
use Validator;
use App\Agama;
use App\Jabatan;
use App\StaffPegawai;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }


    public function index()
    {
        $staffs         = StaffPegawai::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('staff.index', [
            'staffs'    => $staffs
        ]);
    }

    public function create()
    {
        $agamas         = Agama::all();
        $jabatans       = Jabatan::all();
        return view('staff.create', [
            'agamas'    => $agamas,
            'jabatans'  => $jabatans
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama_lengkap'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $staff                          = new StaffPegawai;
            $staff->nama_lengkap            = $this->request->nama_lengkap;
            $staff->jabatan_id              = $this->request->jabatan_id;
            $staff->jenis_kelamin           = $this->request->jenis_kelamin;
            $staff->tempat_lahir            = $this->request->tempat_lahir;
            $staff->tanggal_lahir           = $this->request->tanggal_lahir;
            $staff->email                   = $this->request->email;
            $staff->phone                   = $this->request->phone;
            $staff->wa                      = $this->request->wa;
            $staff->agama_id                = $this->request->agama_id;
            $staff->alamat                  = $this->request->alamat;
            $staff->status                  = $this->request->status;
            $staff->save();
            return redirect()->route('staff.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('staff.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        $staff          = StaffPegawai::findOrFail($id);
        return view('staff', [
            'staff'     => $staff
        ]);
    }

    public function edit($id)
    {
        $agamas         = Agama::all();
        $jabatans       = Jabatan::all();
        $staff          = StaffPegawai::findOrFail($id);
        return view('staff.edit', [
            'agamas'    => $agamas,
            'jabatans'  => $jabatans,
            'staff'     => $staff
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama_lengkap'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $staff                          = StaffPegawai::findOrFail($id);
            $staff->nama_lengkap            = $this->request->nama_lengkap;
            $staff->jabatan_id              = $this->request->jabatan_id;
            $staff->jenis_kelamin           = $this->request->jenis_kelamin;
            $staff->tempat_lahir            = $this->request->tempat_lahir;
            $staff->tanggal_lahir           = $this->request->tanggal_lahir;
            $staff->email                   = $this->request->email;
            $staff->phone                   = $this->request->phone;
            $staff->wa                      = $this->request->wa;
            $staff->agama_id                = $this->request->agama_id;
            $staff->alamat                  = $this->request->alamat;
            $staff->status                  = $this->request->status;
            $staff->save();
            return redirect()->route('staff.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('staff.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $staff          = StaffPegawai::findOrFail($id);
        $staff->delete();
        return redirect()->back();
    }
}
