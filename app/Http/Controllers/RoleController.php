<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(){
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    public function create(){
        $permissions = Permission::all();
        return view('roles.create', compact('permissions'));
    }

    public function store(Request $request){
        $role = new Role;
        $role->name = $request->name;
        $role->guard_name = $request->guard_name;
        $role->save();
        $role->permissions()->sync($request->permissions);
        return redirect()->route('roles.index')->withSuccess('Role data saved');
    }

    public function edit($id){
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        return view('roles.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, $id){
        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->guard_name = $request->guard_name;
        $role->save();
        $role->permissions()->sync($request->permissions);
        return redirect()->route('roles.index')->withSuccess('Role data updated');
    }

    public function delete($id){
        $role = Role::findOrFail($id);
        $role->delete();
        $role->permissions()->sync([]);
        return redirect()->route('roles.index')->withSuccess('Role data deleted');
    }
}
