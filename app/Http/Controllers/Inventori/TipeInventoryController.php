<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\TipeInventory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class TipeInventoryController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $tipeInventories        = TipeInventory::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('inventori.tipe-inventori.index', [
            'tipeInventories'   => $tipeInventories
        ]);
    }

    public function create()
    {
        return view('inventori.tipe-inventori.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'  => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipeInventory              = new TipeInventory;
            $tipeInventory->nama        = $this->request->nama;
            $tipeInventory->save();
            return redirect()->route('tipe-inventori.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('tipe-inventori.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tipeInventory          = TipeInventory::findOrFail($id);
        return view('inventori.tipe-inventori.edit', [
            'tipeInventory'     => $tipeInventory
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'  => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipeInventory              = TipeInventory::findOrFail($id);
            $tipeInventory->nama        = $this->request->nama;
            $tipeInventory->save();
            return redirect()->route('tipe-inventori.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('tipe-inventori.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $tipeInventory          = TipeInventory::findOrFail($id);
        $tipeInventory->delete();
        return redirect()->back();
    }
}
