<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Asset;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class AssetController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $assets         = Asset::orderBy('created_at', 'DESC')
                                ->paginate($this->limit);
        return view('inventori.asset.index', [
            'assets'    => $assets
        ]);
    }

    public function create()
    {
        return view('inventori.asset.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'nama_asset'     => 'required|min:2',
            'no_asset'       => 'required',
            'tanggal'        => 'required|date',
            'biaya_akuisisi' => 'required',
            'kredit_to'      => 'required'
        ]);

        if(!$validate->fails()){
            $asset                  = new Asset;
            $asset->nama_asset      = $this->request->nama_asset;
            $asset->no_asset        = $this->request->no_asset;
            $asset->asset_tetap     = $this->request->asset_tetap;
            $asset->tanggal         = $this->request->tanggal;
            $asset->biaya_akuisisi  = $this->request->biaya_akuisisi;
            $asset->deskripsi       = $this->request->deskripsi;
            $asset->kredit_to       = $this->request->kredit_to;
            $asset->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Asset <strong> $asset->nama_asset </strong> berhasil."
            ]);
            return redirect()->route('asset.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('asset.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $asset          = Asset::findOrFail($id);
        return view('inventori.asset.edit', [
            'asset'     => $asset
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'nama_asset'     => 'required|min:4',
            'no_asset'       => 'required',
            'tanggal'        => 'required|date',
            'biaya_akuisisi' => 'required',
            'kredit_to'      => 'required'
        ]);

        if(!$validate->fails()){
            $asset                  = Asset::findOrFail($id);
            $asset->nama_asset      = $this->request->nama_asset;
            $asset->no_asset        = $this->request->no_asset;
            $asset->asset_tetap     = $this->request->asset_tetap;
            $asset->tanggal         = $this->request->tanggal;
            $asset->biaya_akuisisi  = $this->request->biaya_akuisisi;
            $asset->deskripsi       = $this->request->deskripsi;
            $asset->kredit_to       = $this->request->kredit_to;
            $asset->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Asset berhasil di ubah."
            ]);
            return redirect()->route('asset.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('asset.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $asset                  = Asset::findOrFail($id);
        $asset->delete();
        return redirect()->back();
    }
}
