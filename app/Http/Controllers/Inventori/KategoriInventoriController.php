<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\KategoriInventori;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class KategoriInventoriController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $kategoriInventories        = KategoriInventori::orderBy('created_at', 'DESC')
                                                        ->paginate($this->limit);
        return view('inventori.kategori-inventori.index', [
            'kategoriInventories'   => $kategoriInventories
        ]);
    }

    public function create()
    {
        return view('inventori.kategori-inventori.create');
    }


    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $kategoriInventori          = new KategoriInventori;
            $kategoriInventori->nama    = $this->request->nama;
            $kategoriInventori->save();
            return redirect()->route('kategori-inventori.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('kategori-inventori.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kategoriInventori          = KategoriInventori::findOrFail($id);
        return view('inventori.kategori-inventori.edit', [
            'kategoriInventori'     => $kategoriInventori
        ]);
    }

    public function update($id)
    {
        $validate                   = Validator::make($this->request->all(), [
            'nama'           => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $kategoriInventori          = KategoriInventori::findOrFail($id);
            $kategoriInventori->nama    = $this->request->nama;
            $kategoriInventori->save();
            return redirect()->route('kategori-inventori.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('kategori-inventori.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $kategoriInventori          = KategoriInventori::findOrFail($id);
        $kategoriInventori->delete();
        return redirect()->back();
    }
}
