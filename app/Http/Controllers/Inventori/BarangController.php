<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Barang;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class BarangController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $barangs            = Barang::orderBy('created_at', 'DESC')
                                    ->paginate($this->limit);
        return view('inventori.barang.index', [
            'barangs'       => $barangs
        ]);
    }

    public function create()
    {
        return view('inventori.barang.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'kode'          => 'required|min:2',
            'nama'          => 'required|min:4',
            'unit'          => 'required',
            'tanggal'       => 'required|date'
        ]);

        if(!$validate->fails()){
            $barang                 = new Barang;
            $barang->kode           = $this->request->kode;
            $barang->nama           = $this->request->nama;
            $barang->unit           = $this->request->unit;
            $barang->tanggal        = $this->request->tanggal;
            $barang->description    = $this->request->description;
            $barang->status         = $this->request->status;
            $barang->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang berhasil."
            ]);
            return redirect()->route('barang.index');
        }else{
            $errors                 = $validate->messages();
            return redirect()->route('barang.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $barang             = Barang::findOrFail($id);
        return view('inventori.barang.edit', [
            'barang'        => $barang
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'kode'          => 'required|min:2',
            'nama'          => 'required|min:4',
            'unit'          => 'required',
            'tanggal'       => 'required|date'
        ]);

        if(!$validate->fails()){
            $barang                 = Barang::findOrFail($id);
            $barang->kode           = $this->request->kode;
            $barang->nama           = $this->request->nama;
            $barang->unit           = $this->request->unit;
            $barang->tanggal        = $this->request->tanggal;
            $barang->description    = $this->request->description;
            $barang->status         = $this->request->status;
            $barang->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang berhasil diupdate."
            ]);
            return redirect()->route('barang.index');
        }else{
            $errors                 = $validate->messages();
            return redirect()->route('barang.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $barang             = Barang::findOrFail($id);
        $barang->delete();
        return redirect()->back();
    }
}
