<?php

namespace App\Http\Controllers\Inventori;

use Alert;
use Redirect;
use Validator;
use App\TipeDonasi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class TipeDonasiController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $tipeDonasis            = TipeDonasi::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        return view('inventori.tipe-donasi.index', [
            'tipeDonasis'       => $tipeDonasis
        ]);
    }

    public function create()
    {
        return view('inventori.tipe-donasi.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipeDonasi             = new TipeDonasi;
            $tipeDonasi->nama       = $this->request->nama;
            $tipeDonasi->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Tipe Donasi <strong> $tipeDonasi->nama </strong> berhasil."
            ]);
            return redirect()->route('tipe-donasi.index');
        }else{
            $errors             = $validate->messages();

            return redirect()->route('tipe-donasi.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tipeDonasi             = TipeDonasi::findOrFail($id);
        return view('inventori.tipe-donasi.edit', [
            'tipeDonasi'        => $tipeDonasi
        ]);

    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipeDonasi             = TipeDonasi::findOrFail($id);
            $tipeDonasi->nama       = $this->request->nama;
            $tipeDonasi->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Tipe Donasi <strong> $tipeDonasi->nama </strong> berhasil."
            ]);
            return redirect()->route('tipe-donasi.index');
        }else{
            $errors             = $validate->messages();

            return redirect()->route('tipe-donasi.edit')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $tipeDonasi             = TipeDonasi::findOrFail($id);
        $tipeDonasi->delete();
        Session::flash("flash_notification", [
            "level"             => "danger",
            "message"           => "Tipe Donasi <strong> $tipeDonasi->nama </strong> berhasil."
        ]);
        return redirect()->back();
    }
}
