<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Barang;
use App\BarangKeluar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class BarangKeluarController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $barangKeluars          = BarangKeluar::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        $barangs                = Barang::all();
        return view('inventori.barang-keluar.index', [
            'barangKeluars'     => $barangKeluars,
            'barangs'           => $barangs
        ]);
    }

    public function create()
    {
        $barangs                 = Barang::all();
        return view('inventori.barang-keluar.create',[
            'barangs'           => $barangs
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'harga'         => 'required',
            'tanggal'       => 'required|date',
            'author'        => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $barangKeluar               = new BarangKeluar;
            $barangKeluar->nama         = $this->request->nama;
            $barangKeluar->barang_id    = $this->request->barang_id;
            $barangKeluar->harga        = $this->request->harga;
            $barangKeluar->tanggal      = $this->request->tanggal;
            $barangKeluar->author       = $this->request->author;
            $barangKeluar->status       = $this->request->status;
            $barangKeluar->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang Keluar berhasil di simpan."
            ]);
            return redirect()->route('barang-keluar.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('barang-keluar.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all()); 
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $barangs                = Barang::all();
        $barangKeluar           = BarangKeluar::findOrFail($id);
        return view('inventori.barang-keluar.edit',[
            'barangs'           => $barangs,
            'barangKeluar'      => $barangKeluar
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'harga'         => 'required',
            'tanggal'       => 'required|date',
            'author'        => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $barangKeluar               = BarangKeluar::findOrFail($id);
            $barangKeluar->nama         = $this->request->nama;
            $barangKeluar->barang_id    = $this->request->barang_id;
            $barangKeluar->harga        = $this->request->harga;
            $barangKeluar->tanggal      = $this->request->tanggal;
            $barangKeluar->author       = $this->request->author;
            $barangKeluar->status       = $this->request->status;
            $barangKeluar->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang Keluar berhasil di update."
            ]);
            return redirect()->route('barang-keluar.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('barang-keluar.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all()); 
        }
    }

    public function destroy($id)
    {
        $barangKeluar           = BarangKeluar::findOrFail($id);
        $barangKeluar->delete();
        return redirect()->back();
    }
}
