<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Donasi;
use App\TipeDonasi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class DonasiController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $donasis            = Donasi::orderBy('created_at', 'DESC')
                                    ->paginate($this->limit);
        $tipeDonasis        = TipeDonasi::all();
        return view('inventori.donasi.index', [
            'donasis'       => $donasis,
            'tipeDonasis'   => $tipeDonasis
        ]);
    }

    public function create()
    {
        $tipeDonasis        = TipeDonasi::all();
        return view('inventori.donasi.create', [
            'tipeDonasis'   => $tipeDonasis
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'       => 'required|date',
            'harga'         => 'required',
            'total'         => 'required'
        ]);

        if(!$validate->fails()){
            $donasi                     = new Donasi;
            $donasi->tipe_donasi_id     = $this->request->tipe_donasi_id;
            $donasi->tanggal            = $this->request->tanggal;
            $donasi->harga              = $this->request->harga;
            $donasi->total              = $this->request->total;
            $donasi->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Donasi berhasil."
            ]);
            return redirect()->route('donasi.index');
        }else{
            $errors     = $validate->messages();

            return redirect()->route('donasi.create')
                            ->withErros($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tipeDonasis        = TipeDonasi::all();
        $donasi             = Donasi::findOrFail($id);
        return view('inventori.donasi.edit', [
            'tipeDonasis'   => $tipeDonasis,
            'donasi'        => $donasi
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'       => 'required|date',
            'harga'         => 'required',
            'total'         => 'required'
        ]);

        if(!$validate->fails()){
            $donasi                     = Donasi::findOrFail($id);
            $donasi->tipe_donasi_id     = $this->request->tipe_donasi_id;
            $donasi->tanggal            = $this->request->tanggal;
            $donasi->harga              = $this->request->harga;
            $donasi->total              = $this->request->total;
            $donasi->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Donasi berhasil di update."
            ]);
            return redirect()->route('donasi.index');
        }else{
            $errors     = $validate->messages();

            return redirect()->route('donasi.create')
                            ->withErros($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $donasi             = Donasi::findOrFail($id);
        $donasi->delete();
        return redirect()->back();
    }
}
