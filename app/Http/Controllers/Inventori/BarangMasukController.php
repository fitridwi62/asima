<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Barang;
use App\BarangMasuk;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class BarangMasukController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $barangMasuks           = BarangMasuk::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        $barangs                = Barang::all();
        return view('inventori.barang-masuk.index', [
            'barangMasuks'      => $barangMasuks,
            'barangs'           => $barangs
        ]);
    }

    public function create()
    {
        $barangs            = Barang::all();
        return view('inventori.barang-masuk.create', [
            'barangs'       => $barangs
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'harga'         => 'required',
            'tanggal'       => 'required|date'
        ]);

        if(!$validate->fails()){
            $barangMasuk            = new BarangMasuk;
            $barangMasuk->nama      = $this->request->nama;
            $barangMasuk->barang_id = $this->request->barang_id;
            $barangMasuk->harga     = $this->request->harga;
            $barangMasuk->tanggal   = $this->request->tanggal;
            $barangMasuk->status    = $this->request->status;
            $barangMasuk->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang Masuk berhasil di simpan."
            ]);
            return redirect()->route('barang-masuk.index');

        }else{
            $errors         = $validate->messages();
            return redirect()->route('barang-masuk.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $barangs            = Barang::all();
        $barangMasuk        = BarangMasuk::findOrFail($id);
        return view('inventori.barang-masuk.edit', [
            'barangs'       => $barangs,
            'barangMasuk'   => $barangMasuk
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'harga'         => 'required',
            'tanggal'       => 'required|date'
        ]);

        if(!$validate->fails()){
            $barangMasuk            = BarangMasuk::findOrFail($id);
            $barangMasuk->nama      = $this->request->nama;
            $barangMasuk->barang_id = $this->request->barang_id;
            $barangMasuk->harga     = $this->request->harga;
            $barangMasuk->tanggal   = $this->request->tanggal;
            $barangMasuk->status    = $this->request->status;
            $barangMasuk->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Barang Masuk berhasil di simpan."
            ]);
            return redirect()->route('barang-masuk.index');

        }else{
            $errors         = $validate->messages();
            return redirect()->route('barang-masuk.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $barangMasuk        = BarangMasuk::findOrFail($id);
        $barangMasuk->delete();
        return redirect()->back();
    }
}
