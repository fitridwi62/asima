<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\Gedung;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class GedungController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $gedungs                = Gedung::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('inventori.gedung.index', [
            'gedungs'           => $gedungs
        ]);
    }

    public function create()
    {
        return view('inventori.gedung.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'kode'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $gedung         = new Gedung;
            $gedung->nama   = $this->request->nama;
            $gedung->kode   = $this->request->kode;
            $gedung->save();
            return redirect()->route('gedung.index');
        }else{
            $errors =   $validate->messages();

            return redirect()->route('gedung.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $gedung         = Gedung::findOrFail($id);
        return view('inventori.gedung.edit', [
            'gedung'    => $gedung
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'nama'          => 'required|min:2',
            'kode'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $gedung         = Gedung::findOrFail($id);
            $gedung->nama   = $this->request->nama;
            $gedung->kode   = $this->request->kode;
            $gedung->save();
            return redirect()->route('gedung.index');
        }else{
            $errors =   $validate->messages();

            return redirect()->route('gedung.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $gedung         = Gedung::findOrFail($id);
        $gedung->delete();
        return redirect()->back();
    }
}
