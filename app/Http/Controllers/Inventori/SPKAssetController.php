<?php

namespace App\Http\Controllers\Inventori;

use Redirect;
use Validator;
use App\SpkAsset;
use App\Gedung;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class SPKAssetController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $spkAssets              = SpkAsset::orderBy('created_at')
                                            ->paginate($this->limit);
        $gedungs                = Gedung::all();
        return view('inventori.spk-asset.index', [
            'spkAssets'         => $spkAssets,
            'gedungs'           => $gedungs
        ]);
    }

    public function create()
    {
        $gedungs        = Gedung::all();
        return view('inventori.spk-asset.create',[
            'gedungs'   => $gedungs
        ]);
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(),[
            'tipe_asset'        => 'required',
            'nama'              => 'required|min:2',
            'total'             => 'required'
        ]);

        if(!$validate->fails()){
            $spkAsset                   = new SpkAsset;
            $spkAsset->tipe_asset       = $this->request->tipe_asset;
            $spkAsset->nama             = $this->request->nama;
            $spkAsset->gedung_id        = $this->request->gedung_id;
            $spkAsset->total            = $this->request->total;
            $spkAsset->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "SPK Asset berhasil Disimpan."
            ]);
            return redirect()->route('spk-asset.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('spk-asset.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //=
    }

    public function edit($id)
    {
        $gedungs        = Gedung::all();
        $spkAsset       = SpkAsset::findOrFail($id);
        return view('inventori.spk-asset.edit',[
            'gedungs'   => $gedungs,
            'spkAsset'  => $spkAsset
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(),[
            'tipe_asset'        => 'required',
            'nama'              => 'required|min:2',
            'total'             => 'required'
        ]);

        if(!$validate->fails()){
            $spkAsset                   = SpkAsset::findOrFail($id);
            $spkAsset->tipe_asset       = $this->request->tipe_asset;
            $spkAsset->nama             = $this->request->nama;
            $spkAsset->gedung_id        = $this->request->gedung_id;
            $spkAsset->total            = $this->request->total;
            $spkAsset->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "SPK Asset berhasil Diupdate."
            ]);
            return redirect()->route('spk-asset.index');
        }else{
            $errors         = $validate->messages();

            return redirect()->route('spk-asset.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $spkAsset       = SpkAsset::findOrFail($id);
        $spkAsset->delete();
        return redirect()->back();
    }
}
