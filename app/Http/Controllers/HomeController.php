<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('fe.fe.index');
    }

    public function getProfile(){
        return view('fe.profile.index');
    }
}
