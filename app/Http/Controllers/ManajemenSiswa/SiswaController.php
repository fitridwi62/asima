<?php

namespace App\Http\Controllers\ManajemenSiswa;

use Auth;
use Image;
use Redirect;
use Validator;
use App\Agama;
use App\Jenjang;
use App\Kelas;
use App\Siswa;
use App\TahunAkademik;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $siswas         = Siswa::orderBy('created_at', 'DESC')
                                ->with('agama')
                                ->with('jenjang')
                                ->with('kelas')
                                ->paginate($this->limit);
        $kelass         = Kelas::all();
        $jenjangs       = Jenjang::all();
        return view('manajemen_siswa.siswa.index', [
            'siswas'        => $siswas,
            'kelass'        => $kelass,
            'jenjangs'      => $jenjangs
        ]);
    }

    public function create()
    {
        $agamas         = Agama::all();
        $jenjangs       = Jenjang::all();
        $kelas          = Kelas::all();
        $tahunAkademiks = TahunAkademik::all();
        return view('manajemen_siswa.siswa.create', [
            'agamas'            => $agamas,
            'jenjangs'          => $jenjangs,
            'kelas'             => $kelas,
            'tahunAkademiks'     => $tahunAkademiks
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'akte_photo'                => 'required',
            'kk_photo'                  => 'required',
            'surat_pernyataan_photo'    => 'required',
            'photo'                     => 'required',
            'nis'                       => 'required', 
            'nisn'                       => 'required', 
            'nama_lengkap'              => 'required', 
            'nama_panggilan'            => 'required', 
            'nomor_formulir'            => 'required', 
            'tempat_lahir'              => 'required', 
            'tanggal_lahir'             => 'required', 
            'jenis_kelamin'             => 'required', 
            'phone'                     => 'required', 
            'umur'                      => 'required',
            'nama_ayah_kandung'         => 'required',
            'lulusan_sekolah_ortu'      => 'required',
            'pekerjaan_ortu'            => 'required',
            'jarak_rumah'               => 'required',
            'no_telp_ortu'              => 'required',
            'penghasilan_ortu'          => 'required',
            'nama_ibu_kandung'          => 'required',
            'pekerjaan_ibu'             => 'required',
        ]);
        if(!$validate->fails()){
        $user_id = Auth::user()->id;
        $siswa                      = new Siswa;
        $siswa->nis                  = $this->request->nis;
        $siswa->nisn                 = $this->request->nisn;
        $siswa->nama_lengkap         = $this->request->nama_lengkap;
        $siswa->nama_panggilan       = $this->request->nama_panggilan;
        $siswa->nomor_formulir       = $this->request->nomor_formulir;
        $siswa->tempat_lahir         = $this->request->tempat_lahir;
        $siswa->tanggal_lahir        = $this->request->tanggal_lahir;
        $siswa->jenis_kelamin        = $this->request->jenis_kelamin;
        $siswa->agama_id             = $this->request->agama_id;
        $siswa->jenjang_id           = $this->request->jenjang_id;
        $siswa->kelas_id             = $this->request->kelas_id;
        $siswa->tahun_akademik_id    = $this->request->tahun_akademik_id;
        $siswa->wali_kelas           = $this->request->wali_kelas;
        $siswa->phone                = $this->request->phone; 
        $siswa->umur                 = $this->request->umur;
        $siswa->anak_ke              = $this->request->anak_ke;  
        $siswa->suku                 = $this->request->suku;
        $siswa->nama_ayah_kandung    = $this->request->nama_ayah_kandung;
        $siswa->lulusan_sekolah_ortu = $this->request->lulusan_sekolah_ortu;
        $siswa->pekerjaan_ortu       = $this->request->pekerjaan_ortu;
        $siswa->jarak_rumah          = $this->request->jarak_rumah;
        $siswa->no_telp_ortu         = $this->request->no_telp_ortu;
        $siswa->penghasilan_ortu     = $this->request->penghasilan_ortu;
        $siswa->nama_ibu_kandung     = $this->request->nama_ibu_kandung;
        $siswa->pekerjaan_ibu        = $this->request->pekerjaan_ibu;
        //akte photo
        $file_akte                   = $this->request->file('akte_photo');
        $fileName_akte               = $file_akte->getClientOriginalName();
        $path_akte                   = public_path('siswa/akte/' . $fileName_akte);
        Image::make($file_akte->getRealPath())->resize(1000, 1000)->save($path_akte);
        $siswa->akte_photo           = $fileName_akte;
        //kk photo
        $file_kk                     = $this->request->file('kk_photo');
        $fileName_kk                 = $file_kk->getClientOriginalName();
        $path_kk                     = public_path('siswa/akte/' . $fileName_kk);
        Image::make($file_kk->getRealPath())->resize(1000, 1000)->save($path_kk);
        $siswa->kk_photo             = $fileName_kk;
        //surat pernyataan
        $file_pernyataan             = $this->request->file('surat_pernyataan_photo');
        $fileName_pernyataan         = $file_pernyataan->getClientOriginalName();
        $path_pernyataan             = public_path('siswa/pernyataan/' . $fileName_pernyataan);
        Image::make($file_pernyataan->getRealPath())->resize(1000, 1000)->save($path_pernyataan);
        $siswa->surat_pernyataan_photo  = $file_pernyataan;
        //photo siswa
        $file                        = $this->request->file('photo');
        $fileName                    = $file->getClientOriginalName();
        $path                        = public_path('siswa/' . $fileName);
        Image::make($file->getRealPath())->resize(100, 100)->save($path);
        $siswa->photo                = $fileName;
        $siswa->status_siswa         = $this->request->status_siswa;
        $siswa->keterangan           = $this->request->keterangan;
        $siswa->user_id              = $user_id;
        $siswa->save();
        return redirect()->route('siswa.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('siswa.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        $siswa          = Siswa::findOrFail($id);
        $agamas         = Agama::all();
        $jenjangs       = Jenjang::all();
        $kelas          = Kelas::all();
        $tahunAkademiks = TahunAkademik::all();
        return view('manajemen_siswa.siswa.show', [
            'agamas'    => $agamas,
            'jenjangs'  => $jenjangs,
            'kelas'   => $kelas,
            'siswa'     => $siswa,
            'tahunAkademiks' => $tahunAkademiks
        ]);
    }

    public function edit($id)
    {
        $siswa          = Siswa::findOrFail($id);
        $agamas         = Agama::all();
        $jenjangs       = Jenjang::all();
        $kelas          = Kelas::all();
        $tahunAkademiks = TahunAkademik::all();
        return view('manajemen_siswa.siswa.edit', [
            'agamas'    => $agamas,
            'jenjangs'  => $jenjangs,
            'kelas'   => $kelas,
            'siswa'     => $siswa,
            'tahunAkademiks' => $tahunAkademiks
        ]);
    }

    public function update($id)
    {
        $siswa                       = Siswa::findOrFail($id);
        $siswa->nis                  = $this->request->nis;
        $siswa->nisn                 = $this->request->nisn;
        $siswa->nama_lengkap         = $this->request->nama_lengkap;
        $siswa->nama_panggilan       = $this->request->nama_panggilan;
        $siswa->nomor_formulir       = $this->request->nomor_formulir;
        $siswa->tempat_lahir         = $this->request->tempat_lahir;
        $siswa->tanggal_lahir        = $this->request->tanggal_lahir;
        $siswa->jenis_kelamin        = $this->request->jenis_kelamin;
        $siswa->agama_id             = $this->request->agama_id;
        $siswa->jenjang_id           = $this->request->jenjang_id;
        $siswa->kelas_id             = $this->request->kelas_id;
        $siswa->tahun_akademik_id    = $this->request->tahun_akademik_id;
        $siswa->wali_kelas           = $this->request->wali_kelas;
        $siswa->phone                = $this->request->phone; 
        $siswa->umur                 = $this->request->umur;
        $siswa->anak_ke              = $this->request->anak_ke;  
        $siswa->suku                 = $this->request->suku;
        $siswa->nama_ayah_kandung    = $this->request->nama_ayah_kandung;
        $siswa->lulusan_sekolah_ortu = $this->request->lulusan_sekolah_ortu;
        $siswa->pekerjaan_ortu       = $this->request->pekerjaan_ortu;
        $siswa->jarak_rumah          = $this->request->jarak_rumah;
        $siswa->no_telp_ortu         = $this->request->no_telp_ortu;
        $siswa->penghasilan_ortu     = $this->request->penghasilan_ortu;
        $siswa->nama_ibu_kandung     = $this->request->nama_ibu_kandung;
        $siswa->pekerjaan_ibu        = $this->request->pekerjaan_ibu;
        //akte
        if($this->request->hasFile('akte_photo') == ""){
            $siswa->akte_photo       = $siswa->akte_photo;
        }else{
            $file_akte                 = $this->request->file('akte_photo');
            $fileName_akte             = $file_akte->getClientOriginalName();
            $path_akte                 = public_path('siswa/akte/' . $fileName_akte);
            Image::make($file_akte->getRealPath())->resize(1000, 1000)->save($path_akte);
            $siswa->akte_photo         = $fileName_akte;
        }

        //kk
        if($this->request->hasFile('kk_photo') == ""){
            $siswa->kk_photo       = $siswa->kk_photo;
        }else{
            $file_kk                 = $this->request->file('kk_photo');
            $fileName_kk             = $file_kk->getClientOriginalName();
            $path_kk                 = public_path('siswa/kk/' . $fileName_kk);
            Image::make($file_kk->getRealPath())->resize(1000, 1000)->save($path_kk);
            $siswa->kk_photo         = $fileName_kk;
        }

        //pernyataan
        if($this->request->hasFile('surat_pernyataan_photo') == ""){
            $siswa->surat_pernyataan_photo       = $siswa->surat_pernyataan_photo;
        }else{
            $file_pernyataan         = $this->request->file('surat_pernyataan_photo');
            $fileName_pernyataan     = $file_kk->getClientOriginalName();
            $path_pernyataan         = public_path('siswa/pernyataan/' . $fileName_pernyataan);
            Image::make($file_pernyataan->getRealPath())->resize(1000, 1000)->save($path_pernyataan);
            $siswa->surat_pernyataan_photo         = $fileName_pernyataan;
        }
        //photo
        if($this->request->hasFile('photo') == ""){
            $siswa->photo            = $siswa->photo;
        }else{
            $file                    = $this->request->file('photo');
            $fileName                = $file->getClientOriginalName();
            $path                    = public_path('siswa/' . $fileName);
            Image::make($file->getRealPath())->resize(100, 100)->save($path);
            $siswa->photo                       = $fileName;
        }
        $siswa->status_siswa         = $this->request->status_siswa;
        $siswa->keterangan           = $this->request->keterangan;
        $siswa->save();
        return redirect()->route('siswa.index');
    }

    public function destroy($id)
    {
        $siswa          = Siswa::findOrFail($id);
        $siswa->delete();
        return redirect()->back();
    }
}
