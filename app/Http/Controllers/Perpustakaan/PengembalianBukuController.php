<?php

namespace App\Http\Controllers\Perpustakaan;

use Redirect;
use Validator;
use App\Pengembalian;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PengembalianBukuController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pengembalians           = Pengembalian::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        $peminjamanBukus      = PeminjamanBuku::all();
        return view('perpustakaan.pengembalian-buku.index', [
            'pengembalians'      => $pengembalians,
            'peminjamanBukus'    => $peminjamanBukus
        ]);
    }

    public function create()
    {
        $peminjamanBukus      = PeminjamanBuku::all();
        return view('perpustakaan.pengembalian-buku.create',[
            'peminjamanBukus'     => $peminjamanBukus
        ]);   
    }

    public function store()
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'           => 'required|min:2',
            'tanggal'        => 'required|date'
        ]);

        if(!$validate->fails()){
            $pengembalianBuku                       = new Pengembalian;
            $pengembalianBuku->kode                 = $this->request->kode;
            $pengembalianBuku->pinjamanBuku_id      = $this->request->pinjamanBuku_id;
            $pengembalianBuku->tanggal              = $this->request->tanggal;
            $pengembalianBuku->status               = $this->request->status;
            $pengembalianBuku->save();
            return redirect()->route('pengembalian-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pengembalian-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pengembalianBuku           = Pengembalian::findOrFail($id);
        $peminjamanBukus            = PeminjamanBuku::all();
        return view('perpustakaan.pengembalian-buku.edit',[
            'pengembalianBuku'      => $pengembalianBuku,
            'peminjamanBukus'       => $peminjamanBukus
        ]);
    }

    public function update($id)
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'           => 'required|min:2',
            'tanggal'        => 'required|date'
        ]);

        if(!$validate->fails()){
            $pengembalianBuku                       = Pengembalian::findOrFail($id);
            $pengembalianBuku->kode                 = $this->request->kode;
            $pengembalianBuku->pinjamanBuku_id      = $this->request->pinjamanBuku_id;
            $pengembalianBuku->tanggal              = $this->request->tanggal;
            $pengembalianBuku->status               = $this->request->status;
            $pengembalianBuku->save();
            return redirect()->route('pengembalian-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pengembalian-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pengembalianBuku           = Pengembalian::findOrFail($id);
        $pengembalianBuku->delete();
        return redirect()->back();
    }
}
