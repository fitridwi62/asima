<?php

namespace App\Http\Controllers\Perpustakaan;

use Redirect;
use Validator;
use App\DataBuku;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PeminjamanBukuController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $peminjamanBukus        = PeminjamanBuku::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        $bukus                  = DataBuku::all();
        return view('perpustakaan.peminjaman-buku.index', [
            'peminjamanBukus'   => $peminjamanBukus,
            'bukus'             => $bukus
        ]);
    }

    public function create()
    {
        $bukus          = DataBuku::all();
        return view('perpustakaan.peminjaman-buku.create', [
            'bukus'     => $bukus
        ]);
    }

    public function store()
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'              => 'required|min:2',
            'jumlah_buku'       => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $peminjamanBuku                         = new PeminjamanBuku;
            $peminjamanBuku->kode                   = $this->request->kode;
            $peminjamanBuku->tanggal_peminjam       = $this->request->tanggal_peminjam; 
            $peminjamanBuku->tanggal_kembalian      = $this->request->tanggal_kembalian;
            $peminjamanBuku->jumlah_buku            = $this->request->jumlah_buku;
            $peminjamanBuku->buku_id                = $this->request->buku_id;
            $peminjamanBuku->save();
            return redirect()->route('peminjaman-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('peminjaman-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bukus                  = DataBuku::all();
        $peminjamanBuku         = PeminjamanBuku::findOrFail($id);
        return view('perpustakaan.peminjaman-buku.edit', [
            'bukus'             => $bukus,
            'peminjamanBuku'    => $peminjamanBuku
        ]);
    }

    public function update($id)
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'              => 'required|min:2',
            'jumlah_buku'       => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $peminjamanBuku                         = PeminjamanBuku::findOrFail($id);
            $peminjamanBuku->kode                   = $this->request->kode;
            $peminjamanBuku->tanggal_peminjam       = $this->request->tanggal_peminjam; 
            $peminjamanBuku->tanggal_kembalian      = $this->request->tanggal_kembalian;
            $peminjamanBuku->jumlah_buku            = $this->request->jumlah_buku;
            $peminjamanBuku->buku_id                = $this->request->buku_id;
            $peminjamanBuku->save();
            return redirect()->route('peminjaman-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('peminjaman-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $peminjamanBuku         = PeminjamanBuku::findOrFail($id);
        $peminjamanBuku->delete();
        return redirect()->back();
    }
}
