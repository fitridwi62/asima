<?php

namespace App\Http\Controllers\Perpustakaan;

use Redirect;
use Validator;
use App\PetugasPerpustakaan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PetugasPerpustakaanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $petugass            = PetugasPerpustakaan::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('perpustakaan.petugas.index', [
            'petugass'      => $petugass
        ]);   
    }

    public function create()
    {
        return view('perpustakaan.petugas.create');
    }

    public function store()
    {
        $validate            = Validator::make($this->request->all(), [
            'nama_lengkap'           => 'required|min:4',
            'email'                  => 'required',
            'phone'                  => 'required|numeric',
            'wa'                     => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $petugas                    = new PetugasPerpustakaan;
            $petugas->nama_lengkap      = $this->request->nama_lengkap;
            $petugas->email             = $this->request->email;
            $petugas->phone             = $this->request->phone;
            $petugas->jenis_kelamin     = $this->request->jenis_kelamin;
            $petugas->wa                = $this->request->wa;
            $petugas->status            = $this->request->status;
            $petugas->save();
            return redirect()->route('petugas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('petugas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $petugas            = PetugasPerpustakaan::findOrFail($id);
        return view('perpustakaan.petugas.edit', [
            'petugas'       => $petugas
        ]);
    }

    public function update($id)
    {
        $validate            = Validator::make($this->request->all(), [
            'nama_lengkap'           => 'required|min:4',
            'email'                  => 'required',
            'phone'                  => 'required|numeric',
            'wa'                     => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $petugas                    = PetugasPerpustakaan::findOrFail($id);
            $petugas->nama_lengkap      = $this->request->nama_lengkap;
            $petugas->email             = $this->request->email;
            $petugas->phone             = $this->request->phone;
            $petugas->jenis_kelamin     = $this->request->jenis_kelamin;
            $petugas->wa                = $this->request->wa;
            $petugas->status            = $this->request->status;
            $petugas->save();
            return redirect()->route('petugas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('petugas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $petugas            = PetugasPerpustakaan::findOrFail($id);
        $petugas->delete();
        return redirect()->back();
    }
}
