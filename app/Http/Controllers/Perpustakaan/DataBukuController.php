<?php

namespace App\Http\Controllers\Perpustakaan;

use Redirect;
use Validator;
use App\DataBuku;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class DataBukuController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $dataBukus          = DataBuku::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('perpustakaan.buku.index', [
            'dataBukus'        => $dataBukus
        ]);
    }

    public function create()
    {
        return view('perpustakaan.buku.create');
    }

    public function store()
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'           => 'required|min:2',
            'nama_buku'      => 'required|min:4',
            'pengarang_buku' => 'required|min:4',
            'tahun_terbit'   => 'required',
            'status'         => 'required' 
        ]);

        if(!$validate->fails()){
            $dataBuku                   = new DataBuku;
            $dataBuku->kode             = $this->request->kode;
            $dataBuku->nama_buku        = $this->request->nama_buku;
            $dataBuku->pengarang_buku   = $this->request->pengarang_buku;
            $dataBuku->tahun_terbit     = $this->request->tahun_terbit;
            $dataBuku->status           = $this->request->status;
            $dataBuku->save();
            return redirect()->route('data-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('data-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataBuku           = DataBuku::findOrFail($id);
        return view('perpustakaan.buku.edit', [
            'dataBuku'      => $dataBuku
        ]);
    }

    public function update($id)
    {
        $validate            = Validator::make($this->request->all(), [
            'kode'           => 'required|min:2',
            'nama_buku'      => 'required|min:4',
            'pengarang_buku' => 'required|min:4',
            'tahun_terbit'   => 'required',
            'status'         => 'required' 
        ]);

        if(!$validate->fails()){
            $dataBuku                   = DataBuku::findOrFail($id);
            $dataBuku->kode             = $this->request->kode;
            $dataBuku->nama_buku        = $this->request->nama_buku;
            $dataBuku->pengarang_buku   = $this->request->pengarang_buku;
            $dataBuku->tahun_terbit     = $this->request->tahun_terbit;
            $dataBuku->status           = $this->request->status;
            return redirect()->route('data-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('data-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $dataBuku           = DataBuku::findOrFail($id);
        $dataBuku->delete();
        return redirect()->back();
    }
}
