<?php

namespace App\Http\Controllers\ManajemenKeuanganSiswa;

use Redirect;
use Validator;
use App\JenisPembayaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class JenisPembyaranController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jenisPembayarans       = JenisPembayaran::orderBy('created_at', 'DESC')
                                                 ->paginate($this->limit);
        return view('manajemen_uang_siswa.jenis_pembayaran.index', [
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }

    public function create()
    {
        return view('manajemen_uang_siswa.jenis_pembayaran.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:3'
        ]);

        if(!$validate->fails()){
            $jenisPembayaran        = new JenisPembayaran;
            $jenisPembayaran->nama  = $this->request->nama;
            $jenisPembayaran->save();
            return redirect()->route('jenis-pembayaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('jenis-pembayaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jenisPembayaran        = JenisPembayaran::findOrFail($id);
        return view('manajemen_uang_siswa.jenis_pembayaran.edit', [
            'jenisPembayaran'   => $jenisPembayaran
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:3'
        ]);

        if(!$validate->fails()){
            $jenisPembayaran        = JenisPembayaran::findOrFail($id);
            $jenisPembayaran->nama  = $this->request->nama;
            $jenisPembayaran->save();
            return redirect()->route('jenis-pembayaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('jenis-pembayaran.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jenisPembayaran        = JenisPembayaran::findOrFail($id);
        $jenisPembayaran->delete();
        return redirect()->back();
    }
}
