<?php

namespace App\Http\Controllers\ManajemenKeuanganSiswa;

use Redirect;
use Validator;
use App\PembayaranBuku;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PembayaranBukuController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pembayaranBukus            = PembayaranBuku::orderBy('created_at', 'DESC')
                                                    ->paginate($this->limit);
        return view('manajemen_uang_siswa.pembayaran_buku.index', [
            'pembayaranBukus'       => $pembayaranBukus
        ]);
    }

    public function create()
    {
        return view('manajemen_uang_siswa.pembayaran_buku.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:3',
            'biaya'         =>  'required|numeric'
        ]);

        if(!$validate->fails()){
            $pembayaranBuku        = new PembayaranBuku;
            $pembayaranBuku->nama  = $this->request->nama;
            $pembayaranBuku->biaya = $this->request->biaya;
            $pembayaranBuku->save();
            return redirect()->route('pembayaran-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pembayaranBuku             =  PembayaranBuku::findOrFail($id);
        return view('manajemen_uang_siswa.pembayaran_buku.edit', [
            'pembayaranBuku'        => $pembayaranBuku
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:3',
            'biaya'         =>  'required|numeric'
        ]);

        if(!$validate->fails()){
            $pembayaranBuku        =  PembayaranBuku::findOrFail($id);
            $pembayaranBuku->nama  = $this->request->nama;
            $pembayaranBuku->biaya = $this->request->biaya;
            $pembayaranBuku->save();
            return redirect()->route('pembayaran-buku.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-buku.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pembayaranBuku             =  PembayaranBuku::findOrFail($id);
        $pembayaranBuku->delete();
        return redirect()->back();
    }
}
