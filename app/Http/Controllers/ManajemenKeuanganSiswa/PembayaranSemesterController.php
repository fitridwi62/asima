<?php

namespace App\Http\Controllers\ManajemenKeuanganSiswa;

use Redirect;
use Validator;
use App\PembayaranSemester;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PembayaranSemesterController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pembayaranSemesters         = PembayaranSemester::orderBy('created_at', 'DESC')
                                                        ->paginate($this->limit);
        return view('manajemen_uang_siswa.pembayaran_semester.index', [
            'pembayaranSemesters'   => $pembayaranSemesters
        ]);
    }

    public function create()
    {
        return view('manajemen_uang_siswa.pembayaran_semester.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:3',
            'biaya'         =>  'required|numeric'
        ]);

        if(!$validate->fails()){
            $pembayaranSemester        = new PembayaranSemester;
            $pembayaranSemester->nama  = $this->request->nama;
            $pembayaranSemester->biaya = $this->request->biaya;
            $pembayaranSemester->save();
            return redirect()->route('pembayaran-semester.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-semester.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pembayaranSemester             = PembayaranSemester::findOrFail($id);
        return view('manajemen_uang_siswa.pembayaran_semester.edit', [
            'pemabayaranSemester'       => $pembayaranSemester
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:3',
            'biaya'          =>  'required|numeric'
        ]);

        if(!$validate->fails()){
            $pembayaranSemester        = PembayaranSemester::findOrFail($id);
            $pembayaranSemester->nama  = $this->request->nama;
            $pembayaranSemester->biaya = $this->request->biaya;
            $pembayaranSemester->save();
            return redirect()->route('pembayaran-semester.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-semester.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pembayaranSemester        = PembayaranSemester::findOrFail($id);
        $pembayaranSemester->delete();
        return redirect()->back();
    }
}
