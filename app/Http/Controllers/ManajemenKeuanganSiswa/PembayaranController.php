<?php

namespace App\Http\Controllers\ManajemenKeuanganSiswa;

use Auth;
use Redirect;
use Validator;
use App\Siswa;
use App\PembayaranSpp;
use App\PembayaranSemester;
use App\Pembayaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PembayaranController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pembayarans            = Pembayaran::orderBy('created_at', 'desc')
                                            ->paginate($this->limit);
        $siswas                 = Siswa::all();
        return view('manajemen_uang_siswa.pembayaran.index', [
            'pembayarans'       => $pembayarans,
            'siswas'            => $siswas
        ]);
    }

    public function create()
    {
        $siswas                  = Siswa::all();
        $pembayaranSpps          = PembayaranSpp::all();
        $pembayaranSemesters     = PembayaranSemester::all();
        return view('manajemen_uang_siswa.pembayaran.create', [
            'siswas'            => $siswas,
            'pembayaranSpps'    => $pembayaranSpps,
            'pembayaranSemesters'   => $pembayaranSemesters
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'           => 'required|date'
        ]);

        if(!$validate->fails()){
            $user_id                            = Auth::user()->id;
            $pembayaran                         = new Pembayaran;
            $pembayaran->siswa_id               = $this->request->siswa_id;
            $pembayaran->pembayaranspp_id       = $this->request->pembayaranspp_id;
            $pembayaran->pembayaransemester_id  = $this->request->pembayaransemester_id;
            $pembayaran->tanggal                = $this->request->tanggal;
            $pembayaran->status                 = $this->request->status;
            $pembayaran->user_id                = $user_id;
            $pembayaran->save();
            return redirect()->route('pembayaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $siswas                  = Siswa::all();
        $pembayaranSpps          = PembayaranSpp::all();
        $pembayaranSemesters     = PembayaranSemester::all();
        $pembayaran              = Pembayaran::findOrFail($id);
        return view('manajemen_uang_siswa.pembayaran.edit', [
            'siswas'                    => $siswas,
            'pembayaranSpps'            => $pembayaranSpps,
            'pembayaranSemesters'       => $pembayaranSemesters,
            'pembayaran'                => $pembayaran
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'           => 'required|date'
        ]);

        if(!$validate->fails()){
            $user_id                            = Auth::user()->id;
            $pembayaran                         = Pembayaran::findOrFail($id);
            $pembayaran->siswa_id               = $this->request->siswa_id;
            $pembayaran->pembayaranspp_id       = $this->request->pembayaranspp_id;
            $pembayaran->pembayaransemester_id  = $this->request->pembayaransemester_id;
            $pembayaran->tanggal                = $this->request->tanggal;
            $pembayaran->status                 = $this->request->status;
            $pembayaran->user_id                = $user_id;
            $pembayaran->save();
            return redirect()->route('pembayaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pembayaran              = Pembayaran::findOrFail($id);
        $pembayaran->delete();
        return redirect()->back();
    }
}
