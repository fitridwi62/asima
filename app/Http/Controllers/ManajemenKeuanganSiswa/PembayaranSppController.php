<?php

namespace App\Http\Controllers\ManajemenKeuanganSiswa;

use Redirect;
use Validator;
use App\PembayaranSpp;
use App\JenisPembayaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PembayaranSppController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pembayaranSpps         = PembayaranSpp::orderBy('created_at', 'DESC')
                                                ->with('jenis_pembayaran')
                                                ->paginate($this->limit);
        $jenisPembayaran        = JenisPembayaran::all();
        return view('manajemen_uang_siswa.pembayaran_spp.index', [
            'pembayaranSpps'    => $pembayaranSpps,
            'jenisPembayaran'   => $jenisPembayaran
        ]);
    }

    public function create()
    {
        $jenisPembayaran        = JenisPembayaran::all();
        return view('manajemen_uang_siswa.pembayaran_spp.create', [
            'jenisPembayaran'   => $jenisPembayaran
        ]); 
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'biaya_spp'             => 'required|numeric',
            'tanggal_bayar'         => 'required|date',
            'status'                => 'required'
        ]);

        if(!$validate->fails()){
            $pembayaranSpp                                  =  new PembayaranSpp;
            $pembayaranSpp->jenis_pembayaran_id             = $this->request->jenis_pembayaran_id;
            $pembayaranSpp->biaya_spp                       = $this->request->biaya_spp;
            $pembayaranSpp->pembayaran_extra                = $this->request->pembayaran_extra;
            $pembayaranSpp->pembayaran_bimbingan_belajar    = $this->request->pembayaran_bimbingan_belajar;
            $pembayaranSpp->tanggal_bayar                   = $this->request->tanggal_bayar;
            $pembayaranSpp->status                          = $this->request->status;
            $pembayaranSpp->save();
            return redirect()->route('pembayaran-spp.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-spp.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pembayaranSpp      = PembayaranSpp::findOrFail($id);
        $jenisPembayaran    = JenisPembayaran::all();
        return view('manajemen_uang_siswa.pembayaran_spp.edit', [
            'pembayaranSpp'     => $pembayaranSpp,
            'jenisPembayaran'   => $jenisPembayaran
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'biaya_spp'             => 'required|numeric',
            'tanggal_bayar'         => 'required|date',
            'status'                => 'required'
        ]);

        if(!$validate->fails()){
            $pembayaranSpp                                  = PembayaranSpp::findOrFail($id);
            $pembayaranSpp->jenis_pembayaran_id             = $this->request->jenis_pembayaran_id;
            $pembayaranSpp->biaya_spp                       = $this->request->biaya_spp;
            $pembayaranSpp->pembayaran_extra                = $this->request->pembayaran_extra;
            $pembayaranSpp->pembayaran_bimbingan_belajar    = $this->request->pembayaran_bimbingan_belajar;
            $pembayaranSpp->tanggal_bayar                   = $this->request->tanggal_bayar;
            $pembayaranSpp->status                          = $this->request->status;
            $pembayaranSpp->save();
            return redirect()->route('pembayaran-spp.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pembayaran-spp.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pembayaranSpp      = PembayaranSpp::findOrFail($id);
        $pembayaranSpp->delete();
        return redirect()->back();
    }
}
