<?php

namespace App\Http\Controllers\Pengumuman;

use Redirect;
use Validator;
use Calendar;
use App\pengumumanEvents;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PengumumanEventsController extends Controller
{
    public function index(){
        $pengumumanEvents       = pengumumanEvents::get();
        $pengumumanEvent_list   = [];
        foreach($pengumumanEvents as $key => $pengumumanEvent){
            $pengumumanEvent_list[] = Calendar::event(
                $pengumumanEvent->judul,
                true,
                new \DateTime($pengumumanEvent->start_date),
                new \DateTime($pengumumanEvent->end_date.' +1 day')
            );
        }
        $calendar_details = Calendar::addEvents($pengumumanEvent_list);
        return view('dashbord', [
            'calendar_details'   => $calendar_details
        ]);
    }

    public function create(){
        return view('pengumuman-events.create');
    }

    public function addEvent(Request $request)
    {
        $event = new pengumumanEvents;
        $event->judul = $request['judul'];
        $event->start_date = $request['start_date'];
        $event->end_date = $request['end_date'];
        $event->save();
        return redirect()->route('/');
    }
}
