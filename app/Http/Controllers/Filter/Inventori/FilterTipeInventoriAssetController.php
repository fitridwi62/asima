<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\TipeInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterTipeInventoriAssetController extends Controller
{
    public function search_tipe_inventori_asset(Request $request){
        $search_nama            = $request->nama;
        $tipeInventories        = TipeInventory::where('nama', 'like', "%".$search_nama."%")
                                                ->paginate(10);
        return view('inventori.tipe-inventori.search', [
            'tipeInventories'   => $tipeInventories
        ]);
    }
}
