<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Gedung;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterGedungController extends Controller
{
    public function search_gedung(Request $request){
        $search_nama        = $request->nama;
        $gedungs            = Gedung::where('nama', 'like', "%".$search_nama."%")
                                        ->paginate(10);
        return view('inventori.gedung.search', [
            'gedungs'           => $gedungs
        ]);
    }
}
