<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\TipeDonasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterTipeDonasiController extends Controller
{
    public function search_tipe_donasi(Request $request){
        $search_nama            = $request->nama;
        $tipeDonasis            = TipeDonasi::where('nama', 'like', "%".$search_nama."%")
                                            ->paginate(10);
        return view('inventori.tipe-donasi.search', [
            'tipeDonasis'       => $tipeDonasis
        ]);
    }
}
