<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Barang;
use App\BarangKeluar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterBarangKeluarController extends Controller
{
    public function search_barang_keluar(Request $request){
        $barang_id              = $request->barang_id;
        $barangKeluars          = BarangKeluar::where('barang_id', 'like', "%".$barang_id."%")
                                            ->paginate(10);
        $barangs                = Barang::all();
        return view('inventori.barang-keluar.search', [
            'barangKeluars'     => $barangKeluars,
            'barangs'           => $barangs
        ]);
    }
}
