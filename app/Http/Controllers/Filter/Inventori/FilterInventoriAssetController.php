<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Asset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterInventoriAssetController extends Controller
{
    public function search_inventori_asset(Request $request){
        $no_asset       = $request->no_asset;
        $assets         = Asset::where('no_asset', 'like', "%".$no_asset."%")
                                ->paginate(10);
        return view('inventori.asset.search', [
            'assets'    => $assets
        ]);
    }
}
