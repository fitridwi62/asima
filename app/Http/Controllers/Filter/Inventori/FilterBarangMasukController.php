<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Barang;
use App\BarangMasuk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterBarangMasukController extends Controller
{
    public function search_barang_masuk(Request $request){
        $barang_id              = $request->barang_id;
        $barangMasuks           = BarangMasuk::where('barang_id', 'like', "%".$barang_id."%")
                                                ->paginate(10);
        $barangs                = Barang::all();
        return view('inventori.barang-masuk.index', [
            'barangMasuks'      => $barangMasuks,
            'barangs'           => $barangs
        ]);
    }
}
