<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\SpkAsset;
use App\Gedung;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterSPKAssetController extends Controller
{
    public function search_spk_asset(Request $request){
        $gedung_id              = $request->gedung_id;
        $spkAssets              = SpkAsset::where('gedung_id', 'like', "%".$gedung_id."%")
                                            ->paginate(10);
        $gedungs                = Gedung::all();
        return view('inventori.spk-asset.index', [
            'spkAssets'         => $spkAssets,
            'gedungs'           => $gedungs
        ]);
    }
}
