<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterBarangController extends Controller
{
    public function search_barang(Request $request){
        $kode               = $request->kode;
        $barangs            = Barang::where('kode', 'like', "%".$kode."%")
                                    ->paginate(10);
        return view('inventori.barang.search', [
            'barangs'       => $barangs
        ]);
    }
}
