<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\KategoriInventori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterKategoriInventoriController extends Controller
{
    public function search_kategori_inventori(Request $request){
        $search_nama                = $request->nama;
        $kategoriInventories        = KategoriInventori::where('nama', 'like', "%".$search_nama."%")
                                                        ->paginate(10);
        return view('inventori.kategori-inventori.search', [
            'kategoriInventories'   => $kategoriInventories
        ]);
    }
}
