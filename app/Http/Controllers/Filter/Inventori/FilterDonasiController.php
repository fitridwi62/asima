<?php

namespace App\Http\Controllers\Filter\Inventori;

use Redirect;
use App\Donasi;
use App\TipeDonasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDonasiController extends Controller
{
    public function search_donasi(Request $request){
        $tipe_donasi        = $request->tipe_donasi_id;
        $donasis            = Donasi::where('tipe_donasi_id', 'like', "%".$tipe_donasi."%")
                                    ->paginate(10);
        $tipeDonasis        = TipeDonasi::all();
        return view('inventori.donasi.search', [
            'donasis'       => $donasis,
            'tipeDonasis'   => $tipeDonasis
        ]);
    }
}
