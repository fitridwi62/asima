<?php

namespace App\Http\Controllers\Filter\ManageGuru;

use Redirect;
use App\DataGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDataGuruController extends Controller
{
    public function search_data_guru(Request $request){
        $search_nama            = $request->nama_guru;
        $data_gurus             = DataGuru::where('nama_guru', 'like', "%".$search_nama."%")
                                            ->paginate(10);
        return view('data_master.data_guru.search', [
            'data_gurus'    => $data_gurus
        ]);
    }
}
