<?php

namespace App\Http\Controllers\Filter\ManageGuru;

use Redirect;
use App\DataGuru;
use App\KehadiranGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterKehadiranGuruController extends Controller
{
    public function search_kehadiran_guru(Request $request){
        $data_guru_id           = $request->data_guru_id;
        $kehadiranGurus         = KehadiranGuru::where('data_guru_id', 'like', "%".$data_guru_id."%")
                                                ->paginate(10);
        $dataGurus              = DataGuru::all();
        return view('manajemen_guru.kehadiran_guru.search', [
            'kehadiranGurus'    => $kehadiranGurus,
            'dataGurus'         => $dataGurus
        ]);
    }
}
