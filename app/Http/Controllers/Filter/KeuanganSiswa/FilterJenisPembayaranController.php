<?php

namespace App\Http\Controllers\Filter\KeuanganSiswa;

use Redirect;
use App\JenisPembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJenisPembayaranController extends Controller
{
    public function search_jenis_pembayaran(Request $request){
        $search                 = $request->nama;
        $jenisPembayarans       = JenisPembayaran::where('nama', 'like', "%".$search."%")
                                                 ->paginate(10);
        return view('manajemen_uang_siswa.jenis_pembayaran.search', [
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }
}
