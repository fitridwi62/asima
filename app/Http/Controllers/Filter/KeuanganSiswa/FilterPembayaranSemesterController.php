<?php

namespace App\Http\Controllers\Filter\KeuanganSiswa;

use Redirect;
use App\PembayaranSemester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPembayaranSemesterController extends Controller
{
    public function search_pembayaran_semester(Request $request){
        $search_nama                 = $request->nama;
        $pembayaranSemesters         = PembayaranSemester::where('nama', 'like', "%".$search_nama."%")
                                                        ->paginate(10);
        return view('manajemen_uang_siswa.pembayaran_semester.search', [
            'pembayaranSemesters'   => $pembayaranSemesters
        ]);
    }
}
