<?php

namespace App\Http\Controllers\Filter\KeuanganSiswa;

use Redirect;
use App\Siswa;
use App\PembayaranSpp;
use App\PembayaranSemester;
use App\Pembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPembayaranController extends Controller
{
    public function search_pemabayaran(Request $request){
        $siswa_id               = $request->siswa_id;
        $siswas                 = Siswa::all();
        $pembayarans            = Pembayaran::where('siswa_id', 'like', "%".$siswa_id."%")
                                            ->paginate(10);
        return view('manajemen_uang_siswa.pembayaran.search', [
            'pembayarans'       => $pembayarans,
            'siswas'            => $siswas
        ]);
    }
}
