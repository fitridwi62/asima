<?php

namespace App\Http\Controllers\Filter\KeuanganSiswa;

use Redirect;
use App\PembayaranSpp;
use App\JenisPembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPembayaranSppController extends Controller
{
    public function search_pembayaran_spp(Request $request){
        $jenis_pembayaran_id    = $request->jenis_pembayaran_id;
        $pembayaranSpps         = PembayaranSpp::where('jenis_pembayaran_id', 'like', "%".$jenis_pembayaran_id."%")
                                                ->paginate(10);
        $jenisPembayaran        = JenisPembayaran::all();
        return view('manajemen_uang_siswa.pembayaran_spp.search', [
            'pembayaranSpps'    => $pembayaranSpps,
            'jenisPembayaran'   => $jenisPembayaran
        ]);
    }
}
