<?php

namespace App\Http\Controllers\Filter\KeuanganSiswa;

use Redirect;
use App\PembayaranBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPembayaranBukuController extends Controller
{
    public function search_pembayaran_buku(Request $request){
        $search_nama                = $request->nama;
        $pembayaranBukus            = PembayaranBuku::where('nama', 'like', "%".$search_nama."%")
                                                    ->paginate(10);
        return view('manajemen_uang_siswa.pembayaran_buku.index', [
            'pembayaranBukus'       => $pembayaranBukus
        ]);
    }
}
