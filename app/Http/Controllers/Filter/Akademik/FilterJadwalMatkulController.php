<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\MataPelajaran;
use App\Kelas;
use App\DataGuru;
use App\JadwalMataPelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJadwalMatkulController extends Controller
{
    public function search_jadwal_matkul(Request $request){
        $matkul_id                      = $request->mata_pelajaran_id;
        $mataPelajarans                 = MataPelajaran::all();                 
        $jadwalMataPelajarans           =  JadwalMataPelajaran::where('mata_pelajaran_id', 'like', "%".$matkul_id."%")->paginate(10);
        return view('akademik.jadwal_mata_pelajaran.search',[
            'jadwalMataPelajarans'  => $jadwalMataPelajarans,
            'mataPelajarans'        => $mataPelajarans       
        ]);
    }
}
