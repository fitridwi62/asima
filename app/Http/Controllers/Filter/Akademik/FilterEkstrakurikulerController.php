<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\Ekstrakurikuler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterEkstrakurikulerController extends Controller
{
    public function searchEkstra(Request $request){
        $search             = $request->search;

        $extraKurikulers   = Ekstrakurikuler::where('nama_ekstra', 'like', "%".$search."%")->paginate(10);
        return view('akademik.ekstrakurikuler.search',[
            'extraKurikulers' => $extraKurikulers
        ]);
    }
}
