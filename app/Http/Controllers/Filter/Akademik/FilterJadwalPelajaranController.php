<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use Validator;
use App\MataPelajaran;
use App\Kelas;
use App\DataGuru;
use App\JadwalMataPelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJadwalPelajaranController extends Controller
{
    public function searchJadwalPelajaran(Request $request){
        $search                     = $request->search;

        $jadwalMataPelajarans       = JadwalMataPelajaran::where('jam_mulai','like',"%".$search."%")->paginate();
        return view('akademik.jadwal_mata_pelajaran.search', [
            'jadwalMataPelajarans'  => $jadwalMataPelajarans
        ]);
    }
}
