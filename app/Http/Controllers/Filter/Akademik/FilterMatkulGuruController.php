<?php

namespace App\Http\Controllers\Filter\Akademik;

use App\Kelas;
use App\MataPelajaran;
use App\DataGuru;
use App\MataPelajaranGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterMatkulGuruController extends Controller
{
    public function search_matkul_guru(Request $request){
        $matkul_id          = $request->mata_pelajar_id;
        $mataPelajarans     = MataPelajaran::all();
        $mataPelajaranGurus = MataPelajaranGuru::where('mata_pelajar_id', 'like', "%".$matkul_id."%")->paginate(10);
        return view('akademik.mata_pelajaran_guru.search',[
            'mataPelajarans'        => $mataPelajarans,
            'mataPelajaranGurus'    => $mataPelajaranGurus
        ]);
    }
}
