<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\DataGuru;
use App\BimbinganBelajar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterBimbinganBelajarController extends Controller
{
    public function searchBimbingan(Request $request){
        $search             = $request->search;
        //dd($search);

        $bimbinganBelajars  = BimbinganBelajar::where('nama','like',"%".$search."%")->paginate();
        //dd($bimbinganBelajars);
        return view('akademik.bimbingan_belajar.search', [
            'bimbinganBelajars'  => $bimbinganBelajars
        ]);
    }
}
