<?php

namespace App\Http\Controllers\Filter\Akademik;
use App\MataPelajaran;
use App\Kurikulum;
use App\DataGuru;
use App\Jenjang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterMatapelajaranController extends Controller
{
    public function search(Request $request){
        $search             = $request->search;

        $mata_pelajarans            = MataPelajaran::where('nama', 'like',"%".$search."%")->paginate(10);
        return view('akademik.mata_pelajaran.search', [
            'mata_pelajarans'        => $mata_pelajarans
        ]);
    }
}
