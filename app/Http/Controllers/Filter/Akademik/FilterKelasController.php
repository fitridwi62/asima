<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\Kelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterKelasController extends Controller
{
    public function searchKelas(Request $request){
        $search             = $request->search;

        $kelass             = Kelas::where('nama_kelas', 'like', "%".$search."%")->paginate(10);
        return view('data_master.kelas.search',[
            'kelass'        => $kelass
        ]);
    }
}
