<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\Siswa;
use App\Kelas;
use App\SiswaPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterSiswaPerkelasController extends Controller
{
    public function search_siswa_perkelas(Request $request){
        $siswa_id               = $request->siswa_id;
        $siswas                 = Siswa::all();
        $siswaPerkelasLists     = SiswaPerkelas::where('siswa_id', 'like', "%".$siswa_id."%")
                                                ->paginate(10);
        return view('akademik.siswa_perkelas.search', [
            'siswas'             => $siswas,
            'siswaPerkelasLists' => $siswaPerkelasLists
        ]);
    }
}
