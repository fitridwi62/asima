<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\Siswa;
use App\Kelas;
use App\Jenjang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterSiswaController extends Controller
{
    public function search(Request $request){
        $search             = $request->search;
        $kelas_id           = $request->kelas_id;
        $jenjang_id         = $request->jenjang_id;
        $siswas             = Siswa::where('nama_lengkap','like',"%".$search."%")
                                    ->where('kelas_id', $kelas_id)
                                    ->where('jenjang_id', $jenjang_id)
                                    ->paginate();
        $kelass             = Kelas::all();
        $jenjangs           = Jenjang::all();
        return view('manajemen_siswa.siswa.search', [
            'siswas'     => $siswas,
            'kelass'     => $kelass,
            'jenjangs'   => $jenjangs
        ]);
    }
}
