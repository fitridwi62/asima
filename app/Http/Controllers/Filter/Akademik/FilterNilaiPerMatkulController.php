<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\MataPelajaran;
use App\GridNilai;
use App\DataGuru;
use App\Siswa;
use App\NilaiPermapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterNilaiPerMatkulController extends Controller
{
    public function search_nilai_permatkul(Request $request){
        $mata_pelajaran_id      = $request->mata_pelajaran_id;
        $nilaiPermapels         = NilaiPermapel::where('mata_pelajaran_id', 'like', "%".$mata_pelajaran_id."%")
                                            ->paginate(10);
        $mataPelajarans        = MataPelajaran::all();
        return view('akademik.nilai-permapel.search', [
            'nilaiPermapels'    => $nilaiPermapels,
            'mataPelajarans'    => $mataPelajarans
        ]);
    }
}
