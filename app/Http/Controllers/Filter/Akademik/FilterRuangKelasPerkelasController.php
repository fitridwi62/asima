<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\Kelas;
use App\RuangKelasPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterRuangKelasPerkelasController extends Controller
{
    public function searchRuangKelasPerkelas(Request $request){
        $kelas_id           = $request->kelas_id;

        $kelass             = Kelas::all();
        $ruangKelass        = RuangKelasPerkelas::where('kelas_id', 'like', "%".$kelas_id."%")
                                                ->paginate(10);
        //dd($ruangKelass);
        return view('akademik.ruang_kelas_perkelas.search',[
            'kelass'        => $kelass,
            'ruangKelass'   => $ruangKelass
        ]);
    }
}
