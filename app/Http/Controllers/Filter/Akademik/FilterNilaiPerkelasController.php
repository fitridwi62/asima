<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\MataPelajaran;
use App\Kelas;
use App\Siswa;
use App\Nilaiperkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterNilaiPerkelasController extends Controller
{
    public function search_nilai_perkelas(Request $request){
        $siswa_id            = $request->siswa_id;
        $nilaiPerkelass      = Nilaiperkelas::where('siswa_id', 'like', "%".$siswa_id."%")
                                            ->paginate(10);
        $siswas                = Siswa::all();
        return view('akademik.nilai-perkelas.search', [
            'nilaiPerkelass'    => $nilaiPerkelass,
            'siswas'            => $siswas
        ]);
    }
}
