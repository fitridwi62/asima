<?php

namespace App\Http\Controllers\Filter\Akademik;

use App\Kelas;
use App\MataPelajaran;
use App\MataPelajaranPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterMatkulPerkelasController extends Controller
{
    public function matkul_perkelas(Request $request){
        $matkul_id                  = $request->mata_pelajar_id;
        $mataPelajarans               = MataPelajaran::all();
        $mataPelajarPerkelass       = MataPelajaranPerkelas::where('mata_pelajar_id', 'like', "%".$matkul_id."%")->paginate(10);
        return view('akademik.mata_pelajar_perkelas.search', [
            'mataPelajarans'        => $mataPelajarans,
            'mataPelajarPerkelass'  => $mataPelajarPerkelass
        ]);
    }
}
