<?php

namespace App\Http\Controllers\Filter\Akademik;

use Redirect;
use App\DataAlumni;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDataAlumniController extends Controller
{
    public function search_data_alumni(Request $request){
        $search_nama            = $request->search;
        $dataAlumnis            = DataAlumni::where('nama', 'like', "%".$search_nama."%")
                                            ->paginate(10);
        return view('akademik.data_alumni.search', [
            'dataAlumnis'       => $dataAlumnis
        ]);
    }
}
