<?php

namespace App\Http\Controllers\Filter\Accounting;

use Redirect;
use App\TipePajak;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterTipePajakController extends Controller
{
    public function search_tipe_pajak(Request $request){
        $search_nama        = $request->nama;
        $tipePajaks         = TipePajak::where('nama', 'like', "%".$search_nama."%")
                                        ->paginate(10);
        return view('accounting.tipe-pajak.search', [
            'tipePajaks'    => $tipePajaks
        ]);
    }
}
