<?php

namespace App\Http\Controllers\Filter\Accounting;

use Redirect;
use App\Coa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterCOAController extends Controller
{
    public function search_coa(Request $request){
        $no_akun_coa            = $request->no_akun_coa;
        $coas                   = Coa::where('no_akun_coa', 'like', "%".$no_akun_coa."%")
                                        ->paginate(10);
        return view('accounting.coa.search', [
            'coas'      => $coas
        ]);
    }
}
