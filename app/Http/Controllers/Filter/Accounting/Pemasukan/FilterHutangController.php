<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\Hutang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterHutangController extends Controller
{
    public function search_hutang(Request $request){
        $tanggal            = $request->tanggal;
        $hutangs            = Hutang::where('tanggal', 'like', "%".$tanggal."%")
                                    ->paginate(10);
        return view('accounting.pemasukan.hutang.search', [
            'hutangs'       => $hutangs
        ]);
    }
}
