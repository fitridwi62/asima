<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\RekeningSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterRekeningSiswaController extends Controller
{
    public function search_rekening_siswa(Request $request){
       $nomor_keuangan         = $request->nomor_keuangan;
       $rekeningSiswas         = RekeningSiswa::where('nomor_keuangan', 'like', "%".$nomor_keuangan."%")
                                                ->paginate(10);
        return view('accounting.pemasukan.rekening-siswa.search', [
            'rekeningSiswas'    => $rekeningSiswas
        ]); 
    }
}
