<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\JurnalPBN;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJurnalPBNController extends Controller
{
    public function search_jurnal_pbn(Request $request){
        $tanggal            = $request->tanggal;
        $jurnalPbns         = JurnalPBN::where('tanggal', 'like', "%".$tanggal."%")
                                        ->paginate(10);
        return view('accounting.pemasukan.jurnal-pbn.search', [
            'jurnalPbns'    => $jurnalPbns
        ]);
    }
}
