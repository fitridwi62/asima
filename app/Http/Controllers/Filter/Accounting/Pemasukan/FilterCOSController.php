<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\CutOffSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterCOSController extends Controller
{
    public function search_cos(Request $request){
        $tanggal            = $request->tanggal;
        $cutOffSiswas       = CutOffSiswa::where('tanggal', 'like', "%".$tanggal."%")
                                        ->paginate(10);
        return view('accounting.pemasukan.cut-off-siswa.index', [
            'cutOffSiswas'  => $cutOffSiswas
        ]);
    }
}
