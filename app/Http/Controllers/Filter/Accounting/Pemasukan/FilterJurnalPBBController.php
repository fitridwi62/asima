<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\Jurnalpro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJurnalPBBController extends Controller
{
    public function search_jurnal_pbb(Request $request){
        $tanggal            = $request->tanggal;
        $jurnalPros         = Jurnalpro::where('tanggal', 'like', "%".$tanggal."%")
                                        ->paginate(10);
        return view('accounting.pemasukan.pro.search', [
            'jurnalPros'    => $jurnalPros
        ]);
    }
}
