<?php

namespace App\Http\Controllers\Filter\Accounting\Pemasukan;

use Redirect;
use App\JurnalSPP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJurnalSPPController extends Controller
{
    public function search_jurnal_spp(Request $request){
        $tanggal            = $request->tanggal;
        $jurnalSpps         = JurnalSPP::where('tanggal', 'like', "%".$tanggal."%")
                                        ->paginate(10);
        return view('accounting.pemasukan.spp.search', [
            'jurnalSpps'    => $jurnalSpps
        ]);
    }
}
