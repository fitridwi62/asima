<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\JurnalUangKeluar;
use App\PengelauaranJurnalKasKecilReimbursment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterUangKeluarController extends Controller
{
    public function search_uang_keluar(Request $request){
        $nomor_kwitansi         = $request->nomor_kwitansi;
        $jurnalUangKeluars      = JurnalUangKeluar::where('nomor_kwitansi', 'like', "%".$nomor_kwitansi."%")
                                                    ->paginate(10);
        return view('accounting.pengeluaran.uang-keluar.search',[
            'jurnalUangKeluars'     => $jurnalUangKeluars
        ]);
    }
}
