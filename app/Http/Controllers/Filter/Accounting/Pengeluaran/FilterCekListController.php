<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\PengeluaranCekList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterCekListController extends Controller
{
    public function search_cek_list(Request $request){
        $list               = $request->list;
        $cekLists           = PengeluaranCekList::where('list', 'like', "%".$list."%")
                                                ->paginate(10);
        return view('accounting.pengeluaran.cek-list.search', [
            'cekLists'      => $cekLists
        ]);
    }
}
