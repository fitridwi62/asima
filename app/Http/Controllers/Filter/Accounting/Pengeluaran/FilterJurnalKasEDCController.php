<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\Siswa;
use App\JenisPembayaran;
use App\JurnalEdc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterJurnalKasEDCController extends Controller
{
    public function search_jurnal_kas_edc(Request $request){
        $jenis_pembayaran_id    = $request->jenis_pembayaran_id;
        $jurnalEdcs             = JurnalEdc::where('jenis_pembayaran_id', 'like', "%".$jenis_pembayaran_id."%")
                                            ->paginate(10);
        $jenisPembayarans       = JenisPembayaran::all();
        return view('accounting.pengeluaran.jurnal-edc.index', [
            'jurnalEdcs'        => $jurnalEdcs,
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }
}
