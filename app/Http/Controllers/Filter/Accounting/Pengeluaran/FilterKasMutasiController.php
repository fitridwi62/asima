<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\Siswa;
use App\JurnalKasMutasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterKasMutasiController extends Controller
{
    public function search_kas_mutasi(Request $request){
        $tanggal                = $request->tanggal;
        $jurnalKasMutasis       = JurnalKasMutasi::where('tanggal', 'like', "%".$tanggal."%")
                                                ->paginate(10);
        return view('accounting.pengeluaran.kas-mutasi.search',[
            'jurnalKasMutasis'  => $jurnalKasMutasis
        ]);
    }
}
