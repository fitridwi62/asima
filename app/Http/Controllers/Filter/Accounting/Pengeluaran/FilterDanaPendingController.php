<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\DanaPending;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDanaPendingController extends Controller
{
    public function search_dana_pending(Request $request){
        $tanggal            = $request->tanggal;
        $danaPendings       = DanaPending::where('tanggal', 'like', "%".$tanggal."%")
                                        ->paginate(10);
        return view('accounting.pengeluaran.dana-pending.search', [
            'danaPendings'  => $danaPendings
        ]);
    }
}
