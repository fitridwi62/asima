<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\PengeluaranPermintaanDana;
use App\PengelauaranJurnalKasKecilReimbursment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterReibustmenController extends Controller
{
    public function search_reibustment(Request $request){
        $kode_reibusment        = $request->kode_reibusment;
        $reibustments           = PengelauaranJurnalKasKecilReimbursment::where('kode_reibusment', 'like', "%".$kode_reibusment."%")
                                                                        ->paginate(10);
        return view('accounting.pengeluaran.reibursment.search', [
            'reibustments'      => $reibustments
        ]);
    }
}
