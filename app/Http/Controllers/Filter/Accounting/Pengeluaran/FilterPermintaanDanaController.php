<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\PengeluaranPermintaanDana;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPermintaanDanaController extends Controller
{
    public function search_permintaan_dana(Request $request){
        $kode_permintaan_dana           = $request->kode_permintaan_dana;
        $permintaanDanas                = PengeluaranPermintaanDana::where('kode_permintaan_dana', 'like', "%".$kode_permintaan_dana."%")
                                                                ->paginate(10);
        return view('accounting.pengeluaran.permintaan-dana.search', [
            'permintaanDanas'       => $permintaanDanas
        ]);
    }
}
