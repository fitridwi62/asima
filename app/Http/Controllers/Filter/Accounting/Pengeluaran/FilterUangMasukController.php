<?php

namespace App\Http\Controllers\Filter\Accounting\Pengeluaran;

use Redirect;
use App\Siswa;
use App\JenisPembayaran;
use App\PengeluaranUangMasuk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterUangMasukController extends Controller
{
    public function search_pengeluaran_uang_masuk(Request $request ){
        $jenis_pembayaran_id        = $request->jenis_pembayaran_id;
        $pengeluaranUangMasuks      = PengeluaranUangMasuk::where('jenis_pembayaran_id', 'like', "%".$jenis_pembayaran_id."%")
                                                        ->paginate(10);
        $jenisPembayarans           = JenisPembayaran::all();
        return view('accounting.pengeluaran.uang-masuk.index', [
            'pengeluaranUangMasuks'     => $pengeluaranUangMasuks,
            'jenisPembayarans'          => $jenisPembayarans
        ]);
    }
}
