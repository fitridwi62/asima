<?php

namespace App\Http\Controllers\Filter\DataPegawai;

use Redirect;
use App\Agama;
use App\Jabatan;
use App\StaffPegawai;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDataPegawaiController extends Controller
{
    public function search_data_pegawai(Request $request){
        $search_nama                = $request->nama_lengkap;
        $staffs                     = StaffPegawai::where('nama_lengkap', 'like', "%".$search_nama."%")
                                            ->paginate(10);
        return view('staff.search', [
            'staffs'    => $staffs
        ]);
    }
}
