<?php

namespace App\Http\Controllers\Filter\Elearning;

use Redirect;
use App\DataGuru;
use App\Epengumuman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterpElearningPengumumanController extends Controller
{
    public function search_pengumuman(Request $request){
        $nama                   = $request->nama;
        $epengumumans           = Epengumuman::where('nama', 'like', "%".$nama."%")
                                            ->paginate(10);
        return view('elearning.pengumuman.search', [
            'epengumumans'      => $epengumumans
        ]);
    }
}
