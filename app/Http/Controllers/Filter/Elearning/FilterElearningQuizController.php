<?php

namespace App\Http\Controllers\Filter\Elearning;

use Redirect;
use App\Kelas;
use App\Equiz;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterElearningQuizController extends Controller
{
    public function search_quiz(Request $request){
        $kelas_id           = $request->kelas_id;
        $equizs             = Equiz::where('kelas_id', 'like', "%".$kelas_id."%")
                                    ->with('kelas')
                                    ->paginate(10);
        //dd($equizs);
        $kelass             = Kelas::all();
        return view('elearning.quiz.search', [
            'equizs'        => $equizs,
            'kelass'        => $kelass
        ]);
    }
}
