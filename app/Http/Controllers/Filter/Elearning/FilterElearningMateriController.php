<?php

namespace App\Http\Controllers\Filter\Elearning;

use Redirect;
use App\Kelas;
use App\Emateri;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterElearningMateriController extends Controller
{
    public function search_elearning_materi(Request $request){
        $kelas_id       = $request->kelas_id;
        $emateris       = Emateri::where('kelas_id', 'like', "%".$kelas_id."%")
                                ->with('kelas')
                                ->paginate(10);
        $kelass         = Kelas::all();
        return view('elearning.materi.search',[
            'emateris'  => $emateris,
            'kelass'    => $kelass
        ]);
    }
}
