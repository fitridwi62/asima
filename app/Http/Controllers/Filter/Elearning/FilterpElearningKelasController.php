<?php

namespace App\Http\Controllers\Filter\Elearning;

use Redirect;
use App\Ekelas;
use App\DataGuru;
use App\Kelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterpElearningKelasController extends Controller
{
    public function search_elearning_kelas(Request $request){
        $kelas_id               = $request->kelas_id;
        $ekelass                = Ekelas::where('kelas_id', 'like', "%".$kelas_id."%")
                                        ->paginate(10);
        $kelass                 = Kelas::all();
        return view('elearning.kelas.index', [
            'ekelass'           => $ekelass,
            'kelass'            => $kelass
        ]);
    }
}
