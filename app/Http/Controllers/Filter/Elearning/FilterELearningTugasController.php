<?php

namespace App\Http\Controllers\Filter\Elearning;

use Redirect;
use App\Kelas;
use App\DataGuru;
use App\Jurusan;
use App\MataPelajaran;
use App\ElearningTugas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterELearningTugasController extends Controller
{
    public function search_etugas(Request $request){
        $kelas_id           = $request->kelas_id;
        $etugass            = ElearningTugas::where('kelas_id', 'like', "%".$kelas_id."%")
                                            ->with('kelas')
                                            ->paginate(10);
        $kelass             = Kelas::all();
        return view('elearning.tugas.search', [
            'etugass'        => $etugass,
            'kelass'         => $kelass
        ]);
    }
}
