<?php

namespace App\Http\Controllers\Filter\Perpustakaan;

use Redirect;
use App\DataBuku;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPeminjamController extends Controller
{
    public function search_peminjam_buku(Request $request){
        $buku_id                = $request->buku_id;
        $peminjamanBukus        = PeminjamanBuku::where('buku_id', 'like', "%".$buku_id."%")
                                                ->paginate(10);
        $bukus                  = DataBuku::all();
        return view('perpustakaan.peminjaman-buku.search', [
            'peminjamanBukus'   => $peminjamanBukus,
            'bukus'             => $bukus
        ]);
    }
}
