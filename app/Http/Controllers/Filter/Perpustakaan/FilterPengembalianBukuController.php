<?php

namespace App\Http\Controllers\Filter\Perpustakaan;

use Redirect;
use App\Pengembalian;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPengembalianBukuController extends Controller
{
    public function search_pengembalian_buku(Request $request){
        $pinjamanBuku_id      = $request->pinjamanBuku_id;
        $peminjamanBukus      = PeminjamanBuku::all();
        $pengembalians        = Pengembalian::where('pinjamanBuku_id', 'like', "%".$pinjamanBuku_id."%")
                                            ->paginate(10);
        return view('perpustakaan.pengembalian-buku.search', [
            'pengembalians'      => $pengembalians,
            'peminjamanBukus'    => $peminjamanBukus
        ]);
    }
}
