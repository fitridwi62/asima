<?php

namespace App\Http\Controllers\Filter\Perpustakaan;

use Redirect;
use App\PetugasPerpustakaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterPetugasController extends Controller
{
    public function search_petugas(Request $request){
        $search_petugas         = $request->nama_lengkap;
        $petugass               = PetugasPerpustakaan::where('nama_lengkap', 'like', "%".$search_petugas."%")
                                                ->paginate(10);
        return view('perpustakaan.petugas.index', [
            'petugass'      => $petugass
        ]);
    }
}
