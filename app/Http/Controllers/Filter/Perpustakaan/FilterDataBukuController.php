<?php

namespace App\Http\Controllers\Filter\Perpustakaan;

use Redirect;
use App\DataBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterDataBukuController extends Controller
{
    public function search_data_buku(Request $request){
        $kode_buku          = $request->kode;
        $dataBukus          = DataBuku::where('kode', 'like', "%".$kode_buku."%")
                                    ->paginate(10);
        return view('perpustakaan.buku.search', [
            'dataBukus'        => $dataBukus
        ]);
    }
}
