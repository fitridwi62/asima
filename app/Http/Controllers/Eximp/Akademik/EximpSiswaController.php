<?php

namespace App\Http\Controllers\Eximp\Akademik;

use Redirect;
use App\Agama;
use App\Jenjang;
use App\Jurusan;
use App\Siswa;
use App\Exports\SiswaExport;
use App\Imports\SiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EximpSiswaController extends Controller
{
    public function index(){
        $siswas         = Siswa::orderBy('created_at', 'DESC')
                                ->with('agama')
                                ->with('jenjang')
                                ->with('jurusan')
                                ->paginate(10);
        return view('manajemen_siswa.siswa.index', [
            'siswas'        => $siswas
        ]);
    }

    public function siswaExport(){
        return Excel::download(new SiswaExport, 'siswa.xlsx');
    }

    public function formImportSiswa(){
        return view('manajemen_siswa.siswa.import');
    }

    public function siswaImport(Request $request){
        $file           = $request->file('file');
        $nama_file      = rand().$file->getClientOriginalName();
        $file->move('import',$nama_file);
        Excel::import(new SiswaImport, public_path('/import/'.$nama_file));
        return redirect()->route('siswa.index');
    }
}
