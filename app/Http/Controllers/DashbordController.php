<?php

namespace App\Http\Controllers;

use Charts;
use Redirect;
use Calendar;
use App\Siswa;
use App\pengumumanEvents;
use Illuminate\Http\Request;

class DashbordController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
    public function index(){
        // return \DB::table('users')->get();
        $siswa      = Siswa::all();
        $chart = Charts::database($siswa, 'bar', 'highcharts')
                     ->title('Siswa')
                     ->elementLabel('Total Siswa')
                     ->dimensions(1000, 500)
                     ->colors(['red', 'green', 'blue', 'yellow', 'orange', 'cyan', 'magenta'])
                     ->groupByMonth(date('Y'), true);
        
        //pengumuman
        $pengumumanEvents       = pengumumanEvents::get();
        $pengumumanEvent_list   = [];
        foreach($pengumumanEvents as $key => $pengumumanEvent){
            $pengumumanEvent_list[] = Calendar::event(
                $pengumumanEvent->judul,
                true,
                new \DateTime($pengumumanEvent->start_date),
                new \DateTime($pengumumanEvent->end_date.' +1 day')
            );
        }
        $calendar_details = Calendar::addEvents($pengumumanEvent_list);
        return view('dashbord',[
            'chart'     => $chart,
            'calendar_details'  => $calendar_details
        ]);
    }


    public function addEvent(Request $request)
    {
        $event = new pengumumanEvents;
        $event->judul = $request['judul'];
        $event->start_date = $request['start_date'];
        $event->end_date = $request['end_date'];
        $event->save();
        return redirect()->route('');
    }


}
