<?php

namespace App\Http\Controllers\Register;

use Image;
use Redirect;
use Validator;
use App\DataGuru;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PendaftaranGuruBaruController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
    }

    public function create(){
        return view('register.guru-baru');
    }

    public function store(){
        $validate           = Validator::make($this->request->all(), [
            'no_induk_guru'                     =>'required|min:2',
            'nama_guru'                         =>'required|min:2',
            'nomer_ktp'                         =>'required|max:12',
            'jenis_kelamin'                     =>'required',
            'telp'                              => 'required',
            'alamat'                            => 'required|min:4',
            'email'                             => 'required',
            'pendidikan_terakhir'               => 'required',
            'sertifikat'                        => 'required',
            'photo'                             => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if(!$validate->fails()){
            $data_guru                          = new DataGuru;
            $data_guru->no_induk_guru           = $this->request->no_induk_guru;
            $data_guru->nama_guru               = $this->request->nama_guru;
            $data_guru->nomer_ktp               = $this->request->nomer_ktp;
            $data_guru->jenis_kelamin           = $this->request->jenis_kelamin;
            $data_guru->telp                    = $this->request->telp;
            $data_guru->alamat                  = $this->request->alamat;
            $data_guru->email                   = $this->request->email;
            $data_guru->pendidikan_terakhir     = $this->request->pendidikan_terakhir;
            $data_guru->sertifikat              = $this->request->sertifikat;
            $file                               = $this->request->file('photo');
            $fileName                           = $file->getClientOriginalName();
            $path                               = public_path('photo_guru/' . $fileName);
            Image::make($file->getRealPath())->resize(100, 100)->save($path);
            $data_guru->photo                   = $fileName;
            $data_guru->save();
            return redirect()->route('pendaftaran.guru.selesai');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pendaftaran.guru.baru')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function finish(){
        return view('register.finish');
    }
}
