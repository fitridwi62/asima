<?php

namespace App\Http\Controllers\Register;

use Image;
use Validator;
use Redirect;
use App\Jenjang;
use App\DaftarSiwa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PendafataranSiswaBaruController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
    }
    public function create(){
        $jenjangs       = Jenjang::all();
        return view('register.siswa-baru', [
            'jenjangs'  => $jenjangs
        ]);
    }

    public function store(){
        $validate           = Validator::make($this->request->all(), [
            'no_pendaftaran'            => 'required|min:2',
            'nama_lengkap'              => 'required|min:2',
            'jenis_kelamin'             => 'required',
            'tempat_lahir'              => 'required|min:2',
            'tanggal_lahir'             => 'required|date',
            'alamat'                    => 'required|min:2',
            'telp'                      => 'required',
            'email'                     => 'required',
            'no_wa'                     => 'required',
            'nama_orang_tua'            => 'required|min:2',
            'pekerjaan_orang_tua'       => 'required|min:2',
            'no_telp_ortu'              => 'required',
            'penghasilan_ortu'          => 'required',
            'photo'                     => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        if(!$validate->fails()){
            $daftarSiswa                            = new DaftarSiwa;
            $daftarSiswa->no_pendaftaran            = $this->request->no_pendaftaran;
            $daftarSiswa->nama_lengkap              = $this->request->nama_lengkap;
            $daftarSiswa->jenis_kelamin             = $this->request->jenis_kelamin;
            $daftarSiswa->tempat_lahir              = $this->request->tempat_lahir;
            $daftarSiswa->tanggal_lahir             = $this->request->tanggal_lahir;
            $daftarSiswa->alamat                    = $this->request->alamat;
            $daftarSiswa->telp                      = $this->request->telp;
            $daftarSiswa->jenjang_id                = $this->request->jenjang_id;
            $daftarSiswa->email                     = $this->request->email;
            $daftarSiswa->no_wa                     = $this->request->no_wa;
            $daftarSiswa->nama_orang_tua            = $this->request->nama_orang_tua;
            $daftarSiswa->pekerjaan_orang_tua       = $this->request->pekerjaan_orang_tua;
            $daftarSiswa->no_telp_ortu              = $this->request->no_telp_ortu;
            $daftarSiswa->penghasilan_ortu          = $this->request->penghasilan_ortu;

            $file                                   = $this->request->file('photo');
            $fileName                               = $file->getClientOriginalName();
            $path                                   = public_path('psb/' . $fileName);
            Image::make($file->getRealPath())->resize(150, 150)->save($path);
            $daftarSiswa->photo                     = $fileName;
            $daftarSiswa->status                    = 'proses';
            $daftarSiswa->save();
            return redirect()->route('pendaftaran.siswa.selesai');
        }else{
            $errors = $validate->messages();

            return redirect()->route('pendaftaran.siswa-baru')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }
    public function thanks(){
        return view('register.thanks');
    }
}
