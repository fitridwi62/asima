<?php

namespace App\Http\Controllers\DataMaster;

use Image;
use Redirect;
use Validator;
use App\DataGuru;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class DataGuruController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $data_gurus         = DataGuru::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.data_guru.index', [
            'data_gurus'    => $data_gurus
        ]);
    }

    public function create()
    {
        return view('data_master.data_guru.create');
    }

    public function store()
    {
         $validate           = Validator::make($this->request->all(), [
            'no_induk_guru'                     =>'required|min:4',
            'nama_guru'                         =>'required|min:4',
            'nomer_ktp'                         =>'required|max:12',
            'jenis_kelamin'                     =>'required|max:12',
            'telp'                              => 'required|numeric',
            'alamat'                            => 'required|min:4',
            'email'                             => 'required',
            'pendidikan_terakhir'               => 'required',
            'sertifikat'                        => 'required',
            'photo'                             => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if(!$validate->fails()){
            $data_guru                          = new DataGuru;
            $data_guru->no_induk_guru           = $this->request->no_induk_guru;
            $data_guru->nama_guru               = $this->request->nama_guru;
            $data_guru->nomer_ktp               = $this->request->nomer_ktp;
            $data_guru->jenis_kelamin           = $this->request->jenis_kelamin;
            $data_guru->telp                    = $this->request->telp;
            $data_guru->alamat                  = $this->request->alamat;
            $data_guru->email                   = $this->request->email;
            $data_guru->pendidikan_terakhir     = $this->request->pendidikan_terakhir;
            $data_guru->sertifikat              = $this->request->sertifikat;
            $file                               = $this->request->file('photo');
            $fileName                           = $file->getClientOriginalName();
            $path                               = public_path('photo_guru/' . $fileName);
            Image::make($file->getRealPath())->resize(100, 100)->save($path);
            $data_guru->photo                   = $fileName;
            $data_guru->save();
            return redirect()->route('data_guru.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('data_guru.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        $data_guru          = DataGuru::findOrFail($id);
        return view('data_master.data_guru.show', [
            'data_guru'     => $data_guru
        ]);
    }

    public function edit($id)
    {
        $data_guru          = DataGuru::findOrFail($id);
        return view('data_master.data_guru.edit', [
            'data_guru'     => $data_guru
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'no_induk_guru'                     =>'required|min:4',
            'nama_guru'                         =>'required|min:4',
            'nomer_ktp'                         =>'required|max:12',
            'jenis_kelamin'                     =>'required|max:12',
            'telp'                              => 'required|numeric',
            'alamat'                            => 'required|min:4',
            'email'                             => 'required',
            'pendidikan_terakhir'               => 'required',
            'sertifikat'                        => 'required',
            'photo'                             => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if(!$validate->fails()){
            $data_guru                              = DataGuru::findOrFail($id);
            $data_guru->no_induk_guru               = $this->request->no_induk_guru;
            $data_guru->nama_guru                   = $this->request->nama_guru;
            $data_guru->nomer_ktp                   = $this->request->nomer_ktp;
            $data_guru->jenis_kelamin               = $this->request->jenis_kelamin;
            $data_guru->telp                        = $this->request->telp;
            $data_guru->alamat                      = $this->request->alamat;
            $data_guru->email                       = $this->request->email;
            $data_guru->pendidikan_terakhir         = $this->request->pendidikan_terakhir;
            $data_guru->sertifikat                  = $this->request->sertifikat;
            if($this->request->hasFile('photo') == ""){
                $data_guru->photo                   = $data_guru->photo;
            }else{
                $file                               = $this->request->file('photo');
                $fileName                           = $file->getClientOriginalName();
                $path                               = public_path('photo_guru/' . $fileName);
                Image::make($file->getRealPath())->resize(100, 100)->save($path);
                $data_guru->photo                   = $fileName;
            }

            $data_guru->save();
            return redirect()->route('data_guru.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('data_guru.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $data_guru                              = DataGuru::findOrFail($id);
        $data_guru->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
