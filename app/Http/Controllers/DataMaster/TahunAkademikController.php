<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\TahunAkademik;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class TahunAkademikController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $tahun_akademiks        = TahunAkademik::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.tahun_akademik.index', [
            'tahun_akademiks'   => $tahun_akademiks
        ]);
    }

    public function create()
    {
        return view('data_master.tahun_akademik.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:2'
        ]);

        if(!$validate->fails()){
            $tahun_akademik                      = new TahunAkademik;
            $tahun_akademik->nama                = $this->request->nama;
            $tahun_akademik->status              = 'aktif';
            $tahun_akademik->save();
            return redirect()->route('tahun_akademik.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('tahun_akademik.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tahun_akademik         = TahunAkademik::findOrFail($id);
        return view('data_master.tahun_akademik.edit', [
            'tahun_akademik'    => $tahun_akademik
        ]);
    }


    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:2'
        ]);

        if(!$validate->fails()){
            $tahun_akademik                      = TahunAkademik::findOrFail($id);
            $tahun_akademik->nama                = $this->request->nama;
            $tahun_akademik->status              = 'aktif';
            $tahun_akademik->save();
            return redirect()->route('tahun_akademik.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('tahun_akademik.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $tahun_akademik                      = TahunAkademik::findOrFail($id);
        $tahun_akademik->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
