<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class JurusanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurusans           = Jurusan::orderBy('created_at')->paginate($this->limit);
        return view('data_master.jurusan.index', [
            'jurusans' => $jurusans
        ]);
    }

    public function create()
    {
        return view('data_master.jurusan.create');
    }

    public function store()
    {
         $validate           = Validator::make($this->request->all(), [
            'nama_jurusan'          =>'required|min:2',
            'kode_jurusan'          =>'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurusan                    = new Jurusan;
            $jurusan->nama_jurusan      = $this->request->nama_jurusan;
            $jurusan->kode_jurusan      = $this->request->kode_jurusan;
            $jurusan->save();
            return redirect()->route('jurusan.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jurusan.create');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurusan        = Jurusan::findOrFail($id);
        return view('data_master.jurusan.edit', [
            'jurusan'   => $jurusan
        ]);
    }

    public function update($id)
    {
         $validate           = Validator::make($this->request->all(), [
            'nama_jurusan'          =>'required|min:2',
            'kode_jurusan'          =>'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurusan        = Jurusan::findOrFail($id);
            $jurusan->nama_jurusan      = $this->request->nama_jurusan;
            $jurusan->kode_jurusan      = $this->request->kode_jurusan;
            $jurusan->save();
            return redirect()->route('jurusan.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jurusan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jurusan        = Jurusan::findOrFail($id);
        $jurusan->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
