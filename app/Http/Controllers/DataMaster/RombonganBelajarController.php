<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\RombonganBelajar;
use App\Jurusan;
use App\Kelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RombonganBelajarController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $rombongan_belajars         = RombonganBelajar::select('rombongan_belajars.*', 'kelas.nama_kelas')
                                        ->join('kelas', 'kelas.id', 'rombongan_belajars.kelas')
                                        ->orderBy('rombongan_belajars.created_at', 'DESC')->paginate($this->limit);
        return view('data_master.rombel.index', [
            'rombongan_belajars'    => $rombongan_belajars
        ]);
    }

    public function create()
    {
        $kelas = Kelas::all();
        $jurusans    = Jurusan::all();
        return view('data_master.rombel.create', [
            'jurusans' => $jurusans,
            'kelas' => $kelas
        ]);
    }

    public function store()
    {
        $validate                   = Validator::make($this->request->all(), [
            'nama'                  =>'required|min:4',
            'kelas'                 =>'required'
        ]);

        if(!$validate->fails()){
            $rombel                     = new RombonganBelajar;
            $rombel->nama               = $this->request->nama;
            $rombel->jurusan_id         = $this->request->jurusan_id;
            $rombel->kelas              = $this->request->kelas;
            $rombel->save();
            return redirect()->route('rombongan_belajar.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('rombongan_belajar.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelas = Kelas::all();
        $jurusans    = Jurusan::all();
        $rombel      =  RombonganBelajar::findOrFail($id);
        return view('data_master.rombel.edit', [
            'rombel'            => $rombel,
            'jurusans'          => $jurusans,
            'kelas'          => $kelas
        ]);
    }

    public function update($id)
    {
        $validate                   = Validator::make($this->request->all(), [
            'nama'                  =>'required|min:4',
            'kelas'                 =>'required'
        ]);

        if(!$validate->fails()){
            $rombel                     =  RombonganBelajar::findOrFail($id);
            $rombel->nama               = $this->request->nama;
            $rombel->jurusan_id         = $this->request->jurusan_id;
            $rombel->kelas              = $this->request->kelas;
            $rombel->save();
            return redirect()->route('rombongan_belajar.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('rombongan_belajar.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $rombongan_belajar      =  RombonganBelajar::findOrFail($id);
        $rombongan_belajar->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
