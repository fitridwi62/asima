<?php
/**
*developer : hari , galan & adi
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Pendapatan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PendapatanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pendapatans         = Pendapatan::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.pendapatan.index', [
            'pendapatans'       => $pendapatans
        ]);
    }

    public function create()
    {
        return view('data_master.pendapatan.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'harga'           => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $pendapatan                      = new Pendapatan;
            $pendapatan->harga               = $this->request->harga;
            $pendapatan->save();
            return redirect()->route('pendapatan.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('pendapatan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pendapatan         = Pendapatan::findOrFail($id);
        return view('data_master.pendapatan.edit', [
            'pendapatan'    => $pendapatan
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate           = Validator::make($this->request->all(), [
            'harga'           => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $pendapatan         = Pendapatan::findOrFail($id);
            $pendapatan->harga               = $this->request->harga;
            $pendapatan->save();
            return redirect()->route('pendapatan.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('pendapatan.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pendapatan         = Pendapatan::findOrFail($id);
        $pendapatan->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
