<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Jenjang;
use App\DataSekolah;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class DataSekolahController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $data_sekolahs      = DataSekolah::select('data_sekolahs.*', 'jenjangs.kode_jenjang', 'jenjangs.nama_jenjang')
                            ->join('jenjangs', 'jenjangs.id', 'data_sekolahs.jenjang')
                            ->orderBy('data_sekolahs.created_at', 'DESC')->paginate($this->limit);
        return view('data_master.data_sekolah.index', [
            'data_sekolahs' => $data_sekolahs
        ]);
    }

    public function getJenjang()
    {
        return Jenjang::all();
    }

    public function create()
    {
        $jenjang = $this->getJenjang();
        $jenjang = collect($jenjang)->pluck('nama_jenjang', 'id');
        return view('data_master.data_sekolah.create', compact('jenjang'));
    }

    public function store()
    {
            $data_sekolah       = new DataSekolah;
            $data_sekolah->nama         = $this->request->nama;
            $data_sekolah->email        = $this->request->email;
            $data_sekolah->telephone    = $this->request->telephone;
            $data_sekolah->alamat       = $this->request->alamat;
            $data_sekolah->jenjang      = $this->request->jenjang; //yang disimpian jenjang ID
            $data_sekolah->save();
            return redirect()->route('data_sekolah.index')->with(['success' => 'Data berhasil disimpan']);
        // $validate           = Validator::make($this->request->all(), [
        //     'nama'                  =>'required|min:10|max:255',
        //     'email'                 =>'required',
        //     'telephone'             =>'required|max:12',
        //     'alamat'                =>'required|max:12',
        //     'jenjang'               => 'required'
        // ]);

        // if(!$validate->fails()){
            
        // }else{
        //     $errors = $validate->messages();

        //     return redirect()->route('data_sekolah.create')
        //             ->withErrors($validate)
        //             ->withInput($this->request->input());
        // }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jenjang = $this->getJenjang();
        $jenjang = collect($jenjang)->pluck('nama_jenjang', 'id');
        $data_sekolah           = DataSekolah::findOrFail($id);
        return view('data_master.data_sekolah.edit', [
            'data_sekolah'  => $data_sekolah,
            'jenjang' => $jenjang
        ]);
    }

    public function update($id)
    {
            $data_sekolah               = DataSekolah::findOrFail($id);
            $data_sekolah->nama         = $this->request->nama;
            $data_sekolah->email        = $this->request->email;
            $data_sekolah->telephone    = $this->request->telephone;
            $data_sekolah->alamat       = $this->request->alamat;
            $data_sekolah->jenjang      = $this->request->jenjang;
            $data_sekolah->save();
            return redirect()->route('data_sekolah.index')->with(['success' => 'Data berhasil diupdate']);
        // $validate           = Validator::make($this->request->all(), [
        //     'nama'                  =>'required|max:255',
        //     'email'                 =>'required',
        //     'telephone'             =>'required|max:12',
        //     'alamat'                =>'required|max:12',
        //     'jenjang'               => 'required'
        // ]);

        // if(!$validate->fails()){
        //     $data_sekolah               = DataSekolah::findOrFail($id);
        //     $data_sekolah->nama         = $this->request->nama;
        //     $data_sekolah->email        = $this->request->email;
        //     $data_sekolah->telephone    = $this->request->telephone;
        //     $data_sekolah->alamat       = $this->request->alamat;
        //     $data_sekolah->jenjang      = $this->request->jenjang;
        //     $data_sekolah->save();
        //     return redirect()->route('data_sekolah.index');
        // }else{
        //     $errors = $validate->messages();

        //     return redirect()->route('data_sekolah.edit')
        //             ->withErrors($validate)
        //             ->withInput($this->request->input());
        // }
    }

    public function destroy($id)
    {
        $data_sekolah              = DataSekolah::findOrFail($id);
        $data_sekolah->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
