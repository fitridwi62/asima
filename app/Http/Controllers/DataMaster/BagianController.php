<?php
/**
*developer : hari , galan & adi
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Bagian;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class BagianController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $bagians            = Bagian::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.bagian.index', [
            'bagians'       => $bagians
        ]);
    }

    public function create()
    {
        return view('data_master.bagian.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $bagian                      = new Bagian;
            $bagian->nama                = $this->request->nama;
            $bagian->save();
            return redirect()->route('bagian.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('bagian.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bagian         = Bagian::findOrFail($id);
        return view('data_master.bagian.edit', [
            'bagian'    => $bagian
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4'
        ]);
        if(!$validate->fails()){
            $bagian         = Bagian::findOrFail($id);
            $bagian->nama                = $this->request->nama;
            $bagian->save();
            return redirect()->route('bagian.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('bagian.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $bagian         = Bagian::findOrFail($id);
        $bagian->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
