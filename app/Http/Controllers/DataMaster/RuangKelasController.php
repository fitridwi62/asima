<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\RuangKelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RuangKelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $ruang_kelass        = RuangKelas::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.ruang_kelas.index', [
            'ruang_kelass'   => $ruang_kelass
        ]);
    }

    public function create()
    {
        return view('data_master.ruang_kelas.create');
    }

    public function store()
    {
        $validate                   = Validator::make($this->request->all(), [
            'kode_ruangan'          =>'required|min:2',
            'nama_ruangan'          =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $ruang_kelas                    = new RuangKelas;
            $ruang_kelas->kode_ruangan      = $this->request->kode_ruangan;
            $ruang_kelas->nama_ruangan      = $this->request->nama_ruangan;
            $ruang_kelas->save();
            return redirect()->route('ruang_kelas.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('ruang_kelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ruang_kelas        = RuangKelas::findOrFail($id);
        return view('data_master.ruang_kelas.edit', [
            'ruang_kelas'   => $ruang_kelas
        ]);
    }

    public function update($id)
    {
        $validate                   = Validator::make($this->request->all(), [
            'kode_ruangan'          =>'required|min:2',
            'nama_ruangan'          =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $ruang_kelas        = RuangKelas::findOrFail($id);
            $ruang_kelas->kode_ruangan      = $this->request->kode_ruangan;
            $ruang_kelas->nama_ruangan      = $this->request->nama_ruangan;
            $ruang_kelas->save();
            return redirect()->route('ruang_kelas.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('ruang_kelas.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $ruang_kelas        = RuangKelas::findOrFail($id);
        $ruang_kelas->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
