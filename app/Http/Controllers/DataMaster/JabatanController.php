<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class JabatanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jabatans       = Jabatan::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.jabatan.index', [
            'jabatans'  => $jabatans
        ]);
    }

    public function create()
    {
        return view('data_master.jabatan.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:4'
        ]);
        if(!$validate->fails()){
            $jabatan                        = new Jabatan;
            $jabatan->nama                  = $this->request->nama;
            $jabatan->save();
            return redirect()->route('jabatan.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jabatan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jabatan            = Jabatan::findOrFail($id);
        return view('data_master.jabatan.edit', [
            'jabatan'       => $jabatan
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:4'
        ]);
        if(!$validate->fails()){
            $jabatan            = Jabatan::findOrFail($id);
            $jabatan->nama                  = $this->request->nama;
            $jabatan->save();
            return redirect()->route('jabatan.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jabatan.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jabatan            = Jabatan::findOrFail($id);
        $jabatan->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
