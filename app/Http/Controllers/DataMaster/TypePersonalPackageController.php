<?php

namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\TypePersonalPackage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class TypePersonalPackageController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $typePersonalPackages       =  TypePersonalPackage::paginate($this->limit);
        return view('data_master.type_personal.index',[
            'typePersonalPackages'  => $typePersonalPackages
        ]);
    }

    public function create()
    {
        return view('data_master.type_personal.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $typePersonal                      = new TypePersonalPackage;
            $typePersonal->nama                = $this->request->nama;
            $typePersonal->save();
            return redirect()->route('type-personal-package.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('type-personal-package.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $typePersonal       = TypePersonalPackage::findOrFail($id);
        return view('data_master.type_personal.edit', [
            'typePersonal'  => $typePersonal
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $typePersonal                       = TypePersonalPackage::findOrFail($id);
            $typePersonal->nama                 = $this->request->nama;
            $typePersonal->save();
            return redirect()->route('type-personal-package.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('type-personal-package.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $typePersonal       = TypePersonalPackage::findOrFail($id);
        $typePersonal->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
