<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Kendaraan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class KendaraanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $kendaraans         = Kendaraan::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.kendaraan.index', [
            'kendaraans'      => $kendaraans
        ]);
    }

    public function create()
    {
        return view('data_master.kendaraan.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $kendaraan                      = new Kendaraan;
            $kendaraan->nama                = $this->request->nama;
            $kendaraan->save();
            return redirect()->route('kendaraan.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('kendaraan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kendaraan          = Kendaraan::findOrFail($id);
        return view('data_master.kendaraan.edit', [
            'kendaraan'     => $kendaraan
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $kendaraan                      = Kendaraan::findOrFail($id);
            $kendaraan->nama                = $this->request->nama;
            $kendaraan->save();
            return redirect()->route('kendaraan.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('kendaraan.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $kendaraan          = Kendaraan::findOrFail($id);
        $kendaraan->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
