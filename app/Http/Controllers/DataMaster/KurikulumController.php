<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Kurikulum;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class KurikulumController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $kurikulums         = Kurikulum::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.kurikulum.index', [
            'kurikulums'    => $kurikulums
        ]);
    }

    public function create()
    {
        return view('data_master.kurikulum.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $kurikulum                      = new Kurikulum;
            $kurikulum->nama                = $this->request->nama;
            $kurikulum->status              = $this->request->has('status') ? $this->request->status : 'tidak aktif';
            $kurikulum->save();
            return redirect()->route('kurikulum.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('kurikulum.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kurikulum          =   Kurikulum::findOrFail($id);
        return view('data_master.kurikulum.edit', [
            'kurikulum'     => $kurikulum
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'  =>'required|min:3'
        ]);

        if(!$validate->fails()){
            $kurikulum                      =   Kurikulum::findOrFail($id);
            $kurikulum->nama                = $this->request->nama;
            $kurikulum->status              = $this->request->has('status') ? $this->request->status : 'tidak aktif';
            $kurikulum->save();
            return redirect()->route('kurikulum.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jurusan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $kurikulum          =   Kurikulum::findOrFail($id);
        $kurikulum->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
