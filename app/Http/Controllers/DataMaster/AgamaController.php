<?php
/**
*developer : hari
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Agama;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class AgamaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $agamas         = Agama::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.agama.index', [
            'agamas'    => $agamas
        ]);
    }

    public function create()
    {
        return view('data_master.agama.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $agama                      = new Agama;
            $agama->nama                = $this->request->nama;
            $agama->save();
            return redirect()->route('agama.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('agama.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $agama      = Agama::findOrFail($id);
        return view('data_master.agama.edit', [
            'agama' => $agama
        ]);   
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $agama                      = Agama::findOrFail($id);
            $agama->nama                = $this->request->nama;
            $agama->save();
            return redirect()->route('agama.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('agama.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $agama                      = Agama::findOrFail($id);
        $agama->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
