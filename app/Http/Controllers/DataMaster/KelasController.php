<?php

namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Kelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class KelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $kelass         = Kelas::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.kelas.index', [
            'kelass'    => $kelass
        ]);
    }

    public function create()
    {
        return view('data_master.kelas.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'kode_kelas'           => 'required|min:1',
            'nama_kelas'           => 'required|min:1'
        ]);

        if(!$validate->fails()){
            $kelas                              = new Kelas;
            $kelas->kode_kelas                  = $this->request->kode_kelas;
            $kelas->nama_kelas                  = $this->request->nama_kelas;
            $kelas->save();
            return redirect()->route('kelas.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('kelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelas      = Kelas::findOrFail($id);
        return view('data_master.kelas.edit', [
            'kelas'     => $kelas
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'kode_kelas'           => 'required|min:1',
            'nama_kelas'           => 'required|min:1'
        ]);

        if(!$validate->fails()){
            $kelas                              = Kelas::findOrFail($id);
            $kelas->kode_kelas                  = $this->request->kode_kelas;
            $kelas->nama_kelas                  = $this->request->nama_kelas;
            $kelas->save();
            return redirect()->route('kelas.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('kelas.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $kelas      = Kelas::findOrFail($id);
        $kelas->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
