<?php

namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\DataGuru;
use App\WaliKelas;
use App\Kurikulum;
use App\Kelas;
use App\MataPelajaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class WaliKelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $walikelass         = WaliKelas::select('wali_kelas.*', 'mata_pelajarans.kode', 'mata_pelajarans.nama', 'kelas.nama_kelas', 'kurikulums.nama as kurikulum_nama')
                                    ->join('mata_pelajarans', 'mata_pelajarans.id', 'wali_kelas.kode_mata_pelajaran')
                                    ->join('kelas', 'kelas.id', 'wali_kelas.kelas')
                                    ->join('kurikulums', 'kurikulums.id', 'wali_kelas.kurikulum_id')
                                    ->with('data_guru')->orderBy('wali_kelas.created_at', 'DESC')->paginate($this->limit);
        return view('data_master.wali_kelas.index', [
            'walikelass'    => $walikelass
        ]);
    }

    public function create()
    {
        $kelas = Kelas::all();
        $data_gurus          = DataGuru::all();
        $kurikulums          = Kurikulum::all();
        $mapel = MataPelajaran::all();
        return view('data_master.wali_kelas.create', [
            'data_gurus' => $data_gurus,
            'kurikulums' => $kurikulums,
            'kelas' => $kelas,
            'mapel' => $mapel
        ]);
    }

    public function store()
    {
        $validate                  = Validator::make($this->request->all(), [
            'kode_mata_pelajaran'   =>'required',
            // 'mata_pelajaran'        =>'required|min:3',
            'kelas'                 =>'required'
        ]);
        $mapel = MataPelajaran::findOrFail($this->request->kode_mata_pelajaran);
        if(!$validate->fails()){
            $wali_kelas                         = new WaliKelas;
            $wali_kelas->kurikulum_id           = $this->request->kurikulum_id;
            $wali_kelas->data_guru_id           = $this->request->data_guru_id;
            $wali_kelas->kode_mata_pelajaran    = $this->request->kode_mata_pelajaran;
            $wali_kelas->mata_pelajaran         = $mapel->nama;
            $wali_kelas->kelas                  = $this->request->kelas;
            $wali_kelas->save();
            return redirect()->route('wali_kelas.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('wali_kelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelas = Kelas::all();
        $mapel = MataPelajaran::all();
        $wali_kelas          = WaliKelas::findOrFail($id);
        $data_gurus          = DataGuru::all();
        $kurikulums          = Kurikulum::all();
        return view('data_master.wali_kelas.edit', [
            'data_gurus' => $data_gurus,
            'kurikulums' => $kurikulums,
            'wali_kelas' => $wali_kelas,
            'mapel' => $mapel,
            'kelas' => $kelas
        ]);
    }

    public function update(Request $request, $id)
    {
        // return $this->request->all();
         $validate                  = Validator::make($this->request->all(), [
            'kode_mata_pelajaran'   =>'required|',
            // 'mata_pelajaran'        =>'required|min:3',
            'kelas'                 =>'required'
        ]);

        $mapel = MataPelajaran::findOrFail($this->request->kode_mata_pelajaran);
        if(!$validate->fails()){
            $wali_kelas                         = WaliKelas::findOrFail($id);
            $wali_kelas->kurikulum_id           = $this->request->kurikulum_id;
            $wali_kelas->data_guru_id           = $this->request->data_guru_id;
            $wali_kelas->kode_mata_pelajaran    = $this->request->kode_mata_pelajaran;
            $wali_kelas->mata_pelajaran         = $mapel->nama;
            $wali_kelas->kelas                  = $this->request->kelas;
            $wali_kelas->save();
            return redirect()->route('wali_kelas.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            return $errors = $validate->messages();

            return redirect()->route('wali_kelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $wali_kelas                         = WaliKelas::findOrFail($id);
        $wali_kelas->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
