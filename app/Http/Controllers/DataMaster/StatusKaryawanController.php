<?php
/**
*developer : hari , galan & adi
*/
namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\StatusKaryawan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class StatusKaryawanController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $statusKaryawans        = StatusKaryawan::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.status_karyawan.index', [
            'statusKaryawans'   => $statusKaryawans
        ]);
    }

    public function create()
    {
        return view('data_master.status_karyawan.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $statusKaryawan                      = new StatusKaryawan;
            $statusKaryawan->nama               = $this->request->nama;
            $statusKaryawan->save();
            return redirect()->route('status_karyawan.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('status_karyawan.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $status_karyawan        = StatusKaryawan::findOrFail($id);
        return view('data_master.status_karyawan.edit', [
            'status_karyawan'   => $status_karyawan
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $status_karyawan                     = StatusKaryawan::findOrFail($id);
            $status_karyawan->nama               = $this->request->nama;
            $status_karyawan->save();
            return redirect()->route('status_karyawan.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('status_karyawan.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $status_karyawan                     = StatusKaryawan::findOrFail($id);
        $status_karyawan->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
