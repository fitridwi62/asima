<?php

namespace App\Http\Controllers\DataMaster;

use Redirect;
use Validator;
use App\Jenjang;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class JenjangController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }
    
    public function index()
    {
        $jenjangs       = Jenjang::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('data_master.jenjang.index', [
            'jenjangs'  => $jenjangs
        ]);
    }

    public function create()
    {
        return view('data_master.jenjang.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'kode_jenjang'           => 'required|min:2',
            'nama_jenjang'           => 'required|min:2',
        ]);

        if(!$validate->fails()){
            $jenjang                                = new Jenjang;
            $jenjang->kode_jenjang                  = $this->request->kode_jenjang;
            $jenjang->nama_jenjang                  = $this->request->nama_jenjang;
            $jenjang->save();
            return redirect()->route('jenjang.index')->with(['success' => 'Data berhasil disimpan']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jenjang.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jenjang        = Jenjang::findOrFail($id);
        return view('data_master.jenjang.edit',[
            'jenjang'   => $jenjang
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'kode_jenjang'           => 'required|min:2',
            'nama_jenjang'           => 'required|min:2',
        ]);

        if(!$validate->fails()){
            $jenjang                                = Jenjang::findOrFail($id);
            $jenjang->kode_jenjang                  = $this->request->kode_jenjang;
            $jenjang->nama_jenjang                  = $this->request->nama_jenjang;
            $jenjang->save();
            return redirect()->route('jenjang.index')->with(['success' => 'Data berhasil diupdate']);
        }else{
            $errors = $validate->messages();

            return redirect()->route('jenjang.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jenjang        = Jenjang::findOrFail($id);
        $jenjang->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
}
