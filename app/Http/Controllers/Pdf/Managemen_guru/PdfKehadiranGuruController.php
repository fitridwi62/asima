<?php

namespace App\Http\Controllers\Pdf\Managemen_guru;

use PDF;
use App\DataGuru;
use App\KehadiranGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfKehadiranGuruController extends Controller
{
    public function cetak_kehadiran_guru(){
        $kehadiranGurus         = KehadiranGuru::orderBy('created_at', 'DESC')
                                                ->with('guru')
                                                ->get();
        $pdf                    = PDF::loadview('manajemen_guru.kehadiran_guru.pdf', [
            'kehadiranGurus'    => $kehadiranGurus
        ]);
        return $pdf->download('laporan-kehadiran-guru');
    }
}
