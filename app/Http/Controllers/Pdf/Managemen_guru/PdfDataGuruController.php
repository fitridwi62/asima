<?php

namespace App\Http\Controllers\Pdf\Managemen_guru;

use PDF;
use App\DataGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDataGuruController extends Controller
{
    public function ceta_data_guru(){
        $data_gurus         = DataGuru::orderBy('created_at', 'DESC')->get();

        $pdf                = PDF::loadview('data_master.data_guru.pdf', [
            'data_gurus'    => $data_gurus
        ]);
        
        return $pdf->download('laporan-data-guru');
    }
}
