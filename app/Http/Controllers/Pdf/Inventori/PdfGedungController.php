<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Gedung;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfGedungController extends Controller
{
    public function cetak_gedung(){
        $gedungs                = Gedung::orderBy('created_at', 'DESC')
                                        ->get();
        $pdf                    = PDF::loadview('inventori.gedung.pdf', [
            'gedungs'           => $gedungs
        ]);
        return $pdf->download('laporan-data-gedung');
    }
}
