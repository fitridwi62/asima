<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\KategoriInventori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfKategoriInventoriController extends Controller
{
    public function cetak_kategori_inventori(){
        $kategoriInventories        = KategoriInventori::orderBy('created_at', 'DESC')
                                                        ->get();
        $pdf                        = PDF::loadview('inventori.kategori-inventori.pdf', [
            'kategoriInventories'   => $kategoriInventories
        ]);
        return $pdf->download('laporan-data-kategori-inventori');
    }
}
