<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Barang;
use App\BarangMasuk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfBarangMasukController extends Controller
{
    public function cetak_barang_masuk(){
        $barangMasuks           = BarangMasuk::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                    = PDF::loadview('inventori.barang-masuk.pdf', [
            'barangMasuks'      => $barangMasuks
        ]);
        return $pdf->download('laporan-barang-masuk');
    }
}
