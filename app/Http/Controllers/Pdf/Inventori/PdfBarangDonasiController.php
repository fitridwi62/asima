<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Donasi;
use App\TipeDonasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfBarangDonasiController extends Controller
{
    public function cetak_donasi(){
        $donasis            = Donasi::orderBy('created_at', 'DESC')
                                    ->get();
        $pdf                = PDF::loadview('inventori.donasi.pdf', [
            'donasis'       => $donasis
        ]);
        return $pdf->download('laporan-inventori-donasi');
    }
}
