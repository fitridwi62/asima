<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\TipeDonasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfTipeDonasiController extends Controller
{
    public function cetak_tipe_donasi(){
        $tipeDonasis            = TipeDonasi::orderBy('created_at', 'DESC')
                                            ->get();
        $pdf                    = PDF::loadview('inventori.tipe-donasi.pdf', [
            'tipeDonasis'       => $tipeDonasis
        ]);
        return $pdf->download('laporan-tipe-donasi');
    }
}
