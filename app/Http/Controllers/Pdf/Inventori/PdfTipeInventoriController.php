<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\TipeInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfTipeInventoriController extends Controller
{
    public function cetak_tipe_inventory(){
        $tipeInventories        = TipeInventory::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                    = PDF::loadview('', [
            'tipeInventories'   => $tipeInventories
        ]);
        return $pdf->download('laporan-data-tipe-inventori');
    }
}
