<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfBarangController extends Controller
{
    public function cetak_barang(){
        $barangs            = Barang::orderBy('created_at', 'DESC')
                                    ->get();
        $pdf                = PDF::loadview('inventori.barang.pdf', [
            'barangs'       => $barangs
        ]);
        return $pdf->download('laporan-Barang');
    }
}
