<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Barang;
use App\BarangKeluar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfBarangKeluarController extends Controller
{
    public function cetak_barang_keluar(){
        $barangKeluars          = BarangKeluar::orderBy('created_at', 'DESC')
                                            ->get();
        $pdf                    = PDF::loadview('inventori.barang-keluar.pdf', [
            'barangKeluars'     => $barangKeluars
        ]);
        return $pdf->download('laporan-barang-keluar');
    }
}
