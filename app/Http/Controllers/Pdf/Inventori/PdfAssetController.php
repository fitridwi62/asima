<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\Asset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfAssetController extends Controller
{
    public function cetak_asset(){
        $assets         = Asset::orderBy('created_at', 'DESC')
                                ->get();
        $pdf            = PDF::loadview('inventori.asset.pdf', [
            'assets'    => $assets
        ]);
        return $pdf->download('laporan-data-asset');
    }
}
