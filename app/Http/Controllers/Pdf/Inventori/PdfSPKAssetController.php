<?php

namespace App\Http\Controllers\Pdf\Inventori;

use PDF;
use App\SpkAsset;
use App\Gedung;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfSPKAssetController extends Controller
{
    public function cetak_spk_asset(){
        $spkAssets              = SpkAsset::orderBy('created_at')
                                            ->get();
        $pdf                    = PDF::loadview('inventori.spk-asset.pdf', [
            'spkAssets'         => $spkAssets
        ]);
        return $pdf->download('laporan-data-spk-asset');
    }
}
