<?php

namespace App\Http\Controllers\Pdf\Pegawai;

use PDF;
use App\Agama;
use App\Jabatan;
use App\StaffPegawai;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPegawaiController extends Controller
{
    public function cetak_data_pegawai(){
        $staffs         = StaffPegawai::orderBy('created_at', 'DESC')->get();
        $pdf            = PDF::loadview('staff.pdf', [
            'staffs'    => $staffs
        ]);
        return $pdf->download('laporan-data-staff');
    }
}
