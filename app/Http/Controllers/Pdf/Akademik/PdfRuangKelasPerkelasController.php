<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Kelas;
use App\RuangKelasPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfRuangKelasPerkelasController extends Controller
{
    public function cetak_ruang_kelas_perkelas(){
        $ruangKelass        = RuangKelasPerkelas::orderBy('created_at', 'DESC')
                                                ->with('kelas')
                                                ->get();
        
        $pdf                = PDF::loadview('akademik.ruang_kelas.pdf_ruangKelas_perkelas', [
            'ruangKelass'   => $ruangKelass
        ]);

        return $pdf->download('laporan-ruang-kelas');
    }
}
