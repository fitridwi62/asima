<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Kelas;
use App\MataPelajaran;
use App\DataGuru;
use App\MataPelajaranGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfMatkulGuruController extends Controller
{
    public function cetak_matkul_guru(){
        $mataPelajaranGurus = MataPelajaranGuru::orderBy('created_at', 'DESC')
                                                ->get();
        
        $pdf                = PDF::loadview('akademik.mata_pelajaran_guru.pdf', [
            'mataPelajaranGurus'    => $mataPelajaranGurus
        ]);

        return $pdf->download('laporan-matkul-guru');
    }
}
