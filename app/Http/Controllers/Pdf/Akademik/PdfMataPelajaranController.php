<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use Redirect;
use App\MataPelajaran;
use App\Kurikulum;
use App\DataGuru;
use App\Jenjang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfMataPelajaranController extends Controller
{
    public function cetak_mapel(){
        $mata_pelajarans        = MataPelajaran::with('guru')
                                                ->orderBy('created_at', 'DESC')
                                                ->get();
                                                
        $pdf                    = PDF::loadview('akademik.mata_pelajaran.pdf_mapel',[
            'mata_pelajarans'   => $mata_pelajarans
        ]);
        return $pdf->download('laporan-mata-pelajaran-pdf');
    }
}
