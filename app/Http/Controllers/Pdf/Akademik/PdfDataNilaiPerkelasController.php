<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\MataPelajaran;
use App\Kelas;
use App\Siswa;
use App\Nilaiperkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDataNilaiPerkelasController extends Controller
{
    public function cetak_nilai_perkelas(){
        $nilaiPerkelass      = Nilaiperkelas::orderBy('created_at', 'DESC')
                                            ->get();
        
        $pdf                 = PDF::loadview('akademik.nilai-perkelas.pdf', [
            'nilaiPerkelass'    => $nilaiPerkelass
        ]);
        return $pdf->download('laporan-data-nilai-perkelas');
    }
}
