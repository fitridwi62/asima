<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\GridNilai;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfGridNilaiController extends Controller
{
    public function cetak_grid_nilai(){
        $gridNilais         = GridNilai::orderBy('created_at', 'DESC')->get();

        $pdf                = PDF::loadview('akademik.grid_nilai.pdf', [
            'gridNilais'    => $gridNilais
        ]);

        return $pdf->download('laporan-data-grid-nilai');
    }
}
