<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use Redirect;
use App\Agama;
use App\Jenjang;
use App\Jurusan;
use App\Siswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfSiswaController extends Controller
{
    public function cetak_siswa_pdf(){
        $siswas         = Siswa::orderBy('created_at', 'DESC')
                                ->with('agama')
                                ->with('jenjang')
                                ->with('jurusan')
                                ->get();
        $pdf            = PDF::loadview('manajemen_siswa.siswa.siswa_pdf',[
            'siswas'    => $siswas
        ]);
        return $pdf->download('laporan-siswa-pdf');
    }
}
