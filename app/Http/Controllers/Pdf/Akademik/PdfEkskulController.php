<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Ekstrakurikuler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfEkskulController extends Controller
{
    public function cetak_ekskul(){
        $extraKurikulers        = Ekstrakurikuler::orderBy('created_at', 'DESC')
                                                ->get();
        
        $pdf                    = PDF::loadview('akademik.ekstrakurikuler.pdf_ekskul', [
            'extraKurikulers'   => $extraKurikulers
        ]);

        return $pdf->download('laporan-data-ekskul');
    }
}
