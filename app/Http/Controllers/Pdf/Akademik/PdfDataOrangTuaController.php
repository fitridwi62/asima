<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Siswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDataOrangTuaController extends Controller
{
    public function cetak_data_orang_tua_siswa(){
        $dataOrangTuas       = Siswa::orderBy('created_at', 'DESC')
                                    ->get();
        
        $pdf            = PDF::loadview('manajemen_siswa.data_orang_tua.pdf_ortu',[
            'dataOrangTuas'     => $dataOrangTuas
        ]); 

        return $pdf->download('laporan-data-orangtua-siswa');
    }
}
