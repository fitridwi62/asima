<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use Redirect;
use App\DataGuru;
use App\BimbinganBelajar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfBimbinganBelajarController extends Controller
{
    public function cetak_bimbingan_belajar_pdf(){
        $bimbinganBelajars      = BimbinganBelajar::orderBy('created_at')
                                                ->with('guru')
                                                ->get();
        
        $pdf            = PDF::loadview('akademik.bimbingan_belajar.bimbel_pdf',[
            'bimbinganBelajars'    => $bimbinganBelajars
        ]);
        return $pdf->download('laporan-bimbel-pdf');
    }
}
