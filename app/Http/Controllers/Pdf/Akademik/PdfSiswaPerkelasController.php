<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Siswa;
use App\Kelas;
use App\SiswaPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfSiswaPerkelasController extends Controller
{
    public function cetak_siswa_perkelas(){
        $siswaPerkelasLists         = SiswaPerkelas::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                        = PDF::loadview('akademik.siswa_perkelas.pdf', [
            'siswaPerkelasLists'    => $siswaPerkelasLists
        ]);
        return $pdf->download('laporan-data-siswa-perkelas');
    }
}
