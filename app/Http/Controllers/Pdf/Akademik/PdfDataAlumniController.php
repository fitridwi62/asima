<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use Redirect;
use App\DataAlumni;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDataAlumniController extends Controller
{
    public function cetak_alumni(){
        $dataAlumnis            = DataAlumni::orderBy('created_at', 'DESC')
                                            ->get();

        $pdf                    = PDF::loadview('akademik.data_alumni.pdf_alumni',[
            'dataAlumnis'       => $dataAlumnis
        ]);
        return $pdf->download('laporan-alumni-pdf');
    }
}
