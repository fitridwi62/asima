<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\MataPelajaran;
use App\Kelas;
use App\DataGuru;
use App\JadwalMataPelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JadwalMatkulController extends Controller
{
    public function cetak_jadwal_matkul(){
        $jadwalMataPelajarans       = JadwalMataPelajaran::with('kelas')
                                                        ->with('mata_pelajaran')
                                                        ->with('guru')
                                                        ->get();

        $pdf                        = PDF::loadview('akademik.jadwal_mata_pelajaran.pdf_jadwal_matkul',[
            'jadwalMataPelajarans'  => $jadwalMataPelajarans
        ]);

        return $pdf->download('lapaoran-jadwal-mata-pelajaran');
    }
}
