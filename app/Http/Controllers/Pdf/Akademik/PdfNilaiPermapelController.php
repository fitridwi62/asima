<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\MataPelajaran;
use App\GridNilai;
use App\DataGuru;
use App\Siswa;
use App\NilaiPermapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfNilaiPermapelController extends Controller
{
    public function cetak_nilai_permapel(){
        $nilaiPermapels         = NilaiPermapel::orderBy('created_at')
                                            ->get();

        $pdf                    = PDF::loadview('akademik.nilai-permapel.pdf', [
            'nilaiPermapels'    => $nilaiPermapels
        ]);

        return $pdf->download('laporan-nilai-permaper');
    }
}
