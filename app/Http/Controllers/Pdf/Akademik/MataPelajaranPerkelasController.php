<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Kelas;
use App\MataPelajaran;
use App\MataPelajaranPerkelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MataPelajaranPerkelasController extends Controller
{
    public function matkul_perkelas(){
        $mataPelajarPerkelass         = MataPelajaranPerkelas::orderBy('created_at')
                                                            ->with('kelas')
                                                            ->with('mata_pelajar')
                                                            ->get();
        $pdf                          = PDF::loadview('akademik.mata_pelajar_perkelas.pdf', [
            'mataPelajarPerkelass'    => $mataPelajarPerkelass
        ]);
        return $pdf->download('laporan-matkul-perkelas');
    }
}
