<?php

namespace App\Http\Controllers\Pdf\Akademik;

use PDF;
use App\Kelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfKelasController extends Controller
{
    public function cetak_kelas(){
        $kelass         = Kelas::orderBy('created_at', 'DESC')->get();
        $pdf            = PDF::loadview('data_master.kelas.pdf_kelas', [
            'kelass'    => $kelass
        ]);
        return $pdf->download('laporan-data-kelas');
    }
}
