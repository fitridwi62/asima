<?php

namespace App\Http\Controllers\Pdf\Perpustakaan;

use PDF;
use App\DataBuku;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPeminjamanBukuPerpustakaanController extends Controller
{
    public function cetak_peminjaman_buku(){
        $peminjamanBukus        = PeminjamanBuku::orderBy('created_at', 'DESC')
                                                ->get();
        
        $pdf                    = PDF::loadview('perpustakaan.peminjaman-buku.pdf',[
            'peminjamanBukus'   => $peminjamanBukus
        ]);
        return $pdf->download('laporan-data-peminjaman-buku');
    }
}
