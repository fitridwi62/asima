<?php

namespace App\Http\Controllers\Pdf\Perpustakaan;

use PDF;
use App\PetugasPerpustakaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPetugasPerpustakaanController extends Controller
{
    public function cetak_data_petugas(){
        $petugass            = PetugasPerpustakaan::orderBy('created_at', 'DESC')
                                                ->get();
        
        $pdf                 = PDF::loadview('perpustakaan.petugas.pdf', [
            'petugass'       => $petugass
        ]);
        return $pdf->download('laporan-data-petugas');
    }
}
