<?php

namespace App\Http\Controllers\Pdf\Perpustakaan;

use PDF;
use App\Pengembalian;
use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPengembalianBukuPerpustakaanController extends Controller
{
    public function cetak_pengembalian_buku(){
        $pengembalians           = Pengembalian::orderBy('created_at', 'DESC')
                                            ->get();
        $pdf                     = PDF::loadview('perpustakaan.pengembalian-buku.pdf', [
            'pengembalians'      => $pengembalians
        ]);
        return $pdf->download('laporan-data-pengembalian');
    }
}
