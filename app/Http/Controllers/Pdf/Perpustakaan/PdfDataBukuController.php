<?php

namespace App\Http\Controllers\Pdf\Perpustakaan;

use PDF;
use App\DataBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDataBukuController extends Controller
{
    public function cetak_data_buku(){
        $dataBukus          = DataBuku::orderBy('created_at', 'DESC')
                                      ->get();
        $pdf                = PDF::loadview('perpustakaan.buku.pdf', [
            'dataBukus'     => $dataBukus
        ]);
        return $pdf->download('laporan-data-buku');
    }
}
