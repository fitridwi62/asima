<?php

namespace App\Http\Controllers\Pdf\Keuangan_siswa;

use PDF;
use App\PembayaranBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPembayaranBukuController extends Controller
{
    public function cetak_pembayaran_buku(){
        $pembayaranBukus            = PembayaranBuku::orderBy('created_at', 'DESC')
                                                    ->get();
        $pdf                        = PDF::loadview('manajemen_uang_siswa.pembayaran_buku.pdf', [
            'pembayaranBukus'       => $pembayaranBukus
        ]);
        return $pdf->download('laporan-pembayaran-buku');
    }
}
