<?php

namespace App\Http\Controllers\Pdf\Keuangan_siswa;

use PDF;
use App\PembayaranSemester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPembayaranSemesterController extends Controller
{
    public function cetak_pembayaran_semester(){
        $pembayaranSemesters         = PembayaranSemester::orderBy('created_at', 'DESC')
                                                        ->get();

        $pdf                         = PDF::loadview('manajemen_uang_siswa.pembayaran_semester.pdf', [
            'pembayaranSemesters'    => $pembayaranSemesters
        ]); 
        return $pdf->download('laporan-pembayaran-semester');
    }
}
