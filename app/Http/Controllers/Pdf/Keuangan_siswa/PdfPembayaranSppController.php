<?php

namespace App\Http\Controllers\Pdf\Keuangan_siswa;

use PDF;
use App\PembayaranSpp;
use App\JenisPembayaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPembayaranSppController extends Controller
{
    public function cetak_pembayaran_spp(){
        $pembayaranSpps       = PembayaranSpp::orderBy('created_at', 'DESC')
                                                ->with('jenis_pembayaran')
                                                ->get();
        $pdf                  = PDF::loadview('manajemen_uang_siswa.pembayaran_spp.pdf', [
            'pembayaranSpps'  => $pembayaranSpps
        ]);
        return $pdf->download('laporan-pembayaran-spp');
    }
}
