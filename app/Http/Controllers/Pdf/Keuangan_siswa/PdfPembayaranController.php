<?php

namespace App\Http\Controllers\Pdf\Keuangan_siswa;

use PDF;
use App\Siswa;
use App\PembayaranSpp;
use App\PembayaranSemester;
use App\Pembayaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPembayaranController extends Controller
{
    public function cetak_pembayaran(){
        $pembayarans            = Pembayaran::orderBy('created_at', 'desc')
                                            ->get();
        $pdf                    = PDF::loadview('manajemen_uang_siswa.pembayaran.pdf', [
            'pembayarans'       => $pembayarans
        ]);

        return $pdf->download('laporan-pembayaran');
    }
}
