<?php

namespace App\Http\Controllers\Pdf\Keuangan_siswa;

use PDF;
use App\JenisPembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJenisPembayaranController extends Controller
{
    public function cetak_jenis_pembayaran(){
        $jenisPembayarans       = JenisPembayaran::orderBy('created_at', 'DESC')
                                                 ->get();
        
        $pdf                    = PDF::loadview('manajemen_uang_siswa.jenis_pembayaran.pdf', [
            'jenisPembayarans'  => $jenisPembayarans
        ]);

        return $pdf->download('laporan-jenis-pembayaran'); 
    }
}
