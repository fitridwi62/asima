<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\Jurnalpro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalProController extends Controller
{
    public function cetak_jurnal_pro(){
        $jurnalPros         = Jurnalpro::orderBy('created_at', 'DESC')
                                        ->get();
        $pdf                = PDF::loadview('accounting.pemasukan.pro.pdf', [
            'jurnalPros'    => $jurnalPros
        ]);
        return $pdf->download('laporan-data-jurnal-pro');
    }
}
