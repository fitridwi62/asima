<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\Hutang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfHutangController extends Controller
{
    public function cetak_hutang(){
        $hutangs            = Hutang::orderBy('created_at', 'DESC')
                                    ->get();
        $pdf                = PDF::loadview('accounting.pemasukan.hutang.pdf', [
            'hutangs'       => $hutangs
        ]);
        return $pdf->download('laporan-data-hutang');
    }
}
