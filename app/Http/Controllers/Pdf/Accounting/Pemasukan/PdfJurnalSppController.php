<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\JurnalSPP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalSppController extends Controller
{
    public function cetak_jurnal_spp(){
        $jurnalSpps         = JurnalSPP::orderBy('created_at', 'DESC')
                                        ->get();
        $pdf                = PDF::loadview('accounting.pemasukan.spp.pdf', [
            'jurnalSpps'    => $jurnalSpps
        ]);
        return $pdf->download('laporan-jurnal-spp');
    }
}
