<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\RekeningSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfRekeningSiswaController extends Controller
{
    public function cetak_rekening_siswa(){
        $rekeningSiswas         = RekeningSiswa::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                    = PDF::loadview('accounting.pemasukan.rekening-siswa.pdf', [
            'rekeningSiswas'    => $rekeningSiswas
        ]);
        return $pdf->download('laporan-data-rekening-siswa');
    }
}
