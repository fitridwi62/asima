<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\JurnalPBN;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalPbnController extends Controller
{
    public function cetak_jurnal_pbn(){
        $jurnalPbns         = JurnalPBN::orderBy('created_at', 'DESC')
                                        ->get();
        $pdf                = PDF::loadview('accounting.pemasukan.jurnal-pbn.pdf', [
            'jurnalPbns'    => $jurnalPbns
        ]);
        return $pdf->download('laporan-jurnal-pbn');
    }
}
