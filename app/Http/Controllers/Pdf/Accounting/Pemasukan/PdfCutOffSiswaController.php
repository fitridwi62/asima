<?php

namespace App\Http\Controllers\Pdf\Accounting\Pemasukan;

use PDF;
use App\CutOffSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfCutOffSiswaController extends Controller
{
    public function cetak_cutOffSiswa(){
        $cutOffSiswas       = CutOffSiswa::orderBy('created_at', 'DESC')
                                        ->get();
        $pdf                = PDF::loadview('accounting.pemasukan.cut-off-siswa.pdf',[
            'cutOffSiswas'  => $cutOffSiswas 
        ]);
        return $pdf->download('laporan-data-cut-off-siswa');
    }
}
