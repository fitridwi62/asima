<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\PengelauaranJurnalKasKecilReimbursment;
use App\PengeluaranPermintaanDana;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfReibustmentController extends Controller
{
    public function cetak_reibustment(){
        $reibustments           = PengelauaranJurnalKasKecilReimbursment::orderBy('created_at', 'DESC')
                                                                        ->get();
        $pdf                    = PDF::loadview('accounting.pengeluaran.reibursment.pdf', [
            'reibustments'      => $reibustments
        ]);
        return $pdf->download('laporan-data-reibustment');
    }
}
