<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\Siswa;
use App\JurnalKasMutasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalKasMutasiController extends Controller
{
    public function cetak_kas_mutasi(){
        $jurnalKasMutasis       = JurnalKasMutasi::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                    = PDF::loadview('accounting.pengeluaran.kas-mutasi.pdf', [
            'jurnalKasMutasis'  => $jurnalKasMutasis
        ]);
        return $pdf->download('laporan-jurnal-kas-mutasi');
    }
}
