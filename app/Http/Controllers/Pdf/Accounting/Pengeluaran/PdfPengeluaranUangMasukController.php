<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\Siswa;
use App\JenisPembayaran;
use App\PengeluaranUangMasuk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPengeluaranUangMasukController extends Controller
{
    public function cetak_pengeluaran_uang_masuk(){
        $pengeluaranUangMasuks      = PengeluaranUangMasuk::orderBy('created_at', 'DESC')
                                                        ->get();
        $pdf                        = PDF::loadview('accounting.pengeluaran.uang-masuk.pdf', [
            'pengeluaranUangMasuks' => $pengeluaranUangMasuks
        ]);
        return $pdf->download('laporan-pengeluaran-uang-masuk');
    }
}
