<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\Siswa;
use App\JenisPembayaran;
use App\JurnalEdc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalEdcController extends Controller
{
    public function cetak_jurnal_edc(){
        $jurnalEdcs             = JurnalEdc::orderBy('created_at', 'DESC')
                                            ->get();
        $pdf                    = PDF::loadview('accounting.pengeluaran.jurnal-edc.pdf', [
            'jurnalEdcs'        => $jurnalEdcs
        ]);
        return $pdf->download('laporan-jurnal-edc');
    }
}
