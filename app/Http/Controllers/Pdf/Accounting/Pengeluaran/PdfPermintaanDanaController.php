<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\PengeluaranPermintaanDana;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPermintaanDanaController extends Controller
{
    public function cetak_permintaan_dana(){
        $permintaanDanas            = PengeluaranPermintaanDana::orderBy('created_at', 'DESC')
                                                                ->get();
        $pdf                        = PDF::loadview('accounting.pengeluaran.permintaan-dana.pdf', [
            'permintaanDanas'       => $permintaanDanas
        ]);
        return $pdf->download('laporan-permintaan-dana');
    }
}
