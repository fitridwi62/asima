<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\DanaPending;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfDanaPendingController extends Controller
{
    public function cetak_dana_pending(){
        $danaPendings       = DanaPending::orderBy('created_at', 'DESC')
                                        ->get();
        
        $pdf                = PDF::loadview('accounting.pengeluaran.dana-pending.pdf',[
            'danaPendings'  => $danaPendings
        ]);
        return $pdf->download('laporan-dana-pending');
    }
}
