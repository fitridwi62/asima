<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\JurnalUangKeluar;
use App\PengelauaranJurnalKasKecilReimbursment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfJurnalUangKeluarController extends Controller
{
    public function cetak_jurnal_uang_keluar(){
        $jurnalUangKeluars      = JurnalUangKeluar::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                    = PDF::loadview('accounting.pengeluaran.uang-keluar.pdf',[
            'jurnalUangKeluars' => $jurnalUangKeluars
        ]);
        return $pdf->download('laporan-jurnal-uang-keluar');
    }
}
