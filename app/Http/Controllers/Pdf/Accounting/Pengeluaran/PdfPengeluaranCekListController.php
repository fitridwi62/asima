<?php

namespace App\Http\Controllers\Pdf\Accounting\Pengeluaran;

use PDF;
use App\PengeluaranCekList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfPengeluaranCekListController extends Controller
{
    public function cetak_pengeluaran_cek_list(){
        $cekLists           = PengeluaranCekList::orderBy('created_at', 'DESC')
                                                ->get();
        $pdf                = PDF::loadview('accounting.pengeluaran.cek-list.pdf', [
            'cekLists'      => $cekLists
        ]);
        return $pdf->download('laporan-pengeluaran-cek-list');                                           
    }
}
