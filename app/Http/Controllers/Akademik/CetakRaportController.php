<?php

namespace App\Http\Controllers\Akademik;

use DB;
use Auth;
use Redirect;
use Session;
use App\Raport;
use App\Siswa;
use App\MataPelajaran;
use App\RaportEkstrakurikuler;
use App\RaportMapel;
use App\RaportEkstraItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CetakRaportController extends Controller
{
    public function index(){
        $siswa_lists        = RaportMapel::with('siswaItem')
                                        ->groupBy('siswa_id')
                                        ->get();
        return view('akademik.Raport.index',[
            'siswa_lists'   => $siswa_lists
        ]);
    }

    public function create_ektra(){
        $session_raport_id 	     = Session::get('session_raport_id');
        $raportEkstras 		     = RaportEkstrakurikuler::where(['session_raport_id' => $session_raport_id])
                                                        ->get();
        $siswas                  = Siswa::all();
        return view('akademik.Raport.ekstraCreate',[
            'raportEkstras'      => $raportEkstras,
            'siswas'             => $siswas
        ]);
    }

    public function getNilaiMapel(Request $request){
        $session_raport_id 	= Session::get('session_raport_id');
        if(empty($session_raport_id )){
            $session_raport_id 		    = str_random(40);
            Session::put('session_raport_id', $session_raport_id);
        }
        $nilaiEkstra                            = new RaportEkstrakurikuler;
        $nilaiEkstra->nama_ekstrakurikuler      = $request->nama_ekstrakurikuler;
        $nilaiEkstra->nilai                     = $request->nilai;
        $nilaiEkstra->predikat                  = $request->predikat;
        $nilaiEkstra->siswa_id                  = $request->siswa_id;
        $nilaiEkstra->session_raport_id         = $session_raport_id;
        $nilaiEkstra->save();
        return redirect()->back();
    }
    
    public function getNilaiPersiswa($id){
        $raportMapelNilai            = RaportMapel::whereHas('siswaItem', function($query) use ($id){
          $query->where('siswa_id', $id);  
        })
        ->with('siswaItem')
        ->get();
        $nama_lengkap   = RaportMapel::whereHas('siswaItem', function($query) use ($id){
            $query->where('siswa_id', $id);  
          })
          ->with('siswaItem')
          ->first();
        
        $totalSiswa         = count(Siswa::all());

        $ekstraNilais                 = RaportEkstraItem::whereHas('siswaItem', function($queryEkstra) use ($id){
            $queryEkstra->where('siswa_id', $id);  
        })->with('siswaItem')->get();
       // dd($ekstraNilais);
        return view('akademik.cetak_raport.index',[
            'raportMapelNilai'          => $raportMapelNilai,
            'ekstraNilais'              => $ekstraNilais,
            'nama_lengkap'              => $nama_lengkap,
            'totalSiswa'                => $totalSiswa
        ]);
    }

    public function review_raport(){
        $session_id 	         = Session::get('session_id');
        $raports 		         = Raport::where(['session_id' => $session_id])
                                            ->with('siswaItem')
                                            ->get();

        $session_raport_id 	     = Session::get('session_raport_id');
        $raportEkstras 		     = RaportEkstrakurikuler::where(['session_raport_id' => $session_raport_id])
                                                        ->get();
        return view('akademik.Raport.review-nilai',[
            'raports'           => $raports,
            'raportEkstras'     => $raportEkstras
        ]);
    }

    public function addDataNilaiRaport(Request $request){
        $session_id              = Session::get('session_id');
        $raport_ekstra_id        = DB::getPdo()->lastInsertId();
        $raports 		         = Raport::where(['session_id' => $session_id])
                                            ->with('siswaItem')
                                            ->get();
        
        //$grandTotal = (int)$total
        foreach($raports as $key => $raport){
            $user_id                            = Auth::user()->id;
            $raportMapel                        = new RaportMapel;
            $raportMapel->siswa_id              = $raport->siswa_id;
            $raportMapel->matkul_id             = $raport->matkul_id;
            //$raportMapel->raport_ekstra_id      = $raport_ekstra_id;
            $raportMapel->nilai_pengetahuan     = $raport->nilai_pengetahuan;
            $raportMapel->nilai_keterampilan    = $raport->nilai_keterampilan;
            $raportMapel->nilai_akhir           = $raport->nilai_akhir;
            $raportMapel->predikat              = $raport->predikat;
            $raportMapel->user_id               = $user_id;
            //$raportMapel->total                 = (int)$total + $key;
            $raportMapel->save();
        }

        $session_raport_id 	     = Session::get('session_raport_id');
        $raportEkstras 		     = RaportEkstrakurikuler::where(['session_raport_id' => $session_raport_id])
                                                        ->get();
        foreach($raportEkstras  as $raportEkstra){
            $ekstraItem                         = new RaportEkstraItem;
            $ekstraItem->nama_ekstrakurikuler   = $raportEkstra->nama_ekstrakurikuler;
            $ekstraItem->nilai                  = $raportEkstra->nilai;
            $ekstraItem->predikat               = $raportEkstra->predikat;
            $ekstraItem->siswa_id               = $raportEkstra->siswa_id;
            $ekstraItem->save();
        }
        return redirect()->route('akademik.pengaturan-raport.selesai');
    }

    public function thanks(Request $request){
        $session_raport_id   = Session::get('session_raport_id');
        DB::table('raport_ekstrakurikulers')->where('session_raport_id',$session_raport_id )->delete();
        $session_id   = Session::get('session_id');
        DB::table('raports')->where('session_id',$session_id )->delete();
        return view('akademik.Raport.thanks');
    }
}
