<?php

namespace App\Http\Controllers\Akademik;

use DB;
use Redirect;
use Session;
use Validator;
use App\Raport;
use App\Siswa;
use App\MataPelajaran;
use App\TahunAkademik;
use App\Kelas;
use App\RaportEkstrakurikuler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RaportController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
       
    }

    public function create()
    {
        $siswas                  = Siswa::all();
        $mapels                  = MataPelajaran::all();
        //get data session
        $session_id 	         = Session::get('session_id');
        $raports 		         = Raport::where(['session_id' => $session_id])
                                                        ->with('siswaItem')
                                                        ->get();
        return view('akademik.Raport.create',[
            'siswas'            => $siswas,
            'mapels'            => $mapels,
            'raports'           => $raports
        ]);
    }

    public function store()
    {
        $validate                   = Validator::make($this->request->all(),[
            'siswa_id'              => 'required'
        ]);
        if(!$validate->fails()){
            $session_id 	= Session::get('session_id');
            if(empty($session_id )){
                $session_id 		= str_random(40);
                Session::put('session_id', $session_id);
            }
            $raport                     = new Raport;
            $raport->siswa_id           = $this->request->siswa_id;
            $raport->matkul_id          = $this->request->matkul_id;
            $raport->nilai_pengetahuan  = $this->request->nilai_pengetahuan;
            $raport->nilai_keterampilan = $this->request->nilai_keterampilan;
            $raport->nilai_akhir        = $this->request->nilai_akhir;
            $raport->predikat           = $this->request->predikat;
            $raport->session_id         = $session_id;
            $raport->save();
            return redirect()->back();
        }else{
            $errors             = $validate->messages();
            return redirect()->route('raport.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $raport                  = Raport::findOrFail($id);
        $mapels                  = MataPelajaran::all();
        $siswas                  = Siswa::all();
        $tahun_akademiks         = TahunAkademik::all();
        $kelas                   = Kelas::all();
        return view('akademik.Raport.edit',[
            'raport'    => $raport,
            'mapels'    => $mapels,
            'siswas'    => $siswas,
            'tahun_akademiks' => $tahun_akademiks,
            'kelas'     => $kelas
        ]);
    }

    public function update($id)
    {
        $validate                   = Validator::make($this->request->all(),[
            'nilai_pengetahuan'     => 'required',
            'nilai_akhir'           => 'required',
        ]);
        if(!$validate->fails()){
            $raport                     = Raport::findOrFail($id);
            $raport->siswa_id           = $this->request->siswa_id;
            $raport->tahun_akademik_id  = $this->request->tahun_akademik_id;
            $raport->kelas_id           = $this->request->kelas_id;
            $raport->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Data Raport Berhasil Disimpan"
            ]);
            return redirect()->route('raport.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('raport.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $raport         = Raport::findOrFail($id);
        $raport->delete();
        return redirect()->back();
    }
}
