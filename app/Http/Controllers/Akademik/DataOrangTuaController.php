<?php

namespace App\Http\Controllers\Akademik;

use PDF;
use App\Siswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataOrangTuaController extends Controller
{
    protected $limit = 10;
    public function __construct(){
    	$this->middleware('auth');
    }
    public function index(){
        $dataOrangTuas       = Siswa::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('manajemen_siswa.data_orang_tua.index',[
            'dataOrangTuas'  => $dataOrangTuas
        ]);
    }
}
