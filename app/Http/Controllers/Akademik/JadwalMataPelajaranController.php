<?php

namespace App\Http\Controllers\Akademik;

use Auth;
use Redirect;
use Validator;
use App\MataPelajaran;
use App\Kelas;
use App\Siswa;
use App\DataGuru;
use App\JadwalMataPelajaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class JadwalMataPelajaranController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jadwalMataPelajarans       = JadwalMataPelajaran::with('kelas')
                                                        ->with('mata_pelajaran')
                                                        ->with('guru')
                                                        ->paginate($this->limit);
        $mataPelajarans             = MataPelajaran::all(); 
        return view('akademik.jadwal_mata_pelajaran.index', [
            'jadwalMataPelajarans'  => $jadwalMataPelajarans,
            'mataPelajarans'        => $mataPelajarans
        ]);
    }

    public function create()
    {
        $kelass         = Kelas::all();
        $dataGuru       = DataGuru::all();
        $mataPelajarans  = MataPelajaran::all();
        $siswa           = Siswa::all();
        return view('akademik.jadwal_mata_pelajaran.create',[
            'kelass'            => $kelass,
            'dataGuru'          => $dataGuru,
            'mataPelajarans'     => $mataPelajarans,
            'siswa'             => $siswa
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'             => 'required|date',
            'jam_mulai'           => 'required',
            'jam_selesai'         => 'required',
            'durasi'              => 'required'
        ]);

        if(!$validate->fails()){
            $user_id                                       = Auth::user()->id;
            $jadwalMataPelajaran                           = new JadwalMataPelajaran;
            $jadwalMataPelajaran->kelas_id                 = $this->request->kelas_id;
            $jadwalMataPelajaran->mata_pelajaran_id        = $this->request->mata_pelajaran_id;
            $jadwalMataPelajaran->siswa_id                 = $this->request->siswa_id;
            $jadwalMataPelajaran->tanggal                  = $this->request->tanggal;
            $jadwalMataPelajaran->jam_mulai                = $this->request->jam_mulai;
            $jadwalMataPelajaran->jam_selesai              = $this->request->jam_selesai;
            $jadwalMataPelajaran->durasi                   = $this->request->durasi;
            $jadwalMataPelajaran->data_guru_id             = $this->request->data_guru_id;
            $jadwalMataPelajaran->user_id                  = $user_id;
            $jadwalMataPelajaran->save();
            return redirect()->route('jadwal-mata-pelajaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('jadwal-mata-pelajaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jadwalMataPelajaran        = JadwalMataPelajaran::findOrFail($id);
        $kelass                     = Kelas::all();
        $dataGuru                   = DataGuru::all();
        $mataPelajarans             = MataPelajaran::all();
        $siswa                      = Siswa::all();
        return view('akademik.jadwal_mata_pelajaran.edit',[
            'kelass'                => $kelass,
            'dataGuru'              => $dataGuru,
            'mataPelajarans'        => $mataPelajarans,
            'jadwalMataPelajaran'   => $jadwalMataPelajaran,
            'siswa'                 => $siswa
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'tanggal'             => 'required|date',
            'jam_mulai'           => 'required',
            'jam_selesai'         => 'required',
            'durasi'              => 'required'
        ]);

        if(!$validate->fails()){
            $user_id                                       = Auth::user()->id;
            $jadwalMataPelajaran                           = JadwalMataPelajaran::findOrFail($id);
            $jadwalMataPelajaran->kelas_id                 = $this->request->kelas_id;
            $jadwalMataPelajaran->mata_pelajaran_id        = $this->request->mata_pelajaran_id;
            $jadwalMataPelajaran->siswa_id                 = $this->request->siswa_id;
            $jadwalMataPelajaran->tanggal                  = $this->request->tanggal;
            $jadwalMataPelajaran->jam_mulai                = $this->request->jam_mulai;
            $jadwalMataPelajaran->jam_selesai              = $this->request->jam_selesai;
            $jadwalMataPelajaran->durasi                   = $this->request->durasi;
            $jadwalMataPelajaran->data_guru_id             = $this->request->data_guru_id;
            $jadwalMataPelajaran->user_id                  = $user_id;
            $jadwalMataPelajaran->save();
            return redirect()->route('jadwal-mata-pelajaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('jadwal-mata-pelajaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jadwalMataPelajaran        = JadwalMataPelajaran::findOrFail($id);
        $jadwalMataPelajaran->delete();
        return redirect()->back();
    }
}
