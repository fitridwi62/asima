<?php

namespace App\Http\Controllers\Akademik;

use Auth;
use App\User;
use Redirect;
use App\EkstraSiswaItem;
use App\JadwalMataPelajaran;
use App\Pembayaran;
use App\NilaiPermapel;
use App\RaportMapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(){
        return view('akademik.profile.index');
    }
    //jadwal mapel
    public function getJadwalMapel(){
        $user_id = Auth::user()->id;
        $jadwalPelajaran                = JadwalMataPelajaran::whereHas('siswa' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswa')
        ->with('mata_pelajaran')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.jadwal-mapel',[
            'jadwalPelajaran'       => $jadwalPelajaran
        ]);
    }
    //ekstra
    public function getEkstra(){
        $user_id                       = Auth::user()->id;
        $ekstraPersiswas                = EkstraSiswaItem::whereHas('siswa' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswa')
        ->with('ekstra')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.ekstra',[
            'ekstraPersiswas'    => $ekstraPersiswas
        ]);
    }
    
    //pembayaran
    public function getPembayaran(){
        $user_id                    = Auth::user()->id;
        $pembayarans                = Pembayaran::whereHas('siswa' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswa')
        ->with('pembayaranSPP')
        ->with('pembayaranSemester')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.pembayaran',[
            'pembayarans'    => $pembayarans
        ]);
    }
    //nilai
    public function getNilai(){
        $user_id               = Auth::user()->id;
        $nilais                = NilaiPermapel::whereHas('siswa' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswa')
        ->with('mata_pelajaran')
        ->with('gridNilai')
        ->with('guru')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.nilai',[
            'nilais'    => $nilais
        ]);
    }
    //raport
    public function getRaport(){
        $user_id               = Auth::user()->id;
        $nilaiRaport           = RaportMapel::whereHas('siswaItem' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswaItem')
        ->with('mapel')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.report-kelas',[
            'nilaiRaport'    => $nilaiRaport
        ]);
    }
    //get kelas
    public function getRaportNilai(){
        $user_id               = Auth::user()->id;
        $nilaiRaport           = RaportMapel::whereHas('siswaItem' ,function($q) use($user_id){
            $q->where('user_id', $user_id);
        })
        ->with('siswaItem')
        ->with('mapel')
        ->orderBy('created_at', 'DESC')
        ->get();
        return view('akademik.profile.detail-raport',[
            'nilaiRaport'    => $nilaiRaport
        ]);
    }
    //pengumuman
    public function getPengumuman(){

    }
    //dashbord
    public function getSiswaDashbord(){

    }
}
