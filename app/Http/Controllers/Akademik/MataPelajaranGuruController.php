<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\Kelas;
use App\MataPelajaran;
use App\DataGuru;
use App\MataPelajaranGuru;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class MataPelajaranGuruController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $mataPelajaranGurus = MataPelajaranGuru::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        $mataPelajarans     = MataPelajaran::all();
        return view('akademik.mata_pelajaran_guru.index', [
            'mataPelajaranGurus' => $mataPelajaranGurus,
            'mataPelajarans'     => $mataPelajarans
        ]);
    }

    public function create()
    {
        $kelas                  = Kelas::all();
        $mataPelajarans         = MataPelajaran::all();
        $dataGurus              = DataGuru::all();
        return view('akademik.mata_pelajaran_guru.create', [
            'kelas'             => $kelas,
            'mataPelajarans'    => $mataPelajarans,
            'dataGurus'         => $dataGurus
        ]);
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(), [
            'status'              => 'required|min:3',
        ]);

        if(!$validate->fails()){
            $mataPelajaranGuru                    = new MataPelajaranGuru;
            $mataPelajaranGuru->kelas_id          = $this->request->kelas_id;
            $mataPelajaranGuru->mata_pelajar_id   = $this->request->mata_pelajar_id;
            $mataPelajaranGuru->data_guru_id      = $this->request->data_guru_id;
            $mataPelajaranGuru->status            = $this->request->status;
            $mataPelajaranGuru->save();
            return redirect()->route('mata-pelajaran-guru.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata-pelajaran-guru.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelas                  = Kelas::all();
        $mataPelajarans         = MataPelajaran::all();
        $dataGurus              = DataGuru::all();
        $mataPelajaranGuru      = MataPelajaranGuru::findOrFail($id);
        return view('akademik.mata_pelajaran_guru.edit', [
            'kelas'             => $kelas,
            'mataPelajarans'    => $mataPelajarans,
            'dataGurus'         => $dataGurus,
            'mataPelajaranGuru' => $mataPelajaranGuru
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'status'              => 'required|min:3'
        ]);

        if(!$validate->fails()){
            $mataPelajaranGuru                    = MataPelajaranGuru::findOrFail($id);
            $mataPelajaranGuru->kelas_id          = $this->request->kelas_id;
            $mataPelajaranGuru->mata_pelajar_id   = $this->request->mata_pelajar_id;
            $mataPelajaranGuru->data_guru_id      = $this->request->data_guru_id;
            $mataPelajaranGuru->status            = $this->request->status;
            $mataPelajaranGuru->save();
            return redirect()->route('mata-pelajaran-guru.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata-pelajaran-guru.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $mataPelajaranGuru = MataPelajaranGuru::findOrFail($id);
        $mataPelajaranGuru->delete();
        return redirect()->back();
    }
}
