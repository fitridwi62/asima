<?php

namespace App\Http\Controllers\Akademik;

use Auth;
use DB;
use Session;
use Redirect;
use App\EkstraSiswaItem;
use App\EkstraSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EkstraSiswaItemController extends Controller
{
    public function getEkstraList(){
        $ekstraListItems                = EkstraSiswaItem::with('siswa')
                                                        ->with('ekstra')
                                                        ->groupBy('siswa_id')
                                                        ->get();
        return view('akademik.ekstra-siswa.index', [
            'ekstraListItems'       => $ekstraListItems
        ]) ;
    }

    public function getEkstraPersiswa($id){
        $ekstraPersiswas            = EkstraSiswaItem::whereHas('siswa', function($query) use ($id){
            $query->where('siswa_id', $id);  
          })
          ->with('siswa')
          ->get();
          return view('akademik.ekstra-siswa.detail', [
              'ekstraPersiswas'     => $ekstraPersiswas
          ]);
        
    }

    public function getEkstraSiswa(Request $request){
        $ekstra_siswa_session_id = Session::get('ekstra_siswa_session_id');
        $ekstraSiswa 		     = EkstraSiswa::where(['ekstra_siswa_session_id' => $ekstra_siswa_session_id])
                                                        ->get();
        foreach($ekstraSiswa as $ekstraSiswax){
            $user_id                       = Auth::user()->id;
            $ekstraSiswaItem                = new EkstraSiswaItem;
            $ekstraSiswaItem->siswa_id  = $ekstraSiswax->siswa_id;
            $ekstraSiswaItem->ekstra_id  = $ekstraSiswax->ekstra_id;
            $ekstraSiswaItem->ekstra_id  = $ekstraSiswax->ekstra_id;
            $ekstraSiswaItem->user_id  = $user_id;
            $ekstraSiswaItem->save();
        }
        return redirect()->route('ekstra-item.selesai');
    }

    public function selesai(Request $request){
        $ekstra_siswa_session_id   = Session::get('ekstra_siswa_session_id');
        DB::table('ekstra_siswas')->where('ekstra_siswa_session_id',$ekstra_siswa_session_id )->delete();
        return view('akademik.Raport.thanks');
    }
}
