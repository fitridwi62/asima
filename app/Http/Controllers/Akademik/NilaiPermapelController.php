<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use Auth;
use App\MataPelajaran;
use App\GridNilai;
use App\DataGuru;
use App\Siswa;
use App\NilaiPermapel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class NilaiPermapelController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $nilaiPermapels         = NilaiPermapel::orderBy('created_at')
                                            ->paginate($this->limit);
        $mataPelajarans        = MataPelajaran::all();
        return view('akademik.nilai-permapel.index', [
            'nilaiPermapels'    => $nilaiPermapels,
            'mataPelajarans'    => $mataPelajarans
        ]);
    }

    public function create()
    {
        $mataPelajarans        = MataPelajaran::all();
        $gridNilais            = GridNilai::all();
        $gurus                 = DataGuru::all();
        $siswas                = Siswa::all();
        return view('akademik.nilai-permapel.create', [
            'mataPelajarans'   => $mataPelajarans,
            'gridNilais'       => $gridNilais,
            'gurus'            => $gurus,
            'siswas'           => $siswas 
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nilai'         => 'required|numeric',
            'predikat'      => 'required',
            'description'   => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $user_id                            = Auth::user()->id;
            $nilaiPermapel                      = new NilaiPermapel;
            $nilaiPermapel->mata_pelajaran_id   = $this->request->mata_pelajaran_id; 
            $nilaiPermapel->nilai_id            = $this->request->nilai_id;
            $nilaiPermapel->data_guru_id        = $this->request->data_guru_id;
            $nilaiPermapel->siswa_id            = $this->request->siswa_id;
            $nilaiPermapel->nilai               = $this->request->nilai;
            $nilaiPermapel->predikat            = $this->request->predikat;
            $nilaiPermapel->description         = $this->request->description;
            $nilaiPermapel->user_id             = $user_id;
            $nilaiPermapel->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Nilai Per Mata Pelajaran berhasil disimpan"
            ]);
            return redirect()->route('nilai-per-mata-pelajaran.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('nilai-per-mata-pelajaran.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $mataPelajarans        = MataPelajaran::all();
        $gridNilais            = GridNilai::all();
        $gurus                 = DataGuru::all();
        $siswas                = Siswa::all();
        $nilaiPermapel         = NilaiPermapel::findOrFail($id);
        return view('akademik.nilai-permapel.edit', [
            'mataPelajarans'   => $mataPelajarans,
            'gridNilais'       => $gridNilais,
            'gurus'            => $gurus,
            'siswas'           => $siswas,
            'nilaiPermapel'    => $nilaiPermapel
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nilai'         => 'required|numeric',
            'predikat'      => 'required',
            'description'   => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $user_id                            = Auth::user()->id;
            $nilaiPermapel                      = NilaiPermapel::findOrFail($id);
            $nilaiPermapel->mata_pelajaran_id   = $this->request->mata_pelajaran_id; 
            $nilaiPermapel->nilai_id            = $this->request->nilai_id;
            $nilaiPermapel->data_guru_id        = $this->request->data_guru_id;
            $nilaiPermapel->siswa_id            = $this->request->siswa_id;
            $nilaiPermapel->nilai               = $this->request->nilai;
            $nilaiPermapel->predikat            = $this->request->predikat;
            $nilaiPermapel->description         = $this->request->description;
            $nilaiPermapel->user_id             = $user_id;
            $nilaiPermapel->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Nilai Per Mata Pelajaran berhasil disimpan"
            ]);
            return redirect()->route('nilai-per-mata-pelajaran.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('nilai-per-mata-pelajaran.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $nilaiPermapel         = NilaiPermapel::findOrFail($id);
        $nilaiPermapel->delete();
        return redirect()->back();
    }
}
