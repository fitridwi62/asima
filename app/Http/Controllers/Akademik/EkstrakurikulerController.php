<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\Ekstrakurikuler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class EkstrakurikulerController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $extraKurikulers        = Ekstrakurikuler::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('akademik.ekstrakurikuler.index', [
            'extraKurikulers' => $extraKurikulers
        ]);
    }

    public function create()
    {
        return view('akademik.ekstrakurikuler.create');
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(), [
            'nama_ekstra'       => 'required|min:4',
            'penangung_jawab'   => 'required|min:4',
            'tanggal'           => 'required|date',
            'jam_mulai'         => 'required',
            'jam_selesai'       => 'required',
            'pertemuan'         => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $ekstra                     = new Ekstrakurikuler;
            $ekstra->nama_ekstra        = $this->request->nama_ekstra;
            $ekstra->penangung_jawab    = $this->request->penangung_jawab;
            $ekstra->tanggal            = $this->request->tanggal;
            $ekstra->jam_mulai          = $this->request->jam_mulai; 
            $ekstra->jam_selesai        = $this->request->jam_selesai; 
            $ekstra->pertemuan          = $this->request->pertemuan;
            $ekstra->save();
            return redirect()->route('ekstrakurikuler.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('ekstrakurikuler.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ekstra         = Ekstrakurikuler::findOrFail($id);
        return view('akademik.ekstrakurikuler.edit', [
            'ekstra'    => $ekstra
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'nama_ekstra'       => 'required|min:4',
            'penangung_jawab'   => 'required|min:4',
            'tanggal'           => 'required|date',
            'jam_mulai'         => 'required',
            'jam_selesai'       => 'required',
            'pertemuan'         => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $ekstra                     = Ekstrakurikuler::findOrFail($id);
            $ekstra->nama_ekstra        = $this->request->nama_ekstra;
            $ekstra->penangung_jawab    = $this->request->penangung_jawab;
            $ekstra->tanggal            = $this->request->tanggal;
            $ekstra->jam_mulai          = $this->request->jam_mulai; 
            $ekstra->jam_selesai        = $this->request->jam_selesai; 
            $ekstra->pertemuan          = $this->request->pertemuan;
            $ekstra->save();
            return redirect()->route('ekstrakurikuler.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('ekstrakurikuler.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $ekstra                     = Ekstrakurikuler::findOrFail($id);
        $ekstra->delete();
        return redirect()->back();
    }
}
