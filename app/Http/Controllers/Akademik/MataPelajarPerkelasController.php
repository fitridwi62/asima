<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\Kelas;
use App\MataPelajaran;
use App\MataPelajaranPerkelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class MataPelajarPerkelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $mataPelajarPerkelass         = MataPelajaranPerkelas::orderBy('created_at')
                                                            ->with('kelas')
                                                            ->with('mata_pelajar')
                                                            ->paginate($this->limit);
        $mataPelajarans                 = MataPelajaran::all();
        return view('akademik.mata_pelajar_perkelas.index', [
            'mataPelajarPerkelass'    => $mataPelajarPerkelass,
            'mataPelajarans'          => $mataPelajarans
        ]);
        
    }

    public function create()
    {
        $kelass         = Kelas::all();
        $mataPelajars   = MataPelajaran::all();
        return view('akademik.mata_pelajar_perkelas.create', [
            'kelass'    => $kelass,
            'mataPelajars'  => $mataPelajars
        ]);

    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(), [
            'status'              => 'required|min:3',
        ]);

        if(!$validate->fails()){
            $mataPelajarPerkelas                    = new MataPelajaranPerkelas;
            $mataPelajarPerkelas->kelas_id          = $this->request->kelas_id;
            $mataPelajarPerkelas->mata_pelajar_id   = $this->request->mata_pelajar_id;
            $mataPelajarPerkelas->status            = $this->request->status;
            $mataPelajarPerkelas->save();
            return redirect()->route('mata-pelajar-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata-pelajar-perkelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $mataPelajarPerkelas = MataPelajaranPerkelas::findOrFail($id);
        $kelass         = Kelas::all();
        $mataPelajars   = MataPelajaran::all();
        return view('akademik.mata_pelajar_perkelas.edit', [
            'kelass'                => $kelass,
            'mataPelajars'          => $mataPelajars,
            'mataPelajarPerkelas'   => $mataPelajarPerkelas
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'status'              => 'required|min:3',
        ]);

        if(!$validate->fails()){
            $mataPelajarPerkelas                    = MataPelajaranPerkelas::findOrFail($id);
            $mataPelajarPerkelas->kelas_id          = $this->request->kelas_id;
            $mataPelajarPerkelas->mata_pelajar_id   = $this->request->mata_pelajar_id;
            $mataPelajarPerkelas->status            = $this->request->status;
            $mataPelajarPerkelas->save();
            return redirect()->route('mata-pelajar-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata-pelajar-perkelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $mataPelajarPerkelas = MataPelajaranPerkelas::findOrFail($id);
        $mataPelajarPerkelas->delete();
        return redirect()->back();
    }
}
