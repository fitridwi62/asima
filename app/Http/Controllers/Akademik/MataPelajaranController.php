<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\MataPelajaran;
use App\Kurikulum;
use App\DataGuru;
use App\Jenjang;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class MataPelajaranController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $mata_pelajarans        = MataPelajaran::with('guru')->orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('akademik.mata_pelajaran.index', [
            'mata_pelajarans'   => $mata_pelajarans
        ]);
    }

    public function create()
    {
        $kurikulums         = Kurikulum::all();
        $dataGurus              = DataGuru::all();
        $jenjangs            = Jenjang::all();
        return view('akademik.mata_pelajaran.create',[
            'kurikulums'    => $kurikulums,
            'dataGurus'          => $dataGurus,
            'jenjangs'       => $jenjangs
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'kode'           => 'required|min:4',
            'nama'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $mata_pelajaran                                = new MataPelajaran;
            $mata_pelajaran->kurikulum_id                  = $this->request->kurikulum_id;
            $mata_pelajaran->data_guru_id                  = $this->request->data_guru_id;
            $mata_pelajaran->kode                          = $this->request->kode;
            $mata_pelajaran->nama                          = $this->request->nama;
            $mata_pelajaran->jenjang_id                    = $this->request->jenjang_id;
            $mata_pelajaran->save();
            return redirect()->route('mata_pelajaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata_pelajaran.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $mata_pelajaran         = MataPelajaran::findOrFail($id);
        $kurikulums             = Kurikulum::all();
        $gurus                  = DataGuru::all();
        $jenjangs               = Jenjang::all();
        return view('akademik.mata_pelajaran.edit', [
            'mata_pelajaran'    => $mata_pelajaran,
            'kurikulums'        => $kurikulums,
            'gurus'             => $gurus,
            'jenjangs'          => $jenjangs
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'kode'           => 'required|min:4',
            'nama'           => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $mata_pelajaran                                = MataPelajaran::findOrFail($id);
            $mata_pelajaran->kurikulum_id                  = $this->request->kurikulum_id;
            $mata_pelajaran->data_guru_id                  = $this->request->data_guru_id;
            $mata_pelajaran->kode                          = $this->request->kode;
            $mata_pelajaran->nama                          = $this->request->nama;
            $mata_pelajaran->jenjang_id                    = $this->request->jenjang_id;
            $mata_pelajaran->save();
            return redirect()->route('mata_pelajaran.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('mata_pelajaran.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $mata_pelajaran                                = MataPelajaran::findOrFail($id);
        $mata_pelajaran->delete();
        return redirect()->back();
    }
}
