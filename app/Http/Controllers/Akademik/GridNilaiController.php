<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\GridNilai;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class GridNilaiController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $gridNilais         = GridNilai::orderBy('created_at', 'DESC')->paginate($this->limit);
        return view('akademik.grid_nilai.index', [
            'gridNilais'    => $gridNilais
        ]);
    }

    public function create()
    {
        return view('akademik.grid_nilai.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nilai'           => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $gridNilai                  = new GridNilai;
            $gridNilai->nilai           = $this->request->nilai;
            $gridNilai->categori_nilai  = $this->request->categori_nilai;
            $gridNilai->save();
            return redirect()->route('grid-nilai.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('grid-nilai.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $gridNilai          = GridNilai::findOrFail($id);
        return view('akademik.grid_nilai.edit', [
            'gridNilai'     => $gridNilai
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nilai'           => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $gridNilai                  = GridNilai::findOrFail($id);
            $gridNilai->nilai           = $this->request->nilai;
            $gridNilai->categori_nilai  = $this->request->categori_nilai;
            $gridNilai->save();
            return redirect()->route('grid-nilai.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('grid-nilai.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $gridNilai          = GridNilai::findOrFail($id);
        $gridNilai->delete();
        return redirect()->back();
    }
}
