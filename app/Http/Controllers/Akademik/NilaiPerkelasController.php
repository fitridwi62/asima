<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\MataPelajaran;
use App\Kelas;
use App\Siswa;
use App\Nilaiperkelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class NilaiPerkelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $nilaiPerkelass      = Nilaiperkelas::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        $siswas                = Siswa::all();
        return view('akademik.nilai-perkelas.index', [
            'nilaiPerkelass'    => $nilaiPerkelass,
            'siswas'            => $siswas
        ]);
    }

    public function create()
    {
        $mataPelajarans        = MataPelajaran::all();
        $kelas                 = Kelas::all();
        $siswas                = Siswa::all();
        return view('akademik.nilai-perkelas.create', [
            'mataPelajarans'   => $mataPelajarans,
            'kelas'            => $kelas,
            'siswas'           => $siswas 
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nilai'         => 'required|numeric',
            'predikat'      => 'required',
            'description'   => 'required|min:4'
        ]);
        if(!$validate->fails()){
            $nilaiPerkelas                      = new Nilaiperkelas;
            $nilaiPerkelas->mata_pelajaran_id   = $this->request->mata_pelajaran_id;
            $nilaiPerkelas->siswa_id            = $this->request->siswa_id;
            $nilaiPerkelas->kelas_id            = $this->request->kelas_id;
            $nilaiPerkelas->nilai               = $this->request->nilai;
            $nilaiPerkelas->predikat            = $this->request->predikat;
            $nilaiPerkelas->description         = $this->request->description;
            $nilaiPerkelas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Nilai Perkelas berhasil disimpan"
            ]);
            return redirect()->route('nilai-per-kelas.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('nilai-per-kelas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $mataPelajarans        = MataPelajaran::all();
        $kelas                 = Kelas::all();
        $siswas                = Siswa::all();
        $nilaiPerkelas         = Nilaiperkelas::findOrFail($id); 
        return view('akademik.nilai-perkelas.edit', [
            'mataPelajarans'   => $mataPelajarans,
            'kelas'            => $kelas,
            'siswas'           => $siswas,
            'nilaiPerkelas'    => $nilaiPerkelas 
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nilai'         => 'required|numeric',
            'predikat'      => 'required',
            'description'   => 'required|min:4'
        ]);
        if(!$validate->fails()){
            $nilaiPerkelas                      = Nilaiperkelas::findOrFail($id); 
            $nilaiPerkelas->mata_pelajaran_id   = $this->request->mata_pelajaran_id;
            $nilaiPerkelas->siswa_id            = $this->request->siswa_id;
            $nilaiPerkelas->kelas_id            = $this->request->kelas_id;
            $nilaiPerkelas->nilai               = $this->request->nilai;
            $nilaiPerkelas->predikat            = $this->request->predikat;
            $nilaiPerkelas->description         = $this->request->description;
            $nilaiPerkelas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Nilai Perkelas berhasil disimpan"
            ]);
            return redirect()->route('nilai-per-kelas.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('nilai-per-kelas.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $nilaiPerkelas         = Nilaiperkelas::findOrFail($id); 
        $nilaiPerkelas->delete();
        return redirect()->back();
    }
}
