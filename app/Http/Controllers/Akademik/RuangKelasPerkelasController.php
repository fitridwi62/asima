<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\Kelas;
use App\RuangKelasPerkelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RuangKelasPerkelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $ruangKelass        = RuangKelasPerkelas::orderBy('created_at', 'DESC')
                                                ->with('kelas')
                                                ->paginate($this->limit);
        $kelass             = Kelas::all();
        return view('akademik.ruang_kelas_perkelas.index', [
            'ruangKelass'   => $ruangKelass,
            'kelass'        => $kelass
        ]);
    }

    public function create()
    {
        $kelass      = Kelas::all();
        return view('akademik.ruang_kelas_perkelas.create', [
            'kelass' => $kelass
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:1'
        ]);

        if(!$validate->fails()){
            $ruang_kelas                     = new RuangKelasPerkelas;
            $ruang_kelas->kelas_id           = $this->request->kelas_id;
            $ruang_kelas->nama               = $this->request->nama;
            $ruang_kelas->save();
            return redirect()->route('ruang-kelas-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('ruang-kelas-perkelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ruang_kelas    = RuangKelasPerkelas::findOrFail($id);
        $kelass         = Kelas::all();
        return view('akademik.ruang_kelas_perkelas.edit', [
            'kelass' => $kelass,
            'ruang_kelas' => $ruang_kelas
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'           => 'required|min:1'
        ]);

        if(!$validate->fails()){
            $ruang_kelas     = RuangKelasPerkelas::findOrFail($id);
            $ruang_kelas->kelas_id           = $this->request->kelas_id;
            $ruang_kelas->nama               = $this->request->nama;
            $ruang_kelas->save();
            return redirect()->route('ruang-kelas-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('ruang-kelas-perkelas.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $ruang_kelas     = RuangKelasPerkelas::findOrFail($id);
        $ruang_kelas->delete();
        return redirect()->back();
    }
}
