<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\Siswa;
use App\Kelas;
use App\SiswaPerkelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class SiswaPerkelasController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $siswaPerkelasLists         = SiswaPerkelas::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        $siswas                     = Siswa::all();
        return view('akademik.siswa_perkelas.index', [
            'siswaPerkelasLists'    => $siswaPerkelasLists,
            'siswas'                => $siswas
        ]);
    }

    public function create()
    {
        $kelass                 = Kelas::all();
        $siswas                 = Siswa::all();
        return view('akademik.siswa_perkelas.create', [
            'kelass'    => $kelass,
            'siswas'    => $siswas
        ]);         
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'kelas_id'      => 'required'
        ]);

        if(!$validate->fails()){
            $siswaPerkelas              = new SiswaPerkelas;
            $siswaPerkelas->kelas_id    = $this->request->kelas_id;
            $siswaPerkelas->siswa_id    = $this->request->siswa_id;
            $siswaPerkelas->save();
            return redirect()->route('siswa-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('siswa-perkelas.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelass                 = Kelas::all();
        $siswas                 = Siswa::all();
        $siswaPerkelas          = SiswaPerkelas::findOrFail($id);
        return view('akademik.siswa_perkelas.edit', [
            'kelass'            => $kelass,
            'siswas'            => $siswas,
            'siswaPerkelas'     => $siswaPerkelas
        ]);   
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'kelas_id'      => 'required'
        ]);

        if(!$validate->fails()){
            $siswaPerkelas              = SiswaPerkelas::findOrFail($id);
            //dd($siswaPerkelas);
            $siswaPerkelas->kelas_id    = $this->request->kelas_id;
            $siswaPerkelas->siswa_id    = $this->request->siswa_id;
            $siswaPerkelas->save();
            return redirect()->route('siswa-perkelas.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('siswa-perkelas.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $siswaPerkelas          = SiswaPerkelas::findOrFail($id);
        $siswaPerkelas->delete();
        return redirect()->back();
    }
}
