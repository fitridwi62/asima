<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\DataGuru;
use App\BimbinganBelajar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class BimbinganBelajarController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $bimbinganBelajars      = BimbinganBelajar::orderBy('created_at')
                                                ->with('guru')
                                                ->paginate($this->limit);
        return view('akademik.bimbingan_belajar.index', [
            'bimbinganBelajars' => $bimbinganBelajars
        ]);
    }

    public function create()
    {
        $gurus      = DataGuru::all();
        return view('akademik.bimbingan_belajar.create', [
            'gurus' => $gurus
        ]);
    }


    public function store()
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4',
            'tanggal'           => 'required|date',
            'jam_mulai'         => 'required',
            'jam_selesai'       => 'required',
            'pertemuan'         => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $bimbinganBelajar                   = new BimbinganBelajar;
            $bimbinganBelajar->nama             = $this->request->nama;
            $bimbinganBelajar->data_guru_id     = $this->request->data_guru_id;
            $bimbinganBelajar->tanggal          = $this->request->tanggal;
            $bimbinganBelajar->jam_mulai        = $this->request->jam_mulai; 
            $bimbinganBelajar->jam_selesai      = $this->request->jam_selesai; 
            $bimbinganBelajar->pertemuan        = $this->request->pertemuan;
            $bimbinganBelajar->save();
            return redirect()->route('bimbingan-belajar.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('bimbingan-belajar.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

 
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bimbinganBelajar       = BimbinganBelajar::findOrFail($id);
        $gurus                  = DataGuru::all();
        return view('akademik.bimbingan_belajar.edit', [
            'bimbinganBelajar' => $bimbinganBelajar,
            'gurus'            => $gurus
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4',
            'tanggal'           => 'required|date',
            'jam_mulai'         => 'required',
            'jam_selesai'       => 'required',
            'pertemuan'         => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $bimbinganBelajar                   = BimbinganBelajar::findOrFail($id);
            $bimbinganBelajar->nama             = $this->request->nama;
            $bimbinganBelajar->data_guru_id     = $this->request->data_guru_id;
            $bimbinganBelajar->tanggal          = $this->request->tanggal;
            $bimbinganBelajar->jam_mulai        = $this->request->jam_mulai; 
            $bimbinganBelajar->jam_selesai      = $this->request->jam_selesai; 
            $bimbinganBelajar->pertemuan        = $this->request->pertemuan;
            $bimbinganBelajar->save();
            return redirect()->route('bimbingan-belajar.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('bimbingan-belajar.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $bimbinganBelajar       = BimbinganBelajar::findOrFail($id);
        $bimbinganBelajar->delete();
        return redirect()->back();
    }
}
