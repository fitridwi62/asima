<?php

namespace App\Http\Controllers\Akademik;

use Auth;
use Session;
use Redirect;
use App\Siswa;
use App\Ekstrakurikuler;
use App\EkstraSiswa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class EktraSiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $ekstras        = EkstraSiswa::with('siswaItem')
                                     ->with('ekstra')
                                     ->paginate($this->limit);
        return view('akademik.ekstra-siswa.index',[
            'ekstras'   => $ekstras
        ]);
    }

    public function create()
    {
        $ekstraList         = Ekstrakurikuler::all();
        $siswas             = Siswa::all();
        $session_id 	         = Session::get('ekstra_siswa_session_id');
        $ekstras 		         = EkstraSiswa::where(['ekstra_siswa_session_id' => $session_id])
                                                        ->with('siswaItem')
                                                        ->get();
        return view('akademik.ekstra-siswa.create',[
            'ekstraList'    => $ekstraList,
            'siswas'        => $siswas,
            'ekstras'       => $ekstras
        ]);
    }

    public function store()
    {
        $ekstra_siswa_session_id 	= Session::get('ekstra_siswa_session_id');
        if(empty($ekstra_siswa_session_id )){
            $ekstra_siswa_session_id 		= str_random(40);
            Session::put('ekstra_siswa_session_id', $ekstra_siswa_session_id);
        }
        $user_id                       = Auth::user()->id;
        $ekstraSiswa                   = new EkstraSiswa;
        $ekstraSiswa->siswa_id         = $this->request->siswa_id;
        $ekstraSiswa->ekstra_id        = $this->request->ekstra_id;
        $ekstraSiswa->ekstra_siswa_session_id    = $ekstra_siswa_session_id;
        $ekstraSiswa->user_id           = $user_id;
        $ekstraSiswa->save();
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
