<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Auth;
use Validator;
use App\User;
use App\Siswa;
use App\PengumumanKelas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PengumumanSiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pengumumanKelas        = PengumumanKelas::paginate($this->limit);
        return view('akademik.pengumuman-kelas.index', [
            'pengumumanKelas'   => $pengumumanKelas
        ]);
    }

    public function create()
    {
        $siswas                = Siswa::all();
        return view('akademik.pengumuman-kelas.create',[
            'siswas'           => $siswas
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'judul'             => 'required',
            'keterangan'        => 'required'
        ]);

        if(!$validate->fails()){
            $user_id                        = Auth::user()->id;
            $pengumumanKelas                = new  PengumumanKelas;
            $pengumumanKelas->judul         = $this->request->judul;
            $pengumumanKelas->keterangan    = $this->request->keterangan;
            $pengumumanKelas->siswa_id      = $this->request->siswa_id;
            $pengumumanKelas->user_id       = $user_id;
            $pengumumanKelas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Pengumuman siswa di kelas"
            ]);
            return redirect()->route('pengumuman-kelas-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('pengumuman-kelas-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pengumumanKelas            = PengumumanKelas::findOrFail($id);
        $siswas                     = Siswa::all();
        return view('akademik.pengumuman-kelas.edit',[
            'pengumumanKelas'       => $pengumumanKelas,
            'siswas'                => $siswas
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'judul'             => 'required',
            'keterangan'        => 'required'
        ]);

        if(!$validate->fails()){
            $user_id                        = Auth::user()->id;
            $pengumumanKelas                = PengumumanKelas::findOrFail($id);
            $pengumumanKelas->judul         = $this->request->judul;
            $pengumumanKelas->keterangan    = $this->request->keterangan;
            $pengumumanKelas->siswa_id      = $this->request->siswa_id;
            $pengumumanKelas->user_id       = $user_id;
            $pengumumanKelas->save();
            Session::flash("flash_notification", [
                "success"       => "success",
                "messages"      => "Pengumuman siswa di kelas"
            ]);
            return redirect()->route('pengumuman-kelas-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('pengumuman-kelas-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pengumumanKelas            = PengumumanKelas::findOrFail($id);
        $pengumumanKelas->delete();
        return redirect()->back();
    }
}
