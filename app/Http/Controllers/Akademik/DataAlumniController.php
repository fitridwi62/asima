<?php

namespace App\Http\Controllers\Akademik;

use Redirect;
use Validator;
use App\DataAlumni;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class DataAlumniController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $dataAlumnis            = DataAlumni::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        return view('akademik.data_alumni.index', [
            'dataAlumnis'       => $dataAlumnis
        ]);
    }

    public function create()
    {
        return view('akademik.data_alumni.create');
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4',
            'tahun_angkatan'    => 'required',
            'email'             => 'required',
            'phone'             => 'required|numeric',
            'alamat'            => 'required|min:4',
            'sekolah_ke'        => 'required|min:3'
        ]);

        if(!$validate->fails()){
            $dataAlumni                 = new DataAlumni;
            $dataAlumni->nama           = $this->request->nama;
            $dataAlumni->tahun_angkatan = $this->request->tahun_angkatan;
            $dataAlumni->email          = $this->request->email;
            $dataAlumni->phone          = $this->request->phone;
            $dataAlumni->alamat         = $this->request->alamat;
            $dataAlumni->sekolah_ke     = $this->request->sekolah_ke;
            $dataAlumni->save();
            return redirect()->route('data-alumni.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('data-alumni.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataAlumni             = DataAlumni::findOrFail($id);
        return view('akademik.data_alumni.edit', [
            'dataAlumni'        => $dataAlumni
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(), [
            'nama'              => 'required|min:4',
            'tahun_angkatan'    => 'required',
            'email'             => 'required',
            'phone'             => 'required|numeric',
            'alamat'            => 'required|min:4',
            'sekolah_ke'        => 'required|min:3'
        ]);

        if(!$validate->fails()){
            $dataAlumni                 = DataAlumni::findOrFail($id);
            $dataAlumni->nama           = $this->request->nama;
            $dataAlumni->tahun_angkatan = $this->request->tahun_angkatan;
            $dataAlumni->email          = $this->request->email;
            $dataAlumni->phone          = $this->request->phone;
            $dataAlumni->alamat         = $this->request->alamat;
            $dataAlumni->sekolah_ke     = $this->request->sekolah_ke;
            $dataAlumni->save();
            return redirect()->route('data-alumni.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('data-alumni.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $dataAlumni             = DataAlumni::findOrFail($id);
        $dataAlumni->delete();
        return redirect()->back();
    }
}
