<?php

namespace App\Http\Controllers\Accounting;

use Redirect;
use Validator;
use App\Coa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class CoaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $coas           = Coa::orderBy('created_at', 'DESC')
                                ->paginate($this->limit);
        return view('accounting.coa.index', [
            'coas'      => $coas
        ]);
    }

    public function create()
    {
        return view('accounting.coa.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'nama_coa'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $code_coa               = str_random(6);
            $coa                    = new Coa;
            $coa->no_akun_coa       = $code_coa;
            $coa->nama_coa          = $this->request->nama_coa;
            $coa->save();
            Session::flash("flash_notification", [
                "level"     => 'success',
                "messages"  => "COA <strong>$coa->no_akun_coa</strong> Berhasil"
            ]);
            return redirect()->route('coa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('coa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $coa        = Coa::findOrFail($id);
        return view('accounting.coa.edit',[
            'coa'   => $coa
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'nama_coa'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $code_coa               = str_random(6);
            $coa                    = Coa::findOrFail($id);
            $coa->no_akun_coa       = $code_coa;
            $coa->nama_coa          = $this->request->nama_coa;
            $coa->save();
            Session::flash("flash_notification", [
                "level"     => 'success',
                "messages"  => "COA <strong>$coa->no_akun_coa</strong> Berhasil"
            ]);
            return redirect()->route('coa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('coa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $coa        = Coa::findOrFail($id);
        $coa->delete();
        return redirect()->back();
    }
}
