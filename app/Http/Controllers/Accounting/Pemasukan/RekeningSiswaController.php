<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\RekeningSiswa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class RekeningSiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }


    public function index()
    {
        $rekeningSiswas         = RekeningSiswa::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('accounting.pemasukan.rekening-siswa.index', [
            'rekeningSiswas'    => $rekeningSiswas
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.rekening-siswa.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nomor_keuangan'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2'
        ]);
        if(!$validate->fails()){
            $rekening_siswa                 = new RekeningSiswa;
            $rekening_siswa->nomor_keuangan  = $this->request->nomor_keuangan;
            $rekening_siswa->nama_siswa     = $this->request->nama_siswa;
            $rekening_siswa->pbn_tahunan    = $this->request->pbn_tahunan;
            $rekening_siswa->spp            = $this->request->spp;
            $rekening_siswa->biaya_program  = $this->request->biaya_program;
            $rekening_siswa->status         = $this->request->status;
            $rekening_siswa->save();
            Session::flash("flash_notification", [
                "level"     => 'success',
                "messages"  => "Rekening Siswa <strong>$rekening_siswa->nama_siswa</strong> Berhasil"
            ]);
            return redirect()->route('rekening-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('rekening-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $rekening_siswa         = RekeningSiswa::findOrFail($id);
        return view('accounting.pemasukan.rekening-siswa.edit', [
            'rekening_siswa'    => $rekening_siswa
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nomor_keuangan'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2'
        ]);
        if(!$validate->fails()){
            $rekening_siswa                 = RekeningSiswa::findOrFail($id);
            $rekening_siswa->nomor_keuangan  = $this->request->nomor_keuangan;
            $rekening_siswa->nama_siswa     = $this->request->nama_siswa;
            $rekening_siswa->pbn_tahunan    = $this->request->pbn_tahunan;
            $rekening_siswa->spp            = $this->request->spp;
            $rekening_siswa->biaya_program  = $this->request->biaya_program;
            $rekening_siswa->status         = $this->request->status;
            $rekening_siswa->save();
            Session::flash("flash_notification", [
                "level"     => 'success',
                "messages"  => "Rekening Siswa <strong>$rekening_siswa->nama_siswa</strong> Berhasil"
            ]);
            return redirect()->route('rekening-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('rekening-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $rekening_siswa         = RekeningSiswa::findOrFail($id);
        $rekening_siswa->delete();
        return redirect()->back();
    }
}
