<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\JurnalPBN;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalPbnController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalPbns         = JurnalPBN::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.pemasukan.jurnal-pbn.index', [
            'jurnalPbns'    => $jurnalPbns
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.jurnal-pbn.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalPbn                  = new JurnalPBN;
            $jurnalPbn->tanggal         = $this->request->tanggal;
            $jurnalPbn->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalPbn->nama_siswa      = $this->request->nama_siswa;
            $jurnalPbn->deskripsi       = $this->request->deskripsi;
            $jurnalPbn->kas_debit       = $this->request->kas_debit;
            $jurnalPbn->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Jurnal Program Bangunan Bulanan <strong>$jurnalPbn->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('jurnal-pbn.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-pbn.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurnalPbn          = JurnalPBN::findOrFail($id);
        return view('accounting.pemasukan.jurnal-pbn.edit', [
            'jurnalPbn'     => $jurnalPbn
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalPbn                  = JurnalPBN::findOrFail($id);
            $jurnalPbn->tanggal         = $this->request->tanggal;
            $jurnalPbn->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalPbn->nama_siswa      = $this->request->nama_siswa;
            $jurnalPbn->deskripsi       = $this->request->deskripsi;
            $jurnalPbn->kas_debit       = $this->request->kas_debit;
            $jurnalPbn->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Jurnal Program Bangunan Bulanan <strong>$jurnalPbn->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('jurnal-pbn.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-pbn.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $jurnalPbn          = JurnalPBN::findOrFail($id);
        $jurnalPbn->delete();
        return redirect()->back();
    }
}
