<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\Hutang;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class HutangController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $hutangs            = Hutang::orderBy('created_at', 'DESC')
                                    ->paginate($this->limit);
        return view('accounting.pemasukan.hutang.index', [
            'hutangs'       => $hutangs
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.hutang.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'nomor_rekening'        => 'required|min:2',
            'nama_siswa'            => 'required|min:2',
            'tanggal'               => 'required|date'
        ]);
        if(!$validate->fails()){
            $hutang                         = new Hutang;
            $hutang->nomor_rekening         = $this->request->nomor_rekening;
            $hutang->nama_siswa             = $this->request->nama_siswa;
            $hutang->tanggal                = $this->request->tanggal;
            $hutang->pbn                    = $this->request->pbn;
            $hutang->spp                    = $this->request->spp;
            $hutang->pro                    = $this->request->pro;
            $hutang->status                 = $this->request->status;
            $hutang->save();
            Session::flash("flash_notification", [
                "level" => "success",
                "messages"  => "Hutang <strong>$hutang->nomor_rekening</strong>"
            ]);
            return redirect()->route('hutang.index');

        }else{
            $errors         = $validate->messages();
            return redirect()->route('hutang.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $hutang         = Hutang::findOrFail($id);
        return view('accounting.pemasukan.hutang.edit', [
            'hutang'    => $hutang
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'nomor_rekening'        => 'required|min:4|numeric',
            'nama_siswa'            => 'required|min:4',
            'tanggal'               => 'required|date'
        ]);
        if(!$validate->fails()){
            $hutang                         = Hutang::findOrFail($id);
            $hutang->nomor_rekening         = $this->request->nomor_rekening;
            $hutang->nama_siswa             = $this->request->nama_siswa;
            $hutang->tanggal                = $this->request->tanggal;
            $hutang->pbn                    = $this->request->pbn;
            $hutang->spp                    = $this->request->spp;
            $hutang->pro                    = $this->request->pro;
            $hutang->status                 = $this->request->status;
            $hutang->save();
            Session::flash("flash_notification", [
                "level" => "success",
                "messages"  => "Hutang <strong>$hutang->nomor_rekening</strong>"
            ]);
            return redirect()->route('hutang.index');

        }else{
            $errors         = $validate->messages();
            return redirect()->route('hutang.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $hutang         = Hutang::findOrFail($id);
        $hutang->delete();
        return redirect()->back();
    }
}
