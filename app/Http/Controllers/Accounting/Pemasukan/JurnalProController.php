<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\Jurnalpro;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalProController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalPros         = Jurnalpro::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.pemasukan.pro.index', [
            'jurnalPros'    => $jurnalPros
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.pro.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalPro                  = new Jurnalpro;
            $jurnalPro->tanggal         = $this->request->tanggal;
            $jurnalPro->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalPro->nama_siswa      = $this->request->nama_siswa;
            $jurnalPro->deskripsi       = $this->request->deskripsi;
            $jurnalPro->kas_debit       = $this->request->kas_debit;
            $jurnalPro->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Jurnal Program Bulanan <strong>$jurnalPro->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('jurnal-program-bulanan.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-program-bulanan.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurnalPro          = Jurnalpro::findOrFail($id);
        return view('accounting.pemasukan.pro.edit', [
            'jurnalPro'     => $jurnalPro
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'nama_siswa'        => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalPro                  = Jurnalpro::findOrFail($id);
            $jurnalPro->tanggal         = $this->request->tanggal;
            $jurnalPro->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalPro->nama_siswa      = $this->request->nama_siswa;
            $jurnalPro->deskripsi       = $this->request->deskripsi;
            $jurnalPro->kas_debit       = $this->request->kas_debit;
            $jurnalPro->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Jurnal Program Bulanan <strong>$jurnalPro->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('jurnal-program-bulanan.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-program-bulanan.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $jurnalPro          = Jurnalpro::findOrFail($id);
        $jurnalPro->delete();
        return redirect()->back();
    }
}
