<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\JurnalSPP;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalSppController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalSpps         = JurnalSPP::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.pemasukan.spp.index', [
            'jurnalSpps'    => $jurnalSpps
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.spp.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalSpp                  = new JurnalSPP;
            $jurnalSpp->tanggal         = $this->request->tanggal;
            $jurnalSpp->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalSpp->nama_siswa      = $this->request->nama_siswa;
            $jurnalSpp->deskripsi       = $this->request->deskripsi;
            $jurnalSpp->kas_debit       = $this->request->kas_debit;
            $jurnalSpp->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Jurnal SPP Bulanan <strong>$jurnalSpp->nama_siswa</strong> Berhasil disimpan"
            ]);
            return redirect()->route('jurnal-spp.index'); 
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-spp.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurnalSpp          = JurnalSPP::findOrFail($id);
        return view('accounting.pemasukan.spp.edit', [
            'jurnalSpp'     => $jurnalSpp
        ]);
        
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|min:2',
            'kas_debit'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $jurnalSpp                  = JurnalSPP::findOrFail($id);
            $jurnalSpp->tanggal         = $this->request->tanggal;
            $jurnalSpp->nomor_rekening  = $this->request->nomor_rekening;
            $jurnalSpp->nama_siswa      = $this->request->nama_siswa;
            $jurnalSpp->deskripsi       = $this->request->deskripsi;
            $jurnalSpp->kas_debit       = $this->request->kas_debit;
            $jurnalSpp->save();
            Session::flash("flash_notification", [
                "level"         => "success",
                "messages"      => "Jurnal SPP Bulanan <strong>$jurnalSpp->nama_siswa</strong> Berhasil disimpan"
            ]);
            return redirect()->route('jurnal-spp.index'); 
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-spp.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $jurnalSpp          = JurnalSPP::findOrFail($id);
        $jurnalSpp->delete();
        return redirect()->back();
    }
}
