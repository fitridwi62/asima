<?php

namespace App\Http\Controllers\Accounting\Pemasukan;

use Redirect;
use Validator;
use App\CutOffSiswa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class CutOffSiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $cutOffSiswas       = CutOffSiswa::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.pemasukan.cut-off-siswa.index', [
            'cutOffSiswas'  => $cutOffSiswas
        ]);
    }

    public function create()
    {
        return view('accounting.pemasukan.cut-off-siswa.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required',
            'nama_siswa'        => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'kas_kredit'        => 'required|min:2',
            'dibuat'            => 'required|min:2',
            'diperiksa'         => 'required|min:2',
            'disetujui'         => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $cutOffSiswa                    = new CutOffSiswa;
            $cutOffSiswa->tanggal           = $this->request->tanggal;
            $cutOffSiswa->nomor_rekening    = $this->request->nomor_rekening;
            $cutOffSiswa->nama_siswa        = $this->request->nama_siswa;
            $cutOffSiswa->deskripsi         = $this->request->deskripsi;
            $cutOffSiswa->kas_kredit        = $this->request->kas_kredit;
            $cutOffSiswa->dibuat            = $this->request->dibuat;
            $cutOffSiswa->diperiksa         = $this->request->diperiksa;
            $cutOffSiswa->disetujui         = $this->request->disetujui;
            $cutOffSiswa->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Cut Off Siswa <strong>$cutOffSiswa->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('cut-off-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('cut-off-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
                 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $cutOffSiswa        = CutOffSiswa::findOrFail($id);
        return view('accounting.pemasukan.cut-off-siswa.edit', [
            'cutOffSiswa'   => $cutOffSiswa
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_rekening'    => 'required|numeric',
            'nama_siswa'        => 'required|min:4',
            'deskripsi'         => 'required|min:4',
            'kas_kredit'        => 'required|min:4',
            'dibuat'            => 'required|min:4',
            'diperiksa'         => 'required|min:4',
            'disetujui'         => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $cutOffSiswa                    = CutOffSiswa::findOrFail($id);
            $cutOffSiswa->tanggal           = $this->request->tanggal;
            $cutOffSiswa->nomor_rekening    = $this->request->nomor_rekening;
            $cutOffSiswa->nama_siswa        = $this->request->nama_siswa;
            $cutOffSiswa->deskripsi         = $this->request->deskripsi;
            $cutOffSiswa->kas_kredit        = $this->request->kas_kredit;
            $cutOffSiswa->dibuat            = $this->request->dibuat;
            $cutOffSiswa->diperiksa         = $this->request->diperiksa;
            $cutOffSiswa->disetujui         = $this->request->disetujui;
            $cutOffSiswa->save();
            Session::flash("flash_notification", [
                "level"     => "success",
                "messages"  => "Cut Off Siswa <strong>$cutOffSiswa->tanggal</strong> Berhasil."
            ]);
            return redirect()->route('cut-off-siswa.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('cut-off-siswa.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $cutOffSiswa        = CutOffSiswa::findOrFail($id);
        $cutOffSiswa->delete();
        return redirect()->back();
    }
}
