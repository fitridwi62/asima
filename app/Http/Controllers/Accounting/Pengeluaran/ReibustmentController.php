<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\PengeluaranPermintaanDana;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\PengelauaranJurnalKasKecilReimbursment;

class ReibustmentController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $reibustments           = PengelauaranJurnalKasKecilReimbursment::orderBy('created_at', 'DESC')
                                                                        ->paginate($this->limit);
        return view('accounting.pengeluaran.reibursment.index', [
            'reibustments'      => $reibustments
        ]);
    }

    public function create()
    {
        $permintaanDanas         = PengeluaranPermintaanDana::all();
        return view('accounting.pengeluaran.reibursment.create', [
            'permintaanDanas'    => $permintaanDanas
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'               => 'required|date',
            'kode_reibusment'       => 'required|min:2',
            'deskripsi'             => 'required|min:2',
            'nomor_akun_debit'      => 'required|min:2',
            'debit'                 => 'required'
        ]);
        if(!$validate->fails()){
            $reibursment                        = new PengelauaranJurnalKasKecilReimbursment;
            $reibursment->tanggal               = $this->request->tanggal;
            $reibursment->kode_reibusment       = $this->request->kode_reibusment;
            //$reibursment->permintaanDana_id     = $this->request->permintaanDana_id;
            $reibursment->deskripsi             = $this->request->deskripsi;
            $reibursment->nomor_akun_debit      = $this->request->nomor_akun_debit;
            $reibursment->nomor_akun_kredit     = $this->request->nomor_akun_kredit;
            $reibursment->debit                 = $this->request->debit;
            $reibursment->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Jurnal Kas Kecil Reibursment <strong> $reibursment->kode_reibusment  </strong> berhasil."
            ]);
            return redirect()->route('reibusment.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('reibusment.create')
                             ->withErrors($validate)
                             ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $permintaanDanas         = PengeluaranPermintaanDana::all();
        $reibursment            = PengelauaranJurnalKasKecilReimbursment::findOrFail($id);
        return view('accounting.pengeluaran.reibursment.edit', [
            'permintaanDanas'    => $permintaanDanas,
            'reibursment'       => $reibursment
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'               => 'required|date',
            'kode_reibusment'       => 'required|min:2',
            'deskripsi'             => 'required|min:2',
            'nomor_akun_debit'      => 'required|min:2',
            'debit'                 => 'required'
        ]);
        if(!$validate->fails()){
            $reibursment                        = PengelauaranJurnalKasKecilReimbursment::findOrFail($id);
            $reibursment->tanggal               = $this->request->tanggal;
            $reibursment->kode_reibusment       = $this->request->kode_reibusment;
            //$reibursment->permintaanDana_id     = $this->request->permintaanDana_id;
            $reibursment->deskripsi             = $this->request->deskripsi;
            $reibursment->nomor_akun_debit      = $this->request->nomor_akun_debit;
            $reibursment->nomor_akun_kredit     = $this->request->nomor_akun_kredit;
            $reibursment->debit                 = $this->request->debit;
            $reibursment->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Jurnal Kas Kecil Reibursment <strong> $reibursment->kode_reibusment </strong> berhasil."
            ]);
            return redirect()->route('reibusment.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('reibusment.create')
                             ->withErrors($validate)
                             ->withInput($this->request->input());
        }
    }


    public function destroy($id)
    {
        $reibursment                        = PengelauaranJurnalKasKecilReimbursment::findOrFail($id);
        $reibursment->delete();
        return redirect()->back();
    }
}
