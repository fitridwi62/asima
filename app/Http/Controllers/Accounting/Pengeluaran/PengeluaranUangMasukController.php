<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\Siswa;
use App\JenisPembayaran;
use App\PengeluaranUangMasuk;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PengeluaranUangMasukController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $pengeluaranUangMasuks      = PengeluaranUangMasuk::orderBy('created_at', 'DESC')
                                                        ->paginate($this->limit);
        $jenisPembayarans           = JenisPembayaran::all();
        return view('accounting.pengeluaran.uang-masuk.index', [
            'pengeluaranUangMasuks'     => $pengeluaranUangMasuks,
            'jenisPembayarans'          => $jenisPembayarans
        ]);
    }

    public function create()
    {
        $siswas                 = Siswa::all();
        $jenisPembayarans        = JenisPembayaran::all();
        return view('accounting.pengeluaran.uang-masuk.create', [
            'siswas'            => $siswas,
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nama_siswa'        => 'required|min:2',
            'harga'             => 'required'
        ]);

        if(!$validate->fails()){
            $pengeluaranUangMasuk                           = new PengeluaranUangMasuk;
            $pengeluaranUangMasuk->tanggal                  = $this->request->tanggal;
            $pengeluaranUangMasuk->siswa_id                 = $this->request->siswa_id;
            $pengeluaranUangMasuk->jenis_pembayaran_id      = $this->request->jenis_pembayaran_id;
            $pengeluaranUangMasuk->nama_siswa               = $this->request->nama_siswa;
            $pengeluaranUangMasuk->deskripsi                = $this->request->deskripsi;
            $pengeluaranUangMasuk->no_akun_debit            = $this->request->no_akun_debit;
            $pengeluaranUangMasuk->no_akun_kredit           = $this->request->no_akun_kredit;
            $pengeluaranUangMasuk->harga                    = $this->request->harga;
            $pengeluaranUangMasuk->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Pengeluaran Uang Masuk <strong> $pengeluaranUangMasuk->tanggal </strong> berhasil."
            ]);
            return redirect()->route('uang-masuk.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('uang-masuk.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $siswas                  = Siswa::all();
        $jenisPembayarans        = JenisPembayaran::all();
        $pengeluaranUangMasuk    = PengeluaranUangMasuk::findOrFail($id);
        return view('accounting.pengeluaran.uang-masuk.edit', [
            'siswas'                 => $siswas,
            'jenisPembayarans'       => $jenisPembayarans,
            'pengeluaranUangMasuk'  => $pengeluaranUangMasuk
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nama_siswa'        => 'required|min:2',
            'harga'             => 'required'
        ]);

        if(!$validate->fails()){
            $pengeluaranUangMasuk                           = PengeluaranUangMasuk::findOrFail($id);
            $pengeluaranUangMasuk->tanggal                  = $this->request->tanggal;
            $pengeluaranUangMasuk->siswa_id                 = $this->request->siswa_id;
            $pengeluaranUangMasuk->jenis_pembayaran_id      = $this->request->jenis_pembayaran_id;
            $pengeluaranUangMasuk->nama_siswa               = $this->request->nama_siswa;
            $pengeluaranUangMasuk->deskripsi                = $this->request->deskripsi;
            $pengeluaranUangMasuk->no_akun_debit            = $this->request->no_akun_debit;
            $pengeluaranUangMasuk->no_akun_kredit           = $this->request->no_akun_kredit;
            $pengeluaranUangMasuk->harga                    = $this->request->harga;
            $pengeluaranUangMasuk->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Pengeluaran Uang Masuk <strong> $pengeluaranUangMasuk->tanggal </strong> berhasil."
            ]);
            return redirect()->route('uang-masuk.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('uang-masuk.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $pengeluaranUangMasuk   = PengeluaranUangMasuk::findOrFail($id);
        $pengeluaranUangMasuk->delete();
        return redirect()->back();
    }
}
