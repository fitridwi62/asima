<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\PengeluaranPermintaanDana;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PermintaanDanaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $permintaanDanas            = PengeluaranPermintaanDana::orderBy('created_at', 'DESC')
                                                                ->paginate($this->limit);
        return view('accounting.pengeluaran.permintaan-dana.index', [
            'permintaanDanas'       => $permintaanDanas
        ]);
    }

    public function create()
    {
        return view('accounting.pengeluaran.permintaan-dana.create');
    }

    public function store()
    {
        $validate               = Validator::make($this->request->all(),[
            'kode_permintaan_dana'      => 'required|min:2',
            'tanggal'                   => 'required|date',
            'kode_kwintansi'            => 'required|min:2',
            'harga'                     => 'required'
        ]);

        if(!$validate->fails()){
            $permintaanDana                             = new PengeluaranPermintaanDana;
            $permintaanDana->kode_permintaan_dana       = $this->request->kode_permintaan_dana;
            $permintaanDana->tanggal                    = $this->request->tanggal;
            $permintaanDana->kode_kwintansi             = $this->request->kode_kwintansi;
            $permintaanDana->nomor_akun_debit           = $this->request->nomor_akun_debit;
            $permintaanDana->nomor_akun_kredit          = $this->request->nomor_akun_kredit;
            $permintaanDana->harga                      = $this->request->harga;
            $permintaanDana->status                     = $this->request->status;
            $permintaanDana->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Permintaan Dana <strong> $permintaanDana->kode_permintaan_dana </strong> berhasil."
            ]);
            return redirect()->route('permintaan-dana.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('permintaan-dana.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $permintaanDana             = PengeluaranPermintaanDana::findOrFail($id);
        return view('accounting.pengeluaran.permintaan-dana.edit',[
            'permintaanDana'        => $permintaanDana
        ]);
    }

    public function update($id)
    {
        $validate               = Validator::make($this->request->all(),[
            'kode_permintaan_dana'      => 'required|min:4',
            'tanggal'                   => 'required|date',
            'kode_kwintansi'            => 'required|min:2',
            'harga'                     => 'required'
        ]);

        if(!$validate->fails()){
            $permintaanDana                             = PengeluaranPermintaanDana::findOrFail($id);
            $permintaanDana->kode_permintaan_dana       = $this->request->kode_permintaan_dana;
            $permintaanDana->tanggal                    = $this->request->tanggal;
            $permintaanDana->kode_kwintansi             = $this->request->kode_kwintansi;
            $permintaanDana->nomor_akun_debit           = $this->request->nomor_akun_debit;
            $permintaanDana->nomor_akun_kredit          = $this->request->nomor_akun_kredit;
            $permintaanDana->harga                      = $this->request->harga;
            $permintaanDana->status                     = $this->request->status;
            $permintaanDana->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Permintaan Dana <strong> $permintaanDana->kode_permintaan_dana </strong> berhasil diupdate."
            ]);
            return redirect()->route('permintaan-dana.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('permintaan-dana.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $permintaanDana             = PengeluaranPermintaanDana::findOrFail($id);
        $permintaanDana->delete();
        return redirect()->back();
    }
}
