<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\Siswa;
use App\JenisPembayaran;
use App\JurnalEdc;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalEdcController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalEdcs             = JurnalEdc::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        $jenisPembayarans       = JenisPembayaran::all();
        return view('accounting.pengeluaran.jurnal-edc.index', [
            'jurnalEdcs'        => $jurnalEdcs,
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }

    public function create()
    {
        $siswas                  = Siswa::all();
        $jenisPembayarans        = JenisPembayaran::all();
        return view('accounting.pengeluaran.jurnal-edc.create', [
            'siswas'            => $siswas,
            'jenisPembayarans'  => $jenisPembayarans
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'tanggal'       => 'required|date',
            'deskripsi'     => 'required|min:2',
            'debit'         => 'required'
        ]);

        if(!$validate->fails()){
            $jurnalEdc                         = new JurnalEdc;
            $jurnalEdc->tanggal                = $this->request->tanggal;
            $jurnalEdc->siswa_id               = $this->request->siswa_id;
            $jurnalEdc->jenis_pembayaran_id    = $this->request->jenis_pembayaran_id;
            $jurnalEdc->deskripsi              = $this->request->deskripsi;
            $jurnalEdc->nomor_akun_debit       = $this->request->nomor_akun_debit;
            $jurnalEdc->nomor_akun_kredit      = $this->request->nomor_akun_kredit;
            $jurnalEdc->debit                  = $this->request->debit;
            $jurnalEdc->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Jurnal Kas EDC <strong> $jurnalEdc->tanggal </strong> berhasil."
            ]);
            return redirect()->route('jurnal-edc.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('jurnal-edc.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $siswas                  = Siswa::all();
        $jenisPembayarans        = JenisPembayaran::all();
        $jurnalEdc               = JurnalEdc::findOrFail($id);
        return view('accounting.pengeluaran.jurnal-edc.edit', [
            'siswas'            => $siswas,
            'jenisPembayarans'  => $jenisPembayarans,
            'jurnalEdc'         => $jurnalEdc
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'tanggal'       => 'required|date',
            'deskripsi'     => 'required|min:2',
            'debit'         => 'required'
        ]);

        if(!$validate->fails()){
            $jurnalEdc                          = JurnalEdc::findOrFail($id);
            $jurnalEdc->tanggal                = $this->request->tanggal;
            $jurnalEdc->siswa_id               = $this->request->siswa_id;
            $jurnalEdc->jenis_pembayaran_id    = $this->request->jenis_pembayaran_id;
            $jurnalEdc->deskripsi              = $this->request->deskripsi;
            $jurnalEdc->nomor_akun_debit       = $this->request->nomor_akun_debit;
            $jurnalEdc->nomor_akun_kredit      = $this->request->nomor_akun_kredit;
            $jurnalEdc->debit                  = $this->request->debit;
            $jurnalEdc->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Jurnal Kas EDC <strong> $jurnalEdc->tanggal </strong> berhasil."
            ]);
            return redirect()->route('jurnal-edc.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('jurnal-edc.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $jurnalEdc                          = JurnalEdc::findOrFail($id);
        $jurnalEdc->delete();
        return redirect()->back();
    }
}
