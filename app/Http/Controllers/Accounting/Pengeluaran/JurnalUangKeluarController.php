<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\JurnalUangKeluar;
use App\PengelauaranJurnalKasKecilReimbursment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalUangKeluarController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalUangKeluars      = JurnalUangKeluar::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('accounting.pengeluaran.uang-keluar.index',[
            'jurnalUangKeluars'     => $jurnalUangKeluars
        ]);
    }

    public function create()
    {
        $reibusments    = PengelauaranJurnalKasKecilReimbursment::all();
        return view('accounting.pengeluaran.uang-keluar.create', [
            'reibusments'   => $reibusments
        ]);
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_kwitansi'    => 'required|min:2',
            'description'       => 'required|min:2',
            'debit'             => 'required'  
        ]);

        if(!$validate->fails()){
            $jurnalUangKeluar                       = new JurnalUangKeluar;
            $jurnalUangKeluar->tanggal              = $this->request->tanggal;
            $jurnalUangKeluar->nomor_kwitansi       = $this->request->nomor_kwitansi;
            $jurnalUangKeluar->reimbursment_id      = $this->request->reimbursment_id;
            $jurnalUangKeluar->description          = $this->request->description;
            $jurnalUangKeluar->nomor_akun_debit     = $this->request->nomor_akun_debit;
            $jurnalUangKeluar->nomor_akun_kredit    = $this->request->nomor_akun_kredit;
            $jurnalUangKeluar->debit                = $this->request->debit;
            $jurnalUangKeluar->status               = $this->request->status;
            $jurnalUangKeluar->save();
            Session::flash('flash_notification', [
                'level'             => 'success',
                "message"           => "Jurnal Uang Keluar <strong> $jurnalUangKeluar->nomor_kwitansi </strong> berhasil."
            ]);
            return redirect()->route('jurnal-uang-keluar.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-uang-keluar.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $reibusments        = PengelauaranJurnalKasKecilReimbursment::all();
        $jurnalUangKeluar   = JurnalUangKeluar::findOrFail($id);
        return view('accounting.pengeluaran.uang-keluar.edit', [
            'reibusments'       => $reibusments,
            'jurnalUangKeluar'  => $jurnalUangKeluar
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'nomor_kwitansi'    => 'required|min:2',
            'description'       => 'required|min:3',
            'debit'             => 'required'  
        ]);

        if(!$validate->fails()){
            $jurnalUangKeluar                       = JurnalUangKeluar::findOrFail($id);
            $jurnalUangKeluar->tanggal              = $this->request->tanggal;
            $jurnalUangKeluar->nomor_kwitansi       = $this->request->nomor_kwitansi;
            $jurnalUangKeluar->reimbursment_id      = $this->request->reimbursment_id;
            $jurnalUangKeluar->description          = $this->request->description;
            $jurnalUangKeluar->nomor_akun_debit     = $this->request->nomor_akun_debit;
            $jurnalUangKeluar->nomor_akun_kredit    = $this->request->nomor_akun_kredit;
            $jurnalUangKeluar->debit                = $this->request->debit;
            $jurnalUangKeluar->status               = $this->request->status;
            $jurnalUangKeluar->save();
            Session::flash('flash_notification', [
                'level'             => 'success',
                "message"           => "Jurnal Uang Keluar <strong> $jurnalUangKeluar->nomor_kwitansi </strong> berhasil."
            ]);
            return redirect()->route('jurnal-uang-keluar.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('jurnal-uang-keluar.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }   
    }

    public function destroy($id)
    {
        $jurnalUangKeluar   = JurnalUangKeluar::findOrFail($id);
        $jurnalUangKeluar->delete();
        return redirect()->back();
    }
}
