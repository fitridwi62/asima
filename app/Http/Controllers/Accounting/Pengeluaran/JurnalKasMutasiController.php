<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\Siswa;
use App\JurnalKasMutasi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class JurnalKasMutasiController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $jurnalKasMutasis       = JurnalKasMutasi::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('accounting.pengeluaran.kas-mutasi.index',[
            'jurnalKasMutasis'  => $jurnalKasMutasis
        ]);
    }

    public function create()
    {
        return view('accounting.pengeluaran.kas-mutasi.create');
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(),[
            'tanggal'       => 'required|date',
            'jam_mutasi'    => 'required',
            'debit'         => 'required'
        ]);

        if(!$validate->fails()){
            $kasMutasi                      = new JurnalKasMutasi;
            $kasMutasi->tanggal             = $this->request->tanggal; 
            $kasMutasi->jam_mutasi          = $this->request->jam_mutasi;
            $kasMutasi->nomor_akun_debit    = $this->request->nomor_akun_debit;
            $kasMutasi->nomor_akun_kredit   = $this->request->nomor_akun_kredit;
            $kasMutasi->status              = $this->request->status;
            $kasMutasi->debit               = $this->request->debit;
            $kasMutasi->save();
            Session::flash("flash_notification", [
                'level'     => 'success',
                "messages"  => "Jurnal Kas Mutasi <strong> $kasMutasi->jam_mutasi </strong> berhasil."
            ]);
            return redirect()->route('kas-mutasi.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('kas-mutasi.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kasMutasi      = JurnalKasMutasi::findOrFail($id);
        return view('accounting.pengeluaran.kas-mutasi.edit', [
            'kasMutasi'     => $kasMutasi
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(),[
            'tanggal'       => 'required',
            'jam_mutasi'    => 'required',
            'debit'         => 'required'
        ]);

        if(!$validate->fails()){
            $kasMutasi                      = JurnalKasMutasi::findOrFail($id);
            $kasMutasi->tanggal             = $this->request->tanggal; 
            $kasMutasi->jam_mutasi          = $this->request->jam_mutasi;
            $kasMutasi->nomor_akun_debit    = $this->request->nomor_akun_debit;
            $kasMutasi->nomor_akun_kredit   = $this->request->nomor_akun_kredit;
            $kasMutasi->status              = $this->request->status;
            $kasMutasi->debit               = $this->request->debit;
            $kasMutasi->save();
            Session::flash("flash_notification", [
                'level'     => 'success',
                "messages"  => "Jurnal Kas Mutasi <strong> $kasMutasi->jam_mutasi </strong> berhasil."
            ]);
            return redirect()->route('kas-mutasi.index');
        }else{
            $errors         = $validate->messages();
            return redirect()->route('kas-mutasi.create')
                            ->withErrors($validate)
                            ->withInput($this->request->all());
        }
    }

    public function destroy($id)
    {
        $kasMutasi      = JurnalKasMutasi::findOrFail($id);
        $kasMutasi->delete();
        return redirect()->back();
    }
}
