<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\DanaPending;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class DanaPendingController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $danaPendings       = DanaPending::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.pengeluaran.dana-pending.index', [
            'danaPendings'  => $danaPendings
        ]);
    }

    public function create()
    {
        return view('accounting.pengeluaran.dana-pending.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required|date',
            'kode_formolir'     => 'required|min:2',
            'deskripsi'         => 'required|min:2',
            'jumlah_nominal'    => 'required'
        ]);
        if(!$validate->fails()){
            $danaPending                = new DanaPending;
            $danaPending->tanggal       = $this->request->tanggal;
            $danaPending->kode_formolir = $this->request->kode_formolir;
            $danaPending->deskripsi     = $this->request->deskripsi;
            $danaPending->jumlah_nominal = $this->request->jumlah_nominal;
            $danaPending->save();
            Session::flash("flash_notificatiob", [
                "level"     => 'success',
                "messages"  => "Permintaan Dana Belum Tertagih <strong> $danaPending->kode_formolir</strong> Berhasil"
            ]); 
            return redirect()->route('dana-pending.index');
        }else{
            $errors     = $validate->messages();
            return redirect()->route('dana-pending.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $danaPending        = DanaPending::findOrFail($id);
        return view('accounting.pengeluaran.dana-pending.edit',[
            'danaPending'   => $danaPending
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(),[
            'tanggal'           => 'required',
            'kode_formolir'     => 'required',
            'deskripsi'         => 'required',
            'jumlah_nominal'    => 'required'
        ]);
        if(!$validate->fails()){
            $danaPending                = DanaPending::findOrFail($id);
            $danaPending->tanggal       = $this->request->tanggal;
            $danaPending->kode_formolir = $this->request->kode_formolir;
            $danaPending->deskripsi     = $this->request->deskripsi;
            $danaPending->jumlah_nominal = $this->request->jumlah_nominal;
            $danaPending->save();
            Session::flash("flash_notificatiob", [
                "level"     => 'success',
                "messages"  => "Permintaan Dana Belum Tertagih <strong> $danaPending->kode_formolir</strong> Berhasil"
            ]); 
            return redirect()->route('dana-pending.index');
        }else{
            $errors     = $validate->messages();
            return redirect()->route('dana-pending.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $danaPending        = DanaPending::findOrFail($id);
        $danaPending->delete();
        return redirect()->back();
    }
}
