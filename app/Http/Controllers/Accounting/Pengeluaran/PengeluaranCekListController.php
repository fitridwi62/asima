<?php

namespace App\Http\Controllers\Accounting\Pengeluaran;

use Redirect;
use Validator;
use App\PengeluaranCekList;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PengeluaranCekListController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $cekLists           = PengeluaranCekList::orderBy('created_at', 'DESC')
                                                ->paginate($this->limit);
        return view('accounting.pengeluaran.cek-list.index', [
            'cekLists'      => $cekLists
        ]);
    }

    public function create()
    {
        return view('accounting.pengeluaran.cek-list.create');
    }

    public function store()
    {
        $validate       = Validator::make($this->request->all(), [
            'list'      => 'required|min:2',
            'deskripsi' => 'required|min:2',
            'percent'   => 'required'
        ]);

        if(!$validate->fails()){
            $cekList                = new PengeluaranCekList;
            $cekList->list          = $this->request->list;
            $cekList->deskripsi     = $this->request->deskripsi;
            $cekList->status        = $this->request->status;
            $cekList->percent       = $this->request->percent;
            $cekList->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Cek list <strong> $cekList->percent % </strong> berhasil."
            ]);
            return Redirect()->route('cek-list.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('cek-list.create')
                             ->withErrors($validate)
                             ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $cekList    = PengeluaranCekList::findOrFail($id);
        return view('accounting.pengeluaran.cek-list.edit', [
            'cekList'   => $cekList
        ]);
    }

    public function update($id)
    {
        $validate       = Validator::make($this->request->all(), [
            'list'      => 'required|min:2',
            'deskripsi' => 'required|min:2',
            'percent'   => 'required'
        ]);

        if(!$validate->fails()){
            $cekList                = PengeluaranCekList::findOrFail($id);
            $cekList->list          = $this->request->list;
            $cekList->deskripsi     = $this->request->deskripsi;
            $cekList->status        = $this->request->status;
            $cekList->percent       = $this->request->percent;
            $cekList->save();
            Session::flash('flash_notification', [
                "level"             => "success",
                "message"           => "Cek list <strong> $cekList->percent % </strong> berhasil."
            ]);
            return Redirect()->route('cek-list.index');
        }else{
            $errors             = $validate->messages();
            return redirect()->route('cek-list.create')
                             ->withErrors($validate)
                             ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $cekList    = PengeluaranCekList::findOrFail($id);
        $cekList->delete();
        return redirect()->back();
    }
}
