<?php

namespace App\Http\Controllers\Accounting;

use Redirect;
use Validator;
use App\TipePajak;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class TipePajakController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $tipePajaks         = TipePajak::orderBy('created_at', 'DESC')
                                        ->paginate($this->limit);
        return view('accounting.tipe-pajak.index', [
            'tipePajaks'    => $tipePajaks
        ]);
    }

    public function create()
    {
        return view('accounting.tipe-pajak.create');
    }

    public function store(Request $request)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipePajak              = new TipePajak;
            $tipePajak->nama        = $this->request->nama;
            $tipePajak->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Tipe Pajak <strong> $tipePajak->nama </strong> berhasil."
            ]);
            return redirect()->route('tipe-pajak.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('tipe-pajak.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tipePajak              = TipePajak::findOrFail($id);
        return view('accounting.tipe-pajak.edit', [
            'tipePajak'         => $tipePajak
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'nama'          => 'required|min:2'
        ]);

        if(!$validate->fails()){
            $tipePajak              = TipePajak::findOrFail($id);
            $tipePajak->nama        = $this->request->nama;
            $tipePajak->save();
            Session::flash("flash_notification", [
                "level"             => "success",
                "message"           => "Tipe Pajak <strong> $tipajak->nama </strong> berhasil diupdate."
            ]);
            return redirect()->route('tipe-pajak.index');
        }else{
            $errors                 = $validate->messages();

            return redirect()->route('tipe-pajak.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $tipePajak              = TipePajak::findOrFail($id);
        $tipePajak->delete();
        return redirect()->back();
    }
}
