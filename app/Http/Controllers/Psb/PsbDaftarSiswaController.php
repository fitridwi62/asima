<?php

namespace App\Http\Controllers\Psb;

use Image;
use Validator;
use Redirect;
use App\Jenjang;
use App\Jurusan;
use App\DaftarSiwa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PsbDaftarSiswaController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $daftarSiswas           = DaftarSiwa::orderBy('created_at', 'DESC')
        ->with(['jurusan'  => function($jurusanQuery){
            $jurusanQuery->get();
        }])
        ->with(['jenjang' => function($jenjangQuery){
            $jenjangQuery->get();
        }])
        ->paginate($this->limit);
        return view('data_siswa.daftar_siswa.index', [
            'daftarSiswas' => $daftarSiswas
        ]);
    }

    public function create()
    {
        $jurusans       = Jurusan::all();
        $jenjangs       = Jenjang::all();
        return view('data_siswa.daftar_siswa.create', [
            'jurusans'  => $jurusans,
            'jenjangs'  => $jenjangs
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'no_pendaftaran'            => 'required|min:2',
            'nama_lengkap'              => 'required|min:2',
            'jenis_kelamin'             => 'required',
            'tempat_lahir'              => 'required|min:2',
            'tanggal_lahir'             => 'required|date',
            'alamat'                    => 'required|min:2',
            'telp'                      => 'required',
            'email'                     => 'required',
            'no_wa'                     => 'required',
            'nama_orang_tua'            => 'required|min:2',
            'pekerjaan_orang_tua'       => 'required|min:2',
            'no_telp_ortu'              => 'required',
            'penghasilan_ortu'          => 'required',
            'photo'                     => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        if(!$validate->fails()){
            $daftarSiswa                            = new DaftarSiwa;
            $daftarSiswa->no_pendaftaran            = $this->request->no_pendaftaran;
            $daftarSiswa->nama_lengkap              = $this->request->nama_lengkap;
            $daftarSiswa->jenis_kelamin             = $this->request->jenis_kelamin;
            $daftarSiswa->tempat_lahir              = $this->request->tempat_lahir;
            $daftarSiswa->tanggal_lahir             = $this->request->tanggal_lahir;
            $daftarSiswa->alamat                    = $this->request->alamat;
            $daftarSiswa->telp                      = $this->request->telp;
            $daftarSiswa->jenjang_id                = $this->request->jenjang_id;
            $daftarSiswa->email                     = $this->request->email;
            $daftarSiswa->no_wa                     = $this->request->no_wa;
            $daftarSiswa->jurusan_id                = $this->request->jurusan_id;
            $daftarSiswa->nama_orang_tua            = $this->request->nama_orang_tua;
            $daftarSiswa->pekerjaan_orang_tua       = $this->request->pekerjaan_orang_tua;
            $daftarSiswa->no_telp_ortu              = $this->request->no_telp_ortu;
            $daftarSiswa->penghasilan_ortu          = $this->request->penghasilan_ortu;

            $file                                   = $this->request->file('photo');
            $fileName                               = $file->getClientOriginalName();
            $path                                   = public_path('psb/' . $fileName);
            Image::make($file->getRealPath())->resize(150, 150)->save($path);
            $daftarSiswa->photo                     = $fileName;
            $daftarSiswa->status                    = 'proses';
            $daftarSiswa->save();
            return redirect()->route('daftar-siswa.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('daftar-siswa.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        $daftarSiswa        = DaftarSiwa::findOrFail($id);
        return view('data_siswa.daftar_siswa.show',[
            'daftarSiswa'   => $daftarSiswa
        ]);
    }

    public function edit($id)
    {
        $jurusans       = Jurusan::all();
        $jenjangs       = Jenjang::all();
        $daftarSiswa    = DaftarSiwa::find($id);
       //dd($daftarSiswa);
        return view('data_siswa.daftar_siswa.edit', [
            'jurusans'  => $jurusans,
            'jenjangs'  => $jenjangs,
            'daftarSiswa'   => $daftarSiswa
        ]);   
    }

    public function update($id)
    {
            $daftarSiswa                            = DaftarSiwa::findOrFail($id);;
            $daftarSiswa->no_pendaftaran            = $this->request->no_pendaftaran;
            $daftarSiswa->nama_lengkap              = $this->request->nama_lengkap;
            $daftarSiswa->jenis_kelamin             = $this->request->jenis_kelamin;
            $daftarSiswa->tempat_lahir              = $this->request->tempat_lahir;
            $daftarSiswa->tanggal_lahir             = $this->request->tanggal_lahir;
            $daftarSiswa->alamat                    = $this->request->alamat;
            $daftarSiswa->telp                      = $this->request->telp;
            $daftarSiswa->jenjang_id                = $this->request->jenjang_id;
            $daftarSiswa->email                     = $this->request->email;
            $daftarSiswa->no_wa                     = $this->request->no_wa;
            $daftarSiswa->jurusan_id                = $this->request->jurusan_id;
            $daftarSiswa->nama_orang_tua            = $this->request->nama_orang_tua;
            $daftarSiswa->pekerjaan_orang_tua       = $this->request->pekerjaan_orang_tua;
            $daftarSiswa->no_telp_ortu              = $this->request->no_telp_ortu;
            $daftarSiswa->penghasilan_ortu          = $this->request->penghasilan_ortu;
            $file                                   = $this->request->file('photo');
            $fileName                               = $file->getClientOriginalName();
            $path                                   = public_path('psb' . $fileName);
            Image::make($file->getRealPath())->resize(150, 150)->save($path);
            $daftarSiswa->photo                     = $fileName;
            $daftarSiswa->status                    = $this->request->status;
            $daftarSiswa->save();
            return redirect()->route('daftar-siswa.index');
    }

    public function destroy($id)
    {
        $daftarSiswa    = DaftarSiwa::findOrFail($id);
        $daftarSiswa->delete();
        return redirect()->back();
    }
}
