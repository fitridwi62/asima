<?php

namespace App\Http\Controllers\Psb;

use App\HasilUjian;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PsbSiswaDiterimaController extends Controller
{
    protected $limit = 10;
    public function __construct(){
    	$this->middleware('auth');
    }
    public function index(){
        $siswaDiterimas         = HasilUjian::orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        return view('data_siswa.siswa_diterima.index', [
            'siswaDiterimas'    => $siswaDiterimas
        ]);
    }
}
