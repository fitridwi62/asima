<?php

namespace App\Http\Controllers\Psb;

use Redirect;
use Validator;
use App\DaftarSiwa;
use App\TestInterview;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class TestInterviewController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
    }

    public function index()
    {
        $testInterviews        = TestInterview::orderBy('created_at', 'DESC')
                                                ->with('daftarSiswa')
                                                ->paginate($this->limit);
        return view('data_siswa.test_interview.index', [
            'testInterviews'    => $testInterviews
        ]);
    }

    public function create()
    {
        $daftar_siswas      = DaftarSiwa::all();
        return view('data_siswa.test_interview.create', [
            'daftar_siswas' => $daftar_siswas
        ]);
    }

    public function store()
    {
        $validate                       = Validator::make($this->request->all(), [
            'nilai_terbaik'             => 'required|numeric',
            'prestasi'                  => 'required|min:4',
            'hoby'                      => 'required|min:4',
            'aktivitas'                 => 'required|min:4',
            'jarak_rumah'               => 'required|numeric',
            'pendapatan_orang_tua'      => 'required|numeric',
            'pekerjaan_orang_tua'       => 'required|numeric',
            'note'                      => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $testInterview                          = new TestInterview;
            $testInterview->daftar_siswa_id         = $this->request->daftar_siswa_id;
            $testInterview->nilai_terbaik           = $this->request->nilai_terbaik;
            $testInterview->prestasi                = $this->request->prestasi;
            $testInterview->hoby                    = $this->request->hoby;
            $testInterview->aktivitas               = $this->request->aktivitas;
            $testInterview->jarak_rumah             = $this->request->jarak_rumah;
            $testInterview->pendapatan_orang_tua    = $this->request->pendapatan_orang_tua;
            $testInterview->pekerjaan_orang_tua     = $this->request->pekerjaan_orang_tua;
            $testInterview->note                    = $this->request->note;
            $testInterview->save();
            return redirect()->route('test-interview.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('test-interview.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        $testInterview          = TestInterview::findOrFail($id);
        return view('data_siswa.test_interview.show', [
            'testInterview'     => $testInterview
        ]);
    }

    public function edit($id)
    {
        $testInterview          = TestInterview::findOrFail($id);
        $daftar_siswas          = DaftarSiwa::all();
        return view('data_siswa.test_interview.edit', [
            'daftar_siswas'     => $daftar_siswas,
            'testInterview'     => $testInterview
        ]);

    }

    public function update($id)
    {
        $validate                       = Validator::make($this->request->all(), [
            'nilai_terbaik'             => 'required|numeric',
            'prestasi'                  => 'required|min:4',
            'hoby'                      => 'required|min:4',
            'aktivitas'                 => 'required|min:4',
            'jarak_rumah'               => 'required|numeric',
            'pendapatan_orang_tua'      => 'required|numeric',
            'pekerjaan_orang_tua'       => 'required|numeric',
            'note'                      => 'required|min:4'
        ]);

        if(!$validate->fails()){
            $testInterview                          = TestInterview::findOrFail($id);
            $testInterview->daftar_siswa_id         = $this->request->daftar_siswa_id;
            $testInterview->nilai_terbaik           = $this->request->nilai_terbaik;
            $testInterview->prestasi                = $this->request->prestasi;
            $testInterview->hoby                    = $this->request->hoby;
            $testInterview->aktivitas               = $this->request->aktivitas;
            $testInterview->jarak_rumah             = $this->request->jarak_rumah;
            $testInterview->pendapatan_orang_tua    = $this->request->pendapatan_orang_tua;
            $testInterview->pekerjaan_orang_tua     = $this->request->pekerjaan_orang_tua;
            $testInterview->note                    = $this->request->note;
            $testInterview->save();
            return redirect()->route('test-interview.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('test-interview.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $testInterview          = TestInterview::findOrFail($id);
        $testInterview->delete();
        return redirect()->back();
    }
}
