<?php

namespace App\Http\Controllers\Psb;

use Redirect;
use Validator;
use App\DaftarSiwa;
use App\HasilUjian;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PsbHasilUjianController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;
    public function __construct(Request $request, Response $response){
        $this->request          = $request;
        $this->response         = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $hasilUjians            = HasilUjian::with('siswa')
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($this->limit);
        return view('data_siswa.hasil_ujian.index',[
            'hasilUjians'        => $hasilUjians
        ]);
    }

    public function create()
    {
        $siswas         = DaftarSiwa::all();
        return view('data_siswa.hasil_ujian.create', [
            'siswas'    => $siswas
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'pengetahuan_umum'      => 'required|numeric',
            'minat_bakat'           => 'required|numeric',
            'akademik'              => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $hasilUjian             = new HasilUjian;
            $hasilUjian->daftar_siswa_id = $this->request->daftar_siswa_id;
            $hasilUjian->pengetahuan_umum   = $this->request->pengetahuan_umum;
            $hasilUjian->minat_bakat        = $this->request->minat_bakat;
            $hasilUjian->akademik           = $this->request->akademik;
            $hasilUjian->status             = $this->request->status;
            $hasilUjian->save();
            return redirect()->route('hasil-ujian.index');
        }else{
            $errors             = $validate->messages();

            return redirect()->route('hasil-ujian.create')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $hasilUjian             = HasilUjian::findOrFail($id);
        $siswas                 = DaftarSiwa::all();
        return view('data_siswa.hasil_ujian.edit', [
            'siswas'        => $siswas,
            'hasilUjian'    => $hasilUjian
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'pengetahuan_umum'      => 'required|numeric',
            'minat_bakat'           => 'required|numeric',
            'akademik'              => 'required|numeric'
        ]);

        if(!$validate->fails()){
            $hasilUjian                     = HasilUjian::findOrFail($id);
            $hasilUjian->daftar_siswa_id    = $this->request->daftar_siswa_id;
            $hasilUjian->pengetahuan_umum   = $this->request->pengetahuan_umum;
            $hasilUjian->minat_bakat        = $this->request->minat_bakat;
            $hasilUjian->akademik           = $this->request->akademik;
            $hasilUjian->status             = $this->request->status;
            $hasilUjian->save();
            return redirect()->route('hasil-ujian.index');
        }else{
            $errors             = $validate->messages();

            return redirect()->route('hasil-ujian.edit')
                            ->withErrors($validate)
                            ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $hasilUjian                     = HasilUjian::findOrFail($id);
        $hasilUjian->delete();
        return redirect()->back();
    }
}
