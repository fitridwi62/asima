<?php

namespace App\Http\Controllers\ManajemenGuru;

use Redirect;
use Validator;
use App\DataGuru;
use App\KehadiranGuru;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class KehadiranGuruController extends Controller
{
    protected $request;
    protected $response;
    protected $limit = 10;

    public function __construct(
        Request $request,
        Response $response
    ) {
        $this->request      = $request;
        $this->response     = $response;
        $this->middleware('auth');
    }

    public function index()
    {
        $kehadiranGurus         = KehadiranGuru::orderBy('created_at', 'DESC')
                                                ->with('guru')
                                                ->paginate($this->limit);
        $dataGurus              = DataGuru::all();
        return view('manajemen_guru.kehadiran_guru.index', [
            'kehadiranGurus'    => $kehadiranGurus,
            'dataGurus'         => $dataGurus
        ]);
    }

    public function create()
    {
        $dataGurus          = DataGuru::all();
        return view('manajemen_guru.kehadiran_guru.create', [
            'dataGurus'     => $dataGurus
        ]);
    }

    public function store()
    {
        $validate           = Validator::make($this->request->all(), [
            'date'           => 'required|date',
            'jam_masuk'      => 'required',
            'jam_keluar'    =>  'required'
        ]);

        if(!$validate->fails()){
            $kehadiranGuru                      = new KehadiranGuru;
            $kehadiranGuru->data_guru_id        = $this->request->data_guru_id;
            $kehadiranGuru->date                = $this->request->date;
            $kehadiranGuru->jam_masuk           = $this->request->jam_masuk;
            $kehadiranGuru->jam_keluar          = $this->request->jam_keluar;
            $kehadiranGuru->status              = $this->request->status;
            $kehadiranGuru->save();
            return redirect()->route('kehadiran-guru.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('kehadiran-guru.create')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kehadiranGuru      = KehadiranGuru::findOrFail($id);
        $dataGurus          = DataGuru::all();
        return view('manajemen_guru.kehadiran_guru.edit', [
            'dataGurus'     => $dataGurus,
            'kehadiranGuru' => $kehadiranGuru
        ]);
    }

    public function update($id)
    {
        $validate           = Validator::make($this->request->all(), [
            'date'          => 'required|date',
            'jam_masuk'     => 'required',
            'jam_keluar'    =>  'required'
        ]);

        if(!$validate->fails()){
            $kehadiranGuru      = KehadiranGuru::findOrFail($id);
            $kehadiranGuru->data_guru_id        = $this->request->data_guru_id;
            $kehadiranGuru->date                = $this->request->date;
            $kehadiranGuru->jam_masuk           = $this->request->jam_masuk;
            $kehadiranGuru->jam_keluar          = $this->request->jam_keluar;
            $kehadiranGuru->status              = $this->request->status;
            $kehadiranGuru->save();
            return redirect()->route('kehadiran-guru.index');
        }else{
            $errors = $validate->messages();

            return redirect()->route('kehadiran-guru.edit')
                    ->withErrors($validate)
                    ->withInput($this->request->input());
        }
    }

    public function destroy($id)
    {
        $kehadiranGuru      = KehadiranGuru::findOrFail($id);
        $kehadiranGuru->delete();
        return redirect()->back();
    }
}
