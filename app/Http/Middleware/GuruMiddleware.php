<?php

namespace App\Http\Middleware;

use Auth;
use Redirect;
use App\User;
use Closure;

class GuruMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isGuru()){
            return $next($request);
        }else{
            return redirect('/');
        }
        return $next($request);
    }
}
