<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use Redirect;
use App\User;
class SiswaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isAdminSiswa()){
            return $next($request);
        }else{
            return redirect('/');
        }
        return $next($request);
    }
}
