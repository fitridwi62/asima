<?php

namespace App\Http\Middleware;

use Auth;
use Redirect;
use App\User;
use Closure;

class InventoryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isInventory()){
            return $next($request);
        }else{
            return redirect('/');
        }
        return $next($request);
    }
}
