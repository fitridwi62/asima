<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table = 'mata_pelajarans';
    protected $fillable = [
        'kurikulum_id',
        'data_guru_id',
        'kode',
        'nama',
        'jenjang_id'
    ];

    public function kurikulum(){
        return $this->belongsTo('App\Kurikulum');
    }

    public function jenjang(){
        return $this->belongsTo('App\Jenjang');
    }

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }
}
