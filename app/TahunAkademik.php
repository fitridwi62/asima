<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAkademik extends Model
{
    protected $table = "tahun_akademiks";
    protected $fillable = [
    	'nama',
    	'status'
    ];
}
