<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranPermintaanDana extends Model
{
    protected $table = 'pengeluaran_permintaan_danas';

    protected $fillable = [
        'kode_permintaan_dana',
        'tanggal',
        'kode_kwintansi',
        'nomor_akun_debit',
        'nomor_akun_kredit',
        'harga',
        'status'
    ];
}
