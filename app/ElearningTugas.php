<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElearningTugas extends Model
{
    protected $table = 'elearning_tugas';

    protected $fillable = [
        'data_guru_id',
        'nama',
        'file',
        'deskripsi',
        'batas_waktu',
        'kelas_id',
        'mata_pelajaran_id',
        'jurusan_id'
    ];

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function mata_pelajaran(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    public function jurusan(){
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }
}
