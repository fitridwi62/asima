<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranSpp extends Model
{
    protected $table = 'pembayaran_spps';

    protected $fillable = [
        'jenis_pembayaran_id',
        'biaya_spp',
        'pembayaran_extra',
        'pembayaran_bimbingan_belajar',
        'tanggal_bayar',
        'status'
    ];

    public function jenis_pembayaran(){
        return $this->belongsTo('App\JenisPembayaran', 'jenis_pembayaran_id');
    }
}
