<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranSemester extends Model
{
    protected $table = 'pembayaran_semesters';

    protected $fillable = [
        'nama',
        'biaya',
    ];
}
