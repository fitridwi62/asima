<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BimbinganBelajar extends Model
{
    protected $table = 'bimbingan_belajars';

    protected $fillable = [
        'nama',
        'data_guru_id',
        'tanggal',
        'jam_mulai',
        'jam_selesai',
        'pertemuan'
    ];

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }
}
