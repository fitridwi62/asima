<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RaportMapel extends Model
{
    protected $table = "raport_mapels";
    protected $fillable = [
        'siswa_id',
        'matkul_id',
        'nilai_pengetahuan',
        'nilai_keterampilan',
        'nilai_akhir',
        'predikat',
        'total',
        'user_id'
    ];

    public function siswaItem(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function mapel(){
        return $this->belongsTo('App\MataPelajaran', 'matkul_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
