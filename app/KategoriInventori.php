<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriInventori extends Model
{
    protected $table = 'kategori_inventoris';

    protected $fillable = [
        'nama'
    ];
}
