<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiPermapel extends Model
{
    protected $table = 'nilai_permapels';

    protected $fillable = [
        'mata_pelajaran_id',
        'nilai_id',
        'data_guru_id',
        'siswa_id',
        'nilai',
        'predikat',
        'description',
        'user_id'
    ];

    public function mata_pelajaran(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    public function gridNilai(){
        return $this->belongsTo('App\GridNilai', 'nilai_id');
    }

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'User_id');
    }
}
