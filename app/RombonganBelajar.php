<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RombonganBelajar extends Model
{
    protected $table  = "rombongan_belajars";
    protected $fillable = [
    	'nama',
    	'jurusan_id',
    	'kelas'
    ];

    public function jurusan(){
    	return $this->belongsTo('App\Jurusan');

    }
}
