<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePersonalPackage extends Model
{
    protected $table = 'type_personal_packages';

    protected $fillable = [
        'nama'
    ];
}
