<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table = "kurikulums";
    protected $fillable = [
    	'nama',
    	'status'
    ];

    public function walikelas(){
        return $this->hasMany('App\WaliKelas');
    }

    public function mata_pelajaran(){
        return $this->hasMany('App\MataPelajaran');
    }
}
