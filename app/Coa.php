<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coa extends Model
{
    protected $table = 'coas';

    protected $fillable = [
        'no_akun_coa',
        'nama_coa'
    ];
}
