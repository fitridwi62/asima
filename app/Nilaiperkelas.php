<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilaiperkelas extends Model
{
    protected $table = 'nilaiperkelas';

    protected $fillable = [
        'mata_pelajaran_id',
        'siswa_id',
        'kelas_id',
        'nilai',
        'predikat',
        'description'
    ];

    public function mata_pelajaran(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }
}
