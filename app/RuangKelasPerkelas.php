<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RuangKelasPerkelas extends Model
{
    protected $table = 'ruang_kelas_perkelas';

    protected $fillable = [
        'kelas_id',
        'nama'
    ];

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }
}
