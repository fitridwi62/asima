<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    protected $fillable = [
        'kode',
        'nama',
        'unit',
        'tanggal',
        'description',
        'status'
    ];
}
