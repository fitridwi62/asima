<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendapatan extends Model
{
    protected $table = "pendapatans";

    protected $fillable = [
    	'harga'
    ];
}
