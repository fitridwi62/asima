<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataBuku extends Model
{
    protected $table = 'data_bukus';

    protected $fillabel = [
        'kode',
        'nama_buku',
        'pengarang_buku',
        'tahun_terbit',
        'status'
    ];
}
