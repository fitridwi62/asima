<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raport extends Model
{
    protected $table = "raports";

    protected $fillable = [
        'siswa_id',
        'matkul_id',
        'nilai_pengetahuan',
        'nilai_keterampilan',
        'nilai_akhir',
        'predikat',
        'session_id'
    ];

    public function siswaItem(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function mapel(){
        return $this->belongsTo('App\MataPelajaran', 'matkul_id');
    }

    /*

    public function tahun_akademik(){
        return $this->belongsTo('App\TahunAkademik', 'tahun_akademik_id');
    }

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function raportMapel()
    {
        return $this->hasMany('App\RaportMapel', 'raport_id');
    }

    public function raportEkstra()
    {
        return $this->hasMany('App\RaportEkstrakurikuler', 'raport_id');
    }*/
}
