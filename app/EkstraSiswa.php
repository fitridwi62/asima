<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkstraSiswa extends Model
{
    protected $table = 'ekstra_siswas';

    protected $fillable = [
        'siswa_id',
        'ekstra_id',
        'ekstra_siswa_session_id',
        'user_id'
    ];

    public function siswaItem(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function ekstra(){
        return $this->belongsTo('App\Ekstrakurikuler', 'ekstra_id');
    }
}
