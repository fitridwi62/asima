<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GridNilai extends Model
{
    protected $table = 'grid_nilais';

    protected $fillable = [
        'nilai',
        'categori_nilai'
    ];
}
