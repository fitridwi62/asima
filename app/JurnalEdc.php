<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalEdc extends Model
{
    protected $table = 'jurnal_edcs';

    protected $fillable = [
        'tanggal',
        'siswa_id',
        'jenis_pembayaran_id',
        'deskripsi',
        'nomor_akun_debit',
        'nomor_akun_kredit',
        'debit'
    ];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function pembayaran(){
        return $this->belongsTo('App\JenisPembayaran', 'jenis_pembayaran_id');
    }
}
