<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalSPP extends Model
{
    protected $table = 'jurnal_s_p_ps';

    protected $fillable = [
        'tanggal',
        'nomor_rekening',
        'nama_siswa',
        'deskripsi',
        'kas_debit'
    ];
}
