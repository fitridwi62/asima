<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalUangKeluar extends Model
{
    protected $table = "jurnal_uang_keluars";

    protected $fillable = [
        'tanggal',
        'nomor_kwitansi',
        'reimbursment_id',
        'description',
        'nomor_akun_debit',
        'nomor_akun_kredit',
        'debit',
        'status'
    ];

    public function reimbursment(){
        return $this->belongsTo('App\PengelauaranJurnalKasKecilReimbursment');
    }
}
