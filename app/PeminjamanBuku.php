<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamanBuku extends Model
{
    protected $table = 'peminjaman_bukus';

    protected $fillable = [
        'kode',
        'tanggal_peminjam',
        'tanggal_kembalian',
        'jumlah_buku',
        'buku_id'
    ];

    public function buku(){
        return $this->belongsTo('App\DataBuku', 'buku_id');
    }
}
