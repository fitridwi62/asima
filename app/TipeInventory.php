<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeInventory extends Model
{
    protected $table = 'tipe_inventories';

    protected $fillable = [
        'nama'
    ];
}
