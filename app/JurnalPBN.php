<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalPBN extends Model
{
    protected $table = "jurnal_p_b_ns";

    protected $fillable = [
        'tanggal',
        'nomor_rekening',
        'nama_siswa',
        'deskripsi',
        'kas_debit'
    ];
}
