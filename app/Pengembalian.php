<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = 'pengembalians';

    protected $fillable = [
        'kode',
        'pinjamanBuku_id',
        'tanggal',
        'status'
    ];

    public function peminjan_buku(){
        return $this->belongsTo('App\PeminjamanBuku', 'pinjamanBuku_id');
    }
}
