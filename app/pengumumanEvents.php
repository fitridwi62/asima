<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pengumumanEvents extends Model
{
    protected $table = 'pengumuman_events';

    protected $fillable = [
        'judul',
        'start_date',
        'end_date'
    ];
}
