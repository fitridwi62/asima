<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipePajak extends Model
{
    protected $table = 'tipe_pajaks';

    protected $fillable = [
        'nama'
    ];
}
