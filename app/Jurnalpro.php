<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnalpro extends Model
{
    protected $table = 'jurnalpros';

    protected $fillable = [
        'tanggal',
        'nomor_rekening',
        'nama_siswa',
        'deskripsi',
        'kas_debit'
    ];
}
