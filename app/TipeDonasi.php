<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeDonasi extends Model
{
    protected $table = 'tipe_donasis';

    protected $fillable = [
        'nama'
    ];
}
