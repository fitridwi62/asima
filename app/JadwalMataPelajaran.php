<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalMataPelajaran extends Model
{
    protected $table = "jadwal_mata_pelajarans";
    protected $fillable = [
        'kelas_id',
        'mata_pelajaran_id',
        'siswa_id',
        'tanggal',
        'jam_mulai',
        'jam_selesai',
        'durasi',
        'data_guru_id',
        'user_id'
    ];

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function mata_pelajaran(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
