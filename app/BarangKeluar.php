<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangKeluar extends Model
{
    protected $table = 'barang_keluars';

    protected $fillable = [
        'nama',
        'barang_id',
        'harga',
        'tanggal',
        'author',
        'status'
    ];

    public function barang(){
        return $this->belongsTo('App\Barang', 'barang_id');
    }
}
