<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffPegawai extends Model
{
    protected $table = 'staff_pegawais';

    protected $fillable = [
        'nama_lengkap',
        'jabatan_id',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'email',
        'phone',
        'wa',
        'agama_id',
        'alamat',
        'status'
    ];

    public function jabatan(){
        return $this->belongsTo('App\Jabatan');
    }

    public function agama(){
        return $this->belongsTo('App\Agama');
    }
}
