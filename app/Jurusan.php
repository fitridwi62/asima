<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = "jurusans";
    protected $fillable = [
    	'nama_jurusan',
    	'kode_jurusan'
    ];

    public function rombel(){
    	return $this->hasMany('App\RombonganBelajar');
    }

}
