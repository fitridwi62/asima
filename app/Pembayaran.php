<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayarans';

    protected $fillable = [
        'siswa_id',
        'pembayaranspp_id',
        'pembayaransemester_id',
        'tanggal',
        'status',
        'user_id'
    ];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function pembayaranSPP(){
        return $this->belongsTo('App\PembayaranSpp', 'pembayaranspp_id');
    }

    public function pembayaranSemester(){
        return $this->belongsTo('App\PembayaranSemester', 'pembayaransemester_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
