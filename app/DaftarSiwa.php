<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarSiwa extends Model
{
    protected $table = 'daftar_siwas';

    protected $fillable = [
        'no_pendaftaran',
        'nama_lengkap',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'alamat',
        'telp',
        'jenjang_id',
        'email',
        'no_wa',
        //'jurusan_id',
        'nama_orang_tua',
        'pekerjaan_orang_tua',
        'no_telp_ortu',
        'penghasilan_ortu',
        'photo',
        'status'
    ];

    public function jurusan(){
        return $this->belongsTo('App\Jurusan');
    }

    public function jenjang(){
        return $this->belongsTo('App\Jenjang');
    }
}
