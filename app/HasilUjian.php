<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilUjian extends Model
{
    protected $table = 'hasil_ujians';

    protected $fillable = [
        'daftar_siswa_id',
        'pengetahuan_umum',
        'minat_bakat',
        'akademik',
        'status'
    ];

    public function siswa(){
        return $this->belongsTo('App\DaftarSiwa', 'daftar_siswa_id');
    }
}
