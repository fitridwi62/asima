<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DanaPending extends Model
{
    protected $table = 'dana_pendings';

    protected $fillable = [
        'tanggal',
        'kode_formolir',
        'deskripsi',
        'jumlah_nominal'
    ];
}
