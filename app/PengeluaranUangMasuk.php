<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranUangMasuk extends Model
{
    protected $table = 'pengeluaran_uang_masuks';
    protected $fillable = [
        'tanggal',
        'siswa_id',
        'jenis_pembayaran_id',
        'nama_siswa',
        'deskripsi',
        'no_akun_debit',
        'no_akun_kredit',
        'harga'
    ];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    public function pembayaran(){
        return $this->belongsTo('App\JenisPembayaran', 'jenis_pembayaran_id');
    }
}
