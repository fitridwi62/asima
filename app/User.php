<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isSuperAdmin(){
        if($this->status == 'superadmin'){
            return redirect('admin/dashboard');
        }
        return false;
    }

    public function isAdminSiswa(){
        if($this->status == 'siswa'){
            return redirect('admin/dashboard');
        }
        return false;
    }

    public function isAccounting(){
        if($this->status == 'accounting'){
            return redirect('accounting/tipe-pajak');
        }
        return false;
    }

    public function isInventory(){
        if($this->status == 'inventory'){
            return redirect('inventori/kategori-inventori');
        }
        return false;
    }

    public function isGuru(){
        if($this->status == 'guru'){
            return redirect('data-master/data_guru');
        }
        return false;
    }

    public function isStaff(){
        if($this->status == 'staff'){
            return redirect('manajemen/staff');
        }
        return false;
    }

    public function isPerpus(){
        if($this->status == 'perpus'){
            return redirect('perpustakaan/data-buku');
        }
        return false;
    }

    public function isOrtu(){
        if($this->status == 'ortu'){
            return redirect('akademik/data-orang-tua');
        }
        return false;
    }
}
