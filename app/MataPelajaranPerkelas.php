<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaranPerkelas extends Model
{
    protected $table = 'mata_pelajaran_perkelas';

    protected $fillable = [
        'kelas_id',
        'mata_pelajar_id',
        'status'
    ];

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function mata_pelajar(){
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajar_id');
    }

}
