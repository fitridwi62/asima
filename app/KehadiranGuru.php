<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KehadiranGuru extends Model
{
    protected $table = "kehadiran_gurus";

    protected $fillable = [
        'data_guru_id',
        'date',
        'jam_masuk',
        'jam_keluar',
        'status'
    ];

    public function guru(){
        return $this->belongsTo('App\DataGuru', 'data_guru_id');
    }
}
