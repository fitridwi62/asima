<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaPerkelas extends Model
{
    protected $table = 'siswa_perkelas';

    protected $fillable = [
        'kelas_id',
        'siswa_id'
    ];

    public function kelas(){
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }
}
