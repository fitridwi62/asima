<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalKasMutasi extends Model
{
    protected $table = 'jurnal_kas_mutasis';

    protected $fillable = [
        'tanggal',
        'jam_mutasi',
        'nomor_akun_debit',
        'nomor_akun_kredit',
        'status',
        'debit'
    ];
}
