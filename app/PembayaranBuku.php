<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranBuku extends Model
{
    protected $table = 'pembayaran_bukus';

    protected $fillable = [
        'nama',
        'biaya'
    ];
}
