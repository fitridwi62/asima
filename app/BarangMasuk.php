<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangMasuk extends Model
{
    protected $table = 'barang_masuks';

    protected $fillable = [
        'nama',
        'barang_id',
        'harga',
        'tanggal',
        'status'
    ];

    public function barang(){
        return $this->belongsTo('App\Barang', 'barang_id');
    }
}
