<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalMapelPerkelas extends Model
{
    protected $table = 'jadwal_mapel_perkelas';

    protected $fillable = [
        'siswa_id',
        'matkul_id',
        'task_id',
        ''
    ];
}
