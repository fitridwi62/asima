-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 15, 2020 at 10:13 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `agamas`
--

CREATE TABLE `agamas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_asset` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_asset` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_tetap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya_akuisisi` decimal(10,2) NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kredit_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bagians`
--

CREATE TABLE `bagians` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('habis','ada') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluars`
--

CREATE TABLE `barang_keluars` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `harga` decimal(10,2) NOT NULL,
  `tanggal` date NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('proses','sukses') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuks`
--

CREATE TABLE `barang_masuks` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `harga` decimal(10,2) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('masuk','keluar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bimbingan_belajars`
--

CREATE TABLE `bimbingan_belajars` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mulai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_selesai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pertemuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coas`
--

CREATE TABLE `coas` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_akun_coa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_coa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cut_off_siswas`
--

CREATE TABLE `cut_off_siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kas_kredit` decimal(10,2) NOT NULL,
  `dibuat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diperiksa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disetujui` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_siwas`
--

CREATE TABLE `daftar_siwas` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_pendaftaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_wa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_orang_tua` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan_orang_tua` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penghasilan_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('terima','gagal','proses') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dana_pendings`
--

CREATE TABLE `dana_pendings` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `kode_formolir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_nominal` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_alumnis`
--

CREATE TABLE `data_alumnis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_angkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sekolah_ke` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_bukus`
--

CREATE TABLE `data_bukus` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_buku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengarang_buku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_terbit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ada','kosong') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_gurus`
--

CREATE TABLE `data_gurus` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_induk_guru` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_guru` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomer_ktp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertifikat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_sekolahs`
--

CREATE TABLE `data_sekolahs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donasis`
--

CREATE TABLE `donasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipe_donasi_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `harga` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ekelas`
--

CREATE TABLE `ekelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `grade` int(11) NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ekstrakurikulers`
--

CREATE TABLE `ekstrakurikulers` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_ekstra` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penangung_jawab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mulai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_selesai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pertemuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ekstra_siswas`
--

CREATE TABLE `ekstra_siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `ekstra_id` int(10) UNSIGNED NOT NULL,
  `ekstra_siswa_session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ekstra_siswa_items`
--

CREATE TABLE `ekstra_siswa_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `ekstra_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `elearning_tugas`
--

CREATE TABLE `elearning_tugas` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `batas_waktu` date NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `jurusan_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emateris`
--

CREATE TABLE `emateris` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `jurusan_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `epengumumen`
--

CREATE TABLE `epengumumen` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('penting','active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `equizzes`
--

CREATE TABLE `equizzes` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `jurusan_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `batas_waktu` date NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gedungs`
--

CREATE TABLE `gedungs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grid_nilais`
--

CREATE TABLE `grid_nilais` (
  `id` int(10) UNSIGNED NOT NULL,
  `nilai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categori_nilai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_ujians`
--

CREATE TABLE `hasil_ujians` (
  `id` int(10) UNSIGNED NOT NULL,
  `daftar_siswa_id` int(10) UNSIGNED NOT NULL,
  `pengetahuan_umum` int(11) NOT NULL,
  `minat_bakat` int(11) DEFAULT NULL,
  `akademik` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hutangs`
--

CREATE TABLE `hutangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `pbn` decimal(10,2) DEFAULT NULL,
  `spp` decimal(10,2) DEFAULT NULL,
  `pro` decimal(10,2) DEFAULT NULL,
  `status` enum('tahun-sebelumnya') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_mata_pelajarans`
--

CREATE TABLE `jadwal_mata_pelajarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mulai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_selesai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `durasi` int(11) NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayarans`
--

CREATE TABLE `jenis_pembayarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jenjangs`
--

CREATE TABLE `jenjangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnalpros`
--

CREATE TABLE `jurnalpros` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kas_debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_edcs`
--

CREATE TABLE `jurnal_edcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `jenis_pembayaran_id` int(10) UNSIGNED NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_kas_mutasis`
--

CREATE TABLE `jurnal_kas_mutasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mutasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('valid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_p_b_ns`
--

CREATE TABLE `jurnal_p_b_ns` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kas_debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_s_p_ps`
--

CREATE TABLE `jurnal_s_p_ps` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kas_debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_uang_keluars`
--

CREATE TABLE `jurnal_uang_keluars` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nomor_kwitansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reimbursment_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit` decimal(10,2) NOT NULL,
  `status` enum('valid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE `jurusans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_inventoris`
--

CREATE TABLE `kategori_inventoris` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran_gurus`
--

CREATE TABLE `kehadiran_gurus` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `jam_masuk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_keluar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran_siswas`
--

CREATE TABLE `kehadiran_siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kendaraans`
--

CREATE TABLE `kendaraans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kurikulums`
--

CREATE TABLE `kurikulums` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('tidak aktif','aktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajarans`
--

CREATE TABLE `mata_pelajarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `kurikulum_id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran_gurus`
--

CREATE TABLE `mata_pelajaran_gurus` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajar_id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran_perkelas`
--

CREATE TABLE `mata_pelajaran_perkelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `mata_pelajar_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_07_090422_create_data_sekolahs_table', 1),
(4, '2019_10_07_090741_create_kurikulums_table', 1),
(5, '2019_10_07_091019_create_tahun_akademiks_table', 1),
(6, '2019_10_07_091256_create_ruang_kelas_table', 1),
(7, '2019_10_07_091512_create_jurusans_table', 1),
(8, '2019_10_07_104636_create_jenjangs_table', 1),
(9, '2019_10_09_065511_create_agamas_table', 1),
(10, '2019_10_09_103920_create_jabatans_table', 1),
(11, '2019_10_10_030534_create_bagians_table', 1),
(12, '2019_10_10_030551_create_status_karyawans_table', 1),
(13, '2019_10_10_030632_create_pendapatans_table', 1),
(14, '2019_10_10_040840_create_kendaraans_table', 1),
(15, '2019_10_10_044744_create_rombongan_belajars_table', 1),
(16, '2019_10_10_084052_create_data_gurus_table', 1),
(17, '2019_10_12_031108_create_wali_kelas_table', 1),
(18, '2019_10_16_112453_create_kelas_table', 1),
(19, '2019_10_16_1131458_create_siswas_table', 1),
(20, '2019_10_16_125338_create_mata_pelajarans_table', 1),
(21, '2019_10_25_002406_create_daftar_siwas_table', 1),
(22, '2019_10_25_003310_create_test_interviews_table', 1),
(23, '2019_10_25_235557_create_hasil_ujians_table', 1),
(24, '2019_10_26_050231_create_jadwal_mata_pelajarans_table', 1),
(25, '2019_10_26_134358_create_kehadiran_siswas_table', 1),
(26, '2019_10_26_141516_create_kehadiran_gurus_table', 1),
(27, '2019_10_27_022137_create_ekstrakurikulers_table', 1),
(28, '2019_10_27_025927_create_bimbingan_belajars_table', 1),
(29, '2019_10_27_051639_create_ruang_kelas_perkelas_table', 1),
(30, '2019_10_27_071630_create_jenis_pembayarans_table', 1),
(31, '2019_10_27_083925_create_pembayaran_spps_table', 1),
(32, '2019_10_28_010144_create_data_alumnis_table', 1),
(33, '2019_10_28_223846_create_mata_pelajaran_perkelas_table', 1),
(34, '2019_10_30_010729_create_mata_pelajaran_gurus_table', 1),
(35, '2019_10_31_022149_create_pembayaran_semesters_table', 1),
(36, '2019_10_31_140756_create_pembayaran_bukus_table', 1),
(37, '2019_11_01_013321_create_staff_pegawais_table', 1),
(38, '2019_11_03_081255_create_grid_nilais_table', 1),
(39, '2019_11_04_052312_create_siswa_perkelas_table', 1),
(40, '2019_11_08_002206_create_pembayarans_table', 1),
(41, '2019_11_09_021357_create_data_bukus_table', 1),
(42, '2019_11_09_034039_create_peminjaman_bukus_table', 1),
(43, '2019_11_09_043115_create_pengembalians_table', 1),
(44, '2019_11_09_062207_create_petugas_perpustakaans_table', 1),
(45, '2019_11_09_122153_create_kategori_inventoris_table', 1),
(46, '2019_11_10_105442_create_tipe_inventories_table', 1),
(47, '2019_11_11_072520_create_barangs_table', 1),
(48, '2019_11_13_125915_create_gedungs_table', 1),
(49, '2019_11_13_135729_create_tipe_donasis_table', 1),
(50, '2019_11_14_041121_create_assets_table', 1),
(51, '2019_11_15_031556_create_donasis_table', 1),
(52, '2019_11_15_072318_create_barang_masuks_table', 1),
(53, '2019_11_16_020834_create_barang_keluars_table', 1),
(54, '2019_11_16_021236_create_spk_assets_table', 1),
(55, '2019_11_18_015956_create_tipe_pajaks_table', 1),
(56, '2019_11_21_030918_create_pengeluaran_cek_lists_table', 1),
(57, '2019_11_21_033054_create_pengeluaran_uang_masuks_table', 1),
(58, '2019_11_21_081152_create_pengeluaran_permintaan_danas_table', 1),
(59, '2019_11_21_105140_create_pengelauaran_jurnal_kas_kecil_reimbursments_table', 1),
(60, '2019_11_21_235928_create_jurnal_edcs_table', 1),
(61, '2019_11_22_022639_create_jurnal_uang_keluars_table', 1),
(62, '2019_11_22_045505_create_jurnal_kas_mutasis_table', 1),
(63, '2019_11_22_092042_create_dana_pendings_table', 1),
(64, '2019_11_23_050452_create_rekening_siswas_table', 1),
(65, '2019_11_23_100437_create_jurnal_p_b_ns_table', 1),
(66, '2019_11_23_161849_create_jurnal_s_p_ps_table', 1),
(67, '2019_11_24_012539_create_jurnalpros_table', 1),
(68, '2019_11_24_023452_create_cut_off_siswas_table', 1),
(69, '2019_11_24_055143_create_hutangs_table', 1),
(70, '2019_11_25_052935_create_elearning_tugas_table', 1),
(71, '2019_11_25_232020_create_ekelas_table', 1),
(72, '2019_11_25_232700_create_emateris_table', 1),
(73, '2019_11_25_233741_create_equizzes_table', 1),
(74, '2019_11_25_234247_create_epengumumen_table', 1),
(75, '2019_11_30_060752_create_nilai_permapels_table', 1),
(76, '2019_11_30_093944_create_nilaiperkelas_table', 1),
(77, '2019_11_30_112753_create_coas_table', 1),
(78, '2019_12_08_024220_create_pengumuman_events_table', 1),
(79, '2019_12_24_020853_create_raports_table', 1),
(80, '2019_12_25_030440_create_raport_ekstrakurikulers_table', 1),
(81, '2019_12_25_032003_create_raport_mapels_table', 1),
(82, '2019_12_26_050501_create_permission_tables', 1),
(83, '2020_01_06_062405_create_raport_ekstra_items_table', 1),
(84, '2020_01_12_002745_create_ekstra_siswas_table', 1),
(85, '2020_01_12_031829_create_ekstra_siswa_items_table', 1),
(86, '2020_01_12_200314_create_type_personal_packages_table', 1),
(87, '2020_01_14_230533_create_pengumuman_kelas_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nilaiperkelas`
--

CREATE TABLE `nilaiperkelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `nilai` int(11) NOT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nilai_permapels`
--

CREATE TABLE `nilai_permapels` (
  `id` int(10) UNSIGNED NOT NULL,
  `mata_pelajaran_id` int(10) UNSIGNED NOT NULL,
  `nilai_id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `nilai` int(11) NOT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayarans`
--

CREATE TABLE `pembayarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `pembayaranspp_id` int(10) UNSIGNED NOT NULL,
  `pembayaransemester_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('lunas','belum-lunas') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_bukus`
--

CREATE TABLE `pembayaran_bukus` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_semesters`
--

CREATE TABLE `pembayaran_semesters` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_spps`
--

CREATE TABLE `pembayaran_spps` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_pembayaran_id` int(10) UNSIGNED NOT NULL,
  `biaya_spp` decimal(10,2) NOT NULL,
  `pembayaran_extra` decimal(10,2) DEFAULT NULL,
  `pembayaran_bimbingan_belajar` decimal(10,2) DEFAULT NULL,
  `tanggal_bayar` date NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman_bukus`
--

CREATE TABLE `peminjaman_bukus` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_peminjam` date NOT NULL,
  `tanggal_kembalian` date NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `buku_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pendapatans`
--

CREATE TABLE `pendapatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `harga` decimal(12,0) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengelauaran_jurnal_kas_kecil_reimbursments`
--

CREATE TABLE `pengelauaran_jurnal_kas_kecil_reimbursments` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `kode_reibusment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_cek_lists`
--

CREATE TABLE `pengeluaran_cek_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `list` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ada','habis') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_permintaan_danas`
--

CREATE TABLE `pengeluaran_permintaan_danas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_permintaan_dana` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `kode_kwintansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` decimal(10,2) NOT NULL,
  `status` enum('ada','habis') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_uang_masuks`
--

CREATE TABLE `pengeluaran_uang_masuks` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `jenis_pembayaran_id` int(10) UNSIGNED NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_akun_debit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_akun_kredit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengembalians`
--

CREATE TABLE `pengembalians` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinjamanBuku_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('sudah-dikembalikan','belum-dikembalikan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman_events`
--

CREATE TABLE `pengumuman_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman_kelas`
--

CREATE TABLE `pengumuman_kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `petugas_perpustakaans`
--

CREATE TABLE `petugas_perpustakaans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raports`
--

CREATE TABLE `raports` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `matkul_id` int(10) UNSIGNED NOT NULL,
  `nilai_pengetahuan` int(11) DEFAULT NULL,
  `nilai_keterampilan` int(11) DEFAULT NULL,
  `nilai_akhir` int(11) DEFAULT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raport_ekstrakurikulers`
--

CREATE TABLE `raport_ekstrakurikulers` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `nama_ekstrakurikuler` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_raport_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raport_ekstra_items`
--

CREATE TABLE `raport_ekstra_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `nama_ekstrakurikuler` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raport_mapels`
--

CREATE TABLE `raport_mapels` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `matkul_id` int(10) UNSIGNED NOT NULL,
  `nilai_pengetahuan` int(11) DEFAULT NULL,
  `nilai_keterampilan` int(11) DEFAULT NULL,
  `nilai_akhir` int(11) DEFAULT NULL,
  `predikat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rekening_siswas`
--

CREATE TABLE `rekening_siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_keuangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pbn_tahunan` decimal(10,2) DEFAULT NULL,
  `spp` decimal(10,2) DEFAULT NULL,
  `biaya_program` decimal(10,2) DEFAULT NULL,
  `status` enum('sudah-bayar','belum-bayar') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rombongan_belajars`
--

CREATE TABLE `rombongan_belajars` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan_id` int(10) UNSIGNED NOT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ruang_kelas`
--

CREATE TABLE `ruang_kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_ruangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_ruangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ruang_kelas_perkelas`
--

CREATE TABLE `ruang_kelas_perkelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE `siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nisn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_panggilan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_formulir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama_id` int(10) UNSIGNED NOT NULL,
  `jenjang_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `tahun_akademik_id` int(10) UNSIGNED NOT NULL,
  `wali_kelas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `umur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anak_ke` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suku` enum('papua','non-papua','port-numbay') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_ayah_kandung` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lulusan_sekolah_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jarak_rumah` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penghasilan_ortu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_ibu_kandung` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan_ibu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `akte_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kk_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pernyataan_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_siswa` enum('masuk','grading') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_perkelas`
--

CREATE TABLE `siswa_perkelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `spk_assets`
--

CREATE TABLE `spk_assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipe_asset` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gedung_id` int(10) UNSIGNED NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_pegawais`
--

CREATE TABLE `staff_pegawais` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama_id` int(10) UNSIGNED NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_karyawans`
--

CREATE TABLE `status_karyawans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_akademiks`
--

CREATE TABLE `tahun_akademiks` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('tidak aktif','aktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test_interviews`
--

CREATE TABLE `test_interviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `daftar_siswa_id` int(10) UNSIGNED NOT NULL,
  `nilai_terbaik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prestasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hoby` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktivitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jarak_rumah` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendapatan_orang_tua` decimal(10,2) NOT NULL,
  `pekerjaan_orang_tua` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_donasis`
--

CREATE TABLE `tipe_donasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_inventories`
--

CREATE TABLE `tipe_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_pajaks`
--

CREATE TABLE `tipe_pajaks` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `type_personal_packages`
--

CREATE TABLE `type_personal_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('siswa','guru','staff','perpus','inventory','accounting','ortu','superadmin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'siswa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wali_kelas`
--

CREATE TABLE `wali_kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kurikulum_id` int(10) UNSIGNED NOT NULL,
  `data_guru_id` int(10) UNSIGNED NOT NULL,
  `kode_mata_pelajaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mata_pelajaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agamas`
--
ALTER TABLE `agamas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bagians`
--
ALTER TABLE `bagians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_keluars`
--
ALTER TABLE `barang_keluars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_keluars_barang_id_foreign` (`barang_id`);

--
-- Indexes for table `barang_masuks`
--
ALTER TABLE `barang_masuks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuks_barang_id_foreign` (`barang_id`);

--
-- Indexes for table `bimbingan_belajars`
--
ALTER TABLE `bimbingan_belajars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bimbingan_belajars_data_guru_id_foreign` (`data_guru_id`);

--
-- Indexes for table `coas`
--
ALTER TABLE `coas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cut_off_siswas`
--
ALTER TABLE `cut_off_siswas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daftar_siwas`
--
ALTER TABLE `daftar_siwas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daftar_siwas_jenjang_id_foreign` (`jenjang_id`);

--
-- Indexes for table `dana_pendings`
--
ALTER TABLE `dana_pendings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_alumnis`
--
ALTER TABLE `data_alumnis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_bukus`
--
ALTER TABLE `data_bukus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_gurus`
--
ALTER TABLE `data_gurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sekolahs`
--
ALTER TABLE `data_sekolahs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasis`
--
ALTER TABLE `donasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donasis_tipe_donasi_id_foreign` (`tipe_donasi_id`);

--
-- Indexes for table `ekelas`
--
ALTER TABLE `ekelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ekelas_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `ekelas_kelas_id_foreign` (`kelas_id`);

--
-- Indexes for table `ekstrakurikulers`
--
ALTER TABLE `ekstrakurikulers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekstra_siswas`
--
ALTER TABLE `ekstra_siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ekstra_siswas_siswa_id_foreign` (`siswa_id`),
  ADD KEY `ekstra_siswas_ekstra_id_foreign` (`ekstra_id`),
  ADD KEY `ekstra_siswas_user_id_foreign` (`user_id`);

--
-- Indexes for table `ekstra_siswa_items`
--
ALTER TABLE `ekstra_siswa_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ekstra_siswa_items_siswa_id_foreign` (`siswa_id`),
  ADD KEY `ekstra_siswa_items_ekstra_id_foreign` (`ekstra_id`),
  ADD KEY `ekstra_siswa_items_user_id_foreign` (`user_id`);

--
-- Indexes for table `elearning_tugas`
--
ALTER TABLE `elearning_tugas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elearning_tugas_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `elearning_tugas_kelas_id_foreign` (`kelas_id`),
  ADD KEY `elearning_tugas_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `elearning_tugas_jurusan_id_foreign` (`jurusan_id`);

--
-- Indexes for table `emateris`
--
ALTER TABLE `emateris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emateris_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `emateris_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `emateris_jurusan_id_foreign` (`jurusan_id`),
  ADD KEY `emateris_kelas_id_foreign` (`kelas_id`);

--
-- Indexes for table `epengumumen`
--
ALTER TABLE `epengumumen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `epengumumen_data_guru_id_foreign` (`data_guru_id`);

--
-- Indexes for table `equizzes`
--
ALTER TABLE `equizzes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equizzes_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `equizzes_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `equizzes_jurusan_id_foreign` (`jurusan_id`),
  ADD KEY `equizzes_kelas_id_foreign` (`kelas_id`);

--
-- Indexes for table `gedungs`
--
ALTER TABLE `gedungs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grid_nilais`
--
ALTER TABLE `grid_nilais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_ujians`
--
ALTER TABLE `hasil_ujians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hasil_ujians_daftar_siswa_id_foreign` (`daftar_siswa_id`);

--
-- Indexes for table `hutangs`
--
ALTER TABLE `hutangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_mata_pelajarans`
--
ALTER TABLE `jadwal_mata_pelajarans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jadwal_mata_pelajarans_kelas_id_foreign` (`kelas_id`),
  ADD KEY `jadwal_mata_pelajarans_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `jadwal_mata_pelajarans_siswa_id_foreign` (`siswa_id`),
  ADD KEY `jadwal_mata_pelajarans_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `jadwal_mata_pelajarans_user_id_foreign` (`user_id`);

--
-- Indexes for table `jenis_pembayarans`
--
ALTER TABLE `jenis_pembayarans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenjangs`
--
ALTER TABLE `jenjangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnalpros`
--
ALTER TABLE `jurnalpros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_edcs`
--
ALTER TABLE `jurnal_edcs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurnal_edcs_siswa_id_foreign` (`siswa_id`),
  ADD KEY `jurnal_edcs_jenis_pembayaran_id_foreign` (`jenis_pembayaran_id`);

--
-- Indexes for table `jurnal_kas_mutasis`
--
ALTER TABLE `jurnal_kas_mutasis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_p_b_ns`
--
ALTER TABLE `jurnal_p_b_ns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_s_p_ps`
--
ALTER TABLE `jurnal_s_p_ps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_uang_keluars`
--
ALTER TABLE `jurnal_uang_keluars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurnal_uang_keluars_reimbursment_id_foreign` (`reimbursment_id`);

--
-- Indexes for table `jurusans`
--
ALTER TABLE `jurusans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_inventoris`
--
ALTER TABLE `kategori_inventoris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kehadiran_gurus`
--
ALTER TABLE `kehadiran_gurus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kehadiran_gurus_data_guru_id_foreign` (`data_guru_id`);

--
-- Indexes for table `kehadiran_siswas`
--
ALTER TABLE `kehadiran_siswas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraans`
--
ALTER TABLE `kendaraans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurikulums`
--
ALTER TABLE `kurikulums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mata_pelajarans`
--
ALTER TABLE `mata_pelajarans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mata_pelajarans_kurikulum_id_foreign` (`kurikulum_id`),
  ADD KEY `mata_pelajarans_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `mata_pelajarans_jenjang_id_foreign` (`jenjang_id`);

--
-- Indexes for table `mata_pelajaran_gurus`
--
ALTER TABLE `mata_pelajaran_gurus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mata_pelajaran_gurus_kelas_id_foreign` (`kelas_id`),
  ADD KEY `mata_pelajaran_gurus_mata_pelajar_id_foreign` (`mata_pelajar_id`),
  ADD KEY `mata_pelajaran_gurus_data_guru_id_foreign` (`data_guru_id`);

--
-- Indexes for table `mata_pelajaran_perkelas`
--
ALTER TABLE `mata_pelajaran_perkelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mata_pelajaran_perkelas_kelas_id_foreign` (`kelas_id`),
  ADD KEY `mata_pelajaran_perkelas_mata_pelajar_id_foreign` (`mata_pelajar_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `nilaiperkelas`
--
ALTER TABLE `nilaiperkelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nilaiperkelas_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `nilaiperkelas_siswa_id_foreign` (`siswa_id`),
  ADD KEY `nilaiperkelas_kelas_id_foreign` (`kelas_id`);

--
-- Indexes for table `nilai_permapels`
--
ALTER TABLE `nilai_permapels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nilai_permapels_mata_pelajaran_id_foreign` (`mata_pelajaran_id`),
  ADD KEY `nilai_permapels_nilai_id_foreign` (`nilai_id`),
  ADD KEY `nilai_permapels_data_guru_id_foreign` (`data_guru_id`),
  ADD KEY `nilai_permapels_siswa_id_foreign` (`siswa_id`),
  ADD KEY `nilai_permapels_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pembayarans`
--
ALTER TABLE `pembayarans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembayarans_siswa_id_foreign` (`siswa_id`),
  ADD KEY `pembayarans_pembayaranspp_id_foreign` (`pembayaranspp_id`),
  ADD KEY `pembayarans_pembayaransemester_id_foreign` (`pembayaransemester_id`),
  ADD KEY `pembayarans_user_id_foreign` (`user_id`);

--
-- Indexes for table `pembayaran_bukus`
--
ALTER TABLE `pembayaran_bukus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_semesters`
--
ALTER TABLE `pembayaran_semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_spps`
--
ALTER TABLE `pembayaran_spps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembayaran_spps_jenis_pembayaran_id_foreign` (`jenis_pembayaran_id`);

--
-- Indexes for table `peminjaman_bukus`
--
ALTER TABLE `peminjaman_bukus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `peminjaman_bukus_buku_id_foreign` (`buku_id`);

--
-- Indexes for table `pendapatans`
--
ALTER TABLE `pendapatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengelauaran_jurnal_kas_kecil_reimbursments`
--
ALTER TABLE `pengelauaran_jurnal_kas_kecil_reimbursments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_cek_lists`
--
ALTER TABLE `pengeluaran_cek_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_permintaan_danas`
--
ALTER TABLE `pengeluaran_permintaan_danas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengeluaran_uang_masuks`
--
ALTER TABLE `pengeluaran_uang_masuks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengeluaran_uang_masuks_siswa_id_foreign` (`siswa_id`),
  ADD KEY `pengeluaran_uang_masuks_jenis_pembayaran_id_foreign` (`jenis_pembayaran_id`);

--
-- Indexes for table `pengembalians`
--
ALTER TABLE `pengembalians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengembalians_pinjamanbuku_id_foreign` (`pinjamanBuku_id`);

--
-- Indexes for table `pengumuman_events`
--
ALTER TABLE `pengumuman_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman_kelas`
--
ALTER TABLE `pengumuman_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengumuman_kelas_siswa_id_foreign` (`siswa_id`),
  ADD KEY `pengumuman_kelas_user_id_foreign` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas_perpustakaans`
--
ALTER TABLE `petugas_perpustakaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raports`
--
ALTER TABLE `raports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raports_siswa_id_foreign` (`siswa_id`),
  ADD KEY `raports_matkul_id_foreign` (`matkul_id`);

--
-- Indexes for table `raport_ekstrakurikulers`
--
ALTER TABLE `raport_ekstrakurikulers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raport_ekstrakurikulers_siswa_id_foreign` (`siswa_id`);

--
-- Indexes for table `raport_ekstra_items`
--
ALTER TABLE `raport_ekstra_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raport_ekstra_items_siswa_id_foreign` (`siswa_id`);

--
-- Indexes for table `raport_mapels`
--
ALTER TABLE `raport_mapels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raport_mapels_siswa_id_foreign` (`siswa_id`),
  ADD KEY `raport_mapels_matkul_id_foreign` (`matkul_id`),
  ADD KEY `raport_mapels_user_id_foreign` (`user_id`);

--
-- Indexes for table `rekening_siswas`
--
ALTER TABLE `rekening_siswas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `rombongan_belajars`
--
ALTER TABLE `rombongan_belajars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rombongan_belajars_jurusan_id_foreign` (`jurusan_id`);

--
-- Indexes for table `ruang_kelas`
--
ALTER TABLE `ruang_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ruang_kelas_perkelas`
--
ALTER TABLE `ruang_kelas_perkelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ruang_kelas_perkelas_kelas_id_foreign` (`kelas_id`);

--
-- Indexes for table `siswas`
--
ALTER TABLE `siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswas_agama_id_foreign` (`agama_id`),
  ADD KEY `siswas_jenjang_id_foreign` (`jenjang_id`),
  ADD KEY `siswas_kelas_id_foreign` (`kelas_id`),
  ADD KEY `siswas_tahun_akademik_id_foreign` (`tahun_akademik_id`),
  ADD KEY `siswas_user_id_foreign` (`user_id`);

--
-- Indexes for table `siswa_perkelas`
--
ALTER TABLE `siswa_perkelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa_perkelas_kelas_id_foreign` (`kelas_id`),
  ADD KEY `siswa_perkelas_siswa_id_foreign` (`siswa_id`);

--
-- Indexes for table `spk_assets`
--
ALTER TABLE `spk_assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `spk_assets_gedung_id_foreign` (`gedung_id`);

--
-- Indexes for table `staff_pegawais`
--
ALTER TABLE `staff_pegawais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_pegawais_jabatan_id_foreign` (`jabatan_id`),
  ADD KEY `staff_pegawais_agama_id_foreign` (`agama_id`);

--
-- Indexes for table `status_karyawans`
--
ALTER TABLE `status_karyawans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahun_akademiks`
--
ALTER TABLE `tahun_akademiks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_interviews`
--
ALTER TABLE `test_interviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_interviews_daftar_siswa_id_foreign` (`daftar_siswa_id`);

--
-- Indexes for table `tipe_donasis`
--
ALTER TABLE `tipe_donasis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_inventories`
--
ALTER TABLE `tipe_inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_pajaks`
--
ALTER TABLE `tipe_pajaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_personal_packages`
--
ALTER TABLE `type_personal_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wali_kelas_kurikulum_id_foreign` (`kurikulum_id`),
  ADD KEY `wali_kelas_data_guru_id_foreign` (`data_guru_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agamas`
--
ALTER TABLE `agamas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bagians`
--
ALTER TABLE `bagians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barang_keluars`
--
ALTER TABLE `barang_keluars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barang_masuks`
--
ALTER TABLE `barang_masuks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bimbingan_belajars`
--
ALTER TABLE `bimbingan_belajars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coas`
--
ALTER TABLE `coas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cut_off_siswas`
--
ALTER TABLE `cut_off_siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftar_siwas`
--
ALTER TABLE `daftar_siwas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dana_pendings`
--
ALTER TABLE `dana_pendings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_alumnis`
--
ALTER TABLE `data_alumnis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_bukus`
--
ALTER TABLE `data_bukus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_gurus`
--
ALTER TABLE `data_gurus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_sekolahs`
--
ALTER TABLE `data_sekolahs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donasis`
--
ALTER TABLE `donasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekelas`
--
ALTER TABLE `ekelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekstrakurikulers`
--
ALTER TABLE `ekstrakurikulers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekstra_siswas`
--
ALTER TABLE `ekstra_siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekstra_siswa_items`
--
ALTER TABLE `ekstra_siswa_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `elearning_tugas`
--
ALTER TABLE `elearning_tugas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emateris`
--
ALTER TABLE `emateris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `epengumumen`
--
ALTER TABLE `epengumumen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equizzes`
--
ALTER TABLE `equizzes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gedungs`
--
ALTER TABLE `gedungs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grid_nilais`
--
ALTER TABLE `grid_nilais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hasil_ujians`
--
ALTER TABLE `hasil_ujians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hutangs`
--
ALTER TABLE `hutangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_mata_pelajarans`
--
ALTER TABLE `jadwal_mata_pelajarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis_pembayarans`
--
ALTER TABLE `jenis_pembayarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenjangs`
--
ALTER TABLE `jenjangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnalpros`
--
ALTER TABLE `jurnalpros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_edcs`
--
ALTER TABLE `jurnal_edcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_kas_mutasis`
--
ALTER TABLE `jurnal_kas_mutasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_p_b_ns`
--
ALTER TABLE `jurnal_p_b_ns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_s_p_ps`
--
ALTER TABLE `jurnal_s_p_ps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_uang_keluars`
--
ALTER TABLE `jurnal_uang_keluars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusans`
--
ALTER TABLE `jurusans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_inventoris`
--
ALTER TABLE `kategori_inventoris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kehadiran_gurus`
--
ALTER TABLE `kehadiran_gurus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kehadiran_siswas`
--
ALTER TABLE `kehadiran_siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kendaraans`
--
ALTER TABLE `kendaraans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kurikulums`
--
ALTER TABLE `kurikulums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mata_pelajarans`
--
ALTER TABLE `mata_pelajarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mata_pelajaran_gurus`
--
ALTER TABLE `mata_pelajaran_gurus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mata_pelajaran_perkelas`
--
ALTER TABLE `mata_pelajaran_perkelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `nilaiperkelas`
--
ALTER TABLE `nilaiperkelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nilai_permapels`
--
ALTER TABLE `nilai_permapels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayarans`
--
ALTER TABLE `pembayarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_bukus`
--
ALTER TABLE `pembayaran_bukus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_semesters`
--
ALTER TABLE `pembayaran_semesters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_spps`
--
ALTER TABLE `pembayaran_spps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peminjaman_bukus`
--
ALTER TABLE `peminjaman_bukus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pendapatans`
--
ALTER TABLE `pendapatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengelauaran_jurnal_kas_kecil_reimbursments`
--
ALTER TABLE `pengelauaran_jurnal_kas_kecil_reimbursments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengeluaran_cek_lists`
--
ALTER TABLE `pengeluaran_cek_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengeluaran_permintaan_danas`
--
ALTER TABLE `pengeluaran_permintaan_danas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengeluaran_uang_masuks`
--
ALTER TABLE `pengeluaran_uang_masuks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengembalians`
--
ALTER TABLE `pengembalians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengumuman_events`
--
ALTER TABLE `pengumuman_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengumuman_kelas`
--
ALTER TABLE `pengumuman_kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petugas_perpustakaans`
--
ALTER TABLE `petugas_perpustakaans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raports`
--
ALTER TABLE `raports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raport_ekstrakurikulers`
--
ALTER TABLE `raport_ekstrakurikulers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raport_ekstra_items`
--
ALTER TABLE `raport_ekstra_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raport_mapels`
--
ALTER TABLE `raport_mapels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rekening_siswas`
--
ALTER TABLE `rekening_siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rombongan_belajars`
--
ALTER TABLE `rombongan_belajars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ruang_kelas`
--
ALTER TABLE `ruang_kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ruang_kelas_perkelas`
--
ALTER TABLE `ruang_kelas_perkelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswas`
--
ALTER TABLE `siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_perkelas`
--
ALTER TABLE `siswa_perkelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `spk_assets`
--
ALTER TABLE `spk_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_pegawais`
--
ALTER TABLE `staff_pegawais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_karyawans`
--
ALTER TABLE `status_karyawans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tahun_akademiks`
--
ALTER TABLE `tahun_akademiks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test_interviews`
--
ALTER TABLE `test_interviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipe_donasis`
--
ALTER TABLE `tipe_donasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipe_inventories`
--
ALTER TABLE `tipe_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipe_pajaks`
--
ALTER TABLE `tipe_pajaks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_personal_packages`
--
ALTER TABLE `type_personal_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_keluars`
--
ALTER TABLE `barang_keluars`
  ADD CONSTRAINT `barang_keluars_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `barang_masuks`
--
ALTER TABLE `barang_masuks`
  ADD CONSTRAINT `barang_masuks_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bimbingan_belajars`
--
ALTER TABLE `bimbingan_belajars`
  ADD CONSTRAINT `bimbingan_belajars_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `daftar_siwas`
--
ALTER TABLE `daftar_siwas`
  ADD CONSTRAINT `daftar_siwas_jenjang_id_foreign` FOREIGN KEY (`jenjang_id`) REFERENCES `jenjangs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `donasis`
--
ALTER TABLE `donasis`
  ADD CONSTRAINT `donasis_tipe_donasi_id_foreign` FOREIGN KEY (`tipe_donasi_id`) REFERENCES `tipe_donasis` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ekelas`
--
ALTER TABLE `ekelas`
  ADD CONSTRAINT `ekelas_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ekelas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ekstra_siswas`
--
ALTER TABLE `ekstra_siswas`
  ADD CONSTRAINT `ekstra_siswas_ekstra_id_foreign` FOREIGN KEY (`ekstra_id`) REFERENCES `ekstrakurikulers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ekstra_siswas_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ekstra_siswas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ekstra_siswa_items`
--
ALTER TABLE `ekstra_siswa_items`
  ADD CONSTRAINT `ekstra_siswa_items_ekstra_id_foreign` FOREIGN KEY (`ekstra_id`) REFERENCES `ekstrakurikulers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ekstra_siswa_items_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ekstra_siswa_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `elearning_tugas`
--
ALTER TABLE `elearning_tugas`
  ADD CONSTRAINT `elearning_tugas_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elearning_tugas_jurusan_id_foreign` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elearning_tugas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elearning_tugas_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `emateris`
--
ALTER TABLE `emateris`
  ADD CONSTRAINT `emateris_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `emateris_jurusan_id_foreign` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `emateris_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `emateris_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `epengumumen`
--
ALTER TABLE `epengumumen`
  ADD CONSTRAINT `epengumumen_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `equizzes`
--
ALTER TABLE `equizzes`
  ADD CONSTRAINT `equizzes_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `equizzes_jurusan_id_foreign` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `equizzes_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `equizzes_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hasil_ujians`
--
ALTER TABLE `hasil_ujians`
  ADD CONSTRAINT `hasil_ujians_daftar_siswa_id_foreign` FOREIGN KEY (`daftar_siswa_id`) REFERENCES `daftar_siwas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jadwal_mata_pelajarans`
--
ALTER TABLE `jadwal_mata_pelajarans`
  ADD CONSTRAINT `jadwal_mata_pelajarans_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_mata_pelajarans_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_mata_pelajarans_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_mata_pelajarans_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_mata_pelajarans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `jurnal_edcs`
--
ALTER TABLE `jurnal_edcs`
  ADD CONSTRAINT `jurnal_edcs_jenis_pembayaran_id_foreign` FOREIGN KEY (`jenis_pembayaran_id`) REFERENCES `jenis_pembayarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jurnal_edcs_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jurnal_uang_keluars`
--
ALTER TABLE `jurnal_uang_keluars`
  ADD CONSTRAINT `jurnal_uang_keluars_reimbursment_id_foreign` FOREIGN KEY (`reimbursment_id`) REFERENCES `pengelauaran_jurnal_kas_kecil_reimbursments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kehadiran_gurus`
--
ALTER TABLE `kehadiran_gurus`
  ADD CONSTRAINT `kehadiran_gurus_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mata_pelajarans`
--
ALTER TABLE `mata_pelajarans`
  ADD CONSTRAINT `mata_pelajarans_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mata_pelajarans_jenjang_id_foreign` FOREIGN KEY (`jenjang_id`) REFERENCES `jenjangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mata_pelajarans_kurikulum_id_foreign` FOREIGN KEY (`kurikulum_id`) REFERENCES `kurikulums` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mata_pelajaran_gurus`
--
ALTER TABLE `mata_pelajaran_gurus`
  ADD CONSTRAINT `mata_pelajaran_gurus_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mata_pelajaran_gurus_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mata_pelajaran_gurus_mata_pelajar_id_foreign` FOREIGN KEY (`mata_pelajar_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mata_pelajaran_perkelas`
--
ALTER TABLE `mata_pelajaran_perkelas`
  ADD CONSTRAINT `mata_pelajaran_perkelas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mata_pelajaran_perkelas_mata_pelajar_id_foreign` FOREIGN KEY (`mata_pelajar_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `nilaiperkelas`
--
ALTER TABLE `nilaiperkelas`
  ADD CONSTRAINT `nilaiperkelas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilaiperkelas_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilaiperkelas_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `nilai_permapels`
--
ALTER TABLE `nilai_permapels`
  ADD CONSTRAINT `nilai_permapels_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilai_permapels_mata_pelajaran_id_foreign` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilai_permapels_nilai_id_foreign` FOREIGN KEY (`nilai_id`) REFERENCES `grid_nilais` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilai_permapels_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nilai_permapels_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pembayarans`
--
ALTER TABLE `pembayarans`
  ADD CONSTRAINT `pembayarans_pembayaransemester_id_foreign` FOREIGN KEY (`pembayaransemester_id`) REFERENCES `pembayaran_semesters` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pembayarans_pembayaranspp_id_foreign` FOREIGN KEY (`pembayaranspp_id`) REFERENCES `pembayaran_spps` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pembayarans_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pembayarans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pembayaran_spps`
--
ALTER TABLE `pembayaran_spps`
  ADD CONSTRAINT `pembayaran_spps_jenis_pembayaran_id_foreign` FOREIGN KEY (`jenis_pembayaran_id`) REFERENCES `jenis_pembayarans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `peminjaman_bukus`
--
ALTER TABLE `peminjaman_bukus`
  ADD CONSTRAINT `peminjaman_bukus_buku_id_foreign` FOREIGN KEY (`buku_id`) REFERENCES `data_bukus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pengeluaran_uang_masuks`
--
ALTER TABLE `pengeluaran_uang_masuks`
  ADD CONSTRAINT `pengeluaran_uang_masuks_jenis_pembayaran_id_foreign` FOREIGN KEY (`jenis_pembayaran_id`) REFERENCES `jenis_pembayarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pengeluaran_uang_masuks_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pengembalians`
--
ALTER TABLE `pengembalians`
  ADD CONSTRAINT `pengembalians_pinjamanbuku_id_foreign` FOREIGN KEY (`pinjamanBuku_id`) REFERENCES `peminjaman_bukus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pengumuman_kelas`
--
ALTER TABLE `pengumuman_kelas`
  ADD CONSTRAINT `pengumuman_kelas_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pengumuman_kelas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `raports`
--
ALTER TABLE `raports`
  ADD CONSTRAINT `raports_matkul_id_foreign` FOREIGN KEY (`matkul_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `raports_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `raport_ekstrakurikulers`
--
ALTER TABLE `raport_ekstrakurikulers`
  ADD CONSTRAINT `raport_ekstrakurikulers_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `raport_ekstra_items`
--
ALTER TABLE `raport_ekstra_items`
  ADD CONSTRAINT `raport_ekstra_items_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `raport_mapels`
--
ALTER TABLE `raport_mapels`
  ADD CONSTRAINT `raport_mapels_matkul_id_foreign` FOREIGN KEY (`matkul_id`) REFERENCES `mata_pelajarans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `raport_mapels_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `raport_mapels_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rombongan_belajars`
--
ALTER TABLE `rombongan_belajars`
  ADD CONSTRAINT `rombongan_belajars_jurusan_id_foreign` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ruang_kelas_perkelas`
--
ALTER TABLE `ruang_kelas_perkelas`
  ADD CONSTRAINT `ruang_kelas_perkelas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `siswas`
--
ALTER TABLE `siswas`
  ADD CONSTRAINT `siswas_agama_id_foreign` FOREIGN KEY (`agama_id`) REFERENCES `agamas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `siswas_jenjang_id_foreign` FOREIGN KEY (`jenjang_id`) REFERENCES `jenjangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `siswas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `siswas_tahun_akademik_id_foreign` FOREIGN KEY (`tahun_akademik_id`) REFERENCES `tahun_akademiks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `siswas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `siswa_perkelas`
--
ALTER TABLE `siswa_perkelas`
  ADD CONSTRAINT `siswa_perkelas_kelas_id_foreign` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `siswa_perkelas_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `spk_assets`
--
ALTER TABLE `spk_assets`
  ADD CONSTRAINT `spk_assets_gedung_id_foreign` FOREIGN KEY (`gedung_id`) REFERENCES `gedungs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_pegawais`
--
ALTER TABLE `staff_pegawais`
  ADD CONSTRAINT `staff_pegawais_agama_id_foreign` FOREIGN KEY (`agama_id`) REFERENCES `agamas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_pegawais_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `test_interviews`
--
ALTER TABLE `test_interviews`
  ADD CONSTRAINT `test_interviews_daftar_siswa_id_foreign` FOREIGN KEY (`daftar_siswa_id`) REFERENCES `daftar_siwas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  ADD CONSTRAINT `wali_kelas_data_guru_id_foreign` FOREIGN KEY (`data_guru_id`) REFERENCES `data_gurus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wali_kelas_kurikulum_id_foreign` FOREIGN KEY (`kurikulum_id`) REFERENCES `kurikulums` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
