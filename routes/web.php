<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
//filter search akademik
Route::get('akademik/siswa/search',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterSiswaController@search',
    'as'        => 'siswa.search'
]);

Route::get('akademik/mata-pelajaran/search',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterMatapelajaranController@search',
    'as'        => 'mata-pelajaran.search'
]);

Route::get('bimbingan-belajar/search',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterBimbinganBelajarController@searchBimbingan',
    'as'        => 'bimbingan-belajar.search-filter'
]);

Route::get('bimbingan-belajar/search',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterBimbinganBelajarController@searchBimbingan',
    'as'        => 'bimbingan-belajar.search-filter'
]);


//export & import

Route::get('siswa/export', [
    'uses'      => '\App\Http\Controllers\Eximp\Akademik\EximpSiswaController@siswaExport',
    'as'        => 'data-siswa.export'
]);

Route::get('siswa/import/create', [
    'uses'      => '\App\Http\Controllers\Eximp\Akademik\EximpSiswaController@formImportSiswa',
    'as'        => 'siswa.import-create'
]);

Route::post('data-siswa/import', [
    'uses'      => '\App\Http\Controllers\Eximp\Akademik\EximpSiswaController@siswaImport',
    'as'        => 'data-siswa.import'
]);
//cetak pdf  akademik
Route::get('data/pdf-siswa', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfSiswaController@cetak_siswa_pdf',
    'as'        => 'pdf-siswa'
]);

Route::get('data/akademik/pdf-bimbel', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfBimbinganBelajarController@cetak_bimbingan_belajar_pdf',
    'as'        => 'akademik.pdf-bimbel'
]);

Route::get('data/akademik/pdf-bimbel', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfBimbinganBelajarController@cetak_bimbingan_belajar_pdf',
    'as'        => 'akademik.pdf-bimbel'
]);

Route::get('data/akademik/pdf-alumni', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfDataAlumniController@cetak_alumni',
    'as'        => 'data-alumni_pdf'
]);

Route::get('data/akademik/pdf-mapel', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfMataPelajaranController@cetak_mapel',
    'as'        => 'pdf.mapel'
]);

Route::get('data/akademik/pdf-ortu',[
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfDataOrangTuaController@cetak_data_orang_tua_siswa',
    'as'        => 'pdf.data-ortu'
]);

Route::get('data/akademik/pdf-kelas', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfKelasController@cetak_kelas',
    'as'        => 'pdf.data-kelas'
]);

Route::get('data/akademik/pdf-jadwal-matkul',[
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\JadwalMatkulController@cetak_jadwal_matkul',
    'as'        => 'pdf.data-jadwal-matkul'
]);

Route::get('data/akademik/pdf-ekskul', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfEkskulController@cetak_ekskul',
    'as'        => 'pdf.data-ekskul'
]);

Route::get('data/akademik/pdf-ruang-kelas-perlas', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfRuangKelasPerkelasController@cetak_ruang_kelas_perkelas',
    'as'        => 'pdf.ruang-kelas-perkelas'
]);

Route::get('data/akademik/pdf-matkul-perkelas', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\MataPelajaranPerkelasController@matkul_perkelas',
    'as'        => 'pdf.matkul-perkelas'
]);

Route::get('data/akademik/pdf-matkul-guru', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfMatkulGuruController@cetak_matkul_guru',
    'as'        => 'pdf.matkul-guru'
]);

Route::get('data/akademik/pdf-siswa-perkelas', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfSiswaPerkelasController@cetak_siswa_perkelas',
    'as'        => 'pdf.siswa-perkelas'
]);

Route::get('data/akademik/pdf-nilai-perkelas', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfDataNilaiPerkelasController@cetak_nilai_perkelas',
    'as'        => 'pdf.nilai-perkelas'
]);

Route::get('data/akademik/pdf-nilai-permaper', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfNilaiPermapelController@cetak_nilai_permapel',
    'as'        => 'pdf.nilai-permapel'
]);

Route::get('data/akademik/pdf-grid-nilai', [
    'uses'      => '\App\Http\Controllers\Pdf\Akademik\PdfGridNilaiController@cetak_grid_nilai',
    'as'        => 'pdf.grid-nilai'
]);

//PDF KEUANGAN SISWA
Route::get('data/keuangan-siswa/pdf-jenis-pembayaran', [
    'uses'      => '\App\Http\Controllers\Pdf\Keuangan_siswa\PdfJenisPembayaranController@cetak_jenis_pembayaran',
    'as'        => 'pdf.jenis-pembayaran'
]);

Route::get('data/keuangan-siswa/pdf-pembayaran', [
    'uses'      => '\App\Http\Controllers\Pdf\Keuangan_siswa\PdfPembayaranController@cetak_pembayaran',
    'as'        => 'pdf.pembayaran'
]);

Route::get('data/keuangan-siswa/pdf-pembayaran-spp', [
    'uses'      => '\App\Http\Controllers\Pdf\Keuangan_siswa\PdfPembayaranSppController@cetak_pembayaran_spp',
    'as'        => 'pdf.pembayaran-spp'
]);

Route::get('data/keuangan-siswa/pdf-pembayaran-buku', [
    'uses'      => '\App\Http\Controllers\Pdf\Keuangan_siswa\PdfPembayaranBukuController@cetak_pembayaran_buku',
    'as'        => 'pdf.pembayaran-buku'
]);

Route::get('data/keuangan-siswa/pdf-pembayaran-semester', [
    'uses'      => '\App\Http\Controllers\Pdf\Keuangan_siswa\PdfPembayaranSemesterController@cetak_pembayaran_semester',
    'as'        => 'pdf.pembayaran-semester'
]);

//Managemen Guru
Route::get('data/management_guru/pdf-data-guru', [
    'uses'      => '\App\Http\Controllers\Pdf\Managemen_guru\PdfDataGuruController@ceta_data_guru',
    'as'        => 'pdf.data-guru'
]);

Route::get('data/management_guru/pdf-data-kehadiran-guru', [
    'uses'      => '\App\Http\Controllers\Pdf\Managemen_guru\PdfKehadiranGuruController@cetak_kehadiran_guru',
    'as'        => 'pdf.data-kehadiran-guru'
]);

//staff pegawai
Route::get('data/pdf-staff-pegawai',[
    'uses'      => '\App\Http\Controllers\Pdf\Pegawai\PdfPegawaiController@cetak_data_pegawai',
    'as'        => 'pdf.staff-pegawai'
]);
//perpustakaan
Route::get('data/perpustakaan/pdf-data-buku', [
    'uses'      => '\App\Http\Controllers\Pdf\Perpustakaan\PdfDataBukuController@cetak_data_buku',
    'as'        => 'pdf.data-buku'
]);

Route::get('data/perpustakaan/pdf-petugas', [
    'uses'      => '\App\Http\Controllers\Pdf\Perpustakaan\PdfPetugasPerpustakaanController@cetak_data_petugas',
    'as'        => 'pdf.data-petugas'
]);

Route::get('data/perpustakaan/pdf-peminjaman-buku', [
    'uses'      => '\App\Http\Controllers\Pdf\Perpustakaan\PdfPeminjamanBukuPerpustakaanController@cetak_peminjaman_buku',
    'as'        => 'pdf.data-peminjaman-buku'
]);

Route::get('data/perpustakaan/pdf-pengembalian-buku', [
    'uses'      => '\App\Http\Controllers\Pdf\Perpustakaan\PdfPengembalianBukuPerpustakaanController@cetak_pengembalian_buku',
    'as'        => 'pdf.data-pengembalian-buku'
]);
//inventori
Route::get('data/inventori/asset', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfAssetController@cetak_asset',
    'as'        => 'pdf.asset.inventori'
]);

Route::get('data/inventori/kategori-inventori', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfKategoriInventoriController@cetak_kategori_inventori',
    'as'        => 'pdf.kategori-inventori'
]);

Route::get('data/inventori/barang', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfBarangController@cetak_barang',
    'as'        => 'pdf.barang'
]);

Route::get('data/inventori/barang-keluar', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfBarangKeluarController@cetak_barang_keluar',
    'as'        => 'pdf.barang-keluar'
]);

Route::get('data/inventori/barang-masuk', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfBarangMasukController@cetak_barang_masuk',
    'as'        => 'pdf.barang-masuk'
]);

Route::get('data/inventori/donasi', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfBarangDonasiController@cetak_donasi',
    'as'        => 'pdf.donasi'
]);

Route::get('data/inventori/spk-asset', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfSPKAssetController@cetak_spk_asset',
    'as'        => 'pdf.spk-asset'
]);

Route::get('data/inventori/gedung', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfGedungController@cetak_gedung',
    'as'        => 'pdf.gedung'
]);

Route::get('data/inventori/tipe-donasi', [
    'uses'      => '\App\Http\Controllers\Pdf\Inventori\PdfTipeDonasiController@cetak_tipe_donasi',
    'as'        => 'pdf.tipe-donasi'
]);
//accounting
Route::get('data/accounting/cut-off-siswa', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfCutOffSiswaController@cetak_cutOffSiswa',
    'as'        => 'pdf.cut-off-siswa'
]);

Route::get('data/accounting/pemasukan/hutang', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfHutangController@cetak_hutang',
    'as'        => 'pdf.pemasukan.hutang'
]);

Route::get('data/accounting/pemasukan/jurnal-pbn', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfJurnalPbnController@cetak_jurnal_pbn',
    'as'        => 'pdf.pemasukan.jurnal-pbn'
]);

Route::get('data/accounting/pemasukan/jurnal-pro', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfJurnalProController@cetak_jurnal_pro',
    'as'        => 'pdf.pemasukan.jurnal-pro'
]);

Route::get('data/accounting/pemasukan/jurnal-spp', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfJurnalSppController@cetak_jurnal_spp',
    'as'        => 'pdf.pemasukan.jurnal-spp'
]);

Route::get('data/accounting/pemasukan/rekening-siswa', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pemasukan\PdfRekeningSiswaController@cetak_rekening_siswa',
    'as'        => 'pdf.pemasukan.rekening-siswa'
]);

//penegeluaran
Route::get('data/accounting/pengeluaran/rekening-siswa', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfDanaPendingController@cetak_dana_pending',
    'as'        => 'pdf.pengeluaran.dana-pending'
]);

Route::get('data/accounting/pengeluaran/cek-list', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfPengeluaranCekListController@cetak_pengeluaran_cek_list',
    'as'        => 'pdf.pengeluaran.cek-list'
]);

Route::get('data/accounting/pengeluaran/cetak-jurnal-edc', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfJurnalEdcController@cetak_jurnal_edc',
    'as'        => 'pdf.pengeluaran.cetak-jurnal-edc'
]);

Route::get('data/accounting/pengeluaran/kas-mutasi', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfJurnalKasMutasiController@cetak_kas_mutasi',
    'as'        => 'pdf.pengeluaran.kas-mutasi'
]);

Route::get('data/accounting/pengeluaran/permintaan-dana', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfPermintaanDanaController@cetak_permintaan_dana',
    'as'        => 'pdf.pengeluaran.permintaan-dana'
]);

Route::get('data/accounting/pengeluaran/reibustment', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfReibustmentController@cetak_reibustment',
    'as'        => 'pdf.pengeluaran.reibustment'
]);

Route::get('data/accounting/pengeluaran/uang-masuk', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfPengeluaranUangMasukController@cetak_pengeluaran_uang_masuk',
    'as'        => 'pdf.pengeluaran.uang-masuk'
]);

Route::get('data/accounting/pengeluaran/uang-keluar', [
    'uses'      => '\App\Http\Controllers\Pdf\Accounting\Pengeluaran\PdfJurnalUangKeluarController@cetak_jurnal_uang_keluar',
    'as'        => 'pdf.pengeluaran.uang-keluar'
]);


//filter akademik
Route::get('akademik/ektrakurikuler/search-ekstra',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterEkstrakurikulerController@searchEkstra',
    'as'        => 'ekstra.search'
]);

Route::get('akademik/kelas/search-kelas',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterKelasController@searchKelas',
    'as'        => 'kelas.search'
]);

Route::get('akademik/jadwal-matkul/search-jadwal-matkul',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterJadwalMatkulController@search_jadwal_matkul',
    'as'        => 'jadwal-matkul.search-jadwal-matkul'
]);

Route::get('akademik/filter/ruang-kelas-perkelas',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterRuangKelasPerkelasController@searchRuangKelasPerkelas',
    'as'        => 'akademik.filter.ruang-kelas-perkelas'
]);

Route::get('akademik/filter/matkul-perkelas',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterMatkulPerkelasController@matkul_perkelas',
    'as'        => 'akademik.filter.matkul-perkelas'
]);

Route::get('akademik/filter/matkul-guru',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterMatkulGuruController@search_matkul_guru',
    'as'        => 'akademik.filter.matkul-guru'
]);

Route::get('akademik/filter/siswa-perkelas',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterSiswaPerkelasController@search_siswa_perkelas',
    'as'        => 'akademik.filter.siswa-perkelas'
]);

Route::get('akademik/filter/data-alumni',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterDataAlumniController@search_data_alumni',
    'as'        => 'akademik.filter.data-alumni'
]);

Route::get('akademik/filter/nilai-perkelas',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterNilaiPerkelasController@search_nilai_perkelas',
    'as'        => 'akademik.filter.nilai-perkelas'
]);

Route::get('akademik/filter/nilai-permatkul',[
    'uses'      => '\App\Http\Controllers\Filter\Akademik\FilterNilaiPerMatkulController@search_nilai_permatkul',
    'as'        => 'akademik.filter.nilai-permatkul'
]);

//keuangan siswa filter
Route::get('keuangan-siswa/filter/jenis-pembayaran',[
    'uses'      => '\App\Http\Controllers\Filter\KeuanganSiswa\FilterJenisPembayaranController@search_jenis_pembayaran',
    'as'        => 'keuangan-siswa.filter.jenis-pembayaran'
]);

Route::get('keuangan-siswa/filter/pembayaran-spp',[
    'uses'      => '\App\Http\Controllers\Filter\KeuanganSiswa\FilterPembayaranSppController@search_pembayaran_spp',
    'as'        => 'keuangan-siswa.filter.pembayaran-spp'
]);

Route::get('keuangan-siswa/filter/pembayaran-semester',[
    'uses'      => '\App\Http\Controllers\Filter\KeuanganSiswa\FilterPembayaranSemesterController@search_pembayaran_semester',
    'as'        => 'keuangan-siswa.filter.pembayaran-semester'
]);

Route::get('keuangan-siswa/filter/pembayaran-buku',[
    'uses'      => '\App\Http\Controllers\Filter\KeuanganSiswa\FilterPembayaranBukuController@search_pembayaran_buku',
    'as'        => 'keuangan-siswa.filter.pembayaran-buku'
]);
//filter management guru

Route::get('keuangan-siswa/filter/data-guru',[
    'uses'      => '\App\Http\Controllers\Filter\ManageGuru\FilterDataGuruController@search_data_guru',
    'as'        => 'keuangan-siswa.filter.data-guru'
]);

Route::get('keuangan-siswa/filter/kehadiran-guru',[
    'uses'      => '\App\Http\Controllers\Filter\ManageGuru\FilterKehadiranGuruController@search_kehadiran_guru',
    'as'        => 'keuangan-siswa.filter.kehadiran-guru'
]);
//filter data pegawai
Route::get('staff/filter/staff',[
    'uses'      => '\App\Http\Controllers\Filter\DataPegawai\FilterDataPegawaiController@search_data_pegawai',
    'as'        => 'staff.filter.staff'
]);
//filter perpustakaan
Route::get('staff/filter/data-buku',[
    'uses'      => '\App\Http\Controllers\Filter\Perpustakaan\FilterDataBukuController@search_data_buku',
    'as'        => 'staff.filter.data-buku'
]);

Route::get('perpustakaan/filter/peminjaman-buku',[
    'uses'      => '\App\Http\Controllers\Filter\Perpustakaan\FilterPeminjamController@search_peminjam_buku',
    'as'        => 'perpustakaan.filter.peminjaman-buku'
]);

Route::get('perpustakaan/filter/pengembalian-buku',[
    'uses'      => '\App\Http\Controllers\Filter\Perpustakaan\FilterPengembalianBukuController@search_pengembalian_buku',
    'as'        => 'perpustakaan.filter.pengembalian-buku'
]);

Route::get('perpustakaan/filter/petugas',[
    'uses'      => '\App\Http\Controllers\Filter\Perpustakaan\FilterPetugasController@search_petugas',
    'as'        => 'perpustakaan.filter.petugas'
]);
//filter elearning
Route::get('elearning/filter/pengumuman',[
    'uses'      => '\App\Http\Controllers\Filter\Elearning\FilterpElearningPengumumanController@search_pengumuman',
    'as'        => 'elearning.filter.pengumuman'
]);

Route::get('elearning/filter/e-kelas',[
    'uses'      => '\App\Http\Controllers\Filter\Elearning\FilterpElearningKelasController@search_elearning_kelas',
    'as'        => 'elearning.filter.e-kelas'
]);

Route::get('elearning/filter/e-quiz',[
    'uses'      => '\App\Http\Controllers\Filter\Elearning\FilterElearningQuizController@search_quiz',
    'as'        => 'elearning.filter.e-quiz'
]);

Route::get('elearning/filter/e-materi',[
    'uses'      => '\App\Http\Controllers\Filter\Elearning\FilterElearningMateriController@search_elearning_materi',
    'as'        => 'elearning.filter.e-materi'
]);

Route::get('elearning/filter/e-tugas',[
    'uses'      => '\App\Http\Controllers\Filter\Elearning\FilterELearningTugasController@search_etugas',
    'as'        => 'elearning.filter.e-tugas'
]);
//filter inventori
Route::get('inventori/filter/tipe-inventori-asset',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterTipeInventoriAssetController@search_tipe_inventori_asset',
    'as'        => 'inventori.filter.tipe-inventori-asset'
]);

Route::get('inventori/filter/kategori-inventori',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterKategoriInventoriController@search_kategori_inventori',
    'as'        => 'inventori.filter.kategori-inventori'
]);

Route::get('inventori/filter/gedung',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterGedungController@search_gedung',
    'as'        => 'inventori.filter.gedung'
]);

Route::get('inventori/filter/tipe-donasi',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterTipeDonasiController@search_tipe_donasi',
    'as'        => 'inventori.filter.tipe-donasi'
]);

Route::get('inventori/filter/invetori-asset',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterInventoriAssetController@search_inventori_asset',
    'as'        => 'inventori.filter.invetori-asset'
]);

Route::get('inventori/filter/donasi',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterDonasiController@search_donasi',
    'as'        => 'inventori.filter.donasi'
]);

Route::get('inventori/filter/barang',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterBarangController@search_barang',
    'as'        => 'inventori.filter.barang'
]);

Route::get('inventori/filter/barang-masuk',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterBarangMasukController@search_barang_masuk',
    'as'        => 'inventori.filter.barang-masuk'
]);

Route::get('inventori/filter/barang-keluar',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterBarangKeluarController@search_barang_keluar',
    'as'        => 'inventori.filter.barang-keluar'
]);

Route::get('inventori/filter/spk-asset',[
    'uses'      => '\App\Http\Controllers\Filter\Inventori\FilterSPKAssetController@search_spk_asset',
    'as'        => 'inventori.filter.spk-asset'
]);
//filter accounting
Route::get('accounting/filter/tipe-pajak',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\FilterTipePajakController@search_tipe_pajak',
    'as'        => 'accounting.filter.tipe-pajak'
]);

Route::get('accounting/filter/coa',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\FilterCOAController@search_coa',
    'as'        => 'accounting.filter.coa'
]);

//pengeluaran
Route::get('accounting/filter/pengeluaran/uang-masuk',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterUangMasukController@search_pengeluaran_uang_masuk',
    'as'        => 'accounting.filter.pengeluaran.uang-masuk'
]);

Route::get('accounting/filter/pengeluaran/permintaan-dana',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterPermintaanDanaController@search_permintaan_dana',
    'as'        => 'accounting.filter.pengeluaran.permintaan-dana'
]);

Route::get('accounting/filter/pengeluaran/cek-list',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterCekListController@search_cek_list',
    'as'        => 'accounting.filter.pengeluaran.cek-list'
]);

Route::get('accounting/filter/pengeluaran/reibustmen',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterReibustmenController@search_reibustment',
    'as'        => 'accounting.filter.pengeluaran.reibustmen'
]);

Route::get('accounting/filter/pengeluaran/jurnal-kas-edc',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterJurnalKasEDCController@search_jurnal_kas_edc',
    'as'        => 'accounting.filter.pengeluaran.jurnal-kas-edc'
]);

Route::get('accounting/filter/pengeluaran/uang-keluar',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterUangKeluarController@search_uang_keluar',
    'as'        => 'accounting.filter.pengeluaran.uang-keluar'
]);

Route::get('accounting/filter/pengeluaran/kas-mutasi',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterKasMutasiController@search_kas_mutasi',
    'as'        => 'accounting.filter.pengeluaran.kas-mutasi'
]);

Route::get('accounting/filter/pengeluaran/dana-pending',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pengeluaran\FilterDanaPendingController@search_dana_pending',
    'as'        => 'accounting.filter.pengeluaran.dana-pending'
]);
//pemasukan
Route::get('accounting/filter/pemasukan/rekening-siswa',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterRekeningSiswaController@search_rekening_siswa',
    'as'        => 'accounting.filter.pemasukan.rekening-siswa'
]);

Route::get('accounting/filter/pemasukan/jurnal-pbn',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterJurnalPBNController@search_jurnal_pbn',
    'as'        => 'accounting.filter.pemasukan.jurnal-pbn'
]);

Route::get('accounting/filter/pemasukan/jurnal-spp',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterJurnalSPPController@search_jurnal_spp',
    'as'        => 'accounting.filter.pemasukan.jurnal-spp'
]);

Route::get('accounting/filter/pemasukan/jurnal-pbb',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterJurnalPBBController@search_jurnal_pbb',
    'as'        => 'accounting.filter.pemasukan.jurnal-pbb'
]);

Route::get('accounting/filter/pemasukan/cos',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterCOSController@search_cos',
    'as'        => 'accounting.filter.pemasukan.cos'
]);

Route::get('accounting/filter/pemasukan/hutang',[
    'uses'      => '\App\Http\Controllers\Filter\Accounting\Pemasukan\FilterHutangController@search_hutang',
    'as'        => 'accounting.filter.pemasukan.hutang'
]);



//management role user
Route::resource('/roles', 'RoleController');

Route::resource('/users', 'UserController')->except([
    'show'
]);

Route::resource('data-master/data_sekolah', '\App\Http\Controllers\DataMaster\DataSekolahController');
        Route::resource('data-master/jurusan', '\App\Http\Controllers\DataMaster\JurusanController');
        Route::resource('data-master/kurikulum', '\App\Http\Controllers\DataMaster\KurikulumController');
        Route::resource('data-master/tahun_akademik', '\App\Http\Controllers\DataMaster\TahunAkademikController');
        Route::resource('data-master/ruang_kelas', '\App\Http\Controllers\DataMaster\RuangKelasController');
        Route::resource('data-master/agama', '\App\Http\Controllers\DataMaster\AgamaController');
        Route::resource('data-master/jabatan', '\App\Http\Controllers\DataMaster\JabatanController');
        Route::resource('data-master/bagian', '\App\Http\Controllers\DataMaster\BagianController');
        Route::resource('data-master/pendapatan', '\App\Http\Controllers\DataMaster\PendapatanController');
        Route::resource('data-master/status_karyawan', '\App\Http\Controllers\DataMaster\StatusKaryawanController');
        Route::resource('data-master/kendaraan', '\App\Http\Controllers\DataMaster\KendaraanController');
        Route::resource('data-master/rombongan_belajar', '\App\Http\Controllers\DataMaster\RombonganBelajarController');
        Route::resource('data-master/data_guru', '\App\Http\Controllers\DataMaster\DataGuruController');
        Route::resource('data-master/wali_kelas', '\App\Http\Controllers\DataMaster\WaliKelasController');
        Route::resource('data-master/kelas', '\App\Http\Controllers\DataMaster\KelasController');
        Route::resource('data-master/jenjang', '\App\Http\Controllers\DataMaster\JenjangController');
        //akademik
        Route::resource('akademik/mata_pelajaran', '\App\Http\Controllers\Akademik\MataPelajaranController');
        Route::resource('akademik/raport', '\App\Http\Controllers\Akademik\RaportController');
        Route::resource('manajemen/akademik/jadwal-mata-pelajaran', '\App\Http\Controllers\Akademik\JadwalMataPelajaranController');
        Route::resource('manajemen/siswa', '\App\Http\Controllers\ManajemenSiswa\SiswaController');
        Route::get('akademik/data-orang-tua', [
            'uses'      => '\App\Http\Controllers\Akademik\DataOrangTuaController@index',
            'as'        => 'data-orang-tua.index'
        ]);
        Route::resource('akademik/ekstrakurikuler', '\App\Http\Controllers\Akademik\EkstrakurikulerController');
        Route::resource('akademik/bimbingan-belajar', '\App\Http\Controllers\Akademik\BimbinganBelajarController');
        Route::resource('akademik/data-alumni', '\App\Http\Controllers\Akademik\DataAlumniController');
        Route::resource('akademik/mata-pelajar-perkelas', '\App\Http\Controllers\Akademik\MataPelajarPerkelasController');
        Route::resource('akademik/mata-pelajaran-guru', '\App\Http\Controllers\Akademik\MataPelajaranGuruController');
        Route::resource('akademik/grid-nilai', '\App\Http\Controllers\Akademik\GridNilaiController');
        Route::resource('akademik/siswa-perkelas', '\App\Http\Controllers\Akademik\SiswaPerkelasController');
        Route::resource('akademik/nilai-per-mata-pelajaran', '\App\Http\Controllers\Akademik\NilaiPermapelController');
        Route::resource('akademik/nilai-per-kelas', '\App\Http\Controllers\Akademik\NilaiPerkelasController');
        Route::resource('akademik/ekstra-siswa', '\App\Http\Controllers\Akademik\EktraSiswaController');
        //psb
        Route::resource('siswa/daftar-siswa', '\App\Http\Controllers\Psb\PsbDaftarSiswaController');
        Route::resource('psb/test-interview', '\App\Http\Controllers\Psb\TestInterviewController');
        Route::resource('psb/hasil-ujian', '\App\Http\Controllers\Psb\PsbHasilUjianController');
        Route::get('psb/siswa-diterima', [
            'uses'  => '\App\Http\Controllers\Psb\PsbSiswaDiterimaController@index',
            'as'    => 'siswa-diterima.index'
        ]);

        //manajemen keuangan siswa
        Route::resource('manajemen/keuangan-siswa/jenis-pembayaran', '\App\Http\Controllers\ManajemenKeuanganSiswa\JenisPembyaranController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-spp', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranSppController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-semester', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranSemesterController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-buku', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranBukuController');
        Route::resource('manajemen/keuangan-siswa/pembayaran', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranController');

        //manajement guru
        Route::resource('manajemen/kehadiran-guru', '\App\Http\Controllers\ManajemenGuru\KehadiranGuruController');
        //manajemen keuangan siswa
        Route::resource('manajemen/keuangan-siswa/jenis-pembayaran', '\App\Http\Controllers\ManajemenKeuanganSiswa\JenisPembyaranController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-spp', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranSppController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-semester', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranSemesterController');
        Route::resource('manajemen/keuangan-siswa/pembayaran-buku', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranBukuController');
        Route::resource('manajemen/keuangan-siswa/pembayaran', '\App\Http\Controllers\ManajemenKeuanganSiswa\PembayaranController');
        //staff
        Route::resource('manajemen/staff', '\App\Http\Controllers\Staff\StaffController');
        //perpustakaan
        Route::resource('perpustakaan/data-buku', '\App\Http\Controllers\Perpustakaan\DataBukuController');
        Route::resource('perpustakaan/peminjaman-buku', '\App\Http\Controllers\Perpustakaan\PeminjamanBukuController');
        Route::resource('perpustakaan/pengembalian-buku', '\App\Http\Controllers\Perpustakaan\PengembalianBukuController');
        Route::resource('perpustakaan/petugas', '\App\Http\Controllers\Perpustakaan\PetugasPerpustakaanController');
        //inventories
        Route::resource('inventori/kategori-inventori', '\App\Http\Controllers\Inventori\KategoriInventoriController');
        Route::resource('inventori/tipe-inventori', '\App\Http\Controllers\Inventori\TipeInventoryController');
        Route::resource('inventori/gedung', '\App\Http\Controllers\Inventori\GedungController');
        Route::resource('inventori/tipe-donasi', '\App\Http\Controllers\Inventori\TipeDonasiController');
        Route::resource('inventori/asset', '\App\Http\Controllers\Inventori\AssetController');
        Route::resource('inventori/donasi', '\App\Http\Controllers\Inventori\DonasiController');
        Route::resource('inventori/barang', '\App\Http\Controllers\Inventori\BarangController');
        Route::resource('inventori/barang-masuk', '\App\Http\Controllers\Inventori\BarangMasukController');
        Route::resource('inventori/barang-keluar', '\App\Http\Controllers\Inventori\BarangKeluarController');
        Route::resource('inventori/spk-asset', '\App\Http\Controllers\Inventori\SPKAssetController');
        //accounting
        Route::resource('accounting/tipe-pajak', '\App\Http\Controllers\Accounting\TipePajakController');
        //pengeluaran
        Route::resource('accounting/pengeluaran/uang-masuk', '\App\Http\Controllers\Accounting\Pengeluaran\PengeluaranUangMasukController');
        Route::resource('accounting/pengeluaran/permintaan-dana', '\App\Http\Controllers\Accounting\Pengeluaran\PermintaanDanaController');
        Route::resource('accounting/pengeluaran/cek-list', '\App\Http\Controllers\Accounting\Pengeluaran\PengeluaranCekListController');
        Route::resource('accounting/pengeluaran/reibusment', '\App\Http\Controllers\Accounting\Pengeluaran\ReibustmentController');
        Route::resource('accounting/pengeluaran/jurnal-edc', '\App\Http\Controllers\Accounting\Pengeluaran\JurnalEdcController');
        Route::resource('accounting/pengeluaran/jurnal-uang-keluar', '\App\Http\Controllers\Accounting\Pengeluaran\JurnalUangKeluarController');
        Route::resource('accounting/pengeluaran/kas-mutasi', '\App\Http\Controllers\Accounting\Pengeluaran\JurnalKasMutasiController');
        Route::resource('accounting/pengeluaran/dana-pending', '\App\Http\Controllers\Accounting\Pengeluaran\DanaPendingController');
        //pemasukan
        Route::resource('accounting/pemasukan/rekening-siswa', '\App\Http\Controllers\Accounting\Pemasukan\RekeningSiswaController');
        Route::resource('accounting/pemasukan/jurnal-pbn', '\App\Http\Controllers\Accounting\Pemasukan\JurnalPbnController');
        Route::resource('accounting/pemasukan/jurnal-spp', '\App\Http\Controllers\Accounting\Pemasukan\JurnalSppController');
        Route::resource('accounting/pemasukan/jurnal-program-bulanan', '\App\Http\Controllers\Accounting\Pemasukan\JurnalProController');
        Route::resource('accounting/pemasukan/cut-off-siswa', '\App\Http\Controllers\Accounting\Pemasukan\CutOffSiswaController');
        Route::resource('accounting/pemasukan/hutang', '\App\Http\Controllers\Accounting\Pemasukan\HutangController');
        Route::resource('accounting/coa', '\App\Http\Controllers\Accounting\CoaController');
        
        //elearning
        Route::resource('elearning/tugas', '\App\Http\Controllers\Elearning\EtugasController');
        Route::resource('elearning/e-kelas', '\App\Http\Controllers\Elearning\EkelasController');
        Route::resource('elearning/materi', '\App\Http\Controllers\Elearning\EmateriController');
        Route::resource('elearning/quiz', '\App\Http\Controllers\Elearning\EquizController');
        Route::resource('elearning/pengumuman', '\App\Http\Controllers\Elearning\EpengumumanController');
        
        //pengumuman events
        Route::get('pengumuman/event/create', [
            'uses'      => '\App\Http\Controllers\Pengumuman\PengumumanEventsController@create',
            'as'        => 'pengumuman.event.create'
        ]);
        Route::post('pengumuman/event', [
            'uses'      => '\App\Http\Controllers\Pengumuman\PengumumanEventsController@addEvent',
            'as'        => 'pengumuman.event.store'
        ]);

        //cetak raport
        Route::get('akademik/raport-show/{id}', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@raport',
            'as'        => 'raport-show'
        ]);

        Route::resource('akademik/ruang-kelas-perkelas', '\App\Http\Controllers\Akademik\RuangKelasPerkelasController');
        //raport mapel
        Route::get('akademik/nilai/ekstra-create', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@create_ektra',
            'as'        => 'akademik.nilai.ekstra-create'
        ]);

        Route::match(['get', 'post'], 'raport/mapel', [
            'uses' 		=> '\App\Http\Controllers\Akademik\CetakRaportController@getNilaiMapel',
            'as' 		=> 'raport.mapel'
        ]);

        Route::get('akademik/nilai-raport/persiswa-list', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@index',
            'as'        => 'akademik.nilai-raport.persiswa-list'
        ]);

        Route::get('akademik/nilai-raport/persiswa/{id}', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@getNilaiPersiswa',
            'as'        => 'akademik.nilai-raport.persiswa'
        ]);

        Route::get('akademik/pengaturan-raport/selesai', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@thanks',
            'as'        => 'akademik.pengaturan-raport.selesai'
        ]);

        Route::get('akademik/nilai/review-nilai', [
            'uses'      => '\App\Http\Controllers\Akademik\CetakRaportController@review_raport',
            'as'        => 'akademik.nilai.review-nilai'
        ]);

        Route::match(['get', 'post'], 'raport/add-nilai', [
            'uses' 		=> '\App\Http\Controllers\Akademik\CetakRaportController@addDataNilaiRaport',
            'as' 		=> 'raport.add-nilai'
        ]);

        Route::get('/', [
            'uses'      => 'DashbordController@index',
            'as'        => 'home.index'
        ]);
        //Frontend
        Route::get('daftar/pendaftaran-siswa-baru', [
            'uses'      => '\App\Http\Controllers\Register\PendafataranSiswaBaruController@create',
            'as'        => 'daftar.siswa-baru'
        ]);

        Route::post('register/pendaftaran-siswa-baru/store', [
            'uses'      => '\App\Http\Controllers\Register\PendafataranSiswaBaruController@store',
            'as'        => 'pendaftaran.siswa-baru.store'
        ]);

        Route::get('register/siswa/selesai', [
            'uses'      => '\App\Http\Controllers\Register\PendafataranSiswaBaruController@thanks',
            'as'        => 'pendaftaran.siswa.selesai'
        ]);

        Route::get('register/pendaftaran-guru-baru', [
            'uses'      => '\App\Http\Controllers\Register\PendaftaranGuruBaruController@create',
            'as'        => 'pendaftaran.guru.baru'
        ]);

        Route::post('register/pendaftaran-guru-baru/store', [
            'uses'      => '\App\Http\Controllers\Register\PendaftaranGuruBaruController@store',
            'as'        => 'pendaftaran.guru.baru.store'
        ]);

        Route::get('register/pendaftaran-guru-baru/selesai', [
            'uses'      => '\App\Http\Controllers\Register\PendaftaranGuruBaruController@finish',
            'as'        => 'pendaftaran.guru.selesai'
        ]);

        //ekstra item
        Route::match(['get', 'post'], 'ekstra-item/siswa', [
            'uses' 		=> '\App\Http\Controllers\Akademik\EkstraSiswaItemController@getEkstraSiswa',
            'as' 		=> 'ekstra-item.siswa'
        ]);

      Route::get('akademik/ekstra-siswa-item/selesai',[
        'uses'      => '\App\Http\Controllers\Akademik\EkstraSiswaItemController@selesai',
        'as'        => 'ekstra-item.selesai'
      ]);

      Route::get('akademik/ekstra-siswa-list',[
        'uses'      => '\App\Http\Controllers\Akademik\EkstraSiswaItemController@getEkstraList',
        'as'        => 'ekstra-siswa-list.index'
      ]);

      Route::get('akademik/ekstra-persiswa/{id}',[
        'uses'      => '\App\Http\Controllers\Akademik\EkstraSiswaItemController@getEkstraPersiswa',
        'as'        => 'akademik.ekstra-persiswa'
      ]);


      Route::get('akademik/profile/ektra-persiswa-item',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getEkstra',
        'as'        => 'akademik.ektra-persiswa-item'
      ]);
     //profile mape
      Route::get('akademik/profile/jadwal-mata-pelajaran',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@index',
        'as'        => 'akademik.jadwal-mata-pelajaran'
      ]);

      Route::get('akademik/profile/jadwal-mata-pelajaran/persiswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getJadwalMapel',
        'as'        => 'akademik.jadwal-mata-pelajaran.persiswa'
      ]);

      Route::get('akademik/profile/ekstra/persiswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getEkstra',
        'as'        => 'akademik.getEkstra.persiswa'
      ]);

      Route::get('akademik/profile/pembayaran/persiswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getPembayaran',
        'as'        => 'akademik.pembayaran.persiswa'
      ]);

      Route::get('akademik/profile/nilai/persiswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getNilai',
        'as'        => 'akademik.nilai.persiswa'
      ]);

      Route::get('akademik/profile/raport/persiswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getRaport',
        'as'        => 'akademik.raport.persiswa'
      ]);

      Route::get('akademik/profile/raport/siswa',[
        'uses'      => '\App\Http\Controllers\Akademik\ProfileController@getRaportNilai',
        'as'        => 'akademik.raport.siswa'
      ]);

      Route::resource('data-master/type-personal-package', '\App\Http\Controllers\DataMaster\TypePersonalPackageController');
      Route::resource('akademik/pengumuman-kelas-siswa', '\App\Http\Controllers\Akademik\PengumumanSiswaController');



        